# Translating

Each language has it's own folder within the [data/locales folder](https://gitlab.com/alinex/node-checkup/-/tree/master/data/locales) in which configuration files for each part of the application are located. We use the YAML syntax here which is very simple to read.

To add a new language the best way is to copy a folder and change the files within. The name of the folder has to be the two or four letter ISO code of the target language.

The text elements are identified by a structure using english short names with a colon behind at the start of the line. These are not allowed to translate because the system needs them to find the text elements. But feel free to replace the text behind. There are some variable parts in double curly braces which should not be changed. You may change the position of them but don't change anything inside of them.

!!! Info

    Maybe later if internationalization will be a bigger point htan the currently included two languages we may think about supporting transifex or other translation tools more.

See the [Documentation](https://alinex.gitlab.io/node-checkup/development/i18n/) for more information.
