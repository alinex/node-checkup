import FileSchema from '@alinex/validator/lib/type/file'
import ObjectSchema from '@alinex/validator/lib/type/object'
import NumberSchema from '@alinex/validator/lib/type/number'
import BooleanSchema from '@alinex/validator/lib/type/boolean'
import StringSchema from '@alinex/validator/lib/type/string';
import LogicSchema from '@alinex/validator/lib/type/logic';
import ArraySchema from '@alinex/validator/lib/type/array';
import PortSchema from '@alinex/validator/lib/type/port';

const fileStore = new LogicSchema({
  operator: 'or',
  check: [
    new BooleanSchema(),
    new ObjectSchema({
      item: {
        filename: new StringSchema(),
        dirname: new FileSchema({ type: 'dir', writable: true }),
        datePattern: new StringSchema(),
        frequency: new StringSchema({ allow: [/^\d+[mh]$/] }),
        maxSize: new NumberSchema({ integer: true, min: 0, unit: { from: 'B', to: 'B' } }),
        maxFiles: new NumberSchema({ integer: true, min: 0 }),
        zippedArchive: new BooleanSchema(),
        utc: new BooleanSchema(),
        extension: new StringSchema({ allow: [/^\w+$/] }),
        createSymlink: new BooleanSchema(),
        symlinkName: new StringSchema(),
      }
    })
  ]
})

const exclude = new ArraySchema({
  makeArray: true,
  item: { '*': new StringSchema() },
  min: 1
})
const cleanup = new ObjectSchema({
  item: {
    log: new NumberSchema({
      min: 0, default: 10000 // 10k values
    }),
    'minute5': new NumberSchema({
      unit: { from: 's', to: 's' },
      min: 0, default: 43200 // 12 h // 144 values
    }),
    hour: new NumberSchema({
      unit: { from: 's', to: 's' },
      min: 0, default: 604800 // 7 days // 168 values
    }),
    day: new NumberSchema({
      unit: { from: 's', to: 's' },
      min: 0, default: 7862400 // 3 months // 60 values
    }),
    month: new NumberSchema({
      unit: { from: 's', to: 's' },
      min: 0, default: 157788000 // 5 years // 60 values
    })
  },
  mandatory: true,
  denyUndefined: true
})
const pg = new ObjectSchema({
  title: 'Postgres Database connection',
  item: {
    client: new StringSchema({ allow: ['pg'] }),
    version: new StringSchema({ allow: [/\d+\.\d+/] }),
    connection: new ObjectSchema({
      item: {
        user: new StringSchema(),
        password: new StringSchema(),
        host: new StringSchema({ default: 'localhost' }),
        port: new PortSchema({ default: 5432 }),
        database: new StringSchema()
      }
    }),
    searchPath: new ArraySchema({
      item: {
        '*': new StringSchema()
      }
    }),
    schema: new StringSchema({ alphaNum: true }),
    prefix: new StringSchema({ alphaNum: true }),
    exclude,
    cleanup
  },
  mandatory: ['client', 'connection', 'cleanup'],
  denyUndefined: true
});
const mysql = new ObjectSchema({
  title: 'MySQL Database connection',
  item: {
    client: new StringSchema({ allow: ['mysql'] }),
    version: new StringSchema({ allow: [/\d+\.\d+/] }),
    connection: new ObjectSchema({
      item: {
        user: new StringSchema(),
        password: new StringSchema(),
        host: new StringSchema({ default: 'localhost' }),
        port: new PortSchema({ default: 3306 }),
        database: new StringSchema()
      }
    }),
    prefix: new StringSchema({ alphaNum: true }),
    exclude,
    cleanup
  },
  mandatory: ['client', 'connection', 'cleanup'],
  denyUndefined: true
});
const mysql2 = new ObjectSchema({
  title: 'MySQL2 Database connection',
  item: {
    client: new StringSchema({ allow: ['mysql2'] }),
    version: new StringSchema({ allow: [/\d+\.\d+/] }),
    connection: new ObjectSchema({
      item: {
        user: new StringSchema(),
        password: new StringSchema(),
        host: new StringSchema({ default: 'localhost' }),
        port: new PortSchema({ default: 3306 }),
        database: new StringSchema()
      }
    }),
    prefix: new StringSchema({ alphaNum: true }),
    exclude,
    cleanup
  },
  mandatory: ['client', 'connection', 'cleanup'],
  denyUndefined: true
});
const oracledb = new ObjectSchema({
  title: 'oracledb Database connection',
  item: {
    client: new StringSchema({ allow: ['oracledb'] }),
    version: new StringSchema({ allow: [/\d+\.\d+/] }),
    connection: new ObjectSchema({
      item: {
        user: new StringSchema(),
        password: new StringSchema(),
        host: new StringSchema({ default: 'localhost' }),
        port: new PortSchema({ default: 1521 }),
        database: new StringSchema()
      }
    }),
    schema: new StringSchema({ alphaNum: true }),
    prefix: new StringSchema({ alphaNum: true }),
    exclude,
    cleanup
  },
  mandatory: ['client', 'connection', 'cleanup'],
  denyUndefined: true
});
const mssql = new ObjectSchema({
  title: 'Microsoft SQL Database connection',
  item: {
    client: new StringSchema({ allow: ['mssql'] }),
    version: new StringSchema({ allow: [/\d+\.\d+/] }),
    connection: new ObjectSchema({
      item: {
        user: new StringSchema(),
        password: new StringSchema(),
        host: new StringSchema({ default: 'localhost' }),
        port: new PortSchema({ default: 1433 }),
        database: new StringSchema()
      }
    }),
    schema: new StringSchema({ alphaNum: true }),
    prefix: new StringSchema({ alphaNum: true }),
    exclude,
    cleanup
  },
  mandatory: ['client', 'connection', 'cleanup'],
  denyUndefined: true
});
const sqlite3 = new ObjectSchema({
  title: 'SQLite Database connection',
  item: {
    client: new StringSchema({ allow: ['sqlite3'] }),
    connection: new ObjectSchema({
      item: {
        filename: new FileSchema({ readable: true })
      },
      mandatory: true,
      denyUndefined: true
    }),
    prefix: new StringSchema({ alphaNum: true }),
    exclude,
    cleanup
  },
  mandatory: ['client', 'connection', 'cleanup'],
  denyUndefined: true
});

const store = new ObjectSchema({
  item: {
    file: new ObjectSchema({
      item: {
        dirname: new FileSchema({ type: 'dir', writable: true }),
        status: fileStore,
        error: fileStore,
        data: fileStore,
        action: fileStore
      }
    }),
    database: new LogicSchema({
      title: 'Database connections',
      operator: 'or',
      check: [pg, mysql, mysql2, oracledb, mssql, sqlite3]
    }),
  }
});

export { store }
export default new ObjectSchema({
  title: 'General setup for alinex checkup',
  detail: 'The configuration defines how the checkup will be run.',
  item: {
    config: new FileSchema({
      title: 'Reference to case definition (configuration file)',
      readable: true
    }),
    verbose: new NumberSchema({
      title: 'Verbosity Level (higher is moer verbose)',
      integer: true, min: 0, max: 9, default: 0
    }),
    extend: new BooleanSchema({ title: 'Automatically extend verbosity level in case of problem' }),
    progress: new BooleanSchema({ title: 'Show progress bar instead of direct results' }),
    analyze: new BooleanSchema({ title: 'Should analyzation for possible fixes be made', default: true }),
    autofix: new BooleanSchema({ title: 'Should automatic repair be run' }),
    concurrency: new NumberSchema({
      title: 'How many processes may run in parallel in each state',
      integer: true, min: 1
    }),
    store,
    cron: new StringSchema({ allow: ['none', /^(?:[-*,/0-9]+ ){4}[-*,/0-9]+$/] })
  },
  mandatory: ['verbose', 'analyze']
});
