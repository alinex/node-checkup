import Validator from '@alinex/validator';
import schemaDataSource from '@alinex/validator/lib/dataSource';
import * as Builder from '@alinex/validator/lib/schema';
import { createHash } from 'crypto'
// @ts-ignore
import unflatten from 'obj-unflatten'
import { merge, clone } from '@alinex/data'

import Group from '../group'

const schemaBase = new Builder.ObjectSchema({
  item: {
    group: new Builder.ObjectSchema({
      title: 'Group Settings',
      item: {
        store: new Builder.ArraySchema({
          makeArray: true,
          item: { '*': schemaDataSource }
        })
      },
      mandatory: true,
      denyUndefined: true
    })
  }
})

const schemaList = new Builder.ArraySchema({
  item: { '*': new Builder.ObjectSchema({}) },
  min: 1
})

class GroupList extends Group {
  public async resolve() {
    // check data
    await Validator.load(clone(schemaBase), this.lang, { data: this.case.group, source: `preset:${this.path}.data` })
      .then(val => this.case.group = val.get())

    // get list
    let store = this.case.group?.store
    if (!Array.isArray(store)) store = [store]
    const list = await Validator.load(clone(schemaList), this.lang, ...store)

    // use list
    const parentPath = this.case.parent[this.case.parent.length - 1]
    list.get().forEach((e: any) => {
      const id = e.id || createHash('md5').update(JSON.stringify(e)).digest('base64').substring(0, 12)
      // create case
      let obj = clone(this.case)
      delete obj.group
      obj.title = e.title || Object.values(e)[0]
      if (e.tags) obj.tags = e.tags
      const fullId = `${parentPath}.${id}`
      obj.parent.push(fullId)
      // add data
      Object.keys(e).filter(k => k.startsWith('data.')).forEach(k => {
        const add: any = {}
        add[k] = e[k]
        obj = merge({ data: obj }, { data: unflatten(add) }).data
      })
      // add case
      this.children[fullId] = obj
    })

    return super.resolve()
  }
}

export default GroupList
