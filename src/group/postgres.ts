import Validator from '@alinex/validator';
import * as Builder from '@alinex/validator/lib/schema';
import { clone, merge } from '@alinex/data'
import { basename } from 'path'
import * as async from '@alinex/async';
import { sql } from 'slonik';

import { connection } from '../test/database/postgres'
import database from '../database'

import Group from '.'


const schemaBase = new Builder.ObjectSchema({
  item: {
    group: new Builder.ObjectSchema({
      title: 'Group Settings',
      item: {
        connection: new Builder.ArraySchema({
          item: { '*': connection },
          min: 1
        }),
        include: new Builder.ArraySchema({ item: { '*': new Builder.StringSchema() }, min: 1 }),
        exclude: new Builder.ArraySchema({ item: { '*': new Builder.StringSchema() }, min: 1 }),
      },
      mandatory: ['connection'],
      denyUndefined: true
    }),
    data: new Builder.ObjectSchema({ item: { '*': new Builder.ObjectSchema() }, min: 1 }),
    fix: new Builder.ObjectSchema({ item: { '*': new Builder.ObjectSchema() }, min: 1 }),
  }
})

class GroupPostgres extends Group {
  public async resolve() {
    // check data
    await Validator.load(clone(schemaBase), this.lang, { data: this.case.group, source: `preset:${this.path}.data` })
      .then(val => this.case.group = val.get())

    let connectionList = this.case.group!.connection

    // detect
    const parentPath = this.case.parent[this.case.parent.length - 1]
    await async.each(connectionList, async (connection: any) => {
      // create connection id
      let connectionId = connection.host || 'localhost'
      if (connection.port) connectionId = `${connectionId}:${connection.port}`
      const setup: any = { ...this.case.group, connection }
      const createTest = (test: string, instance?: string, instanceData?: any) => {
        let id = `${connectionId.replace(/[.:]/g, '_')}.${basename(test)}`
        // exclude test
        if (setup.include && !setup.include.filter((c: string) => test.startsWith(c)).length) return
        if (setup.exclude && setup.exclude.filter((c: string) => test.startsWith(c)).length) return
        // exclude instance
        if (instance) {
          id += '.' + instance //.replace(/\./g, '_')
          const iname = `${test}#${instance}`
          if (setup.include && !setup.include.filter((c: string) => iname.startsWith(c)).length) return
          if (setup.exclude && setup.exclude.filter((c: string) => iname.startsWith(c)).length) return
        }
        // create case
        let fullId = `${parentPath}.${id}`
        let obj = clone(this.case)
        delete obj.group
        const ititle = instance ? ` ${instance}` : ''
        obj.title = `${basename(test).substr(0, 1).toUpperCase()}${basename(test).substr(1)}${ititle} at ${connectionId}`
        obj.tags.push(connectionId)
        obj.parent.push(fullId)
        obj.test = test
        // deep merge data
        obj.data = merge(
          { data: { connection } },
          { data: { ...(this.case.data && this.case.data[test]) } },
          { data: { ...(this.case.data && this.case.data[`${test}#${instance}`]) } },
          { data: instanceData }
        ).data
        delete obj.data.connection.url // was dynamically generated
        // deep merge fix
        obj.fix = merge(
          { data: { ...(this.case.fix && this.case.fix[test]) } },
          { data: { ...(this.case.fix && this.case.fix[`${test}#${instance}`]) } },
        ).data
        // merge cron
        obj.cron = this.case.fix[`${test}#${instance}`] || this.case.fix[test] || this.case.cron
        // add case
        this.children[fullId] = obj
        // console.log(fullId)
        // console.log(obj)
      }

      // add simple tests without previous analyzation
      let tests = ['./database/postgres/cluster']
      // create and add tests
      tests.forEach((t: string) => createTest(t));

      // discovery
      setup.client = 'pg'

      // add databases
      const databases = await database.pgPool(setup).connect(conn => conn.anyFirst(sql`
          select datname from pg_stat_database where datname is not null and datname not like 'template%'
          `)
      )
      await async.each(databases.slice(), async (e: any) => {
        createTest('./database/postgres/database', e, { connection: { database: e } })

        // create connection for database
        const dbConn = clone(setup)
        dbConn.connection.database = e
        delete dbConn.connection.url
        // add schemas
        const schemas = await database.pgPool(dbConn).connect(conn => conn.anyFirst(sql`
          select nspname from pg_catalog.pg_namespace where nspname not like 'pg_%' and nspname != 'information_schema'
          `)
        )
        schemas.forEach((s: any) => {
          createTest('./database/postgres/schema', `${e}.${s}`, { connection: { database: e }, schema: s })
        })
        // add tables
        const tables = await database.pgPool(dbConn).connect(conn => conn.any(sql`
          select schemaname, relname from pg_stat_user_tables
          `)
        )
        tables.forEach((t: any) => {
          createTest('./database/postgres/table', `${e}.${t.schemaname}.${t.relname}`, { connection: { database: e }, schema: t.schemaname, table: t.relname })
        })

        // add tablespaces
        const tablespaces = await database.pgPool(setup).connect(conn => conn.anyFirst(sql`
          select spcname from pg_catalog.pg_tablespace where spcname not like 'pg_%'
          `)
        )
        tablespaces.forEach((e: any) => {
          createTest('./database/postgres/space', e, { connection: { database: 'postgres' }, tablespace: e })
        })
        // add users
        const users = await database.pgPool(setup).connect(conn => conn.anyFirst(sql`
          SELECT usename FROM pg_user
          `)
        )
        users.forEach((e: any) => {
          createTest('./database/postgres/user', `${e}`, { user: e })
        })
      });
    })

    return super.resolve()
  }
}

export default GroupPostgres
