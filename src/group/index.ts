import Debug from "debug";

import { Case } from '../index'

class Group {
  public path: string
  public case: Case
  protected debug: debug.Debugger
  public lang: string
  protected children: { [key: string]: any }

  constructor(path: string, testcase: Case, lang: string) {
    this.path = path
    this.case = testcase
    this.lang = lang
    this.debug = Debug(`checkup:${this.constructor.name.replace(/([a-z])([A-Z])/g, '$1:$2').toLowerCase()}`);
    this.children = {}
  }

  public async resolve() {
    // do case creation first in child class
    delete this.case.test
    //    delete this.case.group
    return this.children
  }
}

export default Group
