import Validator from '@alinex/validator';
import schemaDataSource from '@alinex/validator/lib/dataSource';
import * as Builder from '@alinex/validator/lib/schema';
import { clone, merge } from '@alinex/data'
import { basename } from 'path'
import * as async from '@alinex/async';
import { isIP } from 'net'

import Group from '.'
import { validator as valServer, idValidator, shell, execute } from '../shell'

const schemaBase = new Builder.ObjectSchema({
  item: {
    group: new Builder.ObjectSchema({
      title: 'Group Settings',
      item: {
        default: clone(valServer),
        server: new Builder.ArraySchema({
          item: { '*': clone(idValidator) },
          min: 1
        }),
        host: schemaDataSource,
        filter: new Builder.RegExpSchema(),
        include: new Builder.ArraySchema({ item: { '*': new Builder.StringSchema() }, min: 1 }),
        exclude: new Builder.ArraySchema({ item: { '*': new Builder.StringSchema() }, min: 1 })
      },
      denyUndefined: true,
      combination: [
        { key: 'server', without: ['host', 'filter'] },
        { key: 'filter', with: ['host'] }
      ]
    }),
    data: new Builder.ObjectSchema({ item: { '*': new Builder.ObjectSchema() }, min: 1 }),
    fix: new Builder.ObjectSchema({ item: { '*': new Builder.ObjectSchema() }, min: 1 }),
  }
})

const schemaList = new Builder.ArraySchema({
  item: { '*': new Builder.ObjectSchema({}) },
  min: 1
})

class GroupLinux extends Group {
  public async resolve() {
    // check data
    await Validator.load(clone(schemaBase), this.lang, { data: this.case.group, source: `preset:${this.path}.data` })
      .then(val => this.case.group = val.get())
      .catch(e => console.log(e))

    let serverList = this.case.group?.server || [undefined]

    // get hosts list
    if (this.case.group?.host) {
      let host = this.case.group?.host
      if (!Array.isArray(host)) host = [host]
      const list = await Validator.load(clone(schemaList), this.lang, ...host)
      serverList = list.get()
        .map((e: any) => Array.isArray(e) ? e[0] : typeof e === 'object' ? Object.values(e)[0] : e)
      // use server.filter
      if (this.case.group?.filter)
        serverList = serverList.filter((e: any) => ("" + e).match(this.case.group?.filter))
      serverList.map((e: any) => {
        return { host: "" + e }
      })
    }

    // detect
    const parentPath = this.case.parent[this.case.parent.length - 1]
    await async.each(serverList, async (host: any) => {
      // combine with default
      const connection = { ...host, ...this.case.group?.default }
      delete connection.id
      // create server id
      const serverId = host ? connection.host : 'localhost'

      const createTest = (test: string, instance?: string, instanceData?: any) => {
        let id = `${host?.id || isIP(serverId) ? serverId : serverId.split(/\./).reverse().join('.')}.${basename(test)}`
        const setup: any = { ...this.case.group, server: connection }
        // exclude test
        if (setup.include && !setup.include.filter((c: string) => test.startsWith(c)).length) return
        if (setup.exclude && setup.exclude.filter((c: string) => test.startsWith(c)).length) return
        // exclude instance
        if (instance) {
          id += '.' + instance.replace(/\./g, '_')
          const iname = `${test}#${instance}`
          if (setup.include && !setup.include.filter((c: string) => iname.startsWith(c)).length) return
          if (setup.exclude && setup.exclude.filter((c: string) => iname.startsWith(c)).length) return
        }
        // create case
        let fullId = `${parentPath}.${id}`.replace(/(.+)\.\1/, '$1')
        let obj = clone(this.case)
        delete obj.group
        const ititle = instance ? ` ${instance}` : ''
        obj.title = `${basename(test).substr(0, 1).toUpperCase()}${basename(test).substr(1)}${ititle} at ${serverId}`
        obj.tags.push(serverId)
        obj.parent.push(fullId)
        obj.test = test
        // deep merge data
        obj.data = merge(
          { data: { ...(host && { server: setup.server }) } },
          { data: { ...(this.case.data && this.case.data[test]) } },
          { data: { ...(this.case.data && this.case.data[`${test}#${instance}`]) } },
          { data: instanceData }
        ).data
        // deep merge fix
        obj.fix = merge(
          { data: { ...(this.case.fix && this.case.fix[test]) } },
          { data: { ...(this.case.fix && this.case.fix[`${test}#${instance}`]) } },
        ).data
        // merge cron
        obj.cron = this.case.fix[`${test}#${instance}`] || this.case.fix[test] || this.case.cron
        // add case
        this.children[fullId] = obj
        // console.log(obj)
      }

      // add simple tests without previous analyzation
      let tests = ['./linux/uptime', './linux/load', './linux/memory', './linux/os']
      // create and add tests
      tests.forEach((t: string) => createTest(t));

      // discovery
      await shell(connection, async (conn) => {
        // linux/disk
        await execute(conn, 'df -x tmpfs -x devtmpfs -x squashfs --output=target | sed 1d')
          .then((res => {
            res.stdout.split(/\n/).forEach((mount: string) => createTest('./linux/disk', mount, { mount }))
          }))
        // linux/service
        await execute(conn, "systemctl list-unit-files --type=service | head -n -2 | sed '1d;s/.service.*//'")
          .then((res => {
            const possible = ['apache2', 'nginx', 'postgresql', 'mongodb', 'mysql']
            const list = res.stdout.split(/\n/)
            possible.forEach((service: string) => {
              if (list.includes(service)) createTest('./linux/daemon', service, { service })
            })
          }))
      })

    })

    return super.resolve()
  }
}

export default GroupLinux
