import { Client } from 'ssh2';
import { promises } from 'fs'
import { command } from 'execa';
import * as Builder from '@alinex/validator/lib/schema';
import { each } from '@alinex/async'

const { readFile } = promises

export { Client }

const serverCache: any = {}

export const validator = new Builder.ObjectSchema({
  title: 'Remote Server Connection',
  item: {
    username: new Builder.StringSchema(),
    password: new Builder.StringSchema(),
    host: new Builder.StringSchema(),
    port: new Builder.NumberSchema(),
    privateKey: new Builder.FileSchema({ readable: true }),
    passphrase: new Builder.StringSchema()
  },
  combination: [{ key: 'password', with: ['username'] }],
  mandatory: ['host'],
  denyUndefined: true
})

export const idValidator = new Builder.ObjectSchema({
  title: 'Remote ID Server Connection',
  item: {
    id: new Builder.StringSchema({ allow: [/^[a-z0-9.]+$/] }),
    username: new Builder.StringSchema(),
    password: new Builder.StringSchema(),
    host: new Builder.StringSchema(),
    port: new Builder.NumberSchema(),
    privateKey: new Builder.FileSchema({ readable: true }),
    passphrase: new Builder.StringSchema()
  },
  combination: [{ key: 'password', with: ['username'] }],
  mandatory: ['host'],
  denyUndefined: true
})

export async function config(setup: any) {
  const config = setup || {}
  if (setup?.connect) return setup // already resolved
  const key = Object.values(config).join()
  if (serverCache[key]) return serverCache[key] // found in cache
  // create connection string
  let connect = config.host || ''
  if (config.username)
    connect = `${config.username}${config.password ? ':' + config.password : ''}@${connect}`
  if (config.port) connect = `${connect}:${config.port}`
  if (config.host && !config.password && !config.privateKey) config.privateKey = `~/.ssh/id_rsa`
  if (!config.privateKey) {
    serverCache[key] = { ...config, connect }
    return serverCache[key]
  }
  // load key
  config.privateKey = config.privateKey.replace(/^~/, require('os').homedir())
  const file = config.privateKey
  return readFile(file, 'utf8').then(v => serverCache[key] = { ...config, connect, privateKey: v })
    .then(() => serverCache[key])
    .catch(e => { throw new Error(`Could not load private key for SSH connection from ${file}`) })
}

export async function shell(setup: any, fn: (conn: Client | undefined) => Promise<any>) {
  const server = await config(setup)
  if (!server.host) return fn(undefined)
  // remote shell
  return new Promise((resolve, reject) => {
    const conn = new Client();
    conn.on('error', reject)
    conn.on('ready', async () => {
      try {
        const result = await fn(conn)
        conn.end(); // close connection
        resolve(result)
      } catch (e) {
        reject(e)
      }
    }).connect(server);
  })
}

export async function execute(conn: Client | undefined, cmd: string): Promise<any> {
  if (!conn)
    return command(cmd, { shell: true, reject: false }).then(e => {
      return {
        stdout: e.stdout,
        stderr: e.stderr,
        exitCode: e.exitCode,
        signal: e.signal
      }
    });
  // remote shell
  return new Promise((resolve, reject) => {
    conn.exec(cmd, (err, stream) => {
      if (err) { conn.end(); return reject(err); }
      const stdout: Buffer[] = [];
      const stderr: Buffer[] = [];
      let exitCode: number = 0;
      let signal: string | undefined = undefined
      stream
        .on('end', () => {
          resolve({
            stdout: Buffer.concat(stdout).toString().trim(),
            stderr: Buffer.concat(stderr).toString().trim(),
            exitCode,
            signal
          })
        })
        .on('data', (data: Buffer) => stdout.push(data))
        .on('close', (code: number, sig: string) => { exitCode = code; signal = sig })
        .stderr.on('data', (data: Buffer) => stderr.push(data))
    });
  })
}

// common scripts
const scripts = {
  cmdPossible: (cmd: string) => `command -v ${cmd}`,
  // sudoPossible: (cmd: string) => `[ $(id -u) -eq 0 ] && command -v ${cmd} || (sudo -vn 2>/dev/null && sudo -l ${cmd} 2>/dev/null | sed 's/ .*//;s/^/sudo /')`
  sudoPossible: (cmd: string) => `[ $(id -u) -eq 0 ] && command -v ${cmd} || (sudo -l ${cmd} 2>/dev/null | sed 's/ .*//;s/^/sudo -n /')`
}

// commands
// const cmds = await this.commands(['du', 'find'], true)
// const nosudo = Object.keys(cmds).filter(e => e.startsWith('$') && !cmds[e]).join(', ')
export async function commands(conn: Client | undefined, list: string[], sudo = false) {
  const result: { [key: string]: string } = {}
  await each(list, (cmd: string) => {
    return execute(conn, sudo ? scripts.sudoPossible(cmd) : scripts.cmdPossible(cmd))
      .then(res => res.stdout)
      .then(res => {
        result[(sudo ? '$' : '') + cmd] = res || undefined
        if (!res && sudo) return execute(conn, scripts.cmdPossible(cmd)).then(res => result[cmd] = res.stdout)
      })
  })
  return result
}
