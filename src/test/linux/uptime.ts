import { DateTime } from 'luxon'
import Validator from '@alinex/validator';
import * as Builder from '@alinex/validator/lib/schema';
import { clone } from '@alinex/data'

import { T } from '../../i18n';
import { TestLinux, valServer } from '.'
import FixLinuxUsers from '../../fix/linux/users'

const schemaBase = new Builder.ObjectSchema({
  item: {
    server: clone(valServer),
    warn: new Builder.ObjectSchema({
      title: 'Warn Check',
      item: {
        age: new Builder.NumberSchema({
          title: 'Age (since system has booted)',
          unit: { from: 's', to: 's' },
          min: 0
        })
      },
      mandatory: true,
      denyUndefined: true,
      default: { age: 3600 }
    })
  },
  mandatory: ['warn'],
  denyUndefined: true
})
const schemaFix = new Builder.ObjectSchema({
  item: {
    server: clone(valServer),
    users: FixLinuxUsers.valUsers
  },
  denyUndefined: true
})

class TestLinuxUptime extends TestLinux {
  public async run() {
    const t = T(this.lang, 'linux')

    // check data
    await Validator.load(clone(schemaBase), this.lang, { data: this.data, source: `preset:${this.path}.data` })
      .then(val => this.data = val.get())
    await Validator.load(clone(schemaFix), this.lang, { data: this.fixdata, source: `preset:${this.path}.fix` })
      .then(val => this.fixdata = val.get())

    this.description = t('uptime.test.description', { connect: this.data.server?.host || 'localhost', ...this.data })

    // setup validation
    const valResult = new Validator(new Builder.DateTimeSchema({
      title: t('core:test.validateResult'),
      parse: true
    }))
    const valWarn = new Validator(new Builder.ObjectSchema({
      title: t('core:test.validateWarn'),
      item: {
        age: new Builder.NumberSchema({
          title: t('uptime.test.age', { count: this.data.warn.age }),
          min: this.data.warn.age
        })
      }
    }))

    // get data
    return this.shell(async () => {
      return this.execute('uptime -s')
        .then(res => {
          this.logValidation(valResult.describe())
          return valResult.load(this.lang, { data: res.stdout })
        })
    })

      // data collection
      .then(val => {
        this.result = {
          boot: val,
          age: DateTime.local().diff(DateTime.fromJSDate(val), 'seconds').toObject().seconds
        }
        this.logResult(this.result)
        this.status = 'OK'
        return val
      })

      // warning check
      .then(_val => {
        if (!this.data.warn) return
        this.logValidation(valWarn.describe())
        return valWarn.load(this.lang, { data: this.result, source: 'preset:result' })
          .catch(e => {
            this.status = 'WARN';
            this.error = e;
            this.fixes.push(new FixLinuxUsers(this))
          })
      })
  }
}

export default TestLinuxUptime
export { TestLinuxUptime as Test, schemaBase as data, schemaFix as fix } // for interactive mode
