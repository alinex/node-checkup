import Validator from '@alinex/validator';
import * as Builder from '@alinex/validator/lib/schema';
import { clone } from '@alinex/data'

import { T } from '../../i18n';
import { TestLinux, valServer } from '.'
import FixLinuxMemory from '../../fix/linux/memory'

const schemaBase = new Builder.ObjectSchema({
  item: {
    server: clone(valServer),
    warn: new Builder.ObjectSchema({
      title: 'Warn Check',
      item: {
        mem_available: new Builder.NumberSchema({ sanitize: true, min: 0, max: 1 }),
        swap_free: new Builder.NumberSchema({ sanitize: true, min: 0, max: 1 })
      },
      default: { mem_available: 0.1, swap_free: 0.5 },
      denyUndefined: true
    }),
    error: new Builder.ObjectSchema({
      title: 'Error Check',
      item: {
        mem_available: new Builder.NumberSchema({ sanitize: true, min: 0, max: 1 }),
        swap_free: new Builder.NumberSchema({ sanitize: true, min: 0, max: 1 })
      },
      default: { mem_available: 0.01, swap_free: 0.1 },
      denyUndefined: true
    })
  },
  mandatory: ['warn', 'error'],
  denyUndefined: true
})
const schemaFix = new Builder.ObjectSchema({
  item: {
    server: clone(valServer),
    procs: FixLinuxMemory.valProcs
  },
  mandatory: ['procs'],
  denyUndefined: true
})

class TestLinuxMemory extends TestLinux {
  public async run() {
    const t = T(this.lang, 'linux')

    // check data
    await Validator.load(clone(schemaBase), this.lang, { data: this.data, source: `preset:${this.path}.data` })
      .then(val => this.data = val.get())
    await Validator.load(clone(schemaFix), this.lang, { data: this.fixdata, source: `preset:${this.path}.fix` })
      .then(val => this.fixdata = val.get())

    this.description = t('memory.test.description', { connect: this.data.server?.host || 'localhost', ...this.data })

    // setup validation
    const valResult = new Validator(new Builder.ObjectSchema({
      title: t('core:test.validateResult'),
      item: {
        mem_total: new Builder.NumberSchema({ min: 0 }),
        mem_free: new Builder.NumberSchema({ min: 0 }),
        mem_available: new Builder.NumberSchema({ min: 0 }),
        swap_total: new Builder.NumberSchema({ min: 0 }),
        swap_free: new Builder.NumberSchema({ min: 0 })
      }
    }))
    const valWarn = new Validator(new Builder.ObjectSchema({
      title: t('core:test.validateWarn'),
      item: {
        ...(typeof this.data.warn?.swap_free !== 'undefined' && {
          swap_free_percent: new Builder.NumberSchema({
            title: t('memory.test.swap', { min: this.data.warn.swap_free }),
            min: this.data.warn.swap_free
          })
        }),
        mem_available_percent: new Builder.NumberSchema({
          ...(typeof this.data.warn?.mem_available !== 'undefined' && {
            title: t('memory.test.memory', { min: this.data.warn.mem_available }),
            min: this.data.warn.mem_available
          })
        })
      }
    }))
    const valError = new Validator(new Builder.ObjectSchema({
      title: t('core:test.validateError'),
      item: {
        ...(typeof this.data.error?.swap_free !== 'undefined' && {
          swap_free_percent: new Builder.NumberSchema({
            title: t('memory.test.swap', { min: this.data.error.swap_free }),
            min: this.data.error.swap_free
          })
        }),
        mem_available_percent: new Builder.NumberSchema({
          ...(typeof this.data.error?.mem_available !== 'undefined' && {
            title: t('memory.test.memory', { min: this.data.error.mem_available }),
            min: this.data.error.mem_available
          })
        })
      }
    }))

    // get data
    return this.shell(async () => {
      return this.execute('sed \'s/ kB//;s/: \\+/ /\' /proc/meminfo')
        .then(res => {
          const obj: any = {}
          res.stdout.split(/\n/).forEach((e: string) => {
            const p = e.split(/\s+/)
            obj[p[0]] = p[1]
          })
          // validate result
          this.logValidation(valResult.describe())
          return valResult.load(this.lang, { data: obj })
        })
    })

      // data collection
      .then(val => {
        this.result = {
          mem_total: +val.MemTotal,
          mem_free: +val.MemFree,
          mem_available: +val.MemAvailable,
          mem_available_percent: Math.round(((val.MemAvailable / val.MemTotal) + Number.EPSILON) * 100) / 100,
          swap_total: +val.SwapTotal,
          swap_free: +val.SwapFree,
          swap_free_percent: val.SwapTotal == 0 ? 1 : Math.round(((val.SwapFree / val.SwapTotal) + Number.EPSILON) * 100) / 100
        }
        this.logResult(this.result)
        this.status = 'OK'
        return val
      })

      // check error and warning
      .then(() => {
        const ds = { data: this.result, source: 'preset:result' }
        return Promise.resolve().then(() => {
          if (!this.data.error) return
          this.logValidation(valError.describe())
          return valError.load(this.lang, ds)
        })
          .then(_val => {
            if (!this.data.warn) return
            this.logValidation(valWarn.describe())
            return valWarn.load(this.lang, ds)
              .catch(e => {
                this.status = 'WARN'
                this.error = e
                this.fixes.push(new FixLinuxMemory(this))
              })
          })
          .catch(e => {
            this.status = 'ERROR'
            this.error = e
            this.fixes.push(new FixLinuxMemory(this))
          })
      })
  }
}

export default TestLinuxMemory
export { TestLinuxMemory as Test, schemaBase as data, schemaFix as fix } // for interactive mode
