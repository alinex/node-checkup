import { DateTime } from 'luxon'
import Validator from '@alinex/validator';
import * as Builder from '@alinex/validator/lib/schema';
import { clone } from '@alinex/data'

import { T } from '../../i18n';
import { TestLinux, valServer } from '.'
import FixLinuxDaemon from '../../fix/linux/daemon'
import FixLinuxUsers from '../../fix/linux/users'

const schemaBase = new Builder.ObjectSchema({
  item: {
    server: clone(valServer),
    service: new Builder.StringSchema(),
    warn: new Builder.ObjectSchema({
      title: 'Warn Check',
      item: {
        age: new Builder.NumberSchema({
          title: 'Age (since daemon has to be started)',
          unit: { from: 's', to: 's' },
          min: 0
        })
      },
      denyUndefined: true,
      default: { age: 300 }
    }),
    error: new Builder.ObjectSchema({
      title: 'Error Check',
      item: {
        listen: new Builder.LogicSchema({
          operator: 'or',
          check: [
            new Builder.BooleanSchema({ value: true }),
            new Builder.ArraySchema({
              item: {
                '*': new Builder.ObjectSchema({
                  item: {
                    type: new Builder.StringSchema({ allow: ['TCP', 'UDP'] }),
                    ip: new Builder.IPSchema(),
                    port: new Builder.PortSchema()
                  },
                  min: 1
                })
              },
              min: 1
            })
          ]
        })
      },
      denyUndefined: true
    })
  },
  mandatory: ['service', 'warn'],
  denyUndefined: true
})
const schemaFix = new Builder.ObjectSchema({
  item: {
    server: clone(valServer),
    users: FixLinuxUsers.valUsers
  },
  mandatory: ['users'],
  denyUndefined: true
})

class TestLinuxDaemon extends TestLinux {
  public async run() {
    const t = T(this.lang, 'linux')

    // check data
    await Validator.load(clone(schemaBase), this.lang, { data: this.data, source: `preset:${this.path}.data` })
      .then(val => this.data = val.get())
    await Validator.load(clone(schemaFix), this.lang, { data: this.fixdata, source: `preset:${this.path}.fix` })
      .then(val => this.fixdata = val.get())

    this.description = t('daemon.test.description', { connect: this.data.server?.host || 'localhost', ...this.data })

    // setup validation
    const valResult = new Validator(new Builder.StringSchema({
      title: t('core:test.validateResult'),
      allow: [/Active:/]
    }))
    const valWarn = new Validator(new Builder.ObjectSchema({
      title: t('core:test.validateWarn'),
      item: {
        age: new Builder.NumberSchema({
          title: t('daemon.test.age', { count: this.data.warn.age }),
          unit: { from: 's', to: 's' },
          min: this.data.warn.age
        })
      }
    }))
    const item: { [key: string]: Builder.Schema } = {
      active: new Builder.BooleanSchema({ title: t('daemon.test.active'), value: true }),
      status: new Builder.StringSchema({ title: t('daemon.test.status'), allow: ['running'] }),
    }
    if (this.data.error?.listen === true) item.listen = new Builder.ArraySchema({ title: t('daemon.test.listen'), min: 1 })
    else if (this.data.error?.listen) {
      item.listen = new Builder.LogicSchema({
        title: t('daemon.test.listenSpecific'),
        operator: 'and',
        check: this.data.error?.listen.map((e: any) => {
          const filteritem: { [key: string]: Builder.Schema } = {}
          Object.keys(e).forEach(k => filteritem[k] = k === 'port'
            ? new Builder.NumberSchema({ allow: [e[k]] })
            : new Builder.StringSchema({ allow: [e[k]] }))
          return new Builder.ArraySchema({
            title: t('daemon.test.listenArray'),
            filter: new Builder.ObjectSchema({ title: t('daemon.test.listenObject'), item: filteritem }),
            min: 1
          })
        })
      })
    }
    const valError = new Validator(new Builder.ObjectSchema({
      title: t('core:test.validateError'),
      item,
      mandatory: true
    }))

    // get data
    return this.shell(async () => {
      return this.execute(`systemctl status ${this.data.service}`)
        .then(res => {
          if (res.exitCode) throw new Error(res.stderr || `Èxit code ${res.exitCode}`)
          this.logValidation(valResult.describe())
          return valResult.load(this.lang, { data: res.stdout })
        })
        // data collection
        .then(val => {
          this.result = {}
          let m = val.match(/ - ([^\n]+)/)
          if (m) this.result.title = m[1]
          m = val.match(/Loaded: \w+ \((.*?);/)
          if (m) this.result.script = m[1]
          m = val.match(/Active: (\w+)(?: \((\w+)\)) since \w+ (.*?) [A-Z]/)
          if (m) {
            this.result.active = m[1] === 'active'
            this.result.status = m[2]
            this.result.date = Date.parse(m[3])
            this.result.age = DateTime.local().diff(DateTime.fromMillis(Date.parse(m[3])), 'seconds').toObject().seconds
          }
          m = val.match(/Main PID: (\d+)/)
          if (m) this.result.pid = m[1]
          // get ports
          if (!this.data.error?.listen) return
          return this.execute(`sudo -l lsof >/dev/null && sudo lsof -Pan -p ${this.result.pid} -i | grep LISTEN | sed \'s/ \\+/\\t/g;s/\\(.*\\):/\\1\\t/\' | cut -f 8-10`)
            .then(res => {
              if (res.stdout) {
                this.result.listen = []
                res.stdout.split(/\n/).forEach((e: string) => {
                  const cols = e.split(/\s+/)
                  this.result.listen.push({
                    type: cols[0],
                    ip: cols[1],
                    port: cols[2]
                  })
                })
              }
            })
            .catch(() => undefined)
        })
    })
      .then(() => {
        this.logResult(this.result)
        this.status = 'OK'
      })

      // check error and warning
      .then(() => {
        const ds = { data: this.result, source: 'preset:result' }
        return Promise.resolve().then(() => {
          if (!this.data.error) return
          this.logValidation(valError.describe())
          return valError.load(this.lang, ds)
        })
          .then(_val => {
            if (!this.data.warn) return
            this.logValidation(valWarn.describe())
            return valWarn.load(this.lang, ds)
              .catch(e => {
                this.status = 'WARN'
                this.error = e
                this.fixes.push(new FixLinuxUsers(this))
              })
          })
          .catch(e => {
            this.status = 'ERROR'
            this.error = e
            this.fixes.push(new FixLinuxUsers(this))
            this.fixes.push(new FixLinuxDaemon(this))
          })
      })
  }
}

export default TestLinuxDaemon
export { TestLinuxDaemon as Test, schemaBase as data, schemaFix as fix } // for interactive mode
