import Validator from '@alinex/validator';
import * as Builder from '@alinex/validator/lib/schema';
import { clone } from '@alinex/data'

import { T } from '../../i18n';
import { TestLinux, valServer } from '.'

const filetype: { [key: number]: string } = {
  12: 'socket',
  10: 'symbolic link',
  8: 'regular file',
  6: 'block device',
  4: 'directory',
  2: 'character device',
  1: 'FIFO',
}

const item = {
  minSize: new Builder.NumberSchema({
    title: 'minimal file size',
    unit: { from: 'iB', to: 'iB' },
    integer: true,
    min: 0,
  }),
  maxSize: new Builder.NumberSchema({
    title: 'maximal file size',
    unit: { from: 'iB', to: 'iB' },
    integer: true,
    min: 0,
  }),
  minFree: new Builder.NumberSchema({
    title: 'free size for file',
    unit: { from: 'iB', to: 'iB' },
    integer: true,
    min: 0,
  }),
  type: new Builder.StringSchema({
    title: 'type of inode',
    allow: ['socket', 'symbolic link', 'regular file', 'block device', 'directory', 'character device', 'FIFO'],
  }),
  minAccess: new Builder.DateTimeSchema({
    title: 'last access after',
    max: new Date()
  }),
  maxAccess: new Builder.DateTimeSchema({
    title: 'last access before',
    max: new Date()
  }),
  minChange: new Builder.DateTimeSchema({
    title: 'last change after',
    max: new Date()
  }),
  maxChange: new Builder.DateTimeSchema({
    title: 'last change before',
    max: new Date()
  }),
  allow: new Builder.RegExpSchema({
    title: 'pattern must be present'
  }),
  disallow: new Builder.RegExpSchema({
    title: 'pattern not allowed'
  })
}
const schemaBase = new Builder.ObjectSchema({
  item: {
    server: clone(valServer),
    path: new Builder.FileSchema(),
    lines: new Builder.NumberSchema({ min: 0, default: 100 }),
    warn: new Builder.ObjectSchema({
      title: 'Warn Check',
      item,
      denyUndefined: true
    }),
    // { mount: '/', size: 724, free: 305449092, mode: '100644', read: [ 'user', 'group', 'other' ], write: [ 'user', false, false ], execute: [ false, false, false ], sticky: false, _setgid: false, setuid: false, type: 'regular file', uid: 0, owner: 'root', gid: 0, group: 'root', access: 2020-10-08T05:03:59.000Z, change: 2018-04-11T11:10:51.000Z, meta: 2018-04-11T11:10:51.000Z, create: undefined } +0ms_
    error: new Builder.ObjectSchema({
      title: 'Error Check',
      item,
      denyUndefined: true
    })
  },
  mandatory: ['path'],
  denyUndefined: true
})

class TestLinuxFile extends TestLinux {
  public async run() {
    const t = T(this.lang, 'linux')

    // check data
    await Validator.load(clone(schemaBase), this.lang, { data: this.data, source: `preset:${this.path}.data` })
      .then(val => this.data = val.get())
    //    await Validator.load(clone(schemaFix), this.lang, { data: this.fixdata, source: `preset:${this.path}.fix` })
    //          .then(val => this.fixdata = val.get())

    this.description = t('load.test.description', { connect: this.data.server?.host || 'localhost', ...this.data })

    // setup validation
    const valResult = new Validator(new Builder.ArraySchema({
      title: t('core:test.validateResult'),
      split: /[\s\n]+/,
      min: 14,
      max: 14
    }))
    const valWarn = new Validator(new Builder.ObjectSchema({
      title: t('core:test.validateWarn'),
      item: {
        ...((this.data.warn?.minSize || this.data.warn?.maxSize) && {
          size: new Builder.NumberSchema({
            title: this.data.warn?.minSize && this.data.warn?.maxSize ? t('file.test.size', { min: this.data.warn.minSize, max: this.data.warn.maxSize })
              : this.data.warn?.minSize ? t('file.test.size_min', { min: this.data.warn.minSize })
                : t('file.test.size_max', { max: this.data.warn.maxSize }),
            min: this.data.warn.minSize,
            max: this.data.warn.maxSize
          })
        }),
        ...(this.data.warn?.minFree && {
          free: new Builder.NumberSchema({
            title: t('file.test.free', { min: this.data.warn.minFree }),
            min: this.data.warn.minFree
          })
        }),
        ...(this.data.warn?.type && {
          free: new Builder.StringSchema({
            title: t('file.test.type', { type: this.data.warn.type }),
            allow: [this.data.warn.type]
          })
        }),
        ...((this.data.warn?.minAccess || this.data.warn?.maxAccess) && {
          Access: new Builder.DateTimeSchema({
            title: this.data.warn?.minAccess && this.data.warn?.maxAccess ? t('file.test.access', { min: this.data.warn.minAccess, max: this.data.warn.maxAccess })
              : this.data.warn?.minAccess ? t('file.test.access_min', { min: this.data.warn.minAccess })
                : t('file.test.access_max', { max: this.data.warn.maxAccess }),
            min: this.data.warn.minAccess,
            max: this.data.warn.maxAccess
          })
        }),
        ...((this.data.warn?.minChange || this.data.warn?.maxChange) && {
          Change: new Builder.DateTimeSchema({
            title: this.data.warn?.minChange && this.data.warn?.maxChange ? t('file.test.change', { min: this.data.warn.minChange, max: this.data.warn.maxChange })
              : this.data.warn?.minChange ? t('file.test.change_min', { min: this.data.warn.minChange })
                : t('file.test.change_max', { max: this.data.warn.maxChange }),
            min: this.data.warn.minChange,
            max: this.data.warn.maxChange
          })
        }),
        ...((this.data.warn?.allow || this.data.warn?.disallow) && {
          content: new Builder.StringSchema({
            title: t('file.test.type', { type: this.data.warn.type }),
            ...(this.data.warn.allow && { allow: [this.data.warn.allow] }),
            ...(this.data.warn.disallow && { disallow: [this.data.warn.disallow] }),
          })
        }),
      }
    }))
    const valError = new Validator(new Builder.ObjectSchema({
      title: t('core:test.validateError'),
      item: {
        ...((this.data.error?.minSize || this.data.error?.maxSize) && {
          size: new Builder.NumberSchema({
            title: this.data.error?.minSize && this.data.error?.maxSize ? t('file.test.size', { min: this.data.error.minSize, max: this.data.error.maxSize })
              : this.data.error?.minSize ? t('file.test.size_min', { min: this.data.error.minSize })
                : t('file.test.size_max', { max: this.data.error.maxSize }),
            min: this.data.error.minSize,
            max: this.data.error.maxSize
          })
        }),
        ...(this.data.error?.minFree && {
          free: new Builder.NumberSchema({
            title: t('file.test.free', { min: this.data.error.minFree }),
            min: this.data.error.minFree
          })
        }),
        ...(this.data.error?.type && {
          free: new Builder.StringSchema({
            title: t('file.test.type', { type: this.data.error.type }),
            allow: [this.data.error.type]
          })
        }),
        ...((this.data.error?.minAccess || this.data.error?.maxAccess) && {
          Access: new Builder.DateTimeSchema({
            title: this.data.error?.minAccess && this.data.error?.maxAccess ? t('file.test.access', { min: this.data.error.minAccess, max: this.data.error.maxAccess })
              : this.data.error?.minAccess ? t('file.test.access_min', { min: this.data.error.minAccess })
                : t('file.test.access_max', { max: this.data.error.maxAccess }),
            min: this.data.error.minAccess,
            max: this.data.error.maxAccess
          })
        }),
        ...((this.data.error?.minChange || this.data.error?.maxChange) && {
          Change: new Builder.DateTimeSchema({
            title: this.data.error?.minChange && this.data.error?.maxChange ? t('file.test.change', { min: this.data.error.minChange, max: this.data.error.maxChange })
              : this.data.error?.minChange ? t('file.test.change_min', { min: this.data.error.minChange })
                : t('file.test.change_max', { max: this.data.error.maxChange }),
            min: this.data.error.minChange,
            max: this.data.error.maxChange
          })
        }),
        ...((this.data.error?.allow || this.data.error?.disallow) && {
          content: new Builder.StringSchema({
            title: t('file.test.type', { type: this.data.error.type }),
            ...(this.data.error.allow && { allow: [this.data.error.allow] }),
            ...(this.data.error.disallow && { disallow: [this.data.error.disallow] }),
          })
        }),
      }
    }))

    // get data
    return this.shell(async () => {
      const stat = `LANG=C stat -c '%m %f %u %U %g %G %X %Y %Z %W' ${this.data.path}`
      const free = `df --output=avail ${this.data.path} | tail -n 1`
      const size = `du -bsD ${this.data.path} | cut -f 1`
      const mime = `file -biz ${this.data.path} | sed 's/; charset=/ /'`
      return this.execute(`${size} && ${stat} && ${free} && ${mime}`)
        .then(res => {
          this.logValidation(valResult.describe())
          return valResult.load(this.lang, { data: res.stdout })
        })
    })
      // data collection
      .then(async (val) => {
        const type = parseInt(val[2].substring(0, 1), 16)
        const raw = parseInt(val[2], 16)
        this.result = {
          mount: val[1],
          size: parseInt(val[0]),
          free: parseInt(val[11]) * 1024,
          mode: raw.toString(8),
          read: [(raw & 256) != 0 && 'user', (raw & 32) != 0 && 'group', (raw & 4) != 0 && 'other'],
          write: [(raw & 128) != 0 && 'user', (raw & 16) != 0 && 'group', (raw & 2) != 0 && 'other'],
          execute: [(raw & 64) != 0 && 'user', (raw & 8) != 0 && 'group', (raw & 1) != 0 && 'other'],
          sticky: (raw & 512) != 0,
          set_gid: (raw & 1024) != 0,
          set_uid: (raw & 2048) != 0,
          type: filetype[type],
          uid: parseInt(val[3]),
          owner: val[4],
          gid: parseInt(val[5]),
          group: val[6],
          access: new Date(parseInt(val[7]) * 1000),
          change: new Date(parseInt(val[8]) * 1000),
          metadata: new Date(parseInt(val[9]) * 1000),
          create: parseInt(val[10]) ? new Date(parseInt(val[10]) * 1000) : undefined,
          mimetype: val[12],
          charset: val[13]
        }
        // content
        if (this.data.lines !== 0 && (type === 8 || type === 10))
          this.result.content = await this.execute(`head -n ${this.data.lines || 100} ${this.data.path}`).then(res => res.stdout)
        this.logResult(this.result)
        this.status = 'OK'
      })

      // check error and warning
      .then(() => {
        const ds = { data: this.result, source: 'preset:result' }
        return Promise.resolve().then(() => {
          if (!this.data.error) return
          this.logValidation(valError.describe())
          return valError.load(this.lang, ds)
        })
          .then(_val => {
            if (!this.data.warn) return
            this.logValidation(valWarn.describe())
            return valWarn.load(this.lang, ds)
              .catch(e => {
                this.status = 'WARN'
                this.error = e
              })
          })
          .catch(e => {
            this.status = 'ERROR'
            this.error = e
          })
      })
  }
}

export default TestLinuxFile
export { TestLinuxFile as Test, schemaBase as data } // for interactive mode
