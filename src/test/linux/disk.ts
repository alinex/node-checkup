import Validator from '@alinex/validator';
import * as Builder from '@alinex/validator/lib/schema';
import { clone } from '@alinex/data'

import { T } from '../../i18n';
import { TestLinux, valServer } from '.'
import FixLinuxDisk from '../../fix/linux/disk'

const schemaBase = new Builder.ObjectSchema({
  item: {
    server: clone(valServer),
    mount: new Builder.StringSchema({ disallow: [/['"]/], default: '/' }),
    warn: new Builder.ObjectSchema({
      title: 'Warn Check',
      item: {
        percent: new Builder.NumberSchema({ sanitize: true, min: 0 }),
        ipercent: new Builder.NumberSchema({ sanitize: true, min: 0 }),
        free: new Builder.NumberSchema({ unit: { from: 'iB', to: 'KiB' }, min: 0 }),
        ifree: new Builder.NumberSchema({ integer: true, min: 0 }),
      },
      denyUndefined: true,
      default: { percent: 0.2, ipercent: 0.2 }
    }),
    error: new Builder.ObjectSchema({
      title: 'Warn Check',
      item: {
        percent: new Builder.NumberSchema({ sanitize: true, min: 0 }),
        ipercent: new Builder.NumberSchema({ sanitize: true, min: 0 }),
        free: new Builder.NumberSchema({ unit: { from: 'iB', to: 'KiB' }, min: 0 }),
        ifree: new Builder.NumberSchema({ integer: true, min: 0 }),
      },
      denyUndefined: true,
      default: { percent: 0.1, ipercent: 0.1 }
    })
  },
  mandatory: ['mount', 'warn', 'error'],
  denyUndefined: true
})
const schemaFix = new Builder.ObjectSchema({
  item: {
    server: clone(valServer),
    bigNum: FixLinuxDisk.bigNum,
    compress: FixLinuxDisk.compress,
    delete: FixLinuxDisk.delete
  },
  mandatory: ['bigNum', 'compress', 'delete'],
  denyUndefined: true
})

class TestLinuxDisk extends TestLinux {
  public async run() {
    const t = T(this.lang, 'linux')

    // check data
    await Validator.load(clone(schemaBase), this.lang, { data: this.data, source: `preset:${this.path}.data` })
      .then(val => this.data = val.get())
    await Validator.load(clone(schemaFix), this.lang, { data: this.fixdata, source: `preset:${this.path}.fix` })
      .then(val => this.fixdata = val.get())

    if (this.data.mount) this.description = t('disk.test.descriptionMount', { connect: this.data.server?.host || 'localhost', ...this.data })
    else this.description = t('disk.test.description', { connect: this.data.server?.host || 'localhost', ...this.data })

    // setup validation
    const valResult = new Validator(new Builder.ArraySchema({
      title: t('core:test.validateResult'),
      filter: new Builder.ObjectSchema({
        title: t('disk.test.filter'),
        item: {
          Mounted: new Builder.StringSchema({
            title: t('disk.test.filterMount', { path: this.data.mount }),
            allow: [this.data.mount]
          })
        }
      }),
      item: {
        '*': new Builder.ObjectSchema({
          title: t('disk.test.data'),
          item: {
            Filesystem: new Builder.StringSchema(),
            Inodes: new Builder.NumberSchema(),
            IFree: new Builder.NumberSchema(),
            Total: new Builder.NumberSchema(),
            Avail: new Builder.NumberSchema(),
            Mounted: new Builder.StringSchema()
          },
          mandatory: true
        })
      }
    }))
    const valWarn = new Validator(new Builder.ObjectSchema({
      title: t('core:test.validateWarn'),
      item: {
        ...(this.data.warn.percent && {
          percent: new Builder.NumberSchema({
            title: t('disk.test.percent', { greater: this.data.warn.percent }),
            greater: this.data.warn.percent
          })
        }),
        ...(this.data.warn.ipercent && {
          ipercent: new Builder.NumberSchema({
            title: t('disk.test.ipercent', { greater: this.data.warn.ipercent }),
            greater: this.data.warn.ipercent
          })
        }),
        ...(this.data.warn.free && {
          free: new Builder.NumberSchema({
            title: t('disk.test.free', { greater: this.data.warn.free }),
            greater: this.data.warn.free
          })
        }),
        ...(this.data.warn.ifree && {
          ifree: new Builder.NumberSchema({
            title: t('disk.test.ifree', { greater: this.data.warn.ifree }),
            greater: this.data.warn.ifree
          })
        }),
      }
    }))
    const valError = new Validator(new Builder.ObjectSchema({
      title: t('core:test.validateError'),
      item: {
        ...(this.data.warn.percent && {
          percent: new Builder.NumberSchema({
            title: t('disk.test.percent', { greater: this.data.error.percent }),
            greater: this.data.error.percent
          })
        }),
        ...(this.data.warn.ipercent && {
          ipercent: new Builder.NumberSchema({
            title: t('disk.test.ipercent', { greater: this.data.error.ipercent }),
            greater: this.data.error.ipercent
          })
        }),
        ...(this.data.warn.free && {
          free: new Builder.NumberSchema({
            title: t('disk.test.free', { greater: this.data.error.free }),
            greater: this.data.error.free
          })
        }),
        ...(this.data.warn.ifree && {
          ifree: new Builder.NumberSchema({
            title: t('disk.test.ifree', { greater: this.data.error.ifree }),
            greater: this.data.error.ifree
          })
        }),
      }
    }))

    // get data
    return this.shell(async () => {
      return this.execute('LANG=C df -x tmpfs -x devtmpfs -x squashfs --output=source,itotal,iavail,size,avail,target -k | sed \'1d\'')
    })
      .then(res => {
        return res.stdout.split(/\n/).map((e: string) => {
          const cols = e.split(/\s+/)
          return {
            Filesystem: cols[0],
            Inodes: cols[1],
            IFree: cols[2],
            Total: cols[3],
            Avail: cols[4],
            Mounted: cols[5]
          }
        })
      })
      // validate result
      .then(data => {
        this.logValidation(valResult.describe())
        return valResult.load(this.lang, { data })
      })

      // data collection
      .then(val => {
        this.result = val.map((e: any) => {
          return {
            device: e.Filesystem,
            itotal: e.Inodes,
            ifree: e.IFree,
            ipercent: e.Inodes ? e.IFree / e.Inodes : 1,
            total: e.Total,
            free: e.Avail,
            percent: e.Avail / e.Total,
            mount: e.Mounted
          }
        })[0]
        this.logResult(this.result)
        //        if (!this.result.length) throw new Error('No disks found matching ')
        this.status = 'OK'
        return val
      })

      // check error and warning
      .then(() => {
        const ds = { data: this.result, source: 'preset:result' }
        return Promise.resolve().then(() => {
          if (!this.data.error) return
          this.logValidation(valError.describe())
          return valError.load(this.lang, ds)
        })
          .then(_val => {
            if (!this.data.warn) return
            this.logValidation(valWarn.describe())
            return valWarn.load(this.lang, ds)
              .catch(e => {
                this.status = 'WARN'
                this.error = e
              })
          })
          .catch(e => {
            this.status = 'ERROR'
            this.error = e
          })
      })
  }
}

export default TestLinuxDisk
export { TestLinuxDisk as Test, schemaBase as data, schemaFix as fix } // for interactive mode
