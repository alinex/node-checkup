import { DataStore } from "@alinex/datastore";

import { loadNamespace } from '../../i18n'
import { validator, config } from '../../shell'
import Test from '..'

loadNamespace('linux')

export { validator as valServer }

export class TestLinux extends Test {

  protected async load (cmd: string, options: any = {}): Promise<DataStore> {
    const ds = new DataStore()
    this.logRequest(cmd)
    // this.logData('request data')
    const server = await config(this.data.server)
    return ds.load({
      source: `shell://${server.connect}/#${cmd}`,
      options: { format: 'text', privateKey: server.privateKey, passphrase: server.passphrase, ...options }
    }).then(() => {
      if (typeof ds.meta.exitCode !== 'undefined') this.logResponse(`code: ${ds.meta.exitCode}`)
      this.logReturn(ds.get())
      return ds
    })
  }

}

export default TestLinux
