import Validator from '@alinex/validator';
import * as Builder from '@alinex/validator/lib/schema';
import { clone } from '@alinex/data'

import { T } from '../../i18n';
import { TestLinux, valServer } from '.'
import FixLinuxLoad from '../../fix/linux/load'

const schemaBase = new Builder.ObjectSchema({
  item: {
    server: clone(valServer),
    warn: new Builder.ObjectSchema({
      title: 'Warn Check',
      item: {
        load_1m: new Builder.NumberSchema({ sanitize: true, min: 0 }),
        load_5m: new Builder.NumberSchema({ sanitize: true, min: 0 }),
        load_15m: new Builder.NumberSchema({ sanitize: true, min: 0 }),
        iowait: new Builder.NumberSchema({ sanitize: true, min: 0, max: 1 }),
        steal: new Builder.NumberSchema({ sanitize: true, min: 0, max: 1 }),
      },
      default: { load_5m: 2, iowait: 0.3, steal: 0.2 },
      denyUndefined: true
    }),
    error: new Builder.ObjectSchema({
      title: 'Error Check',
      item: {
        load_1m: new Builder.NumberSchema({ sanitize: true, min: 0 }),
        load_5m: new Builder.NumberSchema({ sanitize: true, min: 0 }),
        load_15m: new Builder.NumberSchema({ sanitize: true, min: 0 }),
        iowait: new Builder.NumberSchema({ sanitize: true, min: 0, max: 1 }),
        steal: new Builder.NumberSchema({ sanitize: true, min: 0, max: 1 }),
      },
      default: { load_5m: 5, iowait: 0.5, steal: 0.5 },
      denyUndefined: true
    })
  },
  mandatory: ['warn', 'error'],
  denyUndefined: true
})
const schemaFix = new Builder.ObjectSchema({
  item: {
    server: clone(valServer),
    iowait: FixLinuxLoad.valIowait,
    steal: FixLinuxLoad.valSteal,
    procs: FixLinuxLoad.valProcs
  },
  mandatory: ['iowait', 'steal', 'procs'],
  denyUndefined: true
})

class TestLinuxLoad extends TestLinux {
  public async run() {
    const t = T(this.lang, 'linux')

    // check data
    await Validator.load(clone(schemaBase), this.lang, { data: this.data, source: `preset:${this.path}.data` })
      .then(val => this.data = val.get())
    await Validator.load(clone(schemaFix), this.lang, { data: this.fixdata, source: `preset:${this.path}.fix` })
      .then(val => this.fixdata = val.get())

    this.description = t('load.test.description', { connect: this.data.server?.host || 'localhost', ...this.data })

    // setup validation
    const valResult = new Validator(new Builder.ArraySchema({
      title: t('core:test.validateResult'),
      split: /[\s\n]+/,
      item: {
        0: new Builder.NumberSchema({ title: 'load 1m' }),
        1: new Builder.NumberSchema({ title: 'load 5m' }),
        2: new Builder.NumberSchema({ title: 'load 15m' }),
        5: new Builder.NumberSchema({ title: 'cpus' }),
        6: new Builder.NumberSchema({ title: 'user' }),
        7: new Builder.NumberSchema({ title: 'system' }),
        8: new Builder.NumberSchema({ title: 'nice' }),
        9: new Builder.NumberSchema({ title: 'idle' }),
        10: new Builder.NumberSchema({ title: 'iowait' }),
        11: new Builder.NumberSchema({ title: 'hwirq' }),
        12: new Builder.NumberSchema({ title: 'swirq' }),
        13: new Builder.NumberSchema({ title: 'steal' })
      },
      min: 14
    }))
    const valWarn = new Validator(new Builder.ObjectSchema({
      title: t('core:test.validateWarn'),
      item: {
        ...(this.data.warn.load_1m && {
          load_1m: new Builder.NumberSchema({
            title: t('load.test.load', { count: 1, less: this.data.warn.load_1m }),
            less: this.data.warn.load_1m
          })
        }),
        ...(this.data.warn.load_5m && {
          load_5m: new Builder.NumberSchema({
            title: t('load.test.load', { count: 5, less: this.data.warn.load_5m }),
            less: this.data.warn.load_5m
          })
        }),
        ...(this.data.warn.load_15m && {
          load_15m: new Builder.NumberSchema({
            title: t('load.test.load', { count: 15, less: this.data.warn.load_15m }),
            less: this.data.warn.load_15m
          })
        }),
        ...(this.data.warn.iowait && {
          iowait: new Builder.NumberSchema({
            title: t('load.test.iowait', { less: this.data.warn.iowait }),
            less: this.data.warn.iowait
          })
        }),
        ...(this.data.warn.steal && {
          steal: new Builder.NumberSchema({
            title: t('load.test.steal', { less: this.data.warn.steal }),
            less: this.data.warn.steal
          })
        })
      }
    }))
    const valError = new Validator(new Builder.ObjectSchema({
      title: t('core:test.validateError'),
      item: {
        ...(this.data.error.load_1m && {
          load_1m: new Builder.NumberSchema({
            title: t('load.test.load', { count: 1, less: this.data.error.load_1m }),
            less: this.data.error.load_1m
          })
        }),
        ...(this.data.error.load_5m && {
          load_5m: new Builder.NumberSchema({
            title: t('load.test.load', { count: 5, less: this.data.error.load_5m }),
            less: this.data.error.load_5m
          })
        }),
        ...(this.data.error.load_15m && {
          load_15m: new Builder.NumberSchema({
            title: t('load.test.load', { count: 15, less: this.data.error.load_15m }),
            less: this.data.error.load_15m
          })
        }),
        ...(this.data.error.iowait && {
          iowait: new Builder.NumberSchema({
            title: t('load.test.iowait', { less: this.data.error.iowait }),
            less: this.data.error.iowait
          })
        }),
        ...(this.data.error.steal && {
          steal: new Builder.NumberSchema({
            title: t('load.test.steal', { less: this.data.error.steal }),
            less: this.data.error.steal
          })
        })
      }
    }))

    // get data
    const loadavg = 'cat /proc/loadavg'
    const cpunum = 'grep -c processor /proc/cpuinfo'
    const stat = 'LANG=C top -bn2 | grep Cpu | tail -n 1 | sed \'s/[^0-9. ]//g\''
    return this.shell(async () => {
      return this.execute(`${loadavg} && ${cpunum} && ${stat}`)
        .then(res => {
          this.logValidation(valResult.describe())
          return valResult.load(this.lang, { data: res.stdout })
        })
    })
      // data collection
      .then(val => {
        this.result = {
          load_1m_total: val[0],
          load_5m_total: val[1],
          load_15m_total: val[2],
          cpus: val[5],
          user: val[6] / 100,
          system: val[7] / 100,
          nice: val[8] / 100,
          idle: val[9] / 100,
          iowait: val[10] / 100,
          hw_irq: val[11] / 100,
          sw_irq: val[12] / 100,
          steal: val[13] / 100,
        }
        this.result['load_1m'] = this.result.load_1m_total / this.result.cpus
        this.result['load_5m'] = this.result.load_5m_total / this.result.cpus
        this.result['load_15m'] = this.result.load_15m_total / this.result.cpus
        this.logResult(this.result)
        this.status = 'OK'
        return val
      })

      // check error and warning
      .then(() => {
        const ds = { data: this.result, source: 'preset:result' }
        return Promise.resolve().then(() => {
          if (!this.data.error) return
          this.logValidation(valError.describe())
          return valError.load(this.lang, ds)
        })
          .then(_val => {
            if (!this.data.warn) return
            this.logValidation(valWarn.describe())
            return valWarn.load(this.lang, ds)
              .catch(e => {
                this.status = 'WARN'
                this.error = e
                this.fixes.push(new FixLinuxLoad(this))
              })
          })
          .catch(e => {
            this.status = 'ERROR'
            this.error = e
            this.fixes.push(new FixLinuxLoad(this))
          })
      })
  }
}

export default TestLinuxLoad
export { TestLinuxLoad as Test, schemaBase as data, schemaFix as fix } // for interactive mode
