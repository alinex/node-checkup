import Validator from '@alinex/validator';
import * as Builder from '@alinex/validator/lib/schema';
import { clone } from '@alinex/data'
import { DateTime } from "luxon";

import { T } from '../../i18n';
import { TestLinux, valServer } from '.'
import FixLinuxOS from '../../fix/linux/os'

// @ts-ignore
const EOL: { [key: string]: Date } = {
  // dist: revision: date
  'Debian GNU/Linux: 4': new Date('2010-02-15'),
  'Debian GNU/Linux: 5': new Date('2012-02-06'),
  'Debian GNU/Linux: 6': new Date('2016-02-29'),
  'Debian GNU/Linux: 7': new Date('2018-05-31'),
  'Debian GNU/Linux: 8': new Date('2020-06-30'),
  'Debian GNU/Linux: 9': new Date('2022-06-30'),
  'Debian GNU/Linux: 10': new Date('2024-06-30'),
  'Manjaro LINUX: 20.2.1': new Date('2022-01-01')
}

const schemaBase = new Builder.ObjectSchema({
  item: {
    server: clone(valServer),
    warn: new Builder.ObjectSchema({
      title: 'Warn Check',
      item: {
        eol: new Builder.NumberSchema({
          title: 'Time before end of life',
          unit: { from: 'd', to: 'd' }
        }),
        reboot: new Builder.BooleanSchema({
          title: 'Reboot should not be needed',
          value: true
        }),
        updates: new Builder.NumberSchema({
          title: 'Total updates',
          integer: true,
          min: 1
        }),
        security: new Builder.NumberSchema({
          title: 'Security updates',
          integer: true,
          min: 1
        }),
        removes: new Builder.NumberSchema({
          title: 'Removable packages',
          integer: true,
          min: 1
        })
      },
      denyUndefined: true,
      default: { reboot: true, security: 10, updates: 20 }
    }),
    error: new Builder.ObjectSchema({
      title: 'Error Check',
      item: {
        reboot: new Builder.BooleanSchema({
          title: 'Reboot should not be needed',
          value: true
        }),
        updates: new Builder.NumberSchema({
          title: 'Total updates',
          less: 1
        }),
        security: new Builder.NumberSchema({
          title: 'Security updates',
          less: 1
        }),
        removes: new Builder.NumberSchema({
          title: 'Removable packages',
          less: 1
        })
      },
      denyUndefined: true
    })
  },
  mandatory: ['warn'],
  denyUndefined: true
})

class TestLinuxOS extends TestLinux {
  public async run() {
    const t = T(this.lang, 'linux')

    // check data
    await Validator.load(clone(schemaBase), this.lang, { data: this.data, source: `preset:${this.path}.data` })
      .then(val => this.data = val.get())

    this.description = t('os.test.description', { connect: this.data.server?.host || 'localhost', ...this.data })

    // setup validation
    const valWarn = new Validator(new Builder.ObjectSchema({
      title: t('core:test.validateWarn'),
      item: {
        ...(this.data.warn?.eol && {
          eol_days: new Builder.NumberSchema({
            title: t('os.test.eol', { count: this.data.warn.eol }),
            min: this.data.warn?.eol
          })
        }),
        ...(this.data.warn?.reboot && {
          reboot: new Builder.BooleanSchema({
            title: t('os.test.reboot'),
            value: false
          })
        }),
        ...(this.data.warn?.updates && {
          updates: new Builder.NumberSchema({
            title: t('os.test.updates', { count: this.data.warn.updates }),
            less: this.data.warn.updates
          })
        }),
        ...(this.data.warn?.security && {
          security: new Builder.NumberSchema({
            title: t('os.test.security', { count: this.data.warn.security }),
            less: this.data.warn.security
          })
        }),
        ...(this.data.warn?.removes && {
          removes: new Builder.NumberSchema({
            title: t('os.test.removes', { count: this.data.warn.removes }),
            less: this.data.warn.removes
          })
        })
      }
    }))
    const valError = new Validator(new Builder.ObjectSchema({
      title: t('core:test.validateError'),
      item: {
        ...(this.data.error?.eol && {
          eol_days: new Builder.NumberSchema({
            title: t('os.test.eol', { count: this.data.error.eol }),
            min: this.data.error?.eol
          })
        }),
        ...(this.data.error?.reboot && {
          reboot: new Builder.BooleanSchema({
            title: t('os.test.reboot'),
            value: false
          })
        }),
        ...(this.data.error?.updates && {
          updates: new Builder.NumberSchema({
            title: t('os.test.updates', { count: this.data.error.updates }),
            less: this.data.error.updates
          })
        }),
        ...(this.data.error?.security && {
          security: new Builder.NumberSchema({
            title: t('os.test.security', { count: this.data.error.security }),
            less: this.data.error.security
          })
        }),
        ...(this.data.error?.removes && {
          removes: new Builder.NumberSchema({
            title: t('os.test.removes', { count: this.data.error.removes }),
            less: this.data.error.removes
          })
        })
      }
    }))

    // get data
    return this.shell(async () => {
      this.result = {}
      // os version
      await this.execute("uname | tr '[:upper:]' '[:lower:]' && uname -r && uname -m && ls /etc/arch-release /etc/gentoo-release /etc/redhat-release /etc/SuSE-release /etc/mandrake-release /etc/debian_version 2>/dev/null")
        .then(res => {
          const v = res.stdout.split(/\s+/)
          this.result.os = v.shift()
          this.result.kernel = v.shift()
          this.result.architecture = v.shift()
          this.result.dist_base = v.shift().replace(/^\/etc\/(.*?)[_-].*/, '$1')
        })
      await this.execute("egrep '^NAME=|^DISTRIB_ID=' /etc/os-release /etc/lsb-release 2>/dev/null | head -n 1 && grep '_CODENAME=' /etc/lsb-release /etc/os-release 2>/dev/null | head -n 1 && egrep '^DISTRIB_RELEASE=|^VERSION_ID=' /etc/lsb-release /etc/os-release 2>/dev/null | head -n 1")
        .then(res => {
          const v = res.stdout.split(/\n/)
          this.result.dist = v.shift().replace(/.*=/, '').replace(/"/g, '')
          this.result.code = v.shift().replace(/.*=/, '')
          this.result.revision = v.shift().replace(/.*=/, '').replace(/"/g, '')
        })
      this.result.eol = EOL[`${this.result.dist}: ${this.result.revision}`]
        || EOL[`${this.result.dist}: ${this.result.revision.replace(/\.\d+$/, '')}`]
        || EOL[`${this.result.dist}: ${this.result.revision.replace(/\.*/, '')}`]
        || undefined
      if (this.result.eol) {
        const diff = DateTime.fromJSDate(this.result.eol).diff(DateTime.local(), 'days')
        this.result.eol_days = Math.round(diff.as('days'))
      }
      // host
      await this.execute("grep -q '^flags.* hypervisor' /proc/cpuinfo")
        .then(res => {
          this.result.virtual = res.stdout ? true : false
        })
      await this.execute(this.result.dist_base === 'debian' ? "ls /var/run/reboot-required 2>/dev/null" : "[[ $(pacman -Q linux | cut -d ' ' -f 2) > $(uname -r) ]]")
        .then(res => {
          this.result.reboot = res.stdout ? true : false
        })
      // os specific
      if (this.result.dist_base === 'debian') {
        const cmds = await this.commands(['apt'], true)
        await this.execute(`${cmds.$apt} update`)
        await this.execute("cat /etc/debian_version")
          .then(res => {
            this.result.dist_base_rev = res.stdout.replace(/[./].*/, '')
          })
        await this.execute("apt-get upgrade -s 2>/dev/null | egrep ^Inst | wc -l && apt-get upgrade -s 2>/dev/null | egrep ^Inst | grep -i $(lsb_release -c | cut -f 2)-security | wc -l && apt-get --dry-run autoremove 2>/dev/null | egrep ^Remv | wc -l")
          .then(res => {
            const v = res.stdout.split(/\n/)
            this.result.updates = parseInt(v.shift())
            this.result.security = parseInt(v.shift())
            this.result.removes = parseInt(v.shift())
          })
      } else if (this.result.dist_base === 'arch') {
        const cmds = await this.commands(['pacman'], true)
        await this.execute(`${cmds.$pacman} -Sy`)
        await this.execute("checkupdates | wc -l && pacman -Qtd | wc -l")
          .then(res => {
            const v = res.stdout.split(/\n/)
            this.result.updates = parseInt(v.shift())
            this.result.removes = parseInt(v.shift())
          })
      }
    })
      .then(() => {
        this.logResult(this.result)
        this.status = 'OK'
      })

      // check error and warning
      .then(() => {
        const ds = { data: this.result, source: 'preset:result' }
        return Promise.resolve().then(() => {
          if (!this.data.error) return
          this.logValidation(valError.describe())
          return valError.load(this.lang, ds)
        })
          .then(_val => {
            if (!this.data.warn) return
            this.logValidation(valWarn.describe())
            return valWarn.load(this.lang, ds)
              .catch(e => {
                this.status = 'WARN'
                this.error = e
                this.fixes.push(new FixLinuxOS(this))
              })
          })
          .catch(e => {
            this.status = 'ERROR'
            this.error = e
            this.fixes.push(new FixLinuxOS(this))
          })
      })
  }
}

export default TestLinuxOS
export { TestLinuxOS as Test, schemaBase as data } // for interactive mode
