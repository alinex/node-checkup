import Validator from '@alinex/validator';
import * as Builder from '@alinex/validator/lib/schema';
import { clone } from '@alinex/data'
import { each } from '@alinex/async'

import { T } from '../../i18n';
import { TestLinux, valServer } from '.'
import FixLinuxProcess from '../../fix/linux/process'

const items = {
  cpu_user: new Builder.NumberSchema({
    title: 'Maximum percentage of CPU used by the task while executing at the user level (application).',
    sanitize: true,
    min: 0
  }),
  cpu_system: new Builder.NumberSchema({
    title: 'Maximum percentage of CPU used by the task while executing at the system level (kernel).',
    sanitize: true,
    min: 0
  }),
  cpu_guest: new Builder.NumberSchema({
    title: 'Maximum percentage of CPU spent by the task in virtual machine (running a virtual processor).',
    sanitize: true,
    min: 0
  }),
  cpu_wait: new Builder.NumberSchema({
    title: 'Maximum percentage of CPU spent by the task while waiting to run.',
    sanitize: true,
    min: 0
  }),
  cpu_total: new Builder.NumberSchema({
    title: 'Maximum total percentage of CPU time used by the task.',
    sanitize: true,
    min: 0
  }),
  disk_read: new Builder.NumberSchema({
    title: 'Maximum number of kilobytes the task has caused to be read from disk per second.',
    unit: { from: 'KiB', to: 'KiB' },
    min: 0
  }),
  disk_write: new Builder.NumberSchema({
    title: 'Maximum number of kilobytes the task has caused, or shall cause to be written to disk per second.',
    unit: { from: 'KiB', to: 'KiB' },
    min: 0
  }),
  mem_physical: new Builder.NumberSchema({
    title: 'Maximum Resident Set Size: The non - swapped physical memory used by the task in kilobytes.',
    unit: { from: 'KiB', to: 'KiB' },
    min: 0
  }),
  mem_virtual: new Builder.NumberSchema({
    title: 'Maximum Virtual Size: The virtual memory usage of entire task in kilobytes.',
    unit: { from: 'KiB', to: 'KiB' },
    min: 0
  }),
  mem_stack: new Builder.NumberSchema({
    title: 'Maximum amount of memory in kilobytes reserved for the task as stack, but not necessarily used.',
    unit: { from: 'KiB', to: 'KiB' },
    min: 0
  }),
  mem_swap: new Builder.NumberSchema({
    title: 'Maximum swap memory used by this tasks.',
    unit: { from: 'KiB', to: 'KiB' },
    min: 0
  }),
  threads: new Builder.NumberSchema({
    title: 'Maximum number of threads associated with process.',
    integer: true,
    min: 0
  }),
  files_num: new Builder.NumberSchema({
    title: 'Maximum number of file descriptors associated with process.',
    integer: true,
    min: 0
  }),
  files_free: new Builder.NumberSchema({
    title: 'Minimum number of free file descriptors for the process.',
    integer: true,
    min: 0
  })
}
const schemaBase = new Builder.ObjectSchema({
  item: {
    server: clone(valServer),
    name: new Builder.StringSchema({
      title: 'Name of process to check.',
      min: 1, trim: true
    }),
    warn: new Builder.ObjectSchema({
      title: 'Warn Check',
      item: items,
      denyUndefined: true,
      default: { cpu_guest: 0.3 }
    }),
    error: new Builder.ObjectSchema({
      title: 'Error Check',
      item: items,
      denyUndefined: true,
      default: { cpu_guest: 0.6 }
    })
  },
  mandatory: ['name', 'warn', 'error'],
  denyUndefined: true
})

class TestLinuxProcess extends TestLinux {
  public async run() {
    const t = T(this.lang, 'linux')

    // check data
    await Validator.load(clone(schemaBase), this.lang, { data: this.data, source: `preset:${this.path}.data` })
      .then(val => this.data = val.get())

    this.description = t('process.test.description', { connect: this.data.server?.host || 'localhost', ...this.data })

    // setup validation
    const valWarn = new Validator(new Builder.ObjectSchema({
      title: t('core:test.validateWarn'),
      item: {
        ...(this.data.warn?.cpu_user && {
          cpu_user: new Builder.NumberSchema({
            title: t('process.test.cpu_user', { count: this.data.warn.cpu_user }),
            max: this.data.warn.cpu_user
          })
        }),
        ...(this.data.warn?.cpu_system && {
          cpu_system: new Builder.NumberSchema({
            title: t('process.test.cpu_system', { count: this.data.warn.cpu_system }),
            max: this.data.warn.cpu_system
          })
        }),
        ...(this.data.warn?.cpu_guest && {
          cpu_guest: new Builder.NumberSchema({
            title: t('process.test.cpu_guest', { count: this.data.warn.cpu_guest }),
            max: this.data.warn.cpu_guest
          })
        }),
        ...(this.data.warn?.cpu_wait && {
          cpu_wait: new Builder.NumberSchema({
            title: t('process.test.cpu_wait', { count: this.data.warn.cpu_wait }),
            max: this.data.warn.cpu_wait
          })
        }),
        ...(this.data.warn?.cpu_total && {
          cpu_total: new Builder.NumberSchema({
            title: t('process.test.cpu_total', { count: this.data.warn.cpu_total }),
            max: this.data.warn.cpu_total
          })
        }),
        ...(this.data.warn?.disk_read && {
          disk_read: new Builder.NumberSchema({
            title: t('process.test.disk_read', { count: this.data.warn.disk_read }),
            max: this.data.warn.disk_read
          })
        }),
        ...(this.data.warn?.disk_write && {
          disk_write: new Builder.NumberSchema({
            title: t('process.test.disk_write', { count: this.data.warn.disk_write }),
            max: this.data.warn.disk_write
          })
        }),
        ...(this.data.warn?.mem && {
          mem: new Builder.NumberSchema({
            title: t('process.test.mem', { count: this.data.warn.mem }),
            max: this.data.warn.mem
          })
        }),
        ...(this.data.warn?.mem_physical && {
          mem_physical: new Builder.NumberSchema({
            title: t('process.test.mem_physical', { count: this.data.warn.mem_physical }),
            max: this.data.warn.mem_physical
          })
        }),
        ...(this.data.warn?.mem_virtual && {
          mem_virtual: new Builder.NumberSchema({
            title: t('process.test.mem_virtual', { count: this.data.warn.mem_virtual }),
            max: this.data.warn.mem_virtual
          })
        }),
        ...(this.data.warn?.mem_stack && {
          mem_stack: new Builder.NumberSchema({
            title: t('process.test.mem_stack', { count: this.data.warn.mem_stack }),
            max: this.data.warn.mem_stack
          })
        }),
        ...(this.data.warn?.mem_swap && {
          mem_swap: new Builder.NumberSchema({
            title: t('process.test.mem_swap', { count: this.data.warn.mem_swap }),
            max: this.data.warn.mem_swap
          })
        }),
        ...(this.data.warn?.threads && {
          threads: new Builder.NumberSchema({
            title: t('process.test.threads', { count: this.data.warn.threads }),
            max: this.data.warn.threads
          })
        }),
        ...(this.data.warn?.files_num && {
          files: new Builder.NumberSchema({
            title: t('process.test.files_num', { count: this.data.warn.files_num }),
            max: this.data.warn.files_num
          })
        }),
        ...(this.data.warn?.files_free && {
          files: new Builder.NumberSchema({
            title: t('process.test.files_free', { count: this.data.warn.files_free }),
            min: this.data.warn.files_free
          })
        })
      }
    }))
    const valError = new Validator(new Builder.ObjectSchema({
      title: t('core:test.validateError'),
      item: {
        ...(this.data.error?.cpu_user && {
          cpu_user: new Builder.NumberSchema({
            title: t('process.test.cpu_user', { count: this.data.error.cpu_user }),
            max: this.data.error.cpu_user
          })
        }),
        ...(this.data.error?.cpu_system && {
          cpu_system: new Builder.NumberSchema({
            title: t('process.test.cpu_system', { count: this.data.error.cpu_system }),
            max: this.data.error.cpu_system
          })
        }),
        ...(this.data.error?.cpu_guest && {
          cpu_guest: new Builder.NumberSchema({
            title: t('process.test.cpu_guest', { count: this.data.error.cpu_guest }),
            max: this.data.error.cpu_guest
          })
        }),
        ...(this.data.error?.cpu_wait && {
          cpu_wait: new Builder.NumberSchema({
            title: t('process.test.cpu_wait', { count: this.data.error.cpu_wait }),
            max: this.data.error.cpu_wait
          })
        }),
        ...(this.data.error?.cpu_total && {
          cpu_total: new Builder.NumberSchema({
            title: t('process.test.cpu_total', { count: this.data.error.cpu_total }),
            max: this.data.error.cpu_total
          })
        }),
        ...(this.data.error?.disk_read && {
          disk_read: new Builder.NumberSchema({
            title: t('process.test.disk_read', { count: this.data.error.disk_read }),
            max: this.data.error.disk_read
          })
        }),
        ...(this.data.error?.disk_write && {
          disk_write: new Builder.NumberSchema({
            title: t('process.test.disk_write', { count: this.data.error.disk_write }),
            max: this.data.error.disk_write
          })
        }),
        ...(this.data.error?.mem_physical && {
          mem_physical: new Builder.NumberSchema({
            title: t('process.test.mem_physical', { count: this.data.error.mem_physical }),
            max: this.data.error.mem_physical
          })
        }),
        ...(this.data.error?.mem_virtual && {
          mem_virtual: new Builder.NumberSchema({
            title: t('process.test.mem_virtual', { count: this.data.error.mem_virtual }),
            max: this.data.error.mem_virtual
          })
        }),
        ...(this.data.error?.mem_stack && {
          mem_stack: new Builder.NumberSchema({
            title: t('process.test.mem_stack', { count: this.data.error.mem_stack }),
            max: this.data.error.mem_stack
          })
        }),
        ...(this.data.error?.mem_swap && {
          mem_swap: new Builder.NumberSchema({
            title: t('process.test.mem_swap', { count: this.data.error.mem_swap }),
            max: this.data.error.mem_swap
          })
        }),
        ...(this.data.error?.threads && {
          threads: new Builder.NumberSchema({
            title: t('process.test.threads', { count: this.data.error.threads }),
            max: this.data.error.threads
          })
        }),
        ...(this.data.error?.files_num && {
          files: new Builder.NumberSchema({
            title: t('process.test.files_num', { count: this.data.error.files_num }),
            max: this.data.error.files_num
          })
        }),
        ...(this.data.error?.files_free && {
          files: new Builder.NumberSchema({
            title: t('process.test.files_free', { count: this.data.error.files_free }),
            min: this.data.error.files_free
          })
        })
      }
    }))

    // get data
    return this.shell(async () => {
      const cmds = await this.commands(['pidstat'])
      this.result = {
        uid: [],
        pid: []
      }

      // alternative if no pidstat available
      if (!cmds.pidstat) {
        await this.execute(`ps hf -opid,cmd -C '${this.data.name}' | awk '$2 !~ /^[|\\\\]/ { print $1 }'`)
          .then(res => {
            this.result.pid = res.stdout.split(/\s+/).map((e: string) => parseInt(e))
            return each(this.result.pid, async pid => {
              await this.execute(`ps -o uid,%cpu,rss,vsz,size,nlwp,cmd -p ${pid} | tail -n +2 | sed 's/^[[:space:]]*//'`)
                .then(res => {
                  const fields = res.stdout.split(/\s+/)
                  this.result.uid.push(parseInt(fields[0]))
                  this.result.cpu_total = (this.result.cpu_total || 0) + parseFloat(fields[1]) / 100.0
                  this.result.mem_physical = (this.result.mem_physical || 0) + parseInt(fields[2])
                  this.result.mem_virtual = (this.result.mem_virtual || 0) + parseInt(fields[3])
                  this.result.mem_stack = (this.result.mem_stack || 0) + parseInt(fields[4])
                  this.result.threads = (this.result.threads || 0) + parseInt(fields[5])
                  this.result.command = fields[6]
                })
              await this.execute(`ls -l /proc/${pid}/fd | wc -l`)
                .then(res => {
                  this.result.files_num = (this.result.files_num || 0) + parseInt(res.stdout)
                })
            })
          })
      } else {
        const pidstat = `LANG=C pidstat -C ${this.data.name}`
        await this.execute(`${pidstat} -l | tail -n +3`)
          .then(res => {
            let sum: any = {
              uid: [],
              pid: [],
              cpu_user: 0,
              cpu_system: 0,
              cpu_guest: 0,
              cpu_wait: 0,
              cpu_total: 0,
              cpus: []
            }
            const columns: any = {}
            res.stdout.split(/\n/g).forEach((l: any) => {
              const v = l.split(/\s+/g)
              if (!Object.keys(columns).length) {
                v.forEach((e: string, i: number) => columns[e.toLowerCase()] = i)
                return
              }
              if (!sum.uid.includes(parseInt(v[columns.uid]))) sum.uid.push(parseInt(v[columns.uid]))
              if (!sum.pid.includes(parseInt(v[columns.pid]))) sum.pid.push(parseInt(v[columns.pid]))
              sum.cpu_user += parseFloat(v[columns['%usr']])
              sum.cpu_system += parseFloat(v[columns['%system']])
              sum.cpu_guest += parseFloat(v[columns['%guest']])
              if (columns['%wait']) sum.cpu_wait += parseFloat(v[columns['%wait']])
              sum.cpu_total += parseFloat(v[columns['%cpu']])
              if (!sum.cpus.includes(parseInt(v[columns.cpu]))) sum.cpus.push(parseInt(v[columns.cpu]))
              if (!sum.command) sum.command = v.slice(columns.command).join(' ')
            })
            this.result = sum
          })
        await this.execute(`${pidstat} -d | tail -n +3`)
          .then(res => {
            let sum: any = {
              disk_read: 0,
              disk_write: 0
            }
            const columns: any = {}
            res.stdout.split(/\n/g).forEach((l: any) => {
              const v = l.split(/\s+/g)
              if (!Object.keys(columns).length) {
                v.forEach((e: string, i: number) => columns[e.toLowerCase()] = i)
                return
              }
              sum.disk_read += parseFloat(v[columns['kb_rd/s']])
              sum.disk_write += parseFloat(v[columns['kb_wr/s']])
            })
            this.result = { ...this.result, ...sum }
          })
        await this.execute(`${pidstat} -r | tail -n +3`)
          .then(res => {
            let sum: any = {
              mem_virtual: 0,
              mem_physical: 0
            }
            const columns: any = {}
            res.stdout.split(/\n/g).forEach((l: any) => {
              const v = l.split(/\s+/g)
              if (!Object.keys(columns).length) {
                v.forEach((e: string, i: number) => columns[e.toLowerCase()] = i)
                return
              }
              sum.mem_virtual += parseFloat(v[columns.vsz])
              sum.mem_physical += parseFloat(v[columns.rss])
            })
            this.result = { ...this.result, ...sum }
          })
        await this.execute(`${pidstat} -s | tail -n +3`)
          .then(res => {
            let sum: any = {
              mem_stack: 0
            }
            const columns: any = {}
            res.stdout.split(/\n/g).forEach((l: any) => {
              const v = l.split(/\s+/g)
              if (!Object.keys(columns).length) {
                v.forEach((e: string, i: number) => columns[e.toLowerCase()] = i)
                return
              }
              sum.mem_stack += parseFloat(v[columns.stksize])
            })
            this.result = { ...this.result, ...sum }
          })
        await this.execute(`${pidstat} -v | tail -n +3`)
          .then(res => {
            let sum: any = {
              threads: 0,
              files: 0
            }
            const columns: any = {}
            res.stdout.split(/\n/g).forEach((l: any) => {
              const v = l.split(/\s+/g)
              if (!Object.keys(columns).length) {
                v.forEach((e: string, i: number) => columns[e.toLowerCase()] = i)
                return
              }
              sum.threads += parseFloat(v[columns.threads])
              sum.files += parseFloat(v[columns['fd-nr']])
            })
            this.result = { ...this.result, ...sum }
          })
      }
      // more information
      await this.execute(`grep VmSwap ${this.result.pid.map((pid: number) => `/proc/${pid}/status`).join(' ')}`)
        .then(res => {
          let sum: any = {
            mem_swap: 0
          }
          res.stdout.split(/\n/g).forEach((l: any) => {
            const v = l.split(/\s+/g)
            sum.mem_swap += parseFloat(v[1])
          })
          this.result = { ...this.result, ...sum }
        })
      await each(this.result.pid, async pid => {
        return this.execute(`grep -i 'Max open files' /proc/${pid}/limits | sed 's/ \\+/ /g' | cut -d' ' -f4`)
          .then(res => {
            this.result.filesLimit = (this.result.filesLimit || 0) + parseInt(res.stdout)
          })
      })
      // sort
      if (this.result.cpus) this.result.cpus = this.result.cpus.sort()
      this.result.uid = this.result.uid.sort()
    })

      .then(() => {
        this.logResult(this.result)
        this.status = 'OK'
      })

      // check error and warning
      .then(() => {
        const ds = { data: this.result, source: 'preset:result' }
        return Promise.resolve().then(() => {
          if (!this.data.error) return
          this.logValidation(valError.describe())
          return valError.load(this.lang, ds)
        })
          .then(_val => {
            if (!this.data.warn) return
            this.logValidation(valWarn.describe())
            return valWarn.load(this.lang, ds)
              .catch(e => {
                this.status = 'WARN'
                this.error = e
                this.fixes.push(new FixLinuxProcess(this))
              })
          })
          .catch(e => {
            this.status = 'ERROR'
            this.error = e
            this.fixes.push(new FixLinuxProcess(this))
          })
      })
  }

}

export default TestLinuxProcess
export { TestLinuxProcess as Test, schemaBase as data } // for interactive mode
