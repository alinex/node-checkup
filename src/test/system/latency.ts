import Validator from '@alinex/validator';
import * as Builder from '@alinex/validator/lib/schema';
import { clone } from '@alinex/data'

import { TestSystem } from '.'
import { T } from '../../i18n';

async function lag() {
  return new Promise(resolve => {
    const start = process.hrtime();
    setImmediate(() => {
      const delta = process.hrtime(start);
      resolve(Math.round(delta[0] * 1e12 + delta[1] / 1e3) / 1000);
    });
  });
}

const item = {
  time: new Builder.NumberSchema({
    title: 'Max lag',
    unit: { from: 'ms', to: 'ms' },
    min: 0
  })
}
const schemaBase = new Builder.ObjectSchema({
  item: {
    warn: new Builder.ObjectSchema({
      title: 'Warn Check',
      item,
      mandatory: true,
      denyUndefined: true,
      default: { time: 100 }
    }),
    error: new Builder.ObjectSchema({
      title: 'Warn Check',
      item,
      mandatory: true,
      denyUndefined: true,
      default: { time: 1000 }
    })
  },
  mandatory: true,
  denyUndefined: true
})

class TestSystemLatency extends TestSystem {
  public async run() {
    const t = T(this.lang, 'system')

    // check data
    await Validator.load(clone(schemaBase), this.lang, { data: this.data, source: `preset:${this.path}.data` })
      .then(val => this.data = val.get())

    this.description = t('latency.test.description', this.data)

    // setup validation
    const valWarn = new Validator(new Builder.ObjectSchema({
      title: t('core:test.validateWarn'),
      item: {
        time: new Builder.NumberSchema({
          title: t('latency.test.time', { count: this.data.warn.time }),
          max: this.data.warn.time
        })
      }
    }))
    const valError = new Validator(new Builder.ObjectSchema({
      title: t('core:test.validateError'),
      item: {
        time: new Builder.NumberSchema({
          title: t('latency.test.time', { count: this.data.error.time }),
          max: this.data.error.time
        })
      }
    }))

    // get data
    return lag()
      // data collection
      .then(time => {
        this.result = { time }
        this.logResult(this.result)
        this.status = 'OK'
        return
      })

      // check error and warning
      .then(() => {
        const ds = { data: this.result, source: 'preset:result' }
        return Promise.resolve().then(() => {
          if (!this.data.error) return
          this.logValidation(valError.describe())
          return valError.load(this.lang, ds)
        })
          .then(_val => {
            if (!this.data.warn) return
            this.logValidation(valWarn.describe())
            return valWarn.load(this.lang, ds)
              .catch(e => {
                this.status = 'WARN'
                this.error = e
              })
          })
          .catch(e => {
            this.status = 'ERROR'
            this.error = e
          })
      })
  }
}

export default TestSystemLatency
export { TestSystemLatency as Test, schemaBase as data } // for interactive mode
