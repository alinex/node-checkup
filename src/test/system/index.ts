import { loadNamespace } from '../../i18n'
import Test from '..'

loadNamespace('system')

export class TestSystem extends Test {
}

export default TestSystem
