import Validator from '@alinex/validator';
import * as Builder from '@alinex/validator/lib/schema';
import { clone } from '@alinex/data'

import { TestSystem } from '.'
import { T } from '../../i18n';
import { cpus } from 'os'

const measureTime = 10000

const item = {
  cpu: new Builder.NumberSchema({
    title: 'Percentage of user CPU usage',
    sanitize: true,
    min: 0, max: 1
  }),
  rss: new Builder.NumberSchema({
    title: 'RAM usage',
    unit: { from: 'B', to: 'B' },
    min: 0
  })
}
const schemaBase = new Builder.ObjectSchema({
  item: {
    warn: new Builder.ObjectSchema({
      title: 'Warn Check',
      item,
      denyUndefined: true,
      default: { cpu: 0.5, rss: 512 * 1024 * 1024 }
    }),
    error: new Builder.ObjectSchema({
      title: 'Error Check',
      item,
      denyUndefined: true,
      default: { cpu: 0.8, rss: 1024 * 1024 * 1024 }
    })
  },
  mandatory: true,
  denyUndefined: true
})

class TestSystemProcess extends TestSystem {
  public async run() {
    const t = T(this.lang, 'system')

    // check data
    await Validator.load(clone(schemaBase), this.lang, { data: this.data, source: `preset:${this.path}.data` })
      .then(val => this.data = val.get())

    this.description = t('process.test.description', this.data)

    // setup validation
    const valWarn = new Validator(new Builder.ObjectSchema({
      title: t('core:test.validateWarn'),
      item: {
        ...(typeof this.data.warn?.cpu !== 'undefined' && {
          cpu: new Builder.NumberSchema({
            title: t('process.test.cpu', { count: this.data.warn.cpu }),
            max: this.data.warn.cpu
          })
        }),
        ...(typeof this.data.warn?.rss !== 'undefined' && {
          rss: new Builder.NumberSchema({
            title: t('process.test.rss', { count: this.data.warn.rss }),
            max: this.data.warn.rss
          })
        })
      }
    }))
    const valError = new Validator(new Builder.ObjectSchema({
      title: t('core:test.validateError'),
      item: {
        ...(typeof this.data.error?.cpu !== 'undefined' && {
          cpu: new Builder.NumberSchema({
            title: t('process.test.cpu', { count: this.data.error.cpu }),
            max: this.data.error.cpu
          })
        }),
        ...(typeof this.data.error?.rss !== 'undefined' && {
          rss: new Builder.NumberSchema({
            title: t('process.test.rss', { count: this.data.error.rss }),
            max: this.data.error.rss
          })
        })
      }
    }))

    // get data
    return new Promise(resolve => {
      this.logRequest('process.resourceUsage()')
      const start = process.resourceUsage()
      setTimeout(() => {
        const end = process.resourceUsage()
        this.logRequest('process.resourceUsage()')
        this.logReturn(end)
        const numCpus = cpus().length
        resolve({
          userCPUTime: (end.userCPUTime - start.userCPUTime) / measureTime * 1000,
          systemCPUTime: (end.systemCPUTime - start.systemCPUTime) / measureTime * 1000,
          cpuPercent: (end.userCPUTime + end.systemCPUTime - start.userCPUTime - start.systemCPUTime) / measureTime / numCpus,
          maxRSS: end.maxRSS * 1024,
          fsRead: (end.fsRead - start.fsRead) / measureTime * 1000,
          fsWrite: (end.fsWrite - start.fsWrite) / measureTime * 1000
        });
      }, measureTime);
      this.logReturn(start)
    })
      // data collection
      .then(data => {
        this.result = data
        this.logResult(this.result)
        this.status = 'OK'
        return
      })

      // check error and warning
      .then(() => {
        const ds = { data: this.result, source: 'preset:result' }
        return Promise.resolve().then(() => {
          if (!this.data.error) return
          this.logValidation(valError.describe())
          return valError.load(this.lang, ds)
        })
          .then(_val => {
            if (!this.data.warn) return
            this.logValidation(valWarn.describe())
            return valWarn.load(this.lang, ds)
              .catch(e => {
                this.status = 'WARN'
                this.error = e
              })
          })
          .catch(e => {
            this.status = 'ERROR'
            this.error = e
          })
      })
  }
}

export default TestSystemProcess
export { TestSystemProcess as Test, schemaBase as data } // for interactive mode
