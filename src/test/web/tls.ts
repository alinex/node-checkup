import Validator from '@alinex/validator';
import * as Builder from '@alinex/validator/lib/schema';
import * as https from "https";
import * as http from "http";
import * as tls from 'tls';
import { inspect } from 'util'
import { clone } from '@alinex/data'

import { T } from '../../i18n';
import { TestWeb } from '.'
//import FixNetDns from '../../fix/net/dns'
import FixWebHttp from '../../fix/web/http'
//import FixNetTcp from '../../fix/net/tcp'

const item = {
  time: new Builder.NumberSchema({
    title: 'time the certificate has to be valid',
    unit: { from: 's', to: 's' },
    greater: 0
  })
}
const schemaBase = new Builder.ObjectSchema({
  item: {
    domain: new Builder.DomainSchema(),
    port: new Builder.PortSchema({ default: 443 }),
    warn: new Builder.ObjectSchema({
      title: 'Warn Check',
      item,
      denyUndefined: true,
      default: { time: 604800 } // one week
    }),
    error: new Builder.ObjectSchema({
      title: 'Error Check',
      item,
      denyUndefined: true,
      default: { time: 86400 } // one day
    })
  },
  mandatory: ['domain', 'port'],
  denyUndefined: true
})

class TestWebTLS extends TestWeb {
  public async run() {
    const t = T(this.lang, 'web')

    // check data
    await Validator.load(clone(schemaBase), this.lang, { data: this.data, source: `preset:${this.path}.data` })
      .then(val => this.data = val.get())

    this.description = t('tls.test.description', this.data)

    // setup validation
    const valWarn = new Validator(new Builder.ObjectSchema({
      title: t('core:test.validateWarn'),
      item: {
        ...(typeof this.data.warn?.code !== 'undefined' && {
          remaining: new Builder.NumberSchema({
            title: t('tls.test.time', { value: this.data.warn.time }),
            min: this.data.warn.time
          })
        })
      }
    }))
    const valError = new Validator(new Builder.ObjectSchema({
      title: t('core:test.validateError'),
      item: {
        valid: new Builder.BooleanSchema({
          title: t('tls.test.valid'),
          value: true
        }),
        ...(typeof this.data.error?.code !== 'undefined' && {
          remaining: new Builder.NumberSchema({
            title: t('http.test.time', { value: this.data.error.time }),
            max: this.data.error.time
          })
        })
      }
    }))

    // get data
    return new Promise((resolve, reject) => {
      try {
        this.logRequest(`HEAD https://${this.data.domain}:${this.data.port}`)
        this.logData({ method: "HEAD" })
        const req = https.request({
          host: this.data.domain,
          port: this.data.port,
          agent: false,
          method: "HEAD",
          rejectUnauthorized: false,
        },
          (res: http.IncomingMessage) => {
            let cert = (res.connection as tls.TLSSocket).getPeerCertificate(true)
            this.logResponse(inspect(cert, { depth: 10 }))
            let chain: any = new Set()
            do {
              chain.add(cert);
              cert = cert.issuerCertificate;
            } while (cert && typeof cert === "object" && !chain.has(cert));
            chain = Array.from(chain).map((e: any) => {
              return {
                subject: { ...e.subject },
                from: new Date(e.valid_from),
                to: new Date(e.valid_to),
                ...(e.subjectaltname && {
                  for: e.subjectaltname.replace(/DNS:|IP Address:/g, "").split(", ")
                }),
                remaining: (+new Date(e.valid_to) - +new Date(e.valid_from)) / 1000,
              }
            })
            const result = chain.shift()
            result.valid = ((res.socket as { authorized?: boolean }).authorized as boolean) || false,
              result.chain = chain
            resolve(result);
          }
        );
        req.on("error", reject);
        req.on("timeout", () => {
          req.abort()
          reject(new Error('Timed Out'))
        });
        req.end();
      } catch (e) {
        reject(e);
      }
    })
      // validate result
      .then(res => {
        this.result = res
        this.logResult(this.result)
        this.status = 'OK'
      })

      // check error and warning
      .then(() => {
        const ds = { data: this.result, source: 'preset:result' }
        return Promise.resolve().then(() => {
          if (!this.data.error) return
          this.logValidation(valError.describe())
          return valError.load(this.lang, ds)
        })
          .then(_val => {
            if (!this.data.warn) return
            this.logValidation(valWarn.describe())
            return valWarn.load(this.lang, ds)
              .catch(e => {
                this.status = 'WARN'
                this.error = e
                //                this.fixes.push(new FixWebHttp(this))
              })
          })
          .catch(e => {
            this.status = 'ERROR'
            this.error = e
            this.fixes.push(new FixWebHttp(this))
          })
      })

  }
}

export default TestWebTLS
export { TestWebTLS as Test, schemaBase as data } // for interactive mode
