import { DataStore, Options } from "@alinex/datastore";

import { loadNamespace } from '../../i18n'
import Test from '..'

loadNamespace('web')

export class TestWeb extends Test {

  protected async load(url: string, options: Options = {}): Promise<DataStore> {
    const ds = new DataStore()
    this.logRequest(url)
    if (Object.keys(options).length) this.logData(options)
    return ds.load({
      source: url,
      options: { format: 'text', ...options }
    }).then(() => {
      this.logStatus(`${ds.meta.status} ${ds.meta.statusText}`)
      this.logResponse(Object.keys(ds.meta.headers).map(k => `${k}: ${ds.meta.headers[k]}`).join('\n'))
      this.logReturn(options.format ? ds.get() : ds.get().trim())
      return ds
    })
  }

}

export default TestWeb
