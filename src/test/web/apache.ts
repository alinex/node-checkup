import Validator from '@alinex/validator';
import * as Builder from '@alinex/validator/lib/schema';
import { clone } from '@alinex/data'

import { T } from '../../i18n';
import { TestWeb } from '.'
import FixNetDns from '../../fix/net/dns'
import FixWebHttp from '../../fix/web/http'
import FixNetTcp from '../../fix/net/tcp'

const CACHE_TTL = 600 // maximum time to cache result for diff calculation

const item = {
  time: new Builder.NumberSchema({
    title: 'time to load page',
    unit: { from: 'ms', to: 'ms' },
    greater: 0
  }),
  code: new Builder.ArraySchema({
    title: 'Allow return code',
    makeArray: true,
    item: { '*': new Builder.NumberSchema({ integer: true, min: 100, max: 599 }) },
    min: 1
  }),
}
const schemaBase = new Builder.ObjectSchema({
  item: {
    url: new Builder.URLSchema(),
    warn: new Builder.ObjectSchema({
      title: 'Warn Check',
      item,
      denyUndefined: true,
      default: { time: 5000 }
    }),
    error: new Builder.ObjectSchema({
      title: 'Error Check',
      item,
      denyUndefined: true,
      default: { time: 30000, code: [200] }
    })
  },
  mandatory: true,
  denyUndefined: true
})

class TestWebApache extends TestWeb {
  public async run() {
    const t = T(this.lang, 'web')

    // check data
    await Validator.load(clone(schemaBase), this.lang, { data: this.data, source: `preset:${this.path}.data` })
      .then(val => this.data = val.get())

    this.description = t('apache.test.description', this.data)


    // setup validation
    const valWarn = new Validator(new Builder.ObjectSchema({
      title: t('core:test.validateWarn'),
      item: {
        ...(typeof this.data.warn?.time !== 'undefined' && {
          time: new Builder.NumberSchema({
            title: t('http.test.time', { value: this.data.warn.time }),
            max: this.data.warn.time
          })
        }),
        ...(typeof this.data.warn?.code !== 'undefined' && {
          code: new Builder.NumberSchema({
            title: t('http.test.code', { value: this.data.warn.code.join(', ') }),
            allow: this.data.warn.code
          })
        }),
        ...((typeof this.data.warn?.allow !== 'undefined' || typeof this.data.warn?.disallow !== 'undefined') && {
          body: new Builder.StringSchema({
            title: t('http.test.body', { allow: this.data.warn.allow?.join(', ') || '*', disallow: this.data.warn.allow?.join(', ') || '[]' }),
            ...(typeof this.data.warn?.allow !== 'undefined' && { allow: this.data.warn.allow }),
            ...(typeof this.data.warn?.disallow !== 'undefined' && { disallow: this.data.warn.disallow }),
          })
        })
      }
    }))
    const valError = new Validator(new Builder.ObjectSchema({
      title: t('core:test.validateError'),
      item: {
        ...(typeof this.data.error?.time !== 'undefined' && {
          time: new Builder.NumberSchema({
            title: t('http.test.time', { value: this.data.error.time }),
            max: this.data.error.time
          })
        }),
        ...(typeof this.data.error?.code !== 'undefined' && {
          code: new Builder.NumberSchema({
            title: t('http.test.code', { value: this.data.error.code.join(', ') }),
            allow: this.data.error.code
          })
        }),
        ...((typeof this.data.error?.allow !== 'undefined' || typeof this.data.error?.disallow !== 'undefined') && {
          body: new Builder.StringSchema({
            title: t('http.test.body', { allow: this.data.error.allow?.join(', ') || '*', disallow: this.data.error.allow?.join(', ') || '[]' }),
            ...(typeof this.data.error?.allow !== 'undefined' && { allow: this.data.error.allow }),
            ...(typeof this.data.error?.disallow !== 'undefined' && { disallow: this.data.error.disallow }),
          })
        })
      }
    }))

    // get data
    return this.load(this.data.url + '?auto', { ignoreError: true })
      // validate result
      .then(ds => {
        const data: any = {}
        ds.get().split(/\n/).forEach((l: string) => {
          const parts = l.split(/:\s+/, 2)
          data[parts[0]] = parts[1]
        })
        console.log(data)
        this.result = {
          time: ds.meta.time,
          code: ds.meta.status,
          code_text: ds.meta.statusText || t(`http.fix.${ds.meta.status}`)?.replace(new RegExp(`.*?${ds.meta.status}:\\s+`), '').replace(/\s+-.*/, ''),
          version: data.ServerVersion,
          MPM: data.ServerMPM,
          uptime: parseInt(data.Uptime),
          load_1mTotal: parseFloat(data.Load1),
          load_5mTotal: parseFloat(data.Load5),
          load_15mTotal: parseFloat(data.Load15),
          requests: parseInt(data['Total Accesses']),
          transfer: parseInt(data['Total kBytes']),
          workers_busy: parseInt(data.BusyWorkers),
          workers_idle: parseInt(data.IdleWorkers),
          conn: parseInt(data.ConnsTotal),
          conn_writing: parseInt(data.ConnsAsyncWriting),
          conn_keepalive: parseInt(data.ConnsAsyncKeepAlive),
          conn_losing: parseInt(data.ConnsAsyncClosing),
          cache_usage: parseFloat(data.CacheUsage) / 100,
          cache_index_usage: parseFloat(data.CacheIndexUsage) / 100,
        }
        // add time diffs
        const old: any = this.cache.get(`!${this.path}`)
        this.cache.set(`!${this.path}`, this.result, CACHE_TTL)
        if (old && this.result.uptime > old.uptime) { // calculate
          const timediff = this.result.uptime - old.uptime
          this.result.requests_diff = (this.result.requests - old.requests) / timediff
          this.result.transfer_diff = (this.result.transfer - old.transfer) / timediff
        }
        this.logResult(this.result)
        this.status = 'OK'
        return ds
      })

      // check error and warning
      .then(() => {
        const ds = { data: this.result, source: 'preset:result' }
        return Promise.resolve().then(() => {
          if (!this.data.error) return
          this.logValidation(valError.describe())
          return valError.load(this.lang, ds)
        })
          .then(_val => {
            if (!this.data.warn) return
            this.logValidation(valWarn.describe())
            return valWarn.load(this.lang, ds)
              .catch(e => {
                this.status = 'WARN'
                this.error = e
                this.fixes.push(new FixWebHttp(this))
              })
          })
          .catch(e => {
            this.status = 'ERROR'
            this.error = e
            this.fixes.push(new FixWebHttp(this))
          })
      })

      // general error
      .catch((e: any) => {
        if (e) {
          this.status = 'NODATA'
          this.error = e
          const parsed = new URL(this.data.url)
          if (e.code === 'ENOTFOUND' && e.syscall === 'getaddrinfo')
            this.fixes.push(new FixNetDns(this, { domain: parsed.hostname }))
          else if (e.code === 'ETIMEDOUT')
            this.fixes.push(new FixNetTcp(this, { host: parsed.hostname, port: parsed.port || 80 }))
        }
      })
  }
}

export default TestWebApache
export { TestWebApache as Test, schemaBase as data } // for interactive mode
