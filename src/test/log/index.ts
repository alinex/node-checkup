import { DataStore } from "@alinex/datastore";

import { loadNamespace } from '../../i18n'
import { validator, config } from '../../shell'
import Test from '..'

loadNamespace('log')

const CACHE_TTL = 30

export { validator as valServer }

export class TestLog extends Test {

  public ds: DataStore | undefined

  protected async load(path: string, cmd: string, options: any = {}): Promise<DataStore> {
    const server = await config(this.data.server)
    this.logRequest(`cd ${path || '/'}; ${cmd}`)
    let uri = path ? `shell://${server.connect}${path}#${cmd}` : `shell://${server.connect}/#${cmd}`
    this.ds = <DataStore>this.cache.get(`!log:${uri}`)
    if (this.ds) { // found in cache
      if (typeof this.ds.meta.exitCode !== 'undefined') this.logResponse(`code: ${this.ds.meta.exitCode} (cached)`)
      this.logReturn(this.ds.get())
      return this.ds
    }
    this.ds = new DataStore()
    return this.ds.load({
      source: uri,
      options: { format: 'text', privateKey: server.privateKey, passphrase: server.passphrase, ...options }
    }).then(() => {
      if (typeof this.ds!.meta.exitCode !== 'undefined') this.logResponse(`code: ${this.ds!.meta.exitCode}`)
      this.logReturn(this.ds!.get())
      this.cache.set(`!log:${uri}`, this.ds!, CACHE_TTL)
      return this.ds!
    })
  }

}

export default TestLog
