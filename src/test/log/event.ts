import Validator from '@alinex/validator';
import * as Builder from '@alinex/validator/lib/schema';
import { clone } from '@alinex/data'
import { DateTime } from "luxon";

import { T } from '../../i18n';
import { TestLog, valServer } from '.'
import FixLogView from '../../fix/log/view'

const schemaBase = new Builder.ObjectSchema({
  item: {
    server: clone(valServer),
    path: new Builder.FileSchema(),
    command: new Builder.StringSchema(),
    pattern: new Builder.RegExpSchema(),
    range: new Builder.NumberSchema({ unit: { from: 's', to: 's' } }),
    extract: new Builder.ObjectSchema({
      item: { '*': new Builder.RegExpSchema({ min: 2, max: 3 }) }
    }),
    include: new Builder.ObjectSchema({
      item: { '*': new Builder.RegExpSchema() }
    }),
    exclude: new Builder.ObjectSchema({
      item: { '*': new Builder.RegExpSchema() }
    }),
    warn: new Builder.ObjectSchema({
      title: 'Warn Check',
      item: {
        min: new Builder.NumberSchema({ min: 0 }),
        max: new Builder.NumberSchema({ min: 0 })
      },
      denyUndefined: true
    }),
    error: new Builder.ObjectSchema({
      title: 'Error Check',
      item: {
        min: new Builder.NumberSchema({ min: 0 }),
        max: new Builder.NumberSchema({ min: 0 })
      },
      denyUndefined: true
    })
  },
  mandatory: ['warn', 'error', 'command'],
  denyUndefined: true
})
const schemaFix = new Builder.ObjectSchema({
  item: {
    before: FixLogView.valNum,
    after: FixLogView.valNum
  },
  denyUndefined: true
})

class TestLogEvent extends TestLog {

  // general excludes for store
  public store = { exclude: ['pos', 'matches'] }

  public async run() {
    const t = T(this.lang, 'log')

    // check data
    await Validator.load(clone(schemaBase), this.lang, { data: this.data, source: `preset:${this.path}.data` })
      .then(val => this.data = val.get())
    await Validator.load(clone(schemaFix), this.lang, { data: this.fixdata, source: `preset:${this.path}.fix` })
      .then(val => this.fixdata = val.get())

    this.description = t('event.test.description', { connect: this.data.server?.host || 'localhost' })

    // setup validation
    const valData = new Validator(new Builder.ArraySchema({
      item: {
        '*': new Builder.ObjectSchema({
          item: {
            date: new Builder.DateTimeSchema({ parse: true }),
            message: new Builder.StringSchema({ replace: [{ match: /#033\[\d\d?m/g, replace: '', hint: 'remove ansi escapes' }] }),
            pid: new Builder.NumberSchema()
          },
          mandatory: ['date', 'message']
        })
      }
    })
    )
    const valWarn = new Validator(new Builder.ObjectSchema({
      title: t('core:test.validateWarn'),
      item: {
        ...(typeof this.data.warn.min != 'undefined' && {
          matched: new Builder.NumberSchema({
            title: t('event.test.min', { count: 1, min: this.data.warn.min }),
            integer: true,
            min: this.data.warn.min
          })
        }),
        ...(typeof this.data.warn.max != 'undefined' && {
          matched: new Builder.NumberSchema({
            title: t('event.test.max', { count: 1, max: this.data.warn.max }),
            integer: true,
            min: 0,
            max: this.data.warn.max
          })
        })
      }
    }))
    const valError = new Validator(new Builder.ObjectSchema({
      title: t('core:test.validateError'),
      item: {
        ...(typeof this.data.error.min != 'undefined' && {
          min: new Builder.NumberSchema({
            title: t('event.test.min', { count: 1, min: this.data.error.min }),
            integer: true,
            min: this.data.error.min
          })
        }),
        ...(typeof this.data.error.max != 'undefined' && {
          max: new Builder.NumberSchema({
            title: t('event.test.max', { count: 1, max: this.data.error.max }),
            integer: true,
            min: 0,
            max: this.data.error.max
          })
        })
      }
    }))

    // get data
    return this.load(this.data.path, this.data.command, { pattern: this.data.pattern })
      .then(ds => valData.validate(this.lang, ds))
      .then(_ => valData.get())
      // use specific range
      .then(data => {
        if (this.data.range) {
          const start = DateTime.local().minus({ seconds: this.data.range }).toJSDate()
          return data.filter((e: any) => e.date >= start)
        }
        return data
      })
      // extract variable parts
      .then(data => {
        if (this.data.extract) {
          const keys = Object.keys(this.data.extract)
          data.forEach((e: any) => {
            e.original = e.message
            e.variables = {}
            keys.forEach(key => {
              const group = this.data.extract[key].flags.match(/g/)
              let v: any = []
              e.message = e.message.replace(this.data.extract[key], (m: string, start: string, data: string, ...end: string[]) => {
                if (group) v.push(data)
                else v = data
                return [start, '…', end.length > 2 ? end[0] : ''].join('')
              })
              if (v.length) e.variables[key] = v
            })
            if (!Object.keys(e.variables).length) delete e.variables
          })
        }
        return data
      })
      .then(data => {
        let filter: number[] = data.map((_: any, i: number) => i)
        // filter
        if (this.data.include) {
          Object.keys(this.data.include).forEach(key => {
            const c = key === 'original' && !this.data.extract ? 'message' : key
            filter = filter.filter(e => data[e][c]?.match(this.data.include[key]))
          })
        }
        if (this.data.exclude) {
          Object.keys(this.data.exclude).forEach(key => {
            const c = key === 'original' && !this.data.extract ? 'message' : key
            filter = filter.filter(e => !data[e][key] || !data[e][c].match(this.data.include[key]))
          })
        }

        this.result = {
          num: data.length,
          pos: filter,
          matched: filter.length,
          matches: filter.map(e => {
            const copy = Object.assign({}, data[e])
            delete copy.original
            return copy
          })
        }
        this.logResult(this.result)
        this.status = 'OK'
        return data
      })

      // check error and warning
      .then(() => {
        const ds = { data: this.result, source: 'preset:result' }
        return Promise.resolve().then(() => {
          if (!this.data.error) return
          this.logValidation(valError.describe())
          return valError.load(this.lang, ds)
        })
          .then(_val => {
            if (!this.data.warn) return
            this.logValidation(valWarn.describe())
            return valWarn.load(this.lang, ds)
              .catch(e => {
                this.status = 'WARN'
                this.error = e
              })
          })
          .catch(e => {
            this.status = 'ERROR'
            this.error = e
          })
      })
  }
}

export default TestLogEvent
export { TestLogEvent as Test, schemaBase as data } // for interactive mode
