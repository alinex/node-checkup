import Validator from '@alinex/validator';
import * as Builder from '@alinex/validator/lib/schema';
import { Resolver, lookup } from 'dns';
import { clone } from '@alinex/data'

import { TestNet } from '.'
import { T } from '../../i18n';
import FixNetDNS from '../../fix/net/dns'

const item = {
  time: new Builder.NumberSchema({
    title: 'Time to connect to TCP port',
    unit: { from: 'ms', to: 'ms' },
    min: 0
  }),
  min: new Builder.NumberSchema({
    title: 'Minimum number of entries',
    min: 1
  }),
  max: new Builder.NumberSchema({
    title: 'Maximum number of entries',
    min: 1
  }),
}
const schemaBase = new Builder.ObjectSchema({
  item: {
    domain: new Builder.DomainSchema({
      title: 'Hostname to resolv'
    }),
    nameserver: new Builder.ArraySchema({
      title: 'Nameserver to query',
      item: {
        '*': new Builder.IPSchema({ lookup: true })
      }
    }),
    type: new Builder.StringSchema({
      title: 'Type of records to list',
      allow: ['A', 'AAAA', 'CNAME', 'MX', 'NAPTR', 'NS', 'PTR', 'SOA', 'SRV', 'TXT']
    }),
    warn: new Builder.ObjectSchema({
      title: 'Warn Check',
      item,
      denyUndefined: true,
      default: { time: 500 }
    }),
    error: new Builder.ObjectSchema({
      title: 'Error Check',
      item,
      denyUndefined: true,
      default: { min: 1, time: 5000 }
    })
  },
  mandatory: ['domain', 'warn', 'error'],
  denyUndefined: true
})

class TestNetDNS extends TestNet {
  public async run() {
    const t = T(this.lang, 'net')

    // check data
    await Validator.load(clone(schemaBase), this.lang, { data: this.data, source: `preset:${this.path}.data` })
      .then(val => this.data = val.get())

    this.description = t('dns.test.description', {
      domain: this.data.domain,
      nameserver: this.data.nameserver ? this.data.nameserver.join(', ') + ' nameserver' : 'local lookup',
      type: this.data.type || 'ANY'
    })

    // setup validation
    const valWarn = new Validator(new Builder.ObjectSchema({
      title: t('core:test.validateWarn'),
      item: {
        ...((typeof this.data.warn?.min !== 'undefined' || typeof this.data.warn?.max !== 'undefined') && {
          list: new Builder.ArraySchema({
            title: this.data.warn?.min ? t('dns.test.min', { count: this.data.warn.min }) : ''
              + this.data.warn?.max ? t('dns.test.max', { count: this.data.warn.max }) : '',
            ...(typeof this.data.warn?.min !== 'undefined' && {
              min: this.data.warn.min
            }),
            ...(typeof this.data.warn?.max !== 'undefined' && {
              max: this.data.warn.max
            })
          })
        }),
        ...(typeof this.data.warn?.time !== 'undefined' && {
          time: new Builder.NumberSchema({
            title: t('dns.test.time', { count: this.data.warn.time }),
            max: this.data.warn.time
          })
        }),
      }
    }))
    const valError = new Validator(new Builder.ObjectSchema({
      title: t('core:test.validateError'),
      item: {
        ...((typeof this.data.error?.min !== 'undefined' || typeof this.data.error?.max !== 'undefined') && {
          list: new Builder.ArraySchema({
            title: this.data.error?.min ? t('dns.test.min', { count: this.data.error.min }) : ''
              + this.data.error?.max ? t('dns.test.max', { count: this.data.error.max }) : '',
            ...(typeof this.data.error?.min !== 'undefined' && {
              min: this.data.error.min
            }),
            ...(typeof this.data.error?.max !== 'undefined' && {
              max: this.data.error.max
            })
          })
        }),
        ...(typeof this.data.error?.time !== 'undefined' && {
          time: new Builder.NumberSchema({
            title: t('dns.test.time', { count: this.data.error.time }),
            max: this.data.error.time
          })
        }),
      }
    }))

    // get data
    let prom: Promise<any>
    if (!this.data.nameserver && !this.data.type) {
      prom = new Promise((resolve, reject) => {
        this.logProcess(`nslookup ${this.data.domain}`)
        const start = Date.now()
        lookup(this.data.domain, { all: true, verbatim: true }, (err: any, list: any) => {
          if (err) return reject(err)
          this.logReturn(list)
          const time = Date.now() - start
          resolve({
            time,
            list: list.map((e: any) => {
              e.type = e.family === 4 ? 'A' : 'AAAA'
              delete e.family
              return e
            })
          })
        });
      })
    } else {
      const resolver = new Resolver();
      if (this.data.nameserver) resolver.setServers(this.data.nameserver);
      prom = new Promise((resolve, reject) => {
        this.logProcess(`nslookup ${this.data.domain} ${this.data.nameserver?.join(' ') || ''}`)
        const start = Date.now()
        resolver.resolve(this.data.domain, this.data.type || 'ANY', (err: any, list: any) => {
          if (err) return reject(err)
          this.logReturn(list)
          const time = Date.now() - start
          resolve({ time, list })
        });
      })
    }
    return prom
      .catch(e => {
        this.status = 'ERROR'
        this.error = e
        this.fixes.push(new FixNetDNS(this))
        return Promise.reject()
      })
      .then(val => {
        this.result = val
        this.logResult(val)
        this.status = 'OK'
      })

      // check error and warning
      .then(() => {
        const ds = { data: this.result, source: 'preset:result' }
        return Promise.resolve().then(() => {
          if (!this.data.error) return
          this.logValidation(valError.describe())
          return valError.load(this.lang, ds)
        })
          .then(_val => {
            if (!this.data.warn) return
            this.logValidation(valWarn.describe())
            return valWarn.load(this.lang, ds)
              .catch(e => {
                this.status = 'WARN'
                this.error = e
              })
          })
          .catch(e => {
            this.status = 'ERROR'
            this.error = e
          })
      })
  }
}

export default TestNetDNS
export { TestNetDNS as Test, schemaBase as data } // for interactive mode
