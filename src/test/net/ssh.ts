import Validator from '@alinex/validator';
import * as Builder from '@alinex/validator/lib/schema';
import { Client } from 'ssh2';
import { clone } from '@alinex/data'

import { TestNet } from '.'
import { T } from '../../i18n';
import FixNetTCP from '../../fix/net/tcp'

const item = {
  time: new Builder.NumberSchema({
    title: 'Time to connect to TCP port',
    unit: { from: 'ms', to: 'ms' },
    min: 0
  }),
}
const schemaBase = new Builder.ObjectSchema({
  item: {
    host: new Builder.IPSchema({
      title: 'Hostname or IP to connect',
      lookup: true
    }),
    port: new Builder.PortSchema({
      title: 'Port to connect to',
      default: 22
    }),
    warn: new Builder.ObjectSchema({
      title: 'Warn Check',
      item,
      denyUndefined: true,
      default: { time: 1000 }
    }),
    error: new Builder.ObjectSchema({
      title: 'Error Check',
      item,
      denyUndefined: true,
      default: { time: 5000 }
    })
  },
  mandatory: ['host', 'port', 'warn', 'error'],
  denyUndefined: true
})

class TestNetSSH extends TestNet {
  public async run() {
    const t = T(this.lang, 'net')

    // check data
    await Validator.load(clone(schemaBase), this.lang, { data: this.data, source: `preset:${this.path}.data` })
      .then(val => this.data = val.get())
    this.description = t('ssh.test.description', { host: this.data.host })

    // setup validation
    const valWarn = new Validator(new Builder.ObjectSchema({
      title: t('core:test.validateWarn'),
      item: {
        ...(typeof this.data.warn?.time !== 'undefined' && {
          time: new Builder.NumberSchema({
            title: t('ssh.test.time', { count: this.data.warn.time }),
            max: this.data.warn.time
          })
        }),
      }
    }))
    const valError = new Validator(new Builder.ObjectSchema({
      title: t('core:test.validateError'),
      item: {
        connected: new Builder.BooleanSchema({ value: true }),
        ...(typeof this.data.error?.time !== 'undefined' && {
          time: new Builder.NumberSchema({
            title: t('ssh.test.time', { count: this.data.error.time }),
            max: this.data.error.time
          })
        }),
      }
    }))

    // get data
    let time: Number
    return new Promise((resolve: (value: string[]) => any, reject) => {
      const protocol: string[] = []
      const conn = new Client();
      const server = {
        username: '???',
        password: '',
        host: this.data.host,
        //        privateKey: '',
        //        passphrase: '',
        debug: (debug: string) => protocol.push(debug.replace(/DEBUG: /, ''))
      }
      this.logProcess(`ssh ${server.username}@${server.host}`)
      const start = Date.now()
      conn.on('ready', async () => {
        time = Date.now() - start
        try {
          conn.end(); // close connection
          resolve(protocol)
        } catch (e) {
          reject(e)
        }
      })
      conn.on('error', err => {
        if (err.level !== 'client-authentication')
          return reject(err)
        time = Date.now() - start
        resolve(protocol)
      })
      conn.connect(server);
    })
      .catch(e => {
        this.status = 'ERROR'
        this.error = e
        this.fixes.push(new FixNetTCP(this))
        return Promise.reject()
      })
      .then(val => {
        this.logReturn(val.join('\n'))
        this.result = {
          ident: val.filter(e => e.match(/Remote ident:/))[0].slice(15, -1),
          connected: val.filter(e => e.match(/Client: Connected/))?.length ? true : false,
          time,
          key_algorithm: val.filter(e => e.match(/\(remote\) KEX algorithms:/))[0]?.slice(26).split(/,/),
          key_formats: val.filter(e => e.match(/\(remote\) Host key formats:/))[0]?.slice(28).split(/,/),
          ciphers: val.filter(e => e.match(/\(remote\) Server->Client ciphers:/))[0]?.slice(34).split(/,/),
          hmac: val.filter(e => e.match(/\(remote\) Server->Client HMAC algorithms:/))[0]?.slice(42).split(/,/),
          compression: val.filter(e => e.match(/\(remote\) Server->Client compression algorithms:/))[0]?.slice(49).split(/,/),
        }
        this.logResult(this.result)
        this.status = 'OK'
        return
      })

      // check error and warning
      .then(() => {
        const ds = { data: this.result, source: 'preset:result' }
        return Promise.resolve().then(() => {
          if (!this.data.error) return
          this.logValidation(valError.describe())
          return valError.load(this.lang, ds)
        })
          .then(_val => {
            if (!this.data.warn) return
            this.logValidation(valWarn.describe())
            return valWarn.load(this.lang, ds)
              .catch(e => {
                this.status = 'WARN'
                this.error = e
              })
          })
          .catch(e => {
            this.status = 'ERROR'
            this.error = e
          })
      })
  }
}

export default TestNetSSH
export { TestNetSSH as Test, schemaBase as data } // for interactive mode
