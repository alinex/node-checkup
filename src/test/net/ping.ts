import Validator from '@alinex/validator';
import * as Builder from '@alinex/validator/lib/schema';
import { clone } from '@alinex/data'

import { TestNet } from '.'
import { T } from '../../i18n';

const item = {
  loss: new Builder.NumberSchema({
    title: 'The threshold for loss of packets',
    sanitize: true,
    min: 0, max: 1
  }),
  avg: new Builder.NumberSchema({
    title: 'Average round trip time',
    unit: { from: 'ms', to: 'ms' },
    min: 0
  }),
  min: new Builder.NumberSchema({
    title: 'Minimum round trip time',
    unit: { from: 'ms', to: 'ms' },
    min: 0
  }),
  max: new Builder.NumberSchema({
    title: 'Maximum round trip time',
    unit: { from: 'ms', to: 'ms' },
    min: 0
  })
}
const schemaBase = new Builder.ObjectSchema({
  item: {
    num: new Builder.NumberSchema({
      title: 'Number of packets',
      integer: true,
      min: 1,
      default: 5
    }),
    host: new Builder.IPSchema({
      title: 'Hostname or IP to ping',
      lookup: true
    }),
    warn: new Builder.ObjectSchema({
      title: 'Warn Check',
      item,
      denyUndefined: true,
      default: { avg: 1000 }
    }),
    error: new Builder.ObjectSchema({
      title: 'Error Check',
      item,
      denyUndefined: true,
      default: { loss: 0 }
    })
  },
  mandatory: true,
  denyUndefined: true
})

class TestNetPing extends TestNet {
  public async run() {
    const t = T(this.lang, 'net')

    // check data
    await Validator.load(clone(schemaBase), this.lang, { data: this.data, source: `preset:${this.path}.data` })
      .then(val => this.data = val.get())
    this.description = t('ping.test.description', this.data)

    // setup validation
    const valResult = new Validator(new Builder.ObjectSchema({
      title: t('core:test.validateResult'),
      item: {
        min: new Builder.NumberSchema(),
        avg: new Builder.NumberSchema(),
        max: new Builder.NumberSchema(),
        mdev: new Builder.NumberSchema(),
        total: new Builder.NumberSchema(),
        received: new Builder.NumberSchema(),
        loss: new Builder.NumberSchema({ sanitize: true }),
        time: new Builder.NumberSchema()
      },
      mandatory: true
    }))
    const valWarn = new Validator(new Builder.ObjectSchema({
      title: t('core:test.validateWarn'),
      item: {
        ...(typeof this.data.warn?.loss !== 'undefined' && {
          loss: new Builder.NumberSchema({
            title: t('ping.test.loss', { count: this.data.warn.loss }),
            min: 0,
            max: this.data.warn.loss
          })
        }),
        ...(typeof this.data.warn?.min !== 'undefined' && {
          min: new Builder.NumberSchema({
            title: t('ping.test.min', { count: this.data.warn.min }),
            max: this.data.warn.min
          })
        }),
        ...(typeof this.data.warn?.max !== 'undefined' && {
          max: new Builder.NumberSchema({
            title: t('ping.test.max', { count: this.data.warn.max }),
            max: this.data.warn.max
          })
        }),
        ...(typeof this.data.warn?.avg !== 'undefined' && {
          avg: new Builder.NumberSchema({
            title: t('ping.test.avg', { count: this.data.warn.avg }),
            max: this.data.warn.avg
          })
        })
      }
    }))
    const valError = new Validator(new Builder.ObjectSchema({
      title: t('core:test.validateError'),
      item: {
        ...(typeof this.data.error?.loss !== 'undefined' && {
          loss: new Builder.NumberSchema({
            title: t('ping.test.loss', { count: this.data.error.loss }),
            min: 0,
            max: this.data.error.loss
          })
        }),
        ...(typeof this.data.error?.min !== 'undefined' && {
          min: new Builder.NumberSchema({
            title: t('ping.test.min', { count: this.data.error.min }),
            max: this.data.error.min
          })
        }),
        ...(typeof this.data.error?.max !== 'undefined' && {
          max: new Builder.NumberSchema({
            title: t('ping.test.max', { count: this.data.error.max }),
            max: this.data.error.max
          })
        }),
        ...(typeof this.data.error?.avg !== 'undefined' && {
          avg: new Builder.NumberSchema({
            title: t('ping.test.avg', { count: this.data.error.avg }),
            max: this.data.error.avg
          })
        })
      }
    }))

    // get data
    return this.execute(`LANG=C ping -c ${this.data.num} ${this.data.host}`)
      // data collection
      .then(data => {
        const lines: string[] = data.stdout.split(/\n/)
        let result: any = {}
        let m = lines.pop()?.match(/rtt min\/avg\/max\/mdev = (?<min>[0-9.]+)\/(?<avg>[0-9.]+)\/(?<max>[0-9.]+)\/(?<mdev>[0-9.]+) ms/)
        result = { ...result, ...m?.groups }
        m = lines.pop()?.match(/(?<total>\d+) packets transmitted, (?<received>\d+) received, (?<loss>\d+%) packet loss, time (?<time>\d+)ms/)
        result = { ...result, ...m?.groups }
        return result
      })
      // validate result
      .then(data => {
        this.logValidation(valResult.describe())
        return valResult.load(this.lang, { data })
      })
      .then(val => {
        this.result = val
        this.logResult(val)
        this.status = 'OK'
      })

      // check error and warning
      .then(() => {
        const ds = { data: this.result, source: 'preset:result' }
        return Promise.resolve().then(() => {
          if (!this.data.error) return
          this.logValidation(valError.describe())
          return valError.load(this.lang, ds)
        })
          .then(_val => {
            if (!this.data.warn) return
            this.logValidation(valWarn.describe())
            return valWarn.load(this.lang, ds)
              .catch(e => {
                this.status = 'WARN'
                this.error = e
              })
          })
          .catch(e => {
            this.status = 'ERROR'
            this.error = e
          })
      })
  }
}

export default TestNetPing
export { TestNetPing as Test, schemaBase as data } // for interactive mode
