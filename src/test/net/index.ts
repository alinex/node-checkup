import { loadNamespace } from '../../i18n'
import Test from '..'

loadNamespace('net')

export class TestNet extends Test {
}

export default TestNet
