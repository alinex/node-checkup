import Validator from '@alinex/validator';
import * as Builder from '@alinex/validator/lib/schema';
import { promises as fs } from 'fs'
import { Certificate } from '@fidm/x509'
import { clone } from '@alinex/data'

import { TestNet } from '.'
import { T } from '../../i18n'
import FixNetTLS from '../../fix/net/tls'

const item = {
  score: new Builder.NumberSchema({
    title: 'Perseptual score',
    min: 0,
    max: 1
  }),
  time: new Builder.NumberSchema({
    title: 'time the certificate has to be valid',
    unit: { from: 's', to: 's' },
    greater: 0
  })
}
const schemaBase = new Builder.ObjectSchema({
  item: {
    domain: new Builder.DomainSchema(),
    port: new Builder.PortSchema({ default: 443 }),
    warn: new Builder.ObjectSchema({
      title: 'Warn Check',
      item,
      denyUndefined: true,
      default: { score: 0.8 }
    }),
    error: new Builder.ObjectSchema({
      title: 'Error Check',
      item,
      denyUndefined: true,
      default: { score: 0.5 }
    })
  },
  mandatory: true,
  denyUndefined: true
})

class TestNetTLS extends TestNet {
  public async run() {
    const t = T(this.lang, 'net')

    // check data
    await Validator.load(clone(schemaBase), this.lang, { data: this.data, source: `preset:${this.path}.data` })
      .then((val: any) => this.data = val.get())

    this.description = t('tls.test.description', { domain: this.data.domain })
    // setup validation
    const valWarn = new Validator(new Builder.ObjectSchema({
      title: t('core:test.validateWarn'),
      item: {
        ...(typeof this.data.warn?.time !== 'undefined' && {
          remaining: new Builder.NumberSchema({
            title: t('tls.test.time', { value: this.data.warn.time }),
            min: this.data.warn.time
          })
        }),
        ...(typeof this.data.warn?.score !== 'undefined' && {
          score: new Builder.NumberSchema({
            title: t('tls.test.score', { count: this.data.warn.score }),
            min: this.data.warn.score
          })
        })
      }
    }))
    const valError = new Validator(new Builder.ObjectSchema({
      title: t('core:test.validateError'),
      item: {
        ...(typeof this.data.error?.time !== 'undefined' && {
          remaining: new Builder.NumberSchema({
            title: t('tls.test.time', { value: this.data.error.time }),
            min: this.data.error.time
          })
        }),
        ...(typeof this.data.error?.score !== 'undefined' && {
          score: new Builder.NumberSchema({
            title: t('tls.test.score', { count: this.data.error.score }),
            min: this.data.error.score
          })
        }),
      }
    }))

    // get data
    return this.execute(`${__dirname}/../../../bin/testssl ${this.data.domain}:${this.data.port}`)
      .then(async (res) => {
        const lines = res.stdout.split(/\n/)
        const file = lines[lines.length - 1]
        const data = await fs.readFile(file, 'utf-8');
        //await fs.unlink(file)
        return data
      })
      //    return fs.readFile('test/data/nettls_google.com.json', 'utf-8')
      //    return fs.readFile('test/data/nettls_divibib.com.json', 'utf-8')
      //    return fs.readFile('test/data/nettls_alinex.de.json', 'utf-8')
      .then(data => JSON.parse(data))
      .catch(e => {
        this.status = 'ERROR'
        this.error = e
        return Promise.reject()
      })
      .then(val => {
        const result = val.scanResult[0]
        let cert = result.serverDefaults.filter((e: any) => e.id === 'cert' || e.id.startsWith('cert '))[0]?.finding.replace(/ /g, '\n').replace(/(BEGIN|END)\nCERTIFICATE/g, "$1 CERTIFICATE")
        const attr = Certificate.fromPEM(cert).subject.attributes
        const subject: any = {}
        attr.forEach(e => subject[e.shortName] = e.value)
        this.result = {
          time: val.scanTime,
          SSL2: result.protocols.filter((e: any) => e.id === 'SSLv2')[0]?.finding.match(/^offered/) ? true : false,
          SSL3: result.protocols.filter((e: any) => e.id === 'SSLv3')[0]?.finding.match(/^offered/) ? true : false,
          TLS1: result.protocols.filter((e: any) => e.id === 'TLS1')[0]?.finding.match(/^offered/) ? true : false,
          TLS1_1: result.protocols.filter((e: any) => e.id === 'TLS1_1')[0]?.finding.match(/^offered/) ? true : false,
          TLS1_2: result.protocols.filter((e: any) => e.id === 'TLS1_2')[0]?.finding.match(/^offered/) ? true : false,
          TLS1_3: result.protocols.filter((e: any) => e.id === 'TLS1_3')[0]?.finding.match(/^offered/) ? true : false,
          score: parseInt(result.rating.filter((e: any) => e.id === 'final_score')[0]?.finding) / 100,
          grade: result.rating.filter((e: any) => e.id === 'overall_grade')[0]?.finding,
          problems: result.rating.filter((e: any) => e.id.startsWith('grade_cap_reason')).map((e: any) => e.finding),
          valid: result.serverDefaults.filter((e: any) => e.id === 'cert_chain_of_trust' || e.id.startsWith('cert_chain_of_trust '))[0]?.finding === 'passed.',
          subject: subject,
          from: new Date(result.serverDefaults.filter((e: any) => e.id === 'cert_notBefore' || e.id.startsWith('cert_notBefore '))[0]?.finding),
          to: new Date(result.serverDefaults.filter((e: any) => e.id === 'cert_notAfter' || e.id.startsWith('cert_notAfter '))[0]?.finding),
          for: result.serverDefaults.filter((e: any) => e.id === 'cert_subjectAltName' || e.id.startsWith('cert_subjectAltName '))[0]?.finding.split(/\s+/)
        }
        this.result.remaining = (+this.result.to - +this.result.from) / 1000
        this.logResult(this.result)
        this.status = 'OK'
      })

      // check error and warning
      .then(() => {
        const ds = { data: this.result, source: 'preset:result' }
        return Promise.resolve().then(() => {
          if (!this.data.error) return
          this.logValidation(valError.describe())
          return valError.load(this.lang, ds)
        })
          .then(_val => {
            if (!this.data.warn) return
            this.logValidation(valWarn.describe())
            return valWarn.load(this.lang, ds)
              .catch(e => {
                this.status = 'WARN'
                this.error = e
                this.fixes.push(new FixNetTLS(this))
              })
          })
          .catch(e => {
            this.status = 'ERROR'
            this.error = e
            this.fixes.push(new FixNetTLS(this))
          })
      })
  }
}

export default TestNetTLS
export { TestNetTLS as Test, schemaBase as data } // for interactive mode
