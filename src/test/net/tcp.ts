import Validator from '@alinex/validator';
import * as Builder from '@alinex/validator/lib/schema';
import { Socket } from 'net'
import { clone } from '@alinex/data'

import { TestNet } from '.'
import { T } from '../../i18n';

const item = {
  time: new Builder.NumberSchema({
    title: 'Time to connect to TCP port',
    unit: { from: 'ms', to: 'ms' },
    min: 0
  }),
}
const schemaBase = new Builder.ObjectSchema({
  item: {
    host: new Builder.IPSchema({
      title: 'Hostname or IP to connect',
      lookup: true
    }),
    port: new Builder.PortSchema({
      title: 'Port to connect to'
    }),
    timeout: new Builder.NumberSchema({
      title: 'Timeout',
      unit: { from: 'ms', to: 'ms' },
      min: 1,
      default: 30000
    }),
    warn: new Builder.ObjectSchema({
      title: 'Warn Check',
      item,
      denyUndefined: true,
      default: { time: 500 }
    }),
    error: new Builder.ObjectSchema({
      title: 'Error Check',
      item,
      denyUndefined: true,
      default: { time: 5000 }
    })
  },
  mandatory: true,
  denyUndefined: true
})

class TestNetTCP extends TestNet {
  public async run() {
    const t = T(this.lang, 'net')

    // check data
    await Validator.load(clone(schemaBase), this.lang, { data: this.data, source: `preset:${this.path}.data` })
      .then(val => this.data = val.get())

    this.description = t('tcp.test.description', this.data)

    // setup validation
    const valWarn = new Validator(new Builder.ObjectSchema({
      title: t('core:test.validateWarn'),
      item: {
        ...(typeof this.data.warn?.time !== 'undefined' && {
          time: new Builder.NumberSchema({
            title: t('tcp.test.time', { count: this.data.warn.time }),
            max: this.data.warn.time
          })
        })
      }
    }))
    const valError = new Validator(new Builder.ObjectSchema({
      title: t('core:test.validateError'),
      item: {
        ...(typeof this.data.error?.time !== 'undefined' && {
          time: new Builder.NumberSchema({
            title: t('tcp.test.time', { count: this.data.error.time }),
            max: this.data.error.time
          })
        }),
      }
    }))

    // get data
    return new Promise((resolve, reject) => {
      const socket = new Socket();
      this.logProcess(`telnet ${this.data.host} ${this.data.port} (timeout ${this.data.timeout})`)
      const start = Date.now()
      try {
        socket.setTimeout(this.data.timeout, () => {
          socket.destroy();
          reject(new Error(`Timeout after ${this.data.timeout} ms reached`))
        })
        socket.connect(this.data.port, this.data.host, () => {
          const time = Date.now() - start
          const result = {
            time,
            ip: socket.remoteAddress,
            port: socket.remotePort,
            status: 'connected'
          }
          socket.end();
          resolve(result)
        });
      } catch (err) {
        socket.end();
        reject(err)
      }
    })
      .catch(e => {
        this.status = 'ERROR'
        this.error = e
        return Promise.reject()
      })
      .then(val => {
        this.result = val
        this.logResult(val)
        this.status = 'OK'
      })

      // check error and warning
      .then(() => {
        const ds = { data: this.result, source: 'preset:result' }
        return Promise.resolve().then(() => {
          if (!this.data.error) return
          this.logValidation(valError.describe())
          return valError.load(this.lang, ds)
        })
          .then(_val => {
            if (!this.data.warn) return
            this.logValidation(valWarn.describe())
            return valWarn.load(this.lang, ds)
              .catch(e => {
                this.status = 'WARN'
                this.error = e
              })
          })
          .catch(e => {
            this.status = 'ERROR'
            this.error = e
          })
      })
  }
}

export default TestNetTCP
export { TestNetTCP as Test, schemaBase as data } // for interactive mode
