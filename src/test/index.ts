import Debug from "debug";
import { inspect } from 'util'
import NodeCache from "node-cache"
import ValidationError from '@alinex/validator/lib/error'
import { DateTime } from "luxon";

import Fix from '../fix'
import { Case } from '../index'
import { config, shell, execute, Client, commands } from '../shell'

class Test {
  public path: string
  public case: Case
  public status: string
  public description: string = ''
  public error: ValidationError | Error | undefined
  public result: any
  public fixes: Fix[] = []
  public data: any
  public fixdata: any
  public lang: string
  protected cache: NodeCache
  public start: Date = new Date()
  protected debug: debug.Debugger
  public _log: string[] = []
  private client: Client | undefined
  public store: any = {}

  constructor(path: string, testcase: Case, lang: string, cache: NodeCache) {
    this.status = 'NODATA'
    this.path = path
    this.case = testcase
    this.data = testcase.data
    this.fixdata = testcase.fix
    this.lang = lang
    this.cache = cache
    this.debug = Debug(`checkup:${this.constructor.name.replace(/([a-z])([A-Z])/g, '$1:$2').toLowerCase()}`);
  }

  public async run() {
  }

  protected logRequest(msg: string) {
    this.debug('REQUEST  %s', msg)
    this._log.push(`2:REQUEST  ${msg.replace(/\n/g, '\n         ')}\n         (${DateTime.now()})`)
  }
  protected logData(msg: any) {
    this.debug('DATA     %o', msg)
    this._log.push(`3:DATA     ${(typeof msg === 'string' ? msg : inspect(msg, { depth: 10 })).replace(/\n/g, '\n         ')}`)
  }
  protected logStatus(msg: string) {
    this.debug('STATUS   %s', msg)
    this._log.push(`2:STATUS   ${msg}`)
  }
  protected logResponse(msg: string) {
    this.debug('RESPONSE %s', msg)
    this._log.push(`3:RESPONSE ${msg.replace(/\n/g, '\n         ')}`)
  }
  protected logReturn(msg: any) {
    this.debug('RETURN   %o', msg)
    this._log.push(`3:RETURN   ${(typeof msg === 'string' ? msg : inspect(msg, { depth: 10 })).replace(/\n/g, '\n         ')}\n         (${DateTime.now()})`)
  }

  protected logProcess(msg: string) {
    this.debug('PROCESS  %s', msg)
    this._log.push(`2:PROCESS  ${msg.replace(/\n/g, '\n         ')}\n         (${DateTime.now()})`)
  }
  protected logExitcode(msg: number) {
    this.debug('EXITCODE %s', msg)
    this._log.push(`2:EXITCODE ${msg}`)
  }
  protected logStderr(msg: string) {
    this.debug('STDERR   %s', msg)
    this._log.push(`3:STDERR   ${msg.replace(/\n/g, '\n         ').trim()}\n         (${DateTime.now()})`)
  }
  protected logStdout(msg: string) {
    this.debug('STDOUT   %s', msg)
    this._log.push(`3:STDOUT   ${msg.replace(/\n/g, '\n         ').trim()}\n         (${DateTime.now()})`)
  }

  protected logValidation(msg: string) {
    this.debug('VALIDATE %s', msg)
    this._log.push(`4:VALIDATE ${msg.replace(/\n/g, '\n         ')}`)
  }
  protected logResult(msg: any) {
    this.debug('RESULT   %o', msg)
    this._log.push(`1:RESULT   ${(typeof msg === 'string' ? msg : inspect(msg, { depth: 10 })).replace(/\n/g, '\n         ')}`)
  }

  public log(verbose: number): string {
    return this._log.filter(line => verbose >= new Number(line.charAt(0)))
      .map(line => line.substr(2)).join('\n')
  }

  protected shell(fn: () => Promise<any>) {
    return config(this.data.server).then(setup => {
      if (setup?.connect) this.logProcess(`CONNECT ssh://${setup.connect}`)
      return shell(setup, (conn) => { this.client = conn; return fn() })
    })
  }
  protected execute(cmd: string): Promise<any> {
    this.logProcess(cmd)
    return execute(this.client, cmd).then((res => {
      if (res.stdout) this.logStdout(res.stdout)
      if (res.stderr) this.logStderr(res.stderr)
      this.logExitcode(res.exitCode)
      return res
    }))
  }
  protected commands(list: string[], sudo = false) {
    return commands(this.client, list, sudo)
  }

}

export default Test
