import Validator from '@alinex/validator';
import * as Builder from '@alinex/validator/lib/schema';
import { clone } from '@alinex/data'
import { sql } from 'slonik';

import { TestDatabase } from '..'
import { T } from '../../../i18n';
import { client, version, connection, searchPath } from './index'

const CACHE_TTL = 600 // maximum time to cache result for diff calculation

const SQL_ANALYSIS = sql`select * from (
  select
    case when datconnlimit >= 0 then datconnlimit else current_setting('max_connections')::integer - current_setting('superuser_reserved_connections')::integer end as conn_max
  from pg_database where datname = current_database()
) as base, (
  select
    pg_database_size(current_database()) as size,
    coalesce(sum(pg_total_relation_size(relid)-pg_indexes_size(relid)), 0) as size_table,
    coalesce(sum(pg_relation_size(relid, 'main')), 0) as size_data,
    coalesce(sum(pg_relation_size(relid, 'fsm')), 0) as size_free,
    coalesce(sum(pg_indexes_size(relid)), 0) as size_index,
    count(*)::integer as tables,
    count(case when idx_tup_fetch + seq_tup_read = 0 then 1 end)::integer as tables_unused,
    coalesce(sum(n_live_tup), 0) as rows,
    coalesce(sum(n_dead_tup), 0) as dead
  from pg_stat_user_tables
) as size, (
  select count(*)::integer as indexes,
    count(case when idx_scan = 0 then 1 end)::integer as indexes_unused
  from pg_stat_user_indexes
) as indexes, (
  select
    count(*)::integer as views
  from pg_views where schemaname NOT IN ('pg_catalog', 'information_schema')
) as views, (
  select
    count(*)::integer as matviews
  from pg_matviews where schemaname NOT IN ('pg_catalog', 'information_schema')
) as matviews, (
  select
    count(*)::integer as functions
  from pg_catalog.pg_proc p
  LEFT JOIN pg_catalog.pg_namespace n ON n.oid = p.pronamespace
  WHERE pg_catalog.pg_function_is_visible(p.oid)
    AND n.nspname <> 'pg_catalog'
    AND n.nspname <> 'information_schema'
    and p.probin is null
) as functions, (
  select
    count(*)::integer as sequences
  from pg_sequences where schemaname NOT IN ('pg_catalog', 'information_schema')
) as sequences, (
  select
    count(*)::integer as schemas
  from pg_catalog.pg_namespace where nspname not like 'pg_%' and nspname != 'information_schema'
) as schemas, (
  select
    (select count(*)::integer from pg_catalog.pg_user) as user,
    string_agg(distinct usename, ', ') as user_names,
    string_agg(distinct host(client_addr), ', ') as user_ip,
    string_agg(distinct case when application_name = '' then null else application_name end, ', ') as user_app,
    extract(epoch from current_timestamp - min(xact_start)) as transaction_age,
    extract(epoch from current_timestamp - min(case when state = 'active' then query_start else null end)) as query_age,
    count(*)::integer as conn,
    count(case when state = 'active' then 1 end)::integer as conn_active,
    count(case when state != 'active' then 1 end)::integer as conn_idle,
    count(case when wait_event is not null and wait_event not in('ClientRead') then 1 end)::integer as conn_wait
  from pg_catalog.pg_stat_activity where datname = current_database()
) as activity, (
  select
    xact_commit as commits,
    xact_rollback as rollbacks,
    blks_read as block_read,
    blks_hit as block_cache,
    temp_files as disk_temp,
    blk_read_time as disk_read,
    blk_write_time as disk_write,
    tup_returned as returned,
    tup_fetched as fetched,
    tup_inserted as inserted,
    tup_updated as updated,
    tup_deleted as deleted,
    conflicts,
    deadlocks,
    extract(epoch from now() - stats_reset) as stats_age -- in seconds
  from pg_stat_database where datname = current_database()
) as dynamic`

const DYNAMIC = ['commits', 'rollbacks', 'block_read', 'block_cache', 'disk_temp', 'disk_read', 'disk_write', 'returned', 'fetched', 'inserted', 'updated', 'deleted', 'conflicts', 'deadlocks']

const item = {
  transaction_age: new Builder.NumberSchema({
    title: 'Maximum time for transactions',
    unit: { from: 's', to: 's' },
    min: 0
  }),
  query_age: new Builder.NumberSchema({
    title: 'Maximum time for transactions',
    unit: { from: 's', to: 's' },
    min: 0
  })
}
const schemaBase = new Builder.ObjectSchema({
  title: 'Postgres Database connection',
  item: {
    client,
    version,
    connection,
    searchPath,
    measureTime: new Builder.NumberSchema({
      unit: { from: 's', to: 's' },
      greater: 0,
      default: 30
    }),
    warn: new Builder.ObjectSchema({ title: 'Warn Check', item }),
    error: new Builder.ObjectSchema({ title: 'Error Check', item })
  },
  mandatory: ['client', 'connection', 'measureTime'],
  denyUndefined: true
});

class TestDatabasePostgresDatabase extends TestDatabase {
  public store = { exclude: ['stats_age'] }

  public async run() {
    // general excludes for store  
    const t = T(this.lang, 'database')

    // check data
    await Validator.load(clone(schemaBase), this.lang, { data: this.data, source: `preset:${this.path}.data` })
      .then(val => this.data = val.get())
    this.init('pg')
    this.description = t('postgresdatabase.test.description', { url: this.data.connection.url.replace(/:[^:]*@/, '@') })

    // setup validation
    const valResult = new Validator(new Builder.ObjectSchema({
      item: {
        user_names: new Builder.ArraySchema({ split: /,\s/ }),
        user_ip: new Builder.ArraySchema({ default: [], split: /,\s/ }),
        user_app: new Builder.ArraySchema({ default: [], split: /,\s/ }),
        size: new Builder.NumberSchema(),
        size_table: new Builder.NumberSchema(),
        size_data: new Builder.NumberSchema(),
        size_free: new Builder.NumberSchema(),
        size_index: new Builder.NumberSchema(),
        rows: new Builder.NumberSchema(),
        dead: new Builder.NumberSchema()
      }
    }))
    const valWarn = new Validator(new Builder.ObjectSchema({
      title: t('core:test.validateWarn'),
      item: {
        ...(typeof this.data.warn?.conn !== 'undefined' && {
          conn: new Builder.NumberSchema({
            title: t('postgresdatabase.test.conn', { count: this.data.warn.conn }),
            max: this.data.warn.conn
          })
        }),
        ...(typeof this.data.warn?.conn_free !== 'undefined' && {
          conn_free: new Builder.NumberSchema({
            title: t('postgresdatabase.test.conn_free', { count: this.data.warn.conn_free }),
            min: this.data.warn.conn_free
          })
        }),
        ...(typeof this.data.warn?.transaction_age !== 'undefined' && {
          transaction_age: new Builder.NumberSchema({
            title: t('postgresdatabase.test.transaction_age', { count: this.data.warn.transaction_age }),
            max: this.data.warn.transaction_age
          })
        }),
        ...(typeof this.data.warn?.query_age !== 'undefined' && {
          query_age: new Builder.NumberSchema({
            title: t('postgresdatabase.test.query_age', { count: this.data.warn.query_age }),
            max: this.data.warn.query_age
          })
        })
      }
    }))
    const valError = new Validator(new Builder.ObjectSchema({
      title: t('core:test.validateError'),
      item: {
        ...(typeof this.data.error?.conn !== 'undefined' && {
          conn: new Builder.NumberSchema({
            title: t('postgresdatabase.test.conn', { count: this.data.error.conn }),
            max: this.data.error.conn
          })
        }),
        ...(typeof this.data.error?.conn_free !== 'undefined' && {
          conn_free: new Builder.NumberSchema({
            title: t('postgresdatabase.test.conn_free', { count: this.data.error.conn_free }),
            min: this.data.error.conn_free
          })
        }),
        ...(typeof this.data.error?.transaction_age !== 'undefined' && {
          transaction_age: new Builder.NumberSchema({
            title: t('postgresdatabase.test.transaction_age', { count: this.data.error.transaction_age }),
            max: this.data.error.transaction_age
          })
        }),
        ...(typeof this.data.error?.query_age !== 'undefined' && {
          query_age: new Builder.NumberSchema({
            title: t('postgresdatabase.test.query_age', { count: this.data.error.query_age }),
            max: this.data.error.query_age
          })
        })
      }
    }))

    // get data
    return this.pgPool().connect(async conn => {
      this.logRequest(`Connect: ${this.data.connection.url}`)
      this.logRequest(sql`${SQL_ANALYSIS}`.sql)
      if (SQL_ANALYSIS.values.length) this.logData(SQL_ANALYSIS.values)
      const data = await conn.one(SQL_ANALYSIS)
      this.logReturn(data)
      this.result = { ...this.result, ...data }
      delete this.result.stats_age
    })
      // optimize
      .then(() => valResult.load(this.lang, { data: this.result }))
      .then(val => {
        if (val.conn_max) val.conn_free = val.conn_max - val.conn
        val.block_ratio = val.block_read === 0 ? 1 : val.block_cache / (val.block_cache + val.block_read)
        val.commit_ratio = val.rollbacks === 0 ? 1 : val.commits / (val.commits + val.rollbacks)
        // add time diffs
        const old: any = this.cache.get(`!${this.path}`)
        this.cache.set(`!${this.path}`, val, CACHE_TTL)
        if (old && val.stats_age > old.stats_age) { // calculate
          const timediff = val.stats_age - old.stats_age
          DYNAMIC.forEach(k => val[`${k}_diff`] = (val[k] - old[k]) / timediff)
        }
        // set as result
        this.result = val

      })
      .then(() => {
        this.logResult(this.result)
        this.status = 'OK'
      })
      // check error and warning
      .then(() => {
        const ds = { data: this.result, source: 'preset:result' }
        return Promise.resolve().then(() => {
          if (!this.data.error) return
          this.logValidation(valError.describe())
          return valError.load(this.lang, ds)
        })
          .then(_val => {
            if (!this.data.warn) return
            this.logValidation(valWarn.describe())
            return valWarn.load(this.lang, ds)
              .catch(e => {
                this.status = 'WARN'
                this.error = e
              })
          })
          .catch(e => {
            this.status = 'ERROR'
            this.error = e
          })
      })
  }
}

export default TestDatabasePostgresDatabase
export { TestDatabasePostgresDatabase as Test, schemaBase as data } // for interactive mode
