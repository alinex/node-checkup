import Validator from '@alinex/validator';
import * as Builder from '@alinex/validator/lib/schema';
import { clone } from '@alinex/data'
import { sql } from 'slonik';

import { TestDatabase } from '..'
import { T } from '../../../i18n';
import { client, version, connection, searchPath } from './index'

const CACHE_TTL = 600 // maximum time to cache result for diff calculation

function SQL_ANALYSIS(data: any) {
  return sql`select * from (
	select
    pg_total_relation_size(relid) as size,
    pg_total_relation_size(relid)-pg_indexes_size(relid) as size_table,
    pg_relation_size(relid, 'main') as size_data,
    pg_relation_size(relid, 'fsm') as size_free,
    pg_indexes_size(relid) as size_index,
    n_live_tup as rows,
    n_dead_tup as dead,
    case when idx_tup_fetch + seq_tup_read = 0 then 1 else 0 end as tables_unused,
    n_tup_ins as inserted,
    n_tup_upd as updated,
    n_tup_del as deleted,
    seq_scan as scan_seq,
    idx_scan as scan_index
  from pg_stat_user_tables where schemaname = ${data.schema} and relname=${data.table}
) as size, (
  select
    count(*)::integer as indexes,
    count(case when idx_scan = 0 then 1 end)::integer as indexes_unused
  from pg_stat_user_indexes where schemaname = ${data.schema} and relname=${data.table}
) as index, (
  select
    extract(epoch from now() - stats_reset) as stats_age -- in seconds
  from pg_stat_database where datname = current_database()
) as dynamic`
}

const DYNAMIC = ['inserted', 'updated', 'deleted', 'scan_seq', 'scan_index']

const item = {
  transaction_age: new Builder.NumberSchema({
    title: 'Maximum time for transactions',
    unit: { from: 's', to: 's' },
    min: 0
  }),
  query_age: new Builder.NumberSchema({
    title: 'Maximum time for transactions',
    unit: { from: 's', to: 's' },
    min: 0
  })
}
const schemaBase = new Builder.ObjectSchema({
  title: 'Postgres Database connection',
  item: {
    client,
    version,
    connection,
    searchPath,
    schema: new Builder.StringSchema({ alphaNum: true }),
    table: new Builder.StringSchema({ alphaNum: true }),
    measureTime: new Builder.NumberSchema({
      unit: { from: 's', to: 's' },
      greater: 0,
      default: 30
    }),
    warn: new Builder.ObjectSchema({ title: 'Warn Check', item }),
    error: new Builder.ObjectSchema({ title: 'Error Check', item })
  },
  mandatory: ['client', 'connection', 'schema', 'table', 'measureTime'],
  denyUndefined: true
});

class TestDatabasePostgresTable extends TestDatabase {
  // general excludes for store
  public store = { exclude: ['stats_age'] }

  public async run() {
    const t = T(this.lang, 'database')

    // check data
    await Validator.load(clone(schemaBase), this.lang, { data: this.data, source: `preset:${this.path}.data` })
      .then(val => this.data = val.get())
    this.init('pg')
    this.description = t('postgrestable.test.description', {
      url: this.data.connection.url.replace(/:[^:]*@/, '@'),
      table: `${this.data.schema}.${this.data.table}`
    })

    // setup validation
    const valResult = new Validator(new Builder.ObjectSchema({
      item: {
        size: new Builder.NumberSchema(),
        size_table: new Builder.NumberSchema(),
        size_data: new Builder.NumberSchema(),
        size_free: new Builder.NumberSchema(),
        size_index: new Builder.NumberSchema(),
        rows: new Builder.NumberSchema(),
        dead: new Builder.NumberSchema()
      }
    }))
    const valWarn = new Validator(new Builder.ObjectSchema({
      title: t('core:test.validateWarn'),
      item: {
        ...(typeof this.data.warn?.transaction_age !== 'undefined' && {
          transaction_age: new Builder.NumberSchema({
            title: t('postgrestable.test.transaction_age', { count: this.data.warn.transaction_age }),
            max: this.data.warn.transaction_age
          })
        }),
        ...(typeof this.data.warn?.query_age !== 'undefined' && {
          query_age: new Builder.NumberSchema({
            title: t('postgrestable.test.query_age', { count: this.data.warn.query_age }),
            max: this.data.warn.query_age
          })
        })
      }
    }))
    const valError = new Validator(new Builder.ObjectSchema({
      title: t('core:test.validateError'),
      item: {
        ...(typeof this.data.error?.transaction_age !== 'undefined' && {
          transaction_age: new Builder.NumberSchema({
            title: t('postgrestable.test.transaction_age', { count: this.data.error.transaction_age }),
            max: this.data.error.transaction_age
          })
        }),
        ...(typeof this.data.error?.query_age !== 'undefined' && {
          query_age: new Builder.NumberSchema({
            title: t('postgrestable.test.query_age', { count: this.data.error.query_age }),
            max: this.data.error.query_age
          })
        })
      }
    }))

    // get data
    return this.pgPool().connect(async conn => {
      this.logRequest(`Connect: ${this.data.connection.url}`)
      const query = sql`${SQL_ANALYSIS({ schema: this.data.schema, table: this.data.table })}`
      this.logRequest(query.sql)
      if (query.values.length) this.logData(query.values)
      const data = await conn.one(query)
      this.logReturn(data)
      this.result = { ...this.result, ...data }
      delete this.result.stats_age
    })
      // optimize
      .then(() => valResult.load(this.lang, { data: this.result }))
      .then(val => {
        val.scan_ratio = val.scan_seq === 0 ? 1 : val.scan_index / (val.scan_index + val.scan_seq)
        // add time diffs
        const old: any = this.cache.get(`!${this.path}`)
        this.cache.set(`!${this.path}`, val, CACHE_TTL)
        if (old && val.stats_age > old.stats_age) { // calculate
          const timediff = val.stats_age - old.stats_age
          DYNAMIC.forEach(k => val[`${k}_diff`] = (val[k] - old[k]) / timediff)
        }
        // set as result
        this.result = val
      })
      .then(() => {
        this.logResult(this.result)
        this.status = 'OK'
      })
      // check error and warning
      .then(() => {
        const ds = { data: this.result, source: 'preset:result' }
        return Promise.resolve().then(() => {
          if (!this.data.error) return
          this.logValidation(valError.describe())
          return valError.load(this.lang, ds)
        })
          .then(_val => {
            if (!this.data.warn) return
            this.logValidation(valWarn.describe())
            return valWarn.load(this.lang, ds)
              .catch(e => {
                this.status = 'WARN'
                this.error = e
              })
          })
          .catch(e => {
            this.status = 'ERROR'
            this.error = e
          })
      })
  }
}

export default TestDatabasePostgresTable
export { TestDatabasePostgresTable as Test, schemaBase as data } // for interactive mode
