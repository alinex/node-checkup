import Validator from '@alinex/validator';
import * as Builder from '@alinex/validator/lib/schema';
import { clone } from '@alinex/data'
import { sql } from 'slonik';

import { TestDatabase } from '..'
import { T } from '../../../i18n';
import { client, version, connection, searchPath } from './index'

function SQL_STATIC(data: any) {
  return sql`select * from (
  select
    coalesce(sum(pg_total_relation_size(tables.relid)), 0) as size,
    coalesce(sum(pg_total_relation_size(tables.relid)-pg_indexes_size(tables.relid)), 0) as size_table,
    coalesce(sum(pg_relation_size(tables.relid, 'main')), 0) as size_data,
    coalesce(sum(pg_relation_size(tables.relid, 'fsm')), 0) as size_free,
    coalesce(sum(pg_indexes_size(tables.relid)), 0) as size_index,
    count(*)::integer as tables,
    count(case when idx_tup_fetch + seq_tup_read = 0 then 1 end)::integer as tables_unused,
    coalesce(sum(n_live_tup), 0) as rows,
    coalesce(sum(n_dead_tup), 0) as dead
  from (
    select
      (ts.datname||'.'||t.schemaname||'.'||t.tablename)::regclass::oid as relid
    from pg_tables t
    cross join (
      select d.datname, ts.spcname as default_tablespace
      from pg_database d
      join pg_tablespace ts on ts.oid = d.dattablespace
      --where d.datname = current_database()
    ) ts
    where schemaname not like 'pg_%' and schemaname != 'information_schema'
    and coalesce(t.tablespace, ts.default_tablespace) = ${data.tablespace}
  ) as tables
  left join pg_stat_user_tables sut on sut.relid=tables.relid
) as size, (
  select
    count(*)::integer as indexes,
    count(case when idx_scan = 0 then 1 end)::integer as indexes_unused
  from (
    select
    (ts.datname||'.'||i.schemaname||'.'||i.indexname)::regclass::oid as relid
    from pg_indexes i
    cross join (
      select d.datname, ts.spcname as default_tablespace
      from pg_database d
      join pg_tablespace ts on ts.oid = d.dattablespace
      --where d.datname = current_database()
    ) ts
    where schemaname not like 'pg_%' and schemaname != 'information_schema'
      and coalesce(i.tablespace, ts.default_tablespace) = ${data.tablespace}
    ) as indexes
  left join pg_stat_user_indexes sui on sui.relid=indexes.relid
) as index`
}

const item = {
  transaction_age: new Builder.NumberSchema({
    title: 'Maximum time for transactions',
    unit: { from: 's', to: 's' },
    min: 0
  }),
  query_age: new Builder.NumberSchema({
    title: 'Maximum time for transactions',
    unit: { from: 's', to: 's' },
    min: 0
  })
}
const schemaBase = new Builder.ObjectSchema({
  title: 'Postgres Database connection',
  item: {
    client,
    version,
    connection,
    searchPath,
    tablespace: new Builder.StringSchema({ alphaNum: true }),
    measureTime: new Builder.NumberSchema({
      unit: { from: 's', to: 's' },
      greater: 0,
      default: 30
    }),
    warn: new Builder.ObjectSchema({ title: 'Warn Check', item }),
    error: new Builder.ObjectSchema({ title: 'Error Check', item })
  },
  mandatory: ['client', 'connection', 'tablespace', 'measureTime'],
  denyUndefined: true
});

class TestDatabasePostgresTablespace extends TestDatabase {
  public async run() {
    const t = T(this.lang, 'database')

    // check data
    this.data.connection.database = 'postgres' // always use postgres database
    await Validator.load(clone(schemaBase), this.lang, { data: this.data, source: `preset:${this.path}.data` })
      .then(val => this.data = val.get())
    this.init('pg')
    this.description = t('postgresspace.test.description', {
      url: this.data.connection.url.replace(/:[^:]*@/, '@'),
      tablespace: this.data.tablespace
    })

    // setup validation
    const valResult = new Validator(new Builder.ObjectSchema({
      item: {
        size: new Builder.NumberSchema(),
        size_table: new Builder.NumberSchema(),
        size_data: new Builder.NumberSchema(),
        size_free: new Builder.NumberSchema(),
        size_index: new Builder.NumberSchema(),
        rows: new Builder.NumberSchema(),
        dead: new Builder.NumberSchema()
      }
    }))
    const valWarn = new Validator(new Builder.ObjectSchema({
      title: t('core:test.validateWarn'),
      item: {
        ...(typeof this.data.warn?.transaction_age !== 'undefined' && {
          transaction_age: new Builder.NumberSchema({
            title: t('postgresspace.test.transaction_age', { count: this.data.warn.transaction_age }),
            max: this.data.warn.transaction_age
          })
        }),
        ...(typeof this.data.warn?.query_age !== 'undefined' && {
          query_age: new Builder.NumberSchema({
            title: t('postgresspace.test.query_age', { count: this.data.warn.query_age }),
            max: this.data.warn.query_age
          })
        })
      }
    }))
    const valError = new Validator(new Builder.ObjectSchema({
      title: t('core:test.validateError'),
      item: {
        ...(typeof this.data.error?.transaction_age !== 'undefined' && {
          transaction_age: new Builder.NumberSchema({
            title: t('postgresspace.test.transaction_age', { count: this.data.error.transaction_age }),
            max: this.data.error.transaction_age
          })
        }),
        ...(typeof this.data.error?.query_age !== 'undefined' && {
          query_age: new Builder.NumberSchema({
            title: t('postgresspace.test.query_age', { count: this.data.error.query_age }),
            max: this.data.error.query_age
          })
        })
      }
    }))

    // get data
    let first: any
    return this.pgPool().connect(async conn => {
      this.logRequest(`Connect: ${this.data.connection.url}`)
      const query = sql`${SQL_STATIC({ tablespace: this.data.tablespace })}`
      this.logRequest(query.sql)
      if (query.values.length) this.logData(query.values)
      first = await conn.one(query)
      this.logReturn(first)
      this.result = { ...this.result, ...first }
    })
      // optimize
      .then(() => valResult.load(this.lang, { data: this.result }))
      .then(() => {
        this.logResult(this.result)
        this.status = 'OK'
      })
      // check error and warning
      .then(() => {
        const ds = { data: this.result, source: 'preset:result' }
        return Promise.resolve().then(() => {
          if (!this.data.error) return
          this.logValidation(valError.describe())
          return valError.load(this.lang, ds)
        })
          .then(_val => {
            if (!this.data.warn) return
            this.logValidation(valWarn.describe())
            return valWarn.load(this.lang, ds)
              .catch(e => {
                this.status = 'WARN'
                this.error = e
              })
          })
          .catch(e => {
            this.status = 'ERROR'
            this.error = e
          })
      })
  }
}

export default TestDatabasePostgresTablespace
export { TestDatabasePostgresTablespace as Test, schemaBase as data } // for interactive mode
