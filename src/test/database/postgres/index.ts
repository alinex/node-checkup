import * as Builder from '@alinex/validator/lib/schema';

export const client = new Builder.StringSchema({ allow: ['pg'], default: 'pg' })
export const version = new Builder.StringSchema({ allow: [/\d+\.\d+/] })
export const connection = new Builder.ObjectSchema({
  item: {
    user: new Builder.StringSchema(),
    password: new Builder.StringSchema(),
    host: new Builder.StringSchema({ default: 'localhost' }),
    port: new Builder.PortSchema({ default: 5432 }),
    database: new Builder.StringSchema()
  },
  mandatory: ['host', 'port']
})
export const searchPath = new Builder.ArraySchema({
  item: {
    '*': new Builder.StringSchema()
  }
})
