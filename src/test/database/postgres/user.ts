import Validator from '@alinex/validator';
import * as Builder from '@alinex/validator/lib/schema';
import { clone } from '@alinex/data'
import { sql } from 'slonik';

import { TestDatabase } from '..'
import { T } from '../../../i18n';
import { client, version, connection, searchPath } from './index'

function SQL_STATIC(data: any) {
  return sql`select * from (
  select
    case
      when rolsuper = true and rolconnlimit = -1 then current_setting('max_connections')::integer
      when rolconnlimit = -1 then current_setting('max_connections')::integer - current_setting('superuser_reserved_connections')::integer
      else rolconnlimit
    end as conn_max
  FROM pg_roles WHERE rolname = ${data.user}
) as usr, (
  select
    string_agg(distinct host(client_addr), ', ') as user_ip,
    string_agg(distinct case when application_name = '' then null else application_name end, ', ') as user_app,
    extract(epoch from current_timestamp - min(xact_start)) as transaction_age,
    extract(epoch from current_timestamp - min(case when state = 'active' then query_start else null end)) as query_age,
    count(*)::integer as conn,
    count(case when state = 'active' then 1 end)::integer as conn_active,
    count(case when state != 'active' then 1 end)::integer as conn_idle,
    count(case when wait_event is not null and wait_event not in('ClientRead') then 1 end)::integer as conn_wait
  from pg_catalog.pg_stat_activity where usename = ${data.user}
) as activity`
}

const item = {
  transaction_age: new Builder.NumberSchema({
    title: 'Maximum time for transactions',
    unit: { from: 's', to: 's' },
    min: 0
  }),
  query_age: new Builder.NumberSchema({
    title: 'Maximum time for transactions',
    unit: { from: 's', to: 's' },
    min: 0
  })
}
const schemaBase = new Builder.ObjectSchema({
  title: 'Postgres Database connection',
  item: {
    client,
    version,
    connection,
    searchPath,
    user: new Builder.StringSchema({ alphaNum: true }),
    measureTime: new Builder.NumberSchema({
      unit: { from: 's', to: 's' },
      greater: 0,
      default: 30
    }),
    warn: new Builder.ObjectSchema({ title: 'Warn Check', item }),
    error: new Builder.ObjectSchema({ title: 'Error Check', item })
  },
  mandatory: ['client', 'connection', 'user', 'measureTime'],
  denyUndefined: true
});

class TestDatabasePostgresUser extends TestDatabase {
  public async run() {
    const t = T(this.lang, 'database')

    // check data
    await Validator.load(clone(schemaBase), this.lang, { data: this.data, source: `preset:${this.path}.data` })
      .then(val => this.data = val.get())
    this.init('pg')
    this.description = t('postgresuser.test.description', {
      url: this.data.connection.url.replace(/:[^:]*@/, '@'),
      user: this.data.user
    })

    // setup validation
    const valResult = new Validator(new Builder.ObjectSchema({
      item: {
        user_names: new Builder.ArraySchema({ split: /,\s/ }),
        user_ip: new Builder.ArraySchema({ default: [], split: /,\s/ }),
        user_app: new Builder.ArraySchema({ default: [], split: /,\s/ }),
        size: new Builder.NumberSchema(),
        size_table: new Builder.NumberSchema(),
        size_data: new Builder.NumberSchema(),
        size_free: new Builder.NumberSchema(),
        size_index: new Builder.NumberSchema(),
        rows: new Builder.NumberSchema(),
        dead: new Builder.NumberSchema()
      }
    }))
    const valWarn = new Validator(new Builder.ObjectSchema({
      title: t('core:test.validateWarn'),
      item: {
        ...(typeof this.data.warn?.conn !== 'undefined' && {
          conn: new Builder.NumberSchema({
            title: t('postgresuser.test.conn', { count: this.data.warn.conn }),
            max: this.data.warn.conn
          })
        }),
        ...(typeof this.data.warn?.conn_free !== 'undefined' && {
          conn_free: new Builder.NumberSchema({
            title: t('postgresuser.test.conn_free', { count: this.data.warn.conn_free }),
            min: this.data.warn.conn_free
          })
        }),
        ...(typeof this.data.warn?.transaction_age !== 'undefined' && {
          transaction_age: new Builder.NumberSchema({
            title: t('postgresuser.test.transaction_age', { count: this.data.warn.transaction_age }),
            max: this.data.warn.transaction_age
          })
        }),
        ...(typeof this.data.warn?.query_age !== 'undefined' && {
          query_age: new Builder.NumberSchema({
            title: t('postgresuser.test.query_age', { count: this.data.warn.query_age }),
            max: this.data.warn.query_age
          })
        })
      }
    }))
    const valError = new Validator(new Builder.ObjectSchema({
      title: t('core:test.validateError'),
      item: {
        ...(typeof this.data.error?.conn !== 'undefined' && {
          conn: new Builder.NumberSchema({
            title: t('postgresuser.test.conn', { count: this.data.error.conn }),
            max: this.data.error.conn
          })
        }),
        ...(typeof this.data.error?.conn_free !== 'undefined' && {
          conn_free: new Builder.NumberSchema({
            title: t('postgresuser.test.conn_free', { count: this.data.error.conn_free }),
            min: this.data.error.conn_free
          })
        }),
        ...(typeof this.data.error?.transaction_age !== 'undefined' && {
          transaction_age: new Builder.NumberSchema({
            title: t('postgresuser.test.transaction_age', { count: this.data.error.transaction_age }),
            max: this.data.error.transaction_age
          })
        }),
        ...(typeof this.data.error?.query_age !== 'undefined' && {
          query_age: new Builder.NumberSchema({
            title: t('postgresuser.test.query_age', { count: this.data.error.query_age }),
            max: this.data.error.query_age
          })
        })
      }
    }))

    // get data
    let first: any
    return this.pgPool().connect(async conn => {
      this.logRequest(`Connect: ${this.data.connection.url}`)
      const query = sql`${SQL_STATIC({ user: this.data.user })}`
      this.logRequest(query.sql)
      if (query.values.length) this.logData(query.values)
      first = await conn.one(query)
      this.logReturn(first)
      this.result = { ...this.result, ...first }
    })
      // optimize
      .then(() => valResult.load(this.lang, { data: this.result }))
      .then(val => {
        if (val.conn_max) val.conn_free = val.conn_max - val.conn
        this.result = val
      })
      .then(() => {
        this.logResult(this.result)
        this.status = 'OK'
      })
      // check error and warning
      .then(() => {
        const ds = { data: this.result, source: 'preset:result' }
        return Promise.resolve().then(() => {
          if (!this.data.error) return
          this.logValidation(valError.describe())
          return valError.load(this.lang, ds)
        })
          .then(_val => {
            if (!this.data.warn) return
            this.logValidation(valWarn.describe())
            return valWarn.load(this.lang, ds)
              .catch(e => {
                this.status = 'WARN'
                this.error = e
              })
          })
          .catch(e => {
            this.status = 'ERROR'
            this.error = e
          })
      })
  }
}

export default TestDatabasePostgresUser
export { TestDatabasePostgresUser as Test, schemaBase as data } // for interactive mode
