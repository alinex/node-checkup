import Validator from '@alinex/validator';
import * as Builder from '@alinex/validator/lib/schema';
import { clone } from '@alinex/data'
import { createPool, sql } from 'slonik';

import { TestDatabase } from '..'
import { T } from '../../../i18n';
import { client, version, connection, searchPath } from './index'

const CACHE_TTL = 600 // maximum time to cache result for diff calculation

const SQL_VERSION = sql`SHOW server_version_num`

const SQL_ANALYSIS = sql`select * from (
  select
    extract(epoch from current_timestamp - pg_postmaster_start_time()) as uptime,
    current_setting('cluster_name') as cluster_name,
    current_setting('work_mem') as work_mem,
    current_setting('maintenance_work_mem') as work_mem_maintenance,
    current_setting('shared_buffers') as shared_buffers,
    current_setting('effective_cache_size') as effective_cache_size,
    current_setting('max_connections') as conn_max,
    current_setting('superuser_reserved_connections') as conn_super,
    pg_is_in_recovery() as replica,
    case when pg_is_in_recovery() then extract(epoch from now()-pg_last_xact_replay_timestamp()) end as replica_lag
) as core, (
  select
    (select count(*)::integer from pg_catalog.pg_user) as user,
    string_agg(distinct usename, ', ') as user_names,
    string_agg(distinct host(client_addr), ', ') as user_ip,
    string_agg(distinct case when application_name = '' then null else application_name end, ', ') as user_app,
    extract(epoch from current_timestamp - min(xact_start)) as transaction_age,
    extract(epoch from current_timestamp - min(case when state = 'active' then query_start else null end)) as query_age,
    count(*)::integer as conn,
    count(case when state = 'active' then 1 end)::integer as conn_active,
    count(case when state != 'active' then 1 end)::integer as conn_idle,
    count(case when wait_event is not null and wait_event not in('ClientRead') then 1 end)::integer as conn_wait
  from pg_catalog.pg_stat_activity
) as activity, (
  select
    string_agg(extname, ', ') as extensions
  from pg_extension
) as extension, (
  select
    count(*)::integer as tablespaces,
    sum(pg_tablespace_size(spcname)) as tablespaces_size
  from pg_catalog.pg_tablespace where spcname not like 'pg_%'
) as tablespaces, (
  select
    sum(xact_commit) as commits,
    sum(xact_rollback) as rollbacks,
    sum(blks_read) as block_read,
    sum(blks_hit) as block_cache,
    sum(temp_files) as disk_temp,
    sum(blk_read_time) as disk_read,
    sum(blk_write_time) as disk_write,
    sum(tup_returned) as returned,
    sum(tup_fetched) as fetched,
    sum(tup_inserted) as inserted,
    sum(tup_updated) as updated,
    sum(tup_deleted) as deleted,
    sum(conflicts),
    sum(deadlocks),
    extract(epoch from now() - min(stats_reset)) as stats_age -- in seconds
  from pg_stat_database
) as dynamic`

const DYNAMIC = ['commits', 'rollbacks', 'block_read', 'block_cache', 'disk_temp', 'disk_read', 'disk_write', 'returned', 'fetched', 'inserted', 'updated', 'deleted', 'conflicts', 'deadlocks']

const item = {
  time: new Builder.NumberSchema({
    title: 'Maximum query execution time',
    unit: { from: 'ms', to: 'ms' },
    min: 0
  }),
  uptime: new Builder.NumberSchema({
    title: 'Age (since database was started)',
    unit: { from: 's', to: 's' },
    min: 0
  }),
  conn: new Builder.NumberSchema({
    title: 'Maximum number of current connections',
    min: 0
  }),
  conn_free: new Builder.NumberSchema({
    title: 'Maximum number of free connections',
    min: 0
  }),
  transaction_age: new Builder.NumberSchema({
    title: 'Maximum time for transactions',
    unit: { from: 's', to: 's' },
    min: 0
  }),
  query_age: new Builder.NumberSchema({
    title: 'Maximum time for transactions',
    unit: { from: 's', to: 's' },
    min: 0
  })
}
const schemaBase = new Builder.ObjectSchema({
  title: 'Postgres Database connection',
  item: {
    client,
    version,
    connection,
    searchPath,
    measureTime: new Builder.NumberSchema({
      unit: { from: 's', to: 's' },
      greater: 0,
      default: 30
    }),
    warn: new Builder.ObjectSchema({ title: 'Warn Check', item }),
    error: new Builder.ObjectSchema({ title: 'Error Check', item })
  },
  mandatory: ['client', 'connection', 'measureTime'],
  denyUndefined: true
});

class TestDatabasePostgresCluster extends TestDatabase {
  // general excludes for store
  public store = { exclude: ['stats_age'] }

  public async run() {
    const t = T(this.lang, 'database')

    // check data
    await Validator.load(clone(schemaBase), this.lang, { data: this.data, source: `preset:${this.path}.data` })
      .then(val => this.data = val.get())
    this.init('pg')
    this.description = t('postgrescluster.test.description', { url: this.data.connection.url.replace(/:[^:]*@/, '@') })

    // setup validation
    const valResult = new Validator(new Builder.ObjectSchema({
      item: {
        work_mem: new Builder.NumberSchema({ unit: { from: 'iB', to: 'iB' } }),
        work_mem_maintenance: new Builder.NumberSchema({ unit: { from: 'iB', to: 'iB' } }),
        shared_buffers: new Builder.NumberSchema({ unit: { from: 'iB', to: 'iB' } }),
        effective_cache_size: new Builder.NumberSchema({ unit: { from: 'iB', to: 'iB' } }),
        conn_max: new Builder.NumberSchema(),
        conn_super: new Builder.NumberSchema(),
        user_names: new Builder.ArraySchema({ split: /,\s/ }),
        user_ip: new Builder.ArraySchema({ default: [], split: /,\s/ }),
        user_app: new Builder.ArraySchema({ default: [], split: /,\s/ }),
        extensions: new Builder.ArraySchema({ split: /,\s/ })
      }
    }))
    const valWarn = new Validator(new Builder.ObjectSchema({
      title: t('core:test.validateWarn'),
      item: {
        ...(typeof this.data.warn?.time !== 'undefined' && {
          time: new Builder.NumberSchema({
            title: t('postgrescluster.test.time', { count: this.data.warn.time }),
            max: this.data.warn.time
          })
        }),
        ...(typeof this.data.warn?.uptime !== 'undefined' && {
          uptime: new Builder.NumberSchema({
            title: t('postgrescluster.test.uptime', { count: this.data.warn.uptime }),
            min: this.data.warn.uptime
          })
        }),
        ...(typeof this.data.warn?.conn !== 'undefined' && {
          conn: new Builder.NumberSchema({
            title: t('postgrescluster.test.conn', { count: this.data.warn.conn }),
            max: this.data.warn.conn
          })
        }),
        ...(typeof this.data.warn?.conn_free !== 'undefined' && {
          conn_free: new Builder.NumberSchema({
            title: t('postgrescluster.test.conn_free', { count: this.data.warn.conn_free }),
            min: this.data.warn.conn_free
          })
        }),
        ...(typeof this.data.warn?.transaction_age !== 'undefined' && {
          transaction_age: new Builder.NumberSchema({
            title: t('postgrescluster.test.transaction_age', { count: this.data.warn.transaction_age }),
            max: this.data.warn.transaction_age
          })
        }),
        ...(typeof this.data.warn?.query_age !== 'undefined' && {
          query_age: new Builder.NumberSchema({
            title: t('postgrescluster.test.query_age', { count: this.data.warn.query_age }),
            max: this.data.warn.query_age
          })
        })
      }
    }))
    const valError = new Validator(new Builder.ObjectSchema({
      title: t('core:test.validateError'),
      item: {
        ...(typeof this.data.error?.time !== 'undefined' && {
          time: new Builder.NumberSchema({
            title: t('postgrescluster.test.time', { count: this.data.error.time }),
            max: this.data.error.time
          })
        }),
        ...(typeof this.data.error?.uptime !== 'undefined' && {
          uptime: new Builder.NumberSchema({
            title: t('postgrescluster.test.uptime', { count: this.data.error.uptime }),
            min: this.data.error.uptime
          })
        }),
        ...(typeof this.data.error?.conn !== 'undefined' && {
          conn: new Builder.NumberSchema({
            title: t('postgrescluster.test.conn', { count: this.data.error.conn }),
            max: this.data.error.conn
          })
        }),
        ...(typeof this.data.error?.conn_free !== 'undefined' && {
          conn_free: new Builder.NumberSchema({
            title: t('postgrescluster.test.conn_free', { count: this.data.error.conn_free }),
            min: this.data.error.conn_free
          })
        }),
        ...(typeof this.data.error?.transaction_age !== 'undefined' && {
          transaction_age: new Builder.NumberSchema({
            title: t('postgrescluster.test.transaction_age', { count: this.data.error.transaction_age }),
            max: this.data.error.transaction_age
          })
        }),
        ...(typeof this.data.error?.query_age !== 'undefined' && {
          query_age: new Builder.NumberSchema({
            title: t('postgrescluster.test.query_age', { count: this.data.error.query_age }),
            max: this.data.error.query_age
          })
        })
      }
    }))

    // get data
    const pool = createPool(this.data.connection.url);
    this.result = { connect: false }
    const start = new Date().getTime()
    this.logRequest(`Connect: ${this.data.connection.url}`)
    return pool.connect(async conn => {
      this.result.time = new Date().getTime() - start
      this.result.connect = true
      this.logRequest(SQL_VERSION.sql)
      if (SQL_VERSION.values.length) this.logData(SQL_VERSION.values)
      const version = await conn.one(SQL_VERSION)
      this.logReturn(version)
      this.result.version = parseInt(version.server_version_num!.toString())
      if (this.result.version < 90000) throw new Error('Postgres can only be analyzed starting with version 9.0!')
      // get static data
      this.logRequest(SQL_ANALYSIS.sql)
      if (SQL_ANALYSIS.values.length) this.logData(SQL_ANALYSIS.values)
      const data = await conn.one(SQL_ANALYSIS)
      this.logReturn(data)
      this.result = { ...this.result, ...data }
    })
      // close connection
      .then(() => pool.end())
      // optimize
      .then(() => valResult.load(this.lang, { data: this.result }))
      .then(val => {
        val.conn_free = val.conn_max - val.conn
        val.block_ratio = val.block_read === 0 ? 1 : val.block_cache / (val.block_cache + val.block_read)
        val.commit_ratio = val.rollbacks === 0 ? 1 : val.commits / (val.commits + val.rollbacks)
        // add time diffs
        const old: any = this.cache.get(`!${this.path}`)
        this.cache.set(`!${this.path}`, val, CACHE_TTL)
        if (old && val.stats_age > old.stats_age) { // calculate
          const timediff = val.stats_age - old.stats_age
          DYNAMIC.forEach(k => val[`${k}_diff`] = (val[k] - old[k]) / timediff)
        }
        // set as result
        this.result = val
      })
      .then(() => {
        this.logResult(this.result)
        this.status = 'OK'
      })
      // check error and warning
      .then(() => {
        const ds = { data: this.result, source: 'preset:result' }
        return Promise.resolve().then(() => {
          if (!this.data.error) return
          this.logValidation(valError.describe())
          return valError.load(this.lang, ds)
        })
          .then(_val => {
            if (!this.data.warn) return
            this.logValidation(valWarn.describe())
            return valWarn.load(this.lang, ds)
              .catch(e => {
                this.status = 'WARN'
                this.error = e
              })
          })
          .catch(e => {
            this.status = 'ERROR'
            this.error = e
          })
      })
  }
}

export default TestDatabasePostgresCluster
export { TestDatabasePostgresCluster as Test, schemaBase as data } // for interactive mode
