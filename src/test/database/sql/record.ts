import Validator from '@alinex/validator';
import * as Builder from '@alinex/validator/lib/schema';
import { clone } from '@alinex/data'
import { sql } from 'slonik';
import { raw } from 'slonik-sql-tag-raw';

import { TestDatabase } from '..'
import { T } from '../../../i18n';

const item = {
  '*': new Builder.ObjectSchema({
    title: 'Result field check',
    item: {
      min: new Builder.NumberSchema({ title: 'Minimum value' }),
      max: new Builder.NumberSchema({ title: 'Maximum value' }),
      before: new Builder.DateTimeSchema({ title: 'Maximum date' }),
      after: new Builder.DateTimeSchema({ title: 'Minimum date' }),
      allow: new Builder.RegExpSchema({ title: 'Allowed regular expression' }),
      disallow: new Builder.RegExpSchema({ title: 'Disallowed regular expression' })
    },
    min: 1,
    combination: [
      // only checks for one type
      { key: 'min', without: ['before', 'after', 'allow', 'disallow'] },
      { key: 'max', without: ['before', 'after', 'allow', 'disallow'] },
      { key: 'before', without: ['min', 'max', 'allow', 'disallow'] },
      { key: 'after', without: ['min', 'max', 'allow', 'disallow'] },
    ]
  }),
  time: new Builder.NumberSchema({
    title: 'Maximum query execution time',
    unit: { from: 'ms', to: 'ms' },
    min: 0
  })
}

const pg = new Builder.ObjectSchema({
  title: 'Postgres Database connection',
  item: {
    client: new Builder.StringSchema({ allow: ['pg'] }),
    version: new Builder.StringSchema({ allow: [/\d+\.\d+/] }),
    connection: new Builder.ObjectSchema({
      item: {
        user: new Builder.StringSchema(),
        password: new Builder.StringSchema(),
        host: new Builder.StringSchema({ default: 'localhost' }),
        port: new Builder.PortSchema({ default: 5432 }),
        database: new Builder.StringSchema()
      }
    }),
    searchPath: new Builder.ArraySchema({
      item: {
        '*': new Builder.StringSchema()
      }
    }),
    sql: new Builder.StringSchema({ title: 'Query to execute', allow: [/select/i] }),
    warn: new Builder.ObjectSchema({ title: 'Warn Check', item }),
    error: new Builder.ObjectSchema({ title: 'Error Check', item })
  },
  mandatory: ['client', 'connection', 'sql'],
  denyUndefined: true
});
const mysql = new Builder.ObjectSchema({
  title: 'MySQL Database connection',
  item: {
    client: new Builder.StringSchema({ allow: ['mysql'] }),
    version: new Builder.StringSchema({ allow: [/\d+\.\d+/] }),
    connection: new Builder.ObjectSchema({
      item: {
        user: new Builder.StringSchema(),
        password: new Builder.StringSchema(),
        host: new Builder.StringSchema({ default: 'localhost' }),
        port: new Builder.PortSchema({ default: 3306 }),
        database: new Builder.StringSchema()
      }
    }),
    sql: new Builder.StringSchema({ title: 'Query to execute', allow: [/select/i] }),
    warn: new Builder.ObjectSchema({ title: 'Warn Check', item, denyUndefined: true }),
    error: new Builder.ObjectSchema({ title: 'Error Check', item, denyUndefined: true })
  },
  mandatory: ['client', 'connection', 'sql'],
  denyUndefined: true
});
const mysql2 = new Builder.ObjectSchema({
  title: 'MySQL2 Database connection',
  item: {
    client: new Builder.StringSchema({ allow: ['mysql2'] }),
    version: new Builder.StringSchema({ allow: [/\d+\.\d+/] }),
    connection: new Builder.ObjectSchema({
      item: {
        user: new Builder.StringSchema(),
        password: new Builder.StringSchema(),
        host: new Builder.StringSchema({ default: 'localhost' }),
        port: new Builder.PortSchema({ default: 3306 }),
        database: new Builder.StringSchema()
      }
    }),
    sql: new Builder.StringSchema({ title: 'Query to execute', allow: [/select/i] }),
    warn: new Builder.ObjectSchema({ title: 'Warn Check', item, denyUndefined: true }),
    error: new Builder.ObjectSchema({ title: 'Error Check', item, denyUndefined: true })
  },
  mandatory: ['client', 'connection', 'sql'],
  denyUndefined: true
});
const oracledb = new Builder.ObjectSchema({
  title: 'oracledb Database connection',
  item: {
    client: new Builder.StringSchema({ allow: ['oracledb'] }),
    version: new Builder.StringSchema({ allow: [/\d+\.\d+/] }),
    connection: new Builder.ObjectSchema({
      item: {
        user: new Builder.StringSchema(),
        password: new Builder.StringSchema(),
        host: new Builder.StringSchema({ default: 'localhost' }),
        port: new Builder.PortSchema({ default: 1521 }),
        database: new Builder.StringSchema()
      }
    }),
    sql: new Builder.StringSchema({ title: 'Query to execute', allow: [/select/i] }),
    warn: new Builder.ObjectSchema({ title: 'Warn Check', item, denyUndefined: true }),
    error: new Builder.ObjectSchema({ title: 'Error Check', item, denyUndefined: true })
  },
  mandatory: ['client', 'connection', 'sql'],
  denyUndefined: true
});
const mssql = new Builder.ObjectSchema({
  title: 'Microsoft SQL Database connection',
  item: {
    client: new Builder.StringSchema({ allow: ['mssql'] }),
    version: new Builder.StringSchema({ allow: [/\d+\.\d+/] }),
    connection: new Builder.ObjectSchema({
      item: {
        user: new Builder.StringSchema(),
        password: new Builder.StringSchema(),
        host: new Builder.StringSchema({ default: 'localhost' }),
        port: new Builder.PortSchema({ default: 1433 }),
        database: new Builder.StringSchema()
      }
    }),
    sql: new Builder.StringSchema({ title: 'Query to execute', allow: [/select/i] }),
    warn: new Builder.ObjectSchema({ title: 'Warn Check', item, denyUndefined: true }),
    error: new Builder.ObjectSchema({ title: 'Error Check', item, denyUndefined: true })
  },
  mandatory: ['client', 'connection', 'sql'],
  denyUndefined: true
});
const sqlite3 = new Builder.ObjectSchema({
  title: 'SQLite Database connection',
  item: {
    client: new Builder.StringSchema({ allow: ['sqlite3'] }),
    connection: new Builder.ObjectSchema({
      item: {
        filename: new Builder.FileSchema({ readable: true })
      },
      mandatory: true,
      denyUndefined: true
    }),
    sql: new Builder.StringSchema({ title: 'Query to execute', allow: [/select/i] }),
    warn: new Builder.ObjectSchema({ title: 'Warn Check', item, denyUndefined: true }),
    error: new Builder.ObjectSchema({ title: 'Error Check', item, denyUndefined: true })
  },
  mandatory: ['client', 'connection', 'sql'],
  denyUndefined: true
});
const schemaBase = new Builder.LogicSchema({
  operator: 'or',
  check: [pg, mysql, mysql2, oracledb, mssql, sqlite3]
})

class TestDatabaseSQLRecord extends TestDatabase {
  public async run() {
    const t = T(this.lang, 'database')

    // check data
    await Validator.load(clone(schemaBase), this.lang, { data: this.data, source: `preset:${this.path}.data` })
      .then(val => this.data = val.get())
    this.init()
    this.description = t('sqlrecord.test.description', { url: this.data.connection.url.replace(/:[^:]*@/, '@') })

    // setup validation
    let item: any = {}
    if (this.data.warn) {
      Object.keys(this.data.warn).forEach(key => {
        const c = this.data.warn[key]
        if (c.min || c.max) {
          item[key] = new Builder.NumberSchema({
            min: c.min,
            max: c.max
          })
        } else if (c.before || c.after) {
          item[key] = new Builder.DateTimeSchema({
            min: c.before,
            max: c.after
          })
        } else if (c.allow || c.disallow) {
          item[key] = new Builder.StringSchema({
            allow: c.allow,
            disallow: c.disallow
          })
        }
      })
    }
    const valWarn = new Validator(new Builder.ObjectSchema({
      title: t('core:test.validateWarn'),
      item: {
        ...item,
        ...(typeof this.data.warn?.time !== 'undefined' && {
          time: new Builder.NumberSchema({
            title: t('sqlrecord.test.time', { count: this.data.warn.time }),
            max: this.data.warn.time
          })
        })
      }
    }))

    item = {}
    if (this.data.error) {
      Object.keys(this.data.error).forEach(key => {
        const c = this.data.error[key]
        if (c.min || c.max) {
          item[key] = new Builder.NumberSchema({
            min: c.min,
            max: c.max
          })
        } else if (c.before || c.after) {
          item[key] = new Builder.DateTimeSchema({
            min: c.before,
            max: c.after
          })
        } else if (c.allow || c.disallow) {
          item[key] = new Builder.StringSchema({
            allow: c.allow,
            disallow: c.disallow
          })
        }
      })
    }
    const valError = new Validator(new Builder.ObjectSchema({
      title: t('core:test.validateError'),
      item: {
        ...item,
        ...(typeof this.data.error?.time !== 'undefined' && {
          time: new Builder.NumberSchema({
            title: t('sqlrecord.test.time', { count: this.data.error.time }),
            max: this.data.error.time
          })
        })
      }
    }))

    // get data
    let prom: Promise<void>
    if (this.data.client === 'pg') {
      prom = this.pgPool().connect(async conn => {
        this.logRequest(`Connect: ${this.data.connection.url}`)
        const query = sql`${raw(this.data.sql)}`
        this.logRequest(query.sql)
        const start = Date.now()
        if (query.values.length) this.logData(query.values)
        const data = await conn.one(query)
        const time = Date.now() - start
        this.logReturn(data)
        this.result = { ...data, time }
      })
    } else {
      const knex = this.connectKnex()
      prom = this.queryKnex(knex, this.data.sql)
        .then(res => {
          this.result = { ...res.data, time: res.time }
        })
    }

    // log result
    return prom.then(() => {
      this.logResult(this.result)
      this.status = 'OK'
    })
      // check error and warning
      .then(() => {
        const ds = { data: this.result, source: 'preset:result' }
        return Promise.resolve().then(() => {
          if (!this.data.error) return
          this.logValidation(valError.describe())
          return valError.load(this.lang, ds)
        })
          .then(_val => {
            if (!this.data.warn) return
            this.logValidation(valWarn.describe())
            return valWarn.load(this.lang, ds)
              .catch(e => {
                this.status = 'WARN'
                this.error = e
              })
          })
          .catch(e => {
            this.status = 'ERROR'
            this.error = e
          })
      })
  }
}

export default TestDatabaseSQLRecord
export { TestDatabaseSQLRecord as Test, schemaBase as data } // for interactive mode
