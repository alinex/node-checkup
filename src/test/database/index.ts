import Knex from 'knex';
import { MongoClient } from 'mongodb';

import { loadNamespace } from '../../i18n'
import Test from '..'
import database from '../../database'

loadNamespace('database')

export class TestDatabase extends Test {

  protected connectKnex() {
    this.init();
    //return Knex(this.data);
    return Knex({ ...this.data, pool: { min: 0, max: 1 } });
  }

  protected queryKnex(knex = this.connectKnex(), sql: string) {
    const start = Date.now()
    this.logRequest(sql)
    return knex.raw(sql)
      .then(res => {
        const time = Date.now() - start
        const data = res.rows[0] || undefined
        this.logReturn(data)
        return { time, data }
      })
  }

  protected pgPool(client?: string) {
    this.init(client)
    return database.pgPool(this.data)
  }

  protected init(client?: string) {
    if (client) this.data.client = client
    database.init(this.data)
    return this.data.connection.url
  }

  protected async connectMongo(): Promise<MongoClient> {
    this.init('mongodb')
    const setup = this.data.connection
    this.logRequest('connect to ' + setup.url)
    // open connection
    return MongoClient.connect(setup.url, {
      useNewUrlParser: true,
      useUnifiedTopology: true
    })
  }

}

export default TestDatabase
