import Validator from '@alinex/validator';
import * as Builder from '@alinex/validator/lib/schema';
import { clone } from '@alinex/data'

import { TestDatabase } from '.'
import { T } from '../../i18n';
import FixDatabaseMongoDB from '../../fix/database/mongodb';
import FixNetTcp from '../../fix/net/tcp';
import FixNetPing from '../../fix/net/ping';

const item = {
  time: new Builder.NumberSchema({
    title: 'Maximum connection time',
    unit: { from: 'ms', to: 'ms' },
    min: 0
  }),
  uptime: new Builder.NumberSchema({
    title: 'Age (since database was started)',
    unit: { from: 's', to: 's' },
    min: 0
  }),
  conn: new Builder.NumberSchema({
    title: 'Maximum number of connections',
    integer: true,
    min: 1
  }),
  conn_free: new Builder.NumberSchema({
    title: 'Minimum number of free connections',
    integer: true,
    min: 1
  }),
  cache_free: new Builder.NumberSchema({
    title: 'Minimum number of free bytes in cache',
    unit: { from: 'B', to: 'B' },
    min: 1
  }),
  read_latency: new Builder.NumberSchema({
    title: 'Maximum latency for read operations',
    unit: { from: 'ms', to: 'ms' },
    min: 1
  }),
  write_latency: new Builder.NumberSchema({
    title: 'Maximum latency for write operations',
    unit: { from: 'ms', to: 'ms' },
    min: 1
  }),
  writable: new Builder.BooleanSchema({
    title: 'Should the database be writable',
    tolerant: true,
    value: true
  })
}
const schemaBase = new Builder.ObjectSchema({
  item: {
    connection: new Builder.ObjectSchema({
      title: 'Mongo Database connection',
      item: {
        user: new Builder.StringSchema(),
        password: new Builder.StringSchema(),
        host: new Builder.StringSchema({ default: 'localhost' }),
        port: new Builder.PortSchema({ default: 27017 }),
        database: new Builder.StringSchema()
      },
      mandatory: ['host', 'port', 'database']
    }),
    warn: new Builder.ObjectSchema({
      title: 'Warn Check',
      item,
      denyUndefined: true,
      default: { time: 500, read_latency: 100, write_latency: 100, cache_free: 10485760 }
    }),
    error: new Builder.ObjectSchema({
      title: 'Error Check',
      item,
      denyUndefined: true,
      default: { time: 2000, conn_free: 100 }
    })
  },
  mandatory: ['connection', 'warn', 'error'],
  denyUndefined: true
})

class TestDatabaseMongo extends TestDatabase {
  public async run() {
    const t = T(this.lang, 'database')

    // check data
    await Validator.load(clone(schemaBase), this.lang, { data: this.data, source: `preset:${this.path}.data` })
      .then(val => this.data = val.get())
    this.init('mongodb')
    this.description = t('mongodb.test.description', { url: this.data.connection.url.replace(/:[^:]*@/, '@') })

    // setup validation
    const valWarn = new Validator(new Builder.ObjectSchema({
      title: t('core:test.validateWarn'),
      item: {
        ...(typeof this.data.warn?.time !== 'undefined' && {
          time: new Builder.NumberSchema({
            title: t('mongodb.test.time', { count: this.data.warn.time }),
            max: this.data.warn.time
          })
        }),
        ...(typeof this.data.warn?.uptime !== 'undefined' && {
          uptime: new Builder.NumberSchema({
            title: t('mongodb.test.uptime', { count: this.data.warn.uptime }),
            min: this.data.warn.uptime
          })
        }),
        ...(typeof this.data.warn?.conn !== 'undefined' && {
          conn: new Builder.NumberSchema({
            title: t('mongodb.test.conn', { count: this.data.warn.conn }),
            max: this.data.warn.conn
          })
        }),
        ...(typeof this.data.warn?.conn_free !== 'undefined' && {
          conn_free: new Builder.NumberSchema({
            title: t('mongodb.test.conn_free', { count: this.data.warn.conn_free }),
            min: this.data.warn.conn_free
          })
        }),
        ...(typeof this.data.warn?.cache_free !== 'undefined' && {
          cache_free: new Builder.NumberSchema({
            title: t('mongodb.test.cache_free', { count: this.data.warn.cache_free }),
            min: this.data.warn.cache_free
          })
        }),
        ...(typeof this.data.warn?.read_latency !== 'undefined' && {
          read_latency: new Builder.NumberSchema({
            title: t('mongodb.test.read_latency', { count: this.data.warn.read_latency }),
            max: this.data.warn.read_latency
          })
        }),
        ...(typeof this.data.warn?.write_latency !== 'undefined' && {
          write_latency: new Builder.NumberSchema({
            title: t('mongodb.test.write_latency', { count: this.data.warn.write_latency }),
            max: this.data.warn.write_latency
          })
        }),
        ...(this.data.warn?.writable && {
          read_only: new Builder.BooleanSchema({
            title: t('mongodb.test.writable', { count: this.data.warn.writable }),
            value: !this.data.warn.writable
          })
        })
      }
    }))
    const valError = new Validator(new Builder.ObjectSchema({
      title: t('core:test.validateError'),
      item: {
        ...(typeof this.data.error?.time !== 'undefined' && {
          time: new Builder.NumberSchema({
            title: t('mongodb.test.time', { count: this.data.error.time }),
            max: this.data.error.time
          })
        }),
        ...(typeof this.data.error?.uptime !== 'undefined' && {
          uptime: new Builder.NumberSchema({
            title: t('mongodb.test.uptime', { count: this.data.error.uptime }),
            min: this.data.error.uptime
          })
        }),
        ...(typeof this.data.error?.conn !== 'undefined' && {
          conn: new Builder.NumberSchema({
            title: t('mongodb.test.conn', { count: this.data.error.conn }),
            max: this.data.error.conn
          })
        }),
        ...(typeof this.data.error?.conn_free !== 'undefined' && {
          conn_free: new Builder.NumberSchema({
            title: t('mongodb.test.conn_free', { count: this.data.error.conn_free }),
            min: this.data.error.conn_free
          })
        }),
        ...(typeof this.data.error?.cache_free !== 'undefined' && {
          cache_free: new Builder.NumberSchema({
            title: t('mongodb.test.cache_free', { count: this.data.error.cache_free }),
            min: this.data.error.cache_free
          })
        }),
        ...(typeof this.data.error?.read_latency !== 'undefined' && {
          read_latency: new Builder.NumberSchema({
            title: t('mongodb.test.read_latency', { count: this.data.error.read_latency }),
            max: this.data.error.read_latency
          })
        }),
        ...(typeof this.data.error?.write_latency !== 'undefined' && {
          write_latency: new Builder.NumberSchema({
            title: t('mongodb.test.write_latency', { count: this.data.error.write_latency }),
            max: this.data.error.write_latency
          })
        }),
        ...(this.data.error?.writable && {
          read_only: new Builder.BooleanSchema({
            title: t('mongodb.test.writable', { count: this.data.error.writable }),
            value: !this.data.error.writable
          })
        })
      }
    }))

    // get data
    const start = Date.now()
    return this.connectMongo()
      .then(async client => {
        let result: any
        try {
          const db = client.db(this.data.connection.database)
          this.logRequest('db.ping')
          let server = await db.command({ ping: 1 })
          this.logReturn(server)
          const time = Date.now() - start
          this.logRequest('db.serverStatus')
          server = await db.command({ serverStatus: 1 })
          this.logReturn(server)
          result = {
            time, // ms
            connect: server.ok ? true : false,
            version: server.version,
            pid: server.pid,
            uptime: server.uptime, // s
            warnings: server.asserts.warning,
            memory: server.mem.resident, // MiB
            conn: server.connections.current,
            conn_free: server.connections.available - server.connections.current,
            cache_free: server.wiredTiger.cache['maximum bytes configured'] - server.wiredTiger.cache['bytes currently in the cache'], // B
            queue_per_client: server.globalLock.currentQueue.total ? server.globalLock.currentQueue.total / server.globalLock.activeClients.total : 0,
            read_latency: server.opLatencies.reads.latency / 1000,
            write_latency: server.opLatencies.writes.latency / 1000,
          } // maybe add server.metrics

          this.logRequest('db.isMaster')
          server = await db.command({ isMaster: 1 })
          this.logReturn(server)
          result.is_master = server.ismaster
          result.read_only = server.readOnly
          this.logRequest('db.dbStats')
          server = await db.command({ dbStats: 1 })
          this.logReturn(server)
          result.collections = server.collections
          result.views = server.views
          result.objects = server.objects
          result.indexes = server.indexes
          result.size_data = server.dataSize
          result.size_storage = server.storageSize
          result.size_index = server.indexSize
        } catch (e) {
          client.close();
          return e
        }
        client.close();
        return result
      })
      // validate result
      .then(val => {
        this.result = val
        this.logResult(val)
        this.status = 'OK'
      })

      // check error and warning
      .then(() => {
        const ds = { data: this.result, source: 'preset:result' }
        return Promise.resolve().then(() => {
          if (!this.data.error) return
          this.logValidation(valError.describe())
          return valError.load(this.lang, ds)
        })
          .then(_val => {
            if (!this.data.warn) return
            this.logValidation(valWarn.describe())
            return valWarn.load(this.lang, ds)
              .catch(e => {
                this.status = 'WARN'
                this.error = e
                if (e.work.pos !== 'time') {
                  this.fixes.push(new FixNetTcp(this, { host: this.data.connection.host, port: this.data.connection.port }))
                  this.fixes.push(new FixNetPing(this, { host: this.data.connection.host }))
                  this.fixes.push(new FixDatabaseMongoDB(this))
                }
              })
          })
          .catch(e => {
            this.status = 'ERROR'
            this.error = e
            if (e.work.pos !== 'time') {
              this.fixes.push(new FixNetTcp(this, { host: this.data.connection.host, port: this.data.connection.port }))
              this.fixes.push(new FixNetPing(this, { host: this.data.connection.host }))
              this.fixes.push(new FixDatabaseMongoDB(this))
            }
          })
      })
  }
}

export default TestDatabaseMongo
export { TestDatabaseMongo as Test, schemaBase as data } // for interactive mode
