import * as yargs from 'yargs';
import core from '@alinex/core';
import chalk from 'chalk'
import Validator from '@alinex/validator';
import { DateTime } from "luxon";

import Checkup from './index'
import { T, language } from './i18n';
import { Runner, statusColor } from './runner';
import schema from './setupSchema';

// Support quiet mode through switch
let quiet = false;
for (const a of ['--get-yargs-completions', 'bashrc', '-q', '--quiet']) {
  if (process.argv.includes(a)) quiet = true;
}

const lang = language() // use default
const t = T(lang)

if (!quiet) console.log(core.logo('Checkup System'));

process.on('uncaughtException', err => core.exit(err, 1));
process.on('unhandledRejection', (err: any) => core.exit(err || "Unhandled Rejection", 1));

yargs
  .env('CHECKUP')
  .usage(t('cli.usage'))
  // examples
  .example('$0 --paths alinex.host.load', t('cli.example.path'))
  .example('$0 --paths alinex.host --tag extern', t('cli.example.tag'))
  .example('$0 --paths alinex.host -e', t('cli.example.extended'))
  .example('$0 --autofix', t('cli.example.autofix'))
  // options
  .option('case', {
    alias: 'c',
    describe: t('cli.option.case'),
    type: 'string'
  })
  .option('paths', {
    alias: 'p',
    describe: t('cli.option.paths'),
    type: 'array'
  })
  .option('tags', {
    alias: 't',
    describe: t('cli.option.tags'),
    type: 'array'
  })
  .option('autofix', {
    alias: 'f',
    describe: t('cli.option.autofix'),
    type: 'boolean'
  })
  .option('no-fix', {
    alias: 'F',
    describe: t('cli.option.nofix'),
    type: 'boolean'
  })
  .option('progress', {
    describe: t('cli.option.progress'),
    type: 'boolean',
    count: true
  })
  .option('verbose', {
    alias: 'v',
    describe: t('cli.option.verbose'),
    type: 'boolean',
    count: true
  })
  .option('extend', {
    alias: 'e',
    describe: t('cli.option.extend'),
    type: 'boolean'
  })
  .option('setup', {
    alias: 's',
    describe: t('cli.option.setup'),
    type: 'string'
  })
  .option('scheduler', {
    describe: t('cli.option.scheduler'),
    type: 'boolean'
  })
  .option('interactive', {
    alias: 'i',
    describe: t('cli.option.interactive'),
    type: 'boolean'
  })
  .option('quiet', {
    alias: 'q',
    describe: t('cli.option.quiet'),
    type: 'boolean'
  })
  .wrap(yargs.terminalWidth())
  .alias('h', 'help')
  .alias('V', 'version')
  .help()
  .completion('bashrc-script')
  .strict()
  .epilog('Copyright Alexander Schilling 2020 - 2021');

main(yargs.argv);

async function loadConfig(file?: string) {
  if (!file) return {}
  // get config url
  if (!file.match(/:\/\//)) {
    if (!file.startsWith('/')) file = `${process.cwd()}/${file}`
    file = `file://${file}`
  }
  const val = new Validator(schema, { source: file });
  return val.load()
}

async function main(args: any) {
  // load setup if given
  const setup = await loadConfig(args.setup)
  if (args.case) setup.config = args.case
  if (args.verbose) setup.verbose = args.verbose
  if (typeof args.extend !== 'undefined') setup.extend = args.extend
  if (typeof args.noFix !== 'undefined') setup.analyze = !args.noFix
  if (typeof args.autofix !== 'undefined') setup.autofix = args.autofix
  if (typeof setup.analyze === 'undefined') setup.analyze = true
  const checkup = new Checkup(setup)
  process.removeAllListeners('SIGINT')
  if (args.scheduler)
    return checkup.scheduler({
      paths: args.paths,
      tags: args.tags
    }, (runner: Runner) => {
      runner.on('test', (name) => console.log(DateTime.local().toISO(), runner.report('console', name)))
      runner.on('analyze', (name) => console.log(DateTime.local().toISO(), runner.report('console', name)))
      runner.on('repair', (name) => console.log(DateTime.local().toISO(), runner.report('console', name)))
    })
  if (args.interactive)
    return import('./interactive').then(m => new m.default(checkup, args.paths, args.tags).command())
      .catch((e) => {
        console.error(chalk.bold.red(e || "Terminated by User"))
        if (e.stack) console.error(e.stack.replace(/^.*?\n/, ''))
        if (e.describe) console.error(e.describe())
      })

  // run action
  const runner = await checkup.create({
    paths: args.paths,
    tags: args.tags
  })
  if (args.progress !== 'undefined' ? args.progress : setup.progress) {
    runner.once('repair', () => process.stderr.write('\n')) // run new progress bar on first repair event
    runner.on('step', (pos, max) => {
      const color = statusColor[runner.report('group', '', { recursive: false }).status] || statusColor['undefined']
      process.stderr.cursorTo(0)
      process.stderr.write(color(t('cli.progress', { pos, max, percent: Math.round(100 / max * pos) })))
    })
    await runner.start()
      .then(() => {
        process.stderr.clearLine(-1)
        process.stderr.cursorTo(0)
      })
      .then(() => console.log(runner.report('console')))
      .catch((e: any) => {
        console.error(chalk.bold.red(e))
        console.error(e.stack.replace(/^.*?\n/, ''))
        if (e.describe) console.error(e.describe())
      })
  } else {
    runner.on('test', (name) => console.log(runner.report('console', name)))
    runner.on('analyze', (name) => console.log(runner.report('console', name)))
    runner.on('repair', (name) => console.log(runner.report('console', name)))
    await runner.start()
      .then(() => console.log(runner.report('console', '', { recursive: false })))
      .catch((e: any) => {
        console.error(chalk.bold.red(e))
        console.error(e.stack.replace(/^.*?\n/, ''))
        if (e.describe) console.error(e.describe())
      })
  }

  process.exit(0) // in case some pending connections are still existing
}
