import StringSchema from '@alinex/validator/lib/type/string'
import ArraySchema from '@alinex/validator/lib/type/array'
import ObjectSchema from '@alinex/validator/lib/type/object'
import NumberSchema from '@alinex/validator/lib/type/number'
import BooleanSchema from '@alinex/validator/lib/type/boolean'

export default new ObjectSchema({
  title: 'Cases for alinex checkup',
  detail: 'The configuration defines all possible test cases.',
  item: {
    '*': new ObjectSchema({
      item: {
        title: new StringSchema(),
        description: new StringSchema(),
        tags: new ArraySchema({
          makeArray: true,
          split: /\s*,\s*/,
          item: { '*': new StringSchema() }
        }),
        group: new ObjectSchema({
          item: { class: new StringSchema() },
          mandatory: ['class']
        }),
        test: new StringSchema(),
        data: new ObjectSchema(),
        cache: new NumberSchema({ min: 0 }),
        fix: new ObjectSchema(),
        autofix: new BooleanSchema(),
        cron: new StringSchema({ allow: ['none', /^(?:[-*,/0-9]+ ){4}[-*,/0-9]+$/] })
      },
      mandatory: ['title'],
      denyUndefined: true
    }) // TODO test me
  }
});
