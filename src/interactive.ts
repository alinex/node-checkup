import chalk from 'chalk'
import { prompt } from 'enquirer'
import { lstatSync, readdirSync } from 'fs';

import Checkup from './index'
import Runner from './runner'
import { T, language } from './i18n';

const testFunctions: any[] = [{ name: '-', message: '(group without direct test)' }]
const testDefinitions: any = {}

readdirSync(`${__dirname}/test`)
  .filter(d => lstatSync(`${__dirname}/test/${d}`).isDirectory())
  .forEach(d => {
    readdirSync(`${__dirname}/test/${d}`)
      .map(f => f.replace(/\.[tj]s$/, ''))
      .filter(f => f !== 'index')
      .forEach(f => {
        const full = `./${d}/${f}`
        import(full.replace(/^\.\//, `${__dirname}/test/`))
          .then(m => {
            testFunctions.push({ name: full, message: `${Object.getPrototypeOf(m.Test).name}/${m.Test.name}` })
            testDefinitions[full] = {
              data: m.data.export(),
              fix: m.fix?.export()
            }
          })
      });
  });

function schema2prompt(schema: any, name: string, initial: any) {
  const questions: any[] = []
  Object.keys(schema.item).forEach(k => {
    const v = schema.item[k]
    const fullname = `${name}.${k}`
    const q: any = { type: 'input', name: fullname, message: v.title || fullname, initial: initial[k] }
    switch(v.SCHEMA_TYPE) {
      case 'Object':
        // go deeper
        questions.push(...schema2prompt(v, fullname, initial[k] || {}))
        return
      case 'Number':
        if (v.unit) {
          q.message += ` (in ${v.unit.from})`
        } else if (!v.sanitize) {
          q.type = 'numeral'
          if (v.integer) q.float = false
        }
        break
    }
    // add prompt
    questions.push(q)
  })
  return questions
}

class Interactive {
  // private ui = new inquirer.ui.BottomBar();
  private checkup: Checkup;
  private _lang: string = 'en';
  private commands: any[] = [];
  private status: string = "";
  private paths: string[];
  private tags: string[];
  private t: any

  constructor(initial: Checkup, paths: string[] = [], tags: string[] = []) {
    this.checkup = initial
    this.paths = paths
    this.tags = tags
    this.lang = language()
  }

  set lang (lang: string) {
    this._lang = lang
    this.t = T(<string>lang)
    this.commands = [
      { name: 'list', message: this.t('interactive.list.cmd') },
      { name: 'run', message: this.t('interactive.run.cmd') },
      { name: 'add', message: this.t('interactive.add.cmd') },
      { name: 'edit', message: this.t('interactive.edit.cmd') },
      { name: 'remove', message: this.t('interactive.remove.cmd') },
      { name: 'clear', message: this.t('interactive.clear.cmd') },
      { name: 'config', message: this.t('interactive.config.cmd') },
      { name: 'help', message: this.t('interactive.help.cmd') },
      { name: 'exit', message: this.t('interactive.exit.cmd') }
    ]
    this.checkup.init.then(_ => this.setStatus())
  }
  get lang () { return this._lang }

  private setStatus () {
    let status = `Checkup: Loaded ${Object.keys(this.checkup.config).length} cases - lang ${this.lang} verbose ${this.checkup.verbose}`
    if (this.checkup.extend) status += ' extend'
    if (this.checkup.analyze) status += ' analyze'
    if (this.checkup.autofix) status += ' repair'
    status += ` - ${this.paths?.join(', ') || 'all'}`
    if (this.tags.length) status += ` with tags ${this.tags.join(', ')}`
    this.status = chalk.bgGrey.black(status);
  }

  public async command (args?: any): Promise<void> {
    await this.checkup.init
    console.log(this.status)
    return prompt({
      type: 'select',
      name: 'cmd',
      message: this.t('interactive.command'),
      choices: this.commands
    })
      .then(async (answers: any) => {
        const cmd: 'list' | 'run' | 'add' | 'edit' | 'remove' | 'clear' | 'config' | 'help' | 'exit' = answers.cmd
        await this[cmd]()
        console.log('')
        return this.command()
      })
  }

  private async exit () {
    console.log('')
    console.log(this.t('interactive.exit.bye'))
    process.exit(0)
  }

  private async help () {
    console.log('')
    console.log(this.t('interactive.help.list'))
    this.commands.filter(e => !['help', 'exit'].includes(e.value)).forEach(e => console.log(`- ${e.name}`))
    console.log(this.t('interactive.help.common'))
    this.commands.filter(e => ['help', 'exit'].includes(e.value)).forEach(e => console.log(`- ${e.name}`))
  }

  private async list () {
    console.log('')
    const config = this.checkup.config
    if (!Object.keys(config).length) {
      console.log(this.t('interactive.list.empty'))
      return
    }
    console.log(this.t('interactive.list.heading'))
    Object.keys(config)
      .sort()
      .forEach((k: any) => {
        const c = config[k]
        const indent = "  ".repeat(c.parent.length - 1)
        console.log(`${indent}- ${c.title} (${k})`)
      })
  }

  private async run () {
    let tags: string[] = []
    Object.keys(this.checkup.config).forEach(k => tags.push(...this.checkup.config[k].tags))
    tags = [...new Set(tags)]
    if (!Object.keys(this.checkup.config).length) {
      console.log()
      console.log(this.t('interactive.list.empty'))
      return
    }
    return prompt([
      {
        type: 'autocomplete',
        name: 'path',
        message: this.t('interactive.run.path'),
        // @ts-ignore
        choices: Object.keys(this.checkup.config).sort().map(k => {
          const c = this.checkup.config[k]
          const indent = "  ".repeat(c.parent.length - 1)
          return { name: k, message: `${indent}${c.title} (${k})` }
        }),
        // @ts-ignore
        multiple: true,
        symbols: { indicator: { on: '●', off: '○' } },
        limit: 10,
        // @ts-ignore
        initial: this.paths.filter(e => this.checkup.config[e]),
        footer: () => chalk.dim(this.t('interactive.multiselect')),
        format: (input) => input
      },
      {
        type: 'autocomplete',
        name: 'tags',
        message: this.t('interactive.run.tags'),
        // @ts-ignore
        choices: tags.sort().map(t => {
          return { name: t, message: t }
        }),
        // @ts-ignore
        multiple: true,
        symbols: { indicator: { on: '●', off: '○' } },
        limit: 10,
        // @ts-ignore
        initial: this.tags,
        footer: () => chalk.dim(this.t('interactive.multiselect')),
        format: (input) => input
      }
    ])
      .then((answers: any) => {
        this.paths = answers.path
        this.tags = answers.tags
        this.setStatus()
        console.log('')
      })
      .then(async _ => {
        let runner: Runner
        try {
          runner = await this.checkup.create({
            paths: this.paths,
            tags: this.tags
          })
        } catch (err) {
          console.error(chalk.bold.red(err.message))
          return
        }
        runner.on('test', (name) => console.log(runner.report('console', name)))
        runner.on('analyze', (name) => console.log(runner.report('console', name)))
        runner.on('repair', (name) => console.log(runner.report('console', name)))
        await runner.start()
          .then(() => console.log(runner.report('console', '', { recursive: false })))
          .catch((e: any) => {
            console.error(chalk.bold.red(e))
            console.error(e.stack.replace(/^.*?\n/, ''))
            if (e.describe) console.error(e.describe())
          })
      })
  }

  private async config () {
    return prompt([
      {
        type: 'select',
        name: 'lang',
        message: this.t('interactive.config.lang'),
        // @ts-ignore
        choices: [
          { name: 'en', message: 'English' },
          { name: 'de', message: 'Deutsch' }
        ],
        // @ts-ignore
        initial: this.lang.substr(0, 2)
      },
      {
        type: 'numeral',
        name: 'verbose',
        message: this.t('interactive.config.verbose'),
        float: false,
        initial: this.checkup.verbose,
        format: (input) => parseInt(input) > 9 ? "9" : input,
        result: (input) => parseInt(input) > 9 ? "9" : input
      },
      {
        type: 'toggle',
        name: 'extend',
        message: this.t('interactive.config.extend'),
        initial: this.checkup.extend
      },
      {
        type: 'toggle',
        name: 'analyze',
        message: this.t('interactive.config.analyze'),
        initial: this.checkup.analyze
      },
      {
        type: 'toggle',
        name: 'autofix',
        message: this.t('interactive.config.autofix'),
        initial: this.checkup.autofix
      },
    ])
      .then(async (answers: any) => {
        this.checkup.verbose = answers.verbose
        this.checkup.extend = answers.extend
        this.checkup.analyze = answers.analyze
        this.checkup.autofix = answers.autofix
        this.lang = answers.lang
      })
  }

  private async add () {
    let key: string
    return prompt({
      type: 'input',
      name: 'key',
      message: this.t('interactive.add.key'),
      format: (input) => input.toLocaleLowerCase().replace(/[^a-z.]/g, ''),
      result: (input) => input.toLocaleLowerCase().replace(/[^a-z.]/g, ''),
      validate: (input) => input.toLocaleLowerCase().replace(/[^a-z.]/g, '') ? true : this.t('interactive.required')
    })
      .then((answers: any) => {
        key = answers.key
        return this.askCase({})
      })
      .then(async (answers: any) => {
        this.checkup.config[key] = answers
      })
  }

  private async edit () {
    if (!Object.keys(this.checkup.config).length) {
      console.log(this.t('interactive.list.empty'))
      return
    }
    let keyName: string
    return prompt({
      type: 'autocomplete',
      name: 'path',
      message: this.t('interactive.edit.path'),
      // @ts-ignore
      choices: Object.keys(this.checkup.config).sort().map(k => {
        const c = this.checkup.config[k]
        const indent = "  ".repeat(c.parent.length - 1)
        return { name: k, message: `${indent}${c.title} (${k})` }
      }),
      // @ts-ignore
      limit: 10,
      format: (input) => input
    })
      .then((answers: any) => {
        keyName = answers.path
        return this.askCase(this.checkup.config[keyName])
      })
      .then((answers: any) => {
        this.checkup.config[keyName] = answers
      })
  }

  private async remove () {
    let tags: string[] = []
    if (!Object.keys(this.checkup.config).length) {
      console.log(this.t('interactive.list.empty'))
      return
    }
    Object.keys(this.checkup.config).forEach(k => tags.push(...this.checkup.config[k].tags))
    return prompt({
      type: 'autocomplete',
      name: 'path',
      message: this.t('interactive.remove.path'),
      // @ts-ignore
      choices: Object.keys(this.checkup.config).sort().map(k => {
        const c = this.checkup.config[k]
        const indent = "  ".repeat(c.parent.length - 1)
        return { name: k, message: `${indent}${c.title} (${k})` }
      }),
      // @ts-ignore
      multiple: true,
      symbols: { indicator: { on: '●', off: '○' } },
      limit: 10,
      footer: () => chalk.dim(this.t('interactive.multiselect')),
      format: (input) => input
    })
      .then((answers: any) => {
        answers.path.forEach((e: string) => {
          Object.keys(this.checkup.config)
            .filter(k => k.startsWith(e))
            .forEach(k => delete this.checkup.config[k])
        })
        this.setStatus()
      })
  }

  private async clear () {
    Object.keys(this.checkup.config)
      .forEach(k => delete this.checkup.config[k])
    this.setStatus()
  }

  private async askCase (initial: any) {
    let base: any
    return prompt([
      {
        type: 'input',
        name: 'title',
        message: this.t('interactive.case.title'),
        initial: initial.title,
        result: (input) => input.trim(),
        validate: (input) => input.trim() ? true : this.t('interactive.required')
      },
      {
        type: 'input',
        name: 'description',
        message: this.t('interactive.case.description'),
        initial: initial.description,
        result: (input) => input.trim(),
      },
      {
        // @ts-ignore
        type: 'list',
        name: 'tags',
        message: this.t('interactive.case.tags'),
        initial: initial.tags,
        // @ts-ignore
        format (input) {
          if (Array.isArray(input))
            return input.map(e => e.toLocaleLowerCase().replace(/[^a-z, ]/g, '').replace(/([^,]) /g, '$1')).filter(e => e)
          return input.toLocaleLowerCase().replace(/[^a-z, ]/g, '').replace(/([^,]) /g, '$1')
        },
        result (input) {
          // @ts-ignore
          return input.map(e => e.toLocaleLowerCase().replace(/[^a-z, ]/g, '').replace(/([^,]) /g, '$1')).filter(e => e)
        }
      },
      {
        type: 'autocomplete',
        name: 'test',
        message: this.t('interactive.case.test'),
        // @ts-ignore
        choices: testFunctions,
        initial: initial.test,
        // @ts-ignore
        limit: 10,
        format: (input) => input
      },
      {
        type: 'numeral',
        name: 'cache',
        message: this.t('interactive.case.cache'),
        float: false,
        // @ts-ignore
        initial: initial.cache
      },
      {
        type: 'toggle',
        name: 'autofix',
        message: this.t('interactive.case.autofix'),
        initial: this.checkup.autofix,
        skip () {
          // @ts-ignore
          const test = this.state.answers.test
          if (test === '-') return false
          const def = testDefinitions[test]
          return def.fix ? true : false
        }
      },
    ])
      .then(answers => {
        base = answers
        if (base.test === '-') {
          delete base.test
          return base
        }
        // additional checks
        const def = testDefinitions[base.test]
        const questions = schema2prompt(def.data, 'data', initial.data || {})
        return prompt(questions)
      })
      .then(answers => {
        // combine results
        return { ...base, ...answers }
      })
  }
}

export default Interactive
