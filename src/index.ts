import Debug from "debug";
import Validator from '@alinex/validator';
import NodeCache from "node-cache"

const cacheTTL = 50
const cacheCheckPeriod = 120
const concurrency = 10

const debug = Debug("checkup");

import schema from './caseSchema';
import Runner from './runner'
import Store from './store'

export type Setup = {
  config?: string,
  verbose?: number,
  extend?: boolean,
  analyze?: boolean,
  autofix?: boolean,
  concurrency?: number,
  store?: any,
  cron?: string
}

export type Execution = {
  paths?: string[],
  tags?: string[],
  lang?: string
}

export type Case = {
  parent: string[]
  title: string
  description?: string
  tags: string[]
  cron: string
  group?: { class: string, [key: string]: any }
  test?: string
  data: any
  cache: number
  fix: any
  autofix: boolean
}

function inherit(config: any, cron?: string) {
  // step over case list
  Object.keys(config).forEach(key => {
    let cur = key
    // build path to parent
    const parent: string[] = [key]
    while (key.match(/\./)) {
      key = key.replace(/\.[^.]*$/, '')
      parent.unshift(key)
      const parent2: string[] = [key]
      let key2 = key
      while (key2.match(/\./)) {
        key2 = key2.replace(/\.[^.]*$/, '')
        parent2.unshift(key2)
      }
      parent2.unshift('') // base entry
      config[key] ||= { title: key, parent: [...parent2] }
    }
    parent.unshift('') // base entry
    // inherit fields
    const tags: string[] = []
    let data = {}
    let cache = cacheTTL
    let fix = {}
    let autofix = false
    if (!cron) cron = 'none'
    parent.forEach(p => {
      if (!config[p]) return // used for base entry ''
      if (config[p].tags) tags.push(...config[p].tags)
      if (config[p].data && typeof config[p].data === 'object') data = { ...data, ...config[p].data }
      if (typeof config[p].cache === 'number') cache = config[p].cache
      if (config[p].fix && typeof config[p].fix === 'object') fix = { ...fix, ...config[p].fix }
      if (typeof config[p].autofix === 'boolean') autofix = config[p].autofix
      if (typeof config[p].cron === 'string') cron = config[p].cron
    })
    if (config[cur].test) tags.push(...config[cur].test.split(/\.*\//).filter((e: any) => e))
    config[cur].parent = parent
    config[cur].tags = [...new Set(tags)] // remove duplicates
    config[cur].data = data
    config[cur].cache = cache
    config[cur].fix = fix
    config[cur].autofix = autofix
    config[cur].cron = cron
  })
  // return optimized config
  return <{ [key: string]: Case }>config
}

export function caseFilter(cases: { [key: string]: Case }, paths: string[], tags: string[], cron?: string): { [key: string]: Case } {
  const revPaths = paths?.map(e => e.split('.').reverse().join('.'))
  const filtered: any = {};
  const reverse: any = {};
  Object.keys(cases).forEach(key => {
    // check tags
    if (tags.filter(t => !cases[key].tags.includes(t)).length) return
    // check cron
    if (cron && cron !== cases[key].cron) return
    // check path in paths
    if (paths.length) {
      // add parents and self
      if (paths.filter(p => key.startsWith(p)).length) {
        cases[key].parent.forEach(p => { if (p) filtered[p] = cases[p] })
        filtered[key] = cases[key];
      }
      // check path in reverse paths
      if (revPaths.filter(p => key.startsWith(p)).length) {
        cases[key].parent.forEach(p => { if (p) reverse[p] = cases[p] })
        reverse[key] = cases[key];
      }
    } else {
      // no path defined, add to both
      cases[key].parent.forEach(p => { if (p) filtered[p] = cases[p] })
      filtered[key] = cases[key];
      cases[key].parent.forEach(p => { if (p) reverse[p] = cases[p] })
      reverse[key] = cases[key];
    }
  });
  return Object.keys(filtered).length > Object.keys(reverse).length ? filtered : reverse;
}

const configCache: { [key: string]: any } = {}

export class Suite {

  public config: { [key: string]: any }
  public verbose: number
  public extend: boolean
  public autofix: boolean
  public analyze: boolean
  public concurrency: number = concurrency
  public cron: string | undefined
  public cache: NodeCache
  public stores: Store[]

  public init: Promise<void[]>

  /**
   * Initialize a new data store.
   * @param input one or multiple data sources to later be used in load/save
   */
  constructor(setup: Setup = {}) {
    this.verbose = setup.verbose || 0
    this.extend = setup.extend || false
    this.analyze = typeof setup.analyze === 'undefined' ? true : setup.analyze
    this.autofix = setup.autofix || false
    if (setup.concurrency) this.concurrency = setup.concurrency
    this.cron = setup.cron
    this.cache = new NodeCache({ stdTTL: cacheTTL, checkperiod: cacheCheckPeriod, useClones: false });
    debug(`initialize with ${setup.config || 'default'}`)
    const prom: Promise<void>[] = []
    // load configuration
    this.config = {}
    prom.push(this.loadConfig(setup.config))
    // setup stores
    this.stores = []
    if (setup.store) {
      Object.keys(setup.store).forEach(k => {
        prom.push(
          import(`${__dirname}/store/${k}`)
            .then(Store => {
              const store = new Store.default(setup.store[k], this.concurrency)
              this.stores.push(store)
              return store.init()
            })
        )
      })
    }
    // create instances and call init() of all of them
    this.init = Promise.all(prom)
  }

  private loadConfig(setup?: string) {
    let file = setup || `file://${process.cwd()}/config/cases.yml`
    if (configCache[file]) {
      this.config = configCache[file]
      return Promise.resolve()
    }
    // get config url
    if (!file.match(/:\/\//)) {
      if (!file.startsWith('/')) file = `${process.cwd()}/${setup}`
      file = `file://${file}`
    }
    debug(`load configuration from ${file}`)
    const val = new Validator(schema, { source: file });
    return val.load().catch((e: any) => {
      console.error(e)
      process.exit(1)
    })
      .then((config: any) => {
        this.config = configCache[file] = inherit(config, this.cron)
        return
      })
  }

  public async scheduler(exec: Execution = {}, events?: any) {
    await this.init
    debug(`init scheduler (default: ${this.cron})`)
    // get possible patterns
    const variants = [...new Set(Object.values(this.config).map(v => v.cron).filter(v => v !== "none"))].filter(e => e)
    if (!variants.length) throw new Error('No cron checkups defined in case configuration!')
    // create cron entries
    const cron = await import('node-cron');
    const tasks = variants.map(v => {
      let cases = caseFilter(this.config, exec.paths || [], exec.tags || [], v)
      if (!Object.keys(cases).length) return
      debug('create task for', v)
      // start run
      return cron.schedule(v, () => {
        debug(`starting scheduler at ${new Date} call for ${v}`)
        const runner = new Runner(this, cases, exec)
        events(runner)
        runner.start()
      })
    })
    const stop = () => {
      tasks.forEach(t => t?.destroy())
      debug('scheduler stopped')
    }
    return { stop }
  }

  public async create(exec: Execution = {}): Promise<Runner> {
    await this.init
    // start run
    return new Runner(this, this.config, exec)
  }
}

export default Suite
