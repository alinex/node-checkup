# Alinex Checkup

This command can run a specific set of tests and display a report afterwards.

Commands:
    cli.ts bashrc-script  generate completion script

Options:

    --case, -c     file or URI of case configuration to load             [string]
    --path, -p      select tests from the given subpaths                  [array]
    --tag, -t       select tests which match all the given                [array]
  
    --version       Show version number                                 [boolean]
    --quiet, -q     only output result                                  [boolean]
    --help          Show help                                           [boolean]
  
    --verbose, -v   extend verbosity                                      [count]
    --extend, -e    automatically extend verbosity on error             [boolean]
    --progress      show progress with final report                     [boolean]

    -f, --autofix   automatically fix/repair if possible                [boolean]
    -F, --no-fix    switch off fixes (also the analyzation part)        [boolean]

    --setup, -s     configuration of additional tools                    [string]
    --scheduler     flag to only run the scheduler                      [boolean]
    --interactive, -i   run in interactive mode                         [boolean]

Environment Settings:

    FORCE_COLOR=0   will disable color output also if possible in terminal
    DEBUG=checkup   enable debug output (see below)
    DEBUG_DEPTH=10  enhance depth of object display in debugging

Examples:

    cli.ts --path alinex/service/mongo               run the test case
    cli.ts --path alinex/service/mongo --tag extern  only run the extern tests
    cli.ts --describe --path alinex/service/mongo    only describe what will be checked
    cli.ts --fix --path alinex                       run all tests in folder and fix if possible

## License

(C) Copyright 2020 Alexander Schilling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

> <http://www.apache.org/licenses/LICENSE-2.0>

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
