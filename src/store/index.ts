import Debug from "debug";

class Store {
  protected debug: Debug.Debugger
  protected setup: any
  protected concurrency: number

  constructor(setup: any, concurrency: number) {
    this.setup = setup
    this.concurrency = concurrency
    this.debug = Debug(`checkup:${this.constructor.name.replace(/([a-z])([A-Z])/g, '$1:$2').toLowerCase()}`);
  }

  public async init() { }
  public async cleanup() { }

  public async setUp() { }
  public async tearDown() { }

  public async status(path: string, type: string, start: Date, status: string, error?: string, log?: string): Promise<any> { }
  public async data(path: string, type: string, start: Date, status: string, result: any, testConfig: any): Promise<any> { }
  public async action(path: string, type: string, status: string, error?: string, log?: string): Promise<any> { }

}

export default Store
