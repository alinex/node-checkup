import { each } from '@alinex/async';
import { knex, Knex } from 'knex';
import crypto from 'crypto';
//import { retry } from '@alinex/async';
const { DateTime } = require("luxon");
import { sql } from 'slonik';

import database from '../database'

import Store from './index'

const CLEANUP_TIME = 1800000 // 30 minutes

const statusValue: any = {
  OK: 0,
  WARN: 1,
  ERROR: 2,
  NODATA: -1,
  MANUAL: 1
}

const ranges: any = {
  minute5: (time: any) => time.startOf('minute').minus({ minute: time.get('minute') % 5 }).toString(),
  hour: (time: any) => time.startOf('hour').toString(),
  day: (time: any) => time.startOf('day').toString(),
  month: (time: any) => time.startOf('month').toString()
}

interface LogTable {
  time: Date
  testcase: string
  level: number
  status: string
  message: string
  verbose: string
}
interface NumTable {
  timeslot: Date
  testcase: string
  element: string
  num: number
  min: number
  max: number
  mean: number
  std: number
}
interface DataTable {
  timeslot: Date
  testcase: string
  element: string
  num: number
  md5: string
  json: string
}

class StoreDatabase extends Store {

  private db: Knex<any, unknown[]> | undefined
  private lastCleanup = new Date().getTime()

  constructor(setup: any, concurrency: number) {
    super(setup, concurrency)
  }

  public async init() {
    this.debug('init database')
    this.setup.schema ||= 'public' // use default schema
    if (this.setup.client === 'pg') return this.initSlonik()
    return this.initKnex()
  }

  private async initSlonik() {
    const pool = database.pgPool(this.setup, this.concurrency)

    await pool.connect(conn => {
      const table = (this.setup.prefix || '') + 'log'
      return conn.exists(sql`SELECT 1 FROM pg_tables WHERE schemaname=${this.setup.schema} AND tablename=${table}`)
        .then(exists => {
          if (exists) return
          this.debug(`create log`)
          return conn.query(sql`
              CREATE TABLE ${sql.identifier([this.setup.schema, table])} (
                time timestamptz NOT NULL,
                testcase varchar(255) NOT NULL,
                level int4 NOT NULL,
                status varchar(10) NOT NULL,
                "message" text NOT NULL,
                "verbose" text NOT NULL,
                CONSTRAINT ${sql.identifier([table + '_pkey'])} PRIMARY KEY (time, testcase)
              )`)
        })
    })

    return each(Object.keys(ranges), range => {
      const table = (this.setup.prefix || '') + range
      return pool.connect(conn => {
        return conn.exists(sql`SELECT 1 FROM pg_tables WHERE schemaname=${this.setup.schema} AND tablename=${table + '_num'}`)
          .then(exists => {
            if (exists) return
            this.debug(`create ${table}_num`)
            return conn.query(sql`
              CREATE TABLE ${sql.identifier([this.setup.schema, table + '_num'])} (
                timeslot timestamptz NOT NULL,
                testcase varchar(255) NOT NULL,
                "element" varchar(255) NOT NULL,
                num int4 NOT NULL,
                min float4 NOT NULL,
                max float4 NOT NULL,
                mean float4 NOT NULL,
                std float4 NOT NULL,
                CONSTRAINT ${sql.identifier([table + '_num_pkey'])} PRIMARY KEY (timeslot, testcase, element)
              )`)
          })
          .then(() => conn.exists(sql`SELECT 1 FROM pg_tables WHERE schemaname=${this.setup.schema} AND tablename=${table + '_data'}`))
          .then(exists => {
            if (exists) return
            this.debug(`create ${table}_data`)
            return conn.query(sql`
              CREATE TABLE ${sql.identifier([this.setup.schema, table + '_data'])} (
                timeslot timestamptz NOT NULL,
                testcase varchar(255) NOT NULL,
                "element" varchar(255) NOT NULL,
                num int4 NOT NULL,
                md5 text NOT NULL,
                "json" text NOT NULL,
                CONSTRAINT ${sql.identifier([table + '_data_pkey'])} PRIMARY KEY (timeslot, testcase, element, md5)
              )`)
              .then(() => conn.query(sql`
                CREATE INDEX ${sql.identifier([table + '_data_index'])} ON ${sql.identifier([this.setup.schema, table + '_data'])} USING btree(timeslot, testcase, element)
              `))
          })
      })
    })
  }

  private async initKnex() {
    const db = knex(this.setup);

    const table = (this.setup.prefix || '') + 'log'
    await db.schema.withSchema(this.setup.schema)
      .hasTable(table)
      .then(exists => {
        if (exists) return
        this.debug(`create log`)
        return db.schema.withSchema(this.setup.schema)
          .createTable(table, function (table) {
            table.timestamp('time', { useTz: true, precision: 0 })
            table.string('testcase').notNullable()
            table.integer('level').notNullable()
            table.string('status').notNullable()
            table.text('message').notNullable()
            table.text('verbose').notNullable()
            table.primary(['time', 'testcase'])
          })
      })

    return each(Object.keys(ranges), range => {
      const table = (this.setup.prefix || '') + range
      return db.schema.withSchema(this.setup.schema)
        .hasTable(`${table}_num`)
        .then(exists => {
          if (exists) return
          this.debug(`create ${table}_num`)
          return db.schema.withSchema(this.setup.schema)
            .createTable(`${table}_num`, function (table) {
              table.timestamp('timeslot', { useTz: true, precision: 0 })
              table.string('testcase').notNullable()
              table.string('element').notNullable()
              table.integer('num').notNullable()
              table.float('min').notNullable()
              table.float('max').notNullable()
              table.float('mean').notNullable()
              table.float('std').notNullable()
              table.primary(['timeslot', 'testcase', 'element'])
            })
        })
        .then(() => db.schema.withSchema(this.setup.schema)
          .hasTable(`${table}_data`))
        .then(exists => {
          if (exists === true || Array.isArray(exists) && exists.filter(e => e).length) return
          this.debug(`create ${table}_data`)
          return db.schema.withSchema(this.setup.schema)
            .createTable(`${table}_data`, (table) => {
              table.timestamp('timeslot', { useTz: true, precision: 0 })
              table.string('testcase').notNullable()
              table.string('element').notNullable()
              table.integer('num').notNullable()
              table.text('md5').notNullable()
              table.text('json').notNullable()
              table.primary(['timeslot', 'testcase', 'element', 'md5'])
              table.index(['timeslot', 'testcase', 'element'])
            })
        })
    })
      .then(() => db.destroy())
      .catch((error: any) => console.error(`WARNING: Could not initialize store: ${error}\nThis is no problem if it isn't the first run on this store.`))
  }

  public async cleanup() {
    if (new Date().getTime() - this.lastCleanup < CLEANUP_TIME) return
    this.lastCleanup = new Date().getTime()
    this.debug('cleanup database')
    if (this.setup.client === 'pg') return this.cleanupSlonik()
    return this.cleanupKnex()
  }

  private async cleanupSlonik() {
    const pool = database.pgPool(this.setup, this.concurrency)
    await pool.connect(conn => conn.query(sql`DELETE FROM ${sql.identifier([this.setup.schema, (this.setup.prefix || '') + 'log'])} WHERE time < (SELECT time FROM ${sql.identifier([this.setup.schema, (this.setup.prefix || '') + 'log'])} ORDER BY time DESC LIMIT 1 OFFSET ${this.setup.cleanup.log})`))
    return each(Object.keys(ranges), range => {
      const table = (this.setup.prefix || '') + range
      const time = DateTime.local().minus({ seconds: this.setup.cleanup[range] }).toSQL()
      return pool.connect(conn => conn.query(sql`DELETE FROM ${sql.identifier([this.setup.schema, table + '_num'])} WHERE timeslot < ${time}`)
        .then(() => conn.query(sql`DELETE FROM ${sql.identifier([this.setup.schema, table + '_data'])} WHERE timeslot < ${time}`))
      )
    }, 1) // no concurrency
  }

  private async cleanupKnex() {
    const db = knex(this.setup);
    await db(`${this.setup.schema ? this.setup.schema + '.' : ''}${this.setup.prefix || ''}log`)
      .whereRaw(`time < (SELECT time FROM ${this.setup.schema ? this.setup.schema + '.' : ''}${this.setup.prefix || ''}log ORDER BY time DESC LIMIT 1 OFFSET ${this.setup.cleanup.log})`)
      .del()
    return each(Object.keys(ranges), range => {
      const table = `${this.setup.schema ? this.setup.schema + '.' : ''}${this.setup.prefix || ''}${range}`
      const time = DateTime.local().minus({ seconds: this.setup.cleanup[range] }).toSQL()
      return db(`${table}_num`)
        .whereRaw('timeslot < ?', [time])
        .del()
        .then(() => {
          return db(`${table}_data`)
            .whereRaw('timeslot < ?', [time])
            .del()
        })
    }, 1) // no concurrency
      .catch((error: any) => console.error(`WARNING: Could not cleanup store: ${error}\nThis is no problem if it comes only once.`))
  }

  public async setUp() {
    if (this.setup.client === 'pg') return
    this.debug('connecting database')
    this.db = knex(this.setup);
  }
  public async tearDown() {
    if (this.setup.client === 'pg') return
    this.debug('disconnecting database')
    return this.db?.destroy().catch(() => { })
  }

  private async log(start: Date, testcase: string, level: number, status: string, error: string, log: string) {
    if (this.setup.client === 'pg') return this.logSlonik(start, testcase, level, status, error, log)
    return this.logKnex(start, testcase, level, status, error, log)
  }
  private async logSlonik(start: Date, testcase: string, level: number, status: string, error: string, log: string) {
    const pool = database.pgPool(this.setup, this.concurrency)
    return pool.connect(conn => {
      this.debug(`store log ${testcase} ${status}`)
      const table = `${this.setup.prefix || ''}log`
      console.log(sql`
        INSERT INTO ${sql.identifier([this.setup.schema, table])}
          (time, testcase, level, status, message, "verbose")
        VALUES (${DateTime.fromJSDate(start).toSQL()}, ${testcase}, ${level}, ${status}, ${error}, ${log})`)
      return conn.query(sql`
        INSERT INTO ${sql.identifier([this.setup.schema, table])}
          (time, testcase, level, status, message, "verbose")
        VALUES (${DateTime.fromJSDate(start).toSQL()}, ${testcase}, ${level}, ${status}, ${error}, ${log})`)
    })
  }
  private async logKnex(start: Date, testcase: string, level: number, status: string, error: string, log: string) {
    const table = `${this.setup.schema ? this.setup.schema + '.' : ''}${this.setup.prefix || ''}log`
    if (!this.db) throw new Error('DB not opened before')
    return this.db<LogTable>(table)
      .insert({
        time: start,
        testcase,
        level,
        status,
        message: error,
        verbose: log
      })
  }

  private async store(start: Date, testcase: string, block: any[][]) {
    block = block.filter(e => e[1] !== null && typeof e[1] !== 'undefined' && (!Array.isArray(e[1]) || !e[1].length))
    if (this.setup.client === 'pg') return this.storeSlonik(start, testcase, block)
    return each(block, e => {
      const [element, value] = e
      return this.storeKnex(start, testcase, element, value)
    })
  }

  // no exists, always insert, ignore error
  // update afterwards
  private async storeSlonik(start: Date, testcase: string, block: any[][]) {
    const time = DateTime.fromJSDate(start)
    const pool = database.pgPool(this.setup, this.concurrency)
    return pool.connect(conn => {
      return each(Object.keys(ranges), range => {
        const timeslot = ranges[range](time)
        return each(block, e => {
          const [element, value] = e
          if (typeof value === 'number') {
            this.debug(`store ${timeslot} ${testcase} ${element} ${value}`)
            const table = `${this.setup.prefix || ''}${range}_num`
            return conn.exists(sql`
            SELECT 1 FROM ${sql.identifier([this.setup.schema, table])}
            WHERE timeslot=${timeslot} AND testcase=${testcase} AND element=${element}`)
              .then(exists => {
                if (!exists)
                  return conn.query(sql`
                  INSERT INTO ${sql.identifier([this.setup.schema, table])}
                    (timeslot, testcase, element, num, min, max, mean, std)
                  VALUES (${timeslot}, ${testcase}, ${element}, 1, ${value}, ${value}, ${value}, 0)`)
                // UPDATE:
                // X_n = X_{n-1} + ( x_n - X_{n-1} ) / n   # X = mean; n = new number of values
                // mean = mean + ( value - mean ) / ( num + 1 )
                // s = sqrt( s_{n-1}^2 + ( x_n - X_{n-1} )^2 / n - s_{n-1}^2 / ( n - 1 ) )
                // std = sqrt( std^2 + ( value - mean )^2 / ( num + 1 ) - std^2 / num )
                return conn.query(sql`
                  UPDATE ${sql.identifier([this.setup.schema, table])}
                  SET num = num + 1,
                      min = least(min, ${value}),
                      max = greatest(max, ${value}),
                      mean = mean + (${value} - mean) / (num + 1),
                      std = sqrt( power(std, 2) + power(${value} - mean, 2) / (num+1) - power(std, 2) / num )
                  WHERE timeslot=${timeslot} AND testcase=${testcase} AND element=${element}`)
              })
          } else {
            const json = JSON.stringify(value)
            const md5 = crypto.createHash('md5').update(json).digest("hex")
            this.debug(`store ${timeslot} ${testcase} ${element} ${json}`)
            const table = `${this.setup.prefix || ''}${range}_data`
            return conn.exists(sql`
            SELECT 1 FROM ${sql.identifier([this.setup.schema, table])}
            WHERE timeslot=${timeslot} AND testcase=${testcase} AND element=${element} AND md5=${md5}`)
              .then(exists => {
                if (!exists)
                  return conn.query(sql`
                  INSERT INTO ${sql.identifier([this.setup.schema, table])}
                    (timeslot, testcase, element, num, md5, json)
                  VALUES (${timeslot}, ${testcase}, ${element}, 1, ${md5}, ${json})`)
                return conn.query(sql`
                  UPDATE ${sql.identifier([this.setup.schema, table])}
                  SET    num = num + 1
                  WHERE  timeslot=${timeslot} AND testcase=${testcase} AND element=${element} AND md5=${md5}`)
              })
          }
        }, 1)
      }, 1) // because of one connection run in series
    })
  }

  private async storeKnex(start: Date, testcase: string, element: string, value: any) {
    const time = DateTime.fromJSDate(start)
    return each(Object.keys(ranges), range => {
      if (!this.db) throw new Error('DB not opened before')
      const timeslot = ranges[range](time)
      if (typeof value === 'number') {
        this.debug(`store ${timeslot} ${testcase} ${element} ${value}`)
        const table = `${this.setup.schema ? this.setup.schema + '.' : ''}${this.setup.prefix || ''}${range}_num`
        return this.db<NumTable>(table)
          .insert({
            timeslot,
            testcase,
            element,
            num: 1,
            min: value,
            max: value,
            mean: value,
            std: 0
          })
          .onConflict(['timeslot', 'testcase', 'element'])
          .merge({
            num: this.db.raw('?? + 1', [`${table}.num`]),
            min: this.db.raw('least(??, ?)', [`${table}.min`, value]),
            max: this.db.raw('greatest(??, ?)', [`${table}.max`, value]),
            // X_n = X_{n-1} + ( x_n - X_{n-1} ) / n   # X = mean; n = new number of values
            // mean = mean + ( value - mean ) / ( num + 1 )
            mean: this.db.raw('?? + ( ? - ?? ) / ( ?? + 1 )',
              [`${table}.mean`, value, `${table}.mean`, `${table}.num`]),
            // s = sqrt( s_{n-1}^2 + ( x_n - X_{n-1} )^2 / n - s_{n-1}^2 / ( n - 1 ) )
            // std = sqrt( std^2 + ( value - mean )^2 / ( num + 1 ) - std^2 / num )
            std: this.db.raw('sqrt( power(??, 2) + power( ? - ??, 2) / ( ?? + 1 ) - power(??, 2) / ?? )',
              [`${table}.std`, value, `${table}.mean`, `${table}.num`, `${table}.std`, `${table}.num`]),
          })
      } else {
        const json = JSON.stringify(value)
        const md5 = crypto.createHash('md5').update(json).digest("hex")
        this.debug(`store ${timeslot} ${testcase} ${element} ${json}`)
        const table = `${this.setup.schema ? this.setup.schema + '.' : ''}${this.setup.prefix || ''}${range}_data`
        return this.db<DataTable>(table)
          .insert({
            timeslot,
            testcase,
            element,
            num: 1,
            md5,
            json
          })
          .onConflict(['timeslot', 'testcase', 'element', 'md5'])
          .merge({
            num: this.db.raw('?? + 1', [`${table}.num`])
          })
      }
    })
      .catch(error => console.error(`WARNING: Could not store value: ${error}\nThis is no problem in the checkup but the values are missing in the store!`))
  }

  public async status(path: string, type: string, start: Date, status: string, error?: string, log?: string) {
    const block: any[] = []
    block.push(['status', statusValue[status]])
    if (error) block.push(['error', error])
    if (status !== 'OK') await this.log(start, path, statusValue[status], status, error || '', log || '')
    return this.store(start, path, block)
  }

  public async data(path: string, type: string, start: Date, status: string, result: any, testConfig: any) {
    if (typeof result !== 'object') return Promise.resolve() // no result to store
    const excludes: string[] = this.setup.exclude
    if (testConfig.exclude && !excludes.filter((e: string) => e.startsWith(`${path}:`)).length)
      excludes.push(...testConfig.exclude.map((e: any) => `${path}:${e}`))
    const block: any[] = []
    Object.keys(result).forEach(k => {
      if (excludes) {
        if (excludes.includes(`${path}:${k}`)) return
        if (excludes.includes(path)) return
        if (excludes.includes(`${type}:${k}`)) return
        if (excludes.includes(type)) return
        if (excludes.includes(`:${k}`)) return
      }
      block.push([k, result[k]])
    })
    return this.store(start, path, block)
  }

  public async action(path: string, type: string, status: string, error: string) {
    const start = new Date()
    const block: any[] = []
    block.push(['action', statusValue[status]])
    if (error) block.push(['error', error])
    return this.store(start, path, block)
  }

}

export default StoreDatabase
