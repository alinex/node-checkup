import { createLogger, format, addColors, Logger } from 'winston';
import DailyRotateFile from 'winston-daily-rotate-file';

import Store from './index'
import { indent } from '../string';

addColors({
  error: 'red',
  warn: 'yellow',
  info: 'green',
  http: 'cyan',
  verbose: 'blue',
  debug: 'white',
  silly: 'grey'
});

const statusLogger: { [key: string]: 'info' | 'warn' | 'error' } = {
  OK: 'info',
  WARN: 'warn',
  ERROR: 'error',
  NODATA: 'error',
  MANUAL: 'info'
}

const formatter = format.combine(
  format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss' }),
  format.splat(),
  format.printf(
    (info: any) =>
      `${info.timestamp} ${(info.level === 'INFO' ? 'OK' : info.level.toLocaleUpperCase()).padEnd(8)} ${info.message}` +
      (info.splat !== undefined ? `${info.splat}` : ' ')
  ),
  format.colorize({ all: true })
)
const formatterJson = format.combine(
  format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss' }),
  format.splat(),
  format.json(),
  format.colorize({ all: true })
)

class StoreFile extends Store {

  private dirname: string
  private statusLogger: Logger | undefined
  private errorLogger: Logger | undefined
  private dataLogger: Logger | undefined
  private actionLogger: Logger | undefined

  constructor(setup: any, concurrency: number) {
    super(setup, concurrency)
    this.dirname = setup.dirname || 'log'
    if (setup.status)
      this.statusLogger = createLogger({
        level: 'debug',
        format: formatter,
        transports: [
          new DailyRotateFile({
            dirname: this.dirname || 'log',
            filename: 'status.log',
            datePattern: 'YYYY-MM-DD',
            zippedArchive: true,
            maxFiles: 30,
            createSymlink: true,
            symlinkName: 'status.log',
            ...(typeof setup.status === 'object' ? setup.status : {})
          })
        ]
      })
    if (setup.error)
      this.errorLogger = createLogger({
        level: 'debug',
        format: formatter,
        transports: [
          new DailyRotateFile({
            dirname: this.dirname || 'log',
            filename: 'error.log',
            datePattern: 'YYYY-MM',
            zippedArchive: true,
            maxFiles: 12,
            createSymlink: true,
            symlinkName: 'error.log',
            ...(typeof setup.error === 'object' ? setup.error : {})
          })
        ]
      })
    if (setup.data)
      this.dataLogger = createLogger({
        level: 'debug',
        format: formatterJson,
        transports: [
          new DailyRotateFile({
            dirname: this.dirname || 'log',
            filename: 'data.log',
            datePattern: 'YYYY-MM-DD',
            zippedArchive: true,
            maxFiles: 30,
            createSymlink: true,
            symlinkName: 'data.log',
            ...(typeof setup.data === 'object' ? setup.data : {})
          })
        ]
      })
    if (setup.action)
      this.actionLogger = createLogger({
        level: 'debug',
        format: formatter,
        transports: [
          new DailyRotateFile({
            dirname: this.dirname || 'log',
            filename: 'action.log',
            datePattern: 'YYYY-MM',
            zippedArchive: true,
            maxFiles: 12,
            createSymlink: true,
            symlinkName: 'action.log',
            ...(typeof setup.action === 'object' ? setup.action : {})
          })
        ]
      })
  }

  public async init() {
  }

  public async status(path: string, type: string, start: Date, status: string, error?: string, log?: string) {
    if (this.statusLogger)
      this.statusLogger[statusLogger[status]](`${path} ${error}`)
    if (status !== 'OK' && this.errorLogger)
      this.errorLogger[statusLogger[status]](`${path} ${error}${log ? '\n' + indent(log, 20) : ''}`)
  }

  public async data(path: string, type: string, start: Date, status: string, result: any) {
    if (this.dataLogger)
      this.dataLogger[statusLogger[status]]({ path, status, result })
  }

  public async action(path: string, type: string, status: string, error: string, log?: string) {
    if (this.actionLogger)
      this.actionLogger[statusLogger[status]](`${path} ${error}${log ? '\n' + indent(log, 20) : ''}`)
  }

}

export default StoreFile
