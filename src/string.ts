import wordwrap from 'wordwrap';

export function indent(msg?: string, num: number = 4) {
  if (!msg) return ''
  const pad = ' '.repeat(num)
  return pad + msg.trim().replace(/\n/g, '\n' + pad)
}

export function wrap (msg: string, maxWidth: number = process.stdout.columns) {
  if (!process.stdout.isTTY) maxWidth = 100 // default for non terminal like run by daemon
  if (maxWidth < 60) maxWidth = 60 // in case
  return msg.split(/\n/)
    .map(l => {
      const m = l.match(/^(\s+([A-Z]*\s+|-\s+|\d+.\s+)?)/)
      const prefix = m ? m[1] : ''
      const indent = m ? m[1].length : 0
      return prefix + wordwrap(indent, maxWidth)(l.substr(indent)).replace(/^\s+/, '')
    })
    .join('\n')
}
