import Debug from "debug"
import { each } from '@alinex/async'
import { clone } from '@alinex/data'
import { EventEmitter } from 'events';
import chalk from 'chalk'
import { inspect } from 'util'
import he from 'he'

const debug = Debug("checkup:runner")

import { Suite, Case, Execution, caseFilter } from './index'
import Group from './group'
import Test from './test'
import Fix from './fix'
import { indent, wrap } from './string';
import { T } from './i18n';

const statusSign: any = {
  OK: '✔   ',
  WARN: '⚡  ',
  ERROR: '✖   ',
  NODATA: '❇   ',
  IRRELEVANT: '💤  ',
  MANUAL: '🛠   '
}
export const statusColor: any = {
  undefined: chalk.gray,
  OK: chalk.green,
  WARN: chalk.yellow,
  ERROR: chalk.red,
  NODATA: chalk.magenta,
  fix: {
    NODATA: chalk.magenta.bold,
    IRRELEVANT: chalk.gray,
    MANUAL: chalk.blue,
    OK: chalk.green.bold,
    WARN: chalk.yellow.bold,
    ERROR: chalk.red.bold
  }
}
const htmlStyle = `
table {
    border-spacing: 0;
    border-collapse: collapse;
}
table th, table td { padding: 4px 2px }
body { font-family: Arial, Verdana; }
table .ok.head { background-color: lightgreen }
table .ok td:first-child, table .ok th:first-child { border-left: 6px solid lightgreen }
table .warn.head { color: orange }
table .warn td:first-child, table .warn th:first-child { border-left: 6px solid orange }
table .error.head { color: lightred }
table .error td:first-child, table .error th:first-child { border-left: 6px solid lightred }
table .nodata.head { color: magenta }
table .nodata td:first-child, table .nodata th:first-child { border-left: 6px solid magenta }
table .irrelevant.head { color: silver }
table .irrelevant td:first-child, table .irrelevant th:first-child { border-left: 6px solid silver }
table .manual.head { color: lightblue }
table .manual td:first-child, table .manual th:first-child { border-left: 6px solid lightblue }
table .fail { font-weight: boldest; color: firebrick }
`

type FormatterOptions = {
  recursive?: boolean,
  result?: boolean
}
const defaultFormatter = {
  recursive: true
}
const inum = 4 // number of characters to indent in console output
const cacheGroupTTL = 900 // 15 minutes

export class Runner extends EventEmitter {

  private suite
  private cases: { [key: string]: Case }
  private lang
  private t
  private paths
  private tags

  public num: { [key: string]: { total: number, [key: string]: number } }
  private tests: { [key: string]: Test }

  constructor(suite: Suite, cases: { [key: string]: Case }, exec: Execution) {
    super()
    this.suite = suite
    this.cases = clone(cases); // clone because groups may change it
    this.lang = <string>exec.lang
    this.t = T(this.lang)
    this.paths = exec.paths
    this.tags = exec.tags
    this.tests = {}
    this.num = { '': { total: 0 } }
  }

  public async init() {
    debug('init cases')
    return each(Object.keys(this.cases), async k => {
      const v = this.cases[k]
      if (!v.group) return // no dynamic group
      const found: any = this.suite.cache.get(`group:${k}`)
      if (found) {
        debug('skip group resolve (use cache) %s', k)
        this.cases = { ...this.cases, ...found }
        return
      }
      debug('resolve dynamic group', v.group.class)
      return import(v.group.class.replace(/^\.\//, `${__dirname}/group/`))
        .then(group => {
          const g: Group = new group.default(k, v, this.lang)
          return g.resolve()
            .catch(e => console.error(chalk.redBright.bold(`Could not resolve dynamic group ${k}.\n${e}`)))
        })
        .then(list => {
          // add missing parents
          if (list) Object.keys(list).forEach(key => {
            let cur = key
            const parent: string[] = [key]
            while (key.match(/\./)) {
              key = key.replace(/\.[^.]*$/, '')
              parent.unshift(key)
              const parent2: string[] = [key]
              let key2 = key
              while (key2.match(/\./)) {
                key2 = key2.replace(/\.[^.]*$/, '')
                parent2.unshift(key2)
              }
              parent2.unshift('') // base entry
              if (!this.cases[key]) list[key] = { title: key, parent: [...parent2] }
            }
            parent.unshift('') // base entry
            list[cur].parent = parent
          })
          this.suite.cache.set(`group:${k}`, list, v.group!.cache || cacheGroupTTL) // ttl to be kept while running
          this.cases = { ...this.cases, ...list }
        })
    }).then(() => {
      this.cases = caseFilter(this.cases, this.paths || [], this.tags || [])
      if (!Object.keys(this.cases).length) throw new Error(this.t('noCase'))
      // init counter for cases
      Object.keys(this.cases).forEach(k => {
        let v = this.cases[k]
        this.num[k] = { total: 0 }
        if (!v.test || v.group) return
        v.parent.forEach(e => this.num[e].total++)
      })
    })
  }

  public async start() {
    let pos = 0
    let max = 0
    return each(this.suite.stores, s => s.setUp())
      .then(() => this.init())
      .then(() => {
        debug('started %s cases', this.num[''].total)
        return each(Object.keys(this.cases), async k => {
          max = this.num[''].total * (this.suite.analyze ? 2 : 1)
          const v = this.cases[<any>k]
          if (!v.test || v.group) return
          // check for cache
          const found: Promise<any> | undefined = this.suite.cache.get(`${this.lang}:${k}`)
          if (found) {
            debug('skip test (use cache): %s', k)
            // if found in cache, also throw events here
            return found.then((test) => {
              this.tests[k] = test
              v.parent.forEach((e: string) => this.num[e][test.status] = (this.num[e][test.status] || 0) + 1)
              this.emit('test', k)
              this.emit('step', ++pos, max)
              if (this.suite.analyze)
                this.tests[k].fixes.forEach((f, i) => {
                  this.emit('analyze', `${k}:${i}`)
                  this.emit('step', ++pos, max)
                })
            })
          }
          // create test
          const worker = import(v.test.replace(/^\.\//, `${__dirname}/test/`)).then(test => {
            debug('start test: %s', k)
            const t: Test = new test.default(k, v, this.lang, this.suite.cache)
            this.tests[t.path] = t
            return t.run()
              .catch(e => {
                if (e) {
                  t.status = 'NODATA'
                  t.error = e
                }
              })
              .then(() => this.suite.cache.ttl(`${this.lang}:${k}`, v.cache)) // set defined ttl for cache
              .then(() => {
                debug('finished test: %s', k)
                v.parent.forEach(e => this.num[e][t.status] = (this.num[e][t.status] || 0) + 1)
                this.emit('test', k)
                this.emit('step', ++pos, max)
                return each(this.suite.stores, s => {
                  const prom = []
                  prom.push(s.status(t.path, <string>t.case.test, t.start, t.status, t.error?.toString().replace(' (preset:result)', '') || '', t.error && t.log(9)))
                  prom.push(s.data(t.path, <string>t.case.test, t.start, t.status, t.result, t.store))
                  return Promise.all(prom)
                })
              })
              .then(async () => {
                // analyze fixes
                if (this.suite.analyze) {
                  if (t.fixes.length) await each(t.fixes, (f, i) => {
                    debug('analyze: %s', t.case.parent[t.case.parent.length - 1])
                    return f.analyze().then(() => {
                      this.emit('analyze', `${k}:${i}-${f.constructor.name}`)
                      this.emit('step', ++pos, max)
                    })
                  })
                  else this.emit('step', ++pos, max) // no analyze needed in this step so goon
                }
              })
              .then(() => t)
          })
          this.suite.cache.set(`${this.lang}:${k}`, worker, 21600) // 6h ttl to be kept while running
          return worker
        }, this.suite.concurrency)
      })
      .then(() => {
        debug('maybe run repair')
        return this.repair()
      })
      // all done
      .then(() => {
        debug('all done')
        this.removeAllListeners()
      })
      .catch(e => {
        each(this.suite.stores, s => s.tearDown())
        throw e
      })
      .then(() => each(this.suite.stores, s => s.cleanup().then(() => s.tearDown())))
  }

  private async repair() {
    // run fixes
    return each(Object.keys(this.cases), async k => {
      const test = this.tests[k]
      const config = this.cases[<any>k]
      if (!test) return // skip group entries
      if (test.fixes && !config.autofix)
        test.fixes.forEach(f => f.type = 'manual')
      if (this.suite.autofix && config.autofix && test.fixes?.length) {
        const max = test.fixes.filter(f => f.type === 'auto' && !f.error).length
        let pos = 0
        await each(test.fixes, (f, i) => {
          if (f.type === 'auto' && !f.error) return f.repair().then(() => {
            this.emit('repair', `${k}:${i}-${f.constructor.name}`)
            this.emit('step', ++pos, max)
            return each(this.suite.stores, s => {
              return s.action(f.test.path, `${f.test.case.test}:${i}-${f.constructor.name}`, f.status, f.error?.toString().replace(' (preset:result)', '') || '', f.error && f.log(9))
            })
          })
          return Promise.resolve()
        })
          .then(() => this.suite.cache.del(`${this.lang}:${k}`))
      }
    }, this.suite.concurrency)
  }

  public report(format: string, name = '', prop: FormatterOptions = defaultFormatter) {
    const nparts = name.split(/:/)
    const test = this.tests[nparts[0]]
    if (test && nparts[1]) {
      // report fix
      const formatter = this.formatter[`${format}_fix`]
      if (!formatter) throw new Error(`Internal Problem: formatter['${format}_fix'] undefined`)
      const fix = test.fixes[parseInt(nparts[1])]
      let report = formatter(fix, prop)
      if (format == 'console') report = wrap(report)
      return report
    } else if (test) {
      // report test
      const formatter = this.formatter[`${format}_test`]
      if (!formatter) throw new Error(`Internal Problem: formatter['${format}_test'] undefined`)
      let report = formatter(test, prop)
      if (format == 'console') report = wrap(report)
      return report
    } else if (!nparts[0] || this.cases[<any>nparts[0]]) {
      // report group
      const formatter = this.formatter[`${format}_group`]
      if (!formatter) throw new Error(`Internal Problem: formatter['${format}_group'] undefined`)
      const group = this.cases[<any>nparts[0]]
      let report = formatter(group, prop)
      if (format == 'console') report = wrap(report)
      return report
    } else throw new Error(`Name '${name}' not defined for this run`)
  }

  private formatter: { [key: string]: (obj: any, prop?: any, depth?: number) => any } = {
    status_test: (test: Test, prop) => {
      return { status: test.status, error: test.error?.toString(this.lang) }
    },
    status_fix: (fix: Fix) => {
      return { status: fix.status, error: fix.error?.toString(this.lang) }
    },
    status_group: (group: Case, prop) => {
      const all = this.formatter.list_group(group, prop)
      return {
        status: all._status,
        error: Object.keys(all).map(k => all[k].error).filter(e => e).join('\n')
      }
    },

    list_test: (test: Test, prop) => {
      // report test
      const verbose = this.suite.verbose
      const result: any = {
        name: test.case.parent[test.case.parent.length - 1],
        ...test?.case,
        description: test.description,
        status: test.status,
        error: test.error?.toString(this.lang),
        result: test.result,
        start: test.start,
        log: test.log(test.error && this.suite.extend && verbose < 4 ? 4 : verbose)
      }
      if (prop.recursive) {
        // report fixes
        result.fixes = []
        test.fixes.forEach(fix => result.fixes.push(this.formatter.list_fix(fix)))
      }
      return result
    },
    list_fix: (fix: Fix) => {
      // report fix
      const verbose = this.suite.verbose
      const { title, type, description, status, action, error } = fix
      return {
        title,
        description: description.trim,
        type, status, action, error,
        log: fix.log(fix.error && this.suite.extend && verbose < 4 ? (fix.status === 'NODATA' ? 6 : 4) : verbose)
      }
    },
    list_group: (group: Case, prop) => {
      const name = group?.parent[group.parent.length - 1] || ''
      let result: any = {}
      Object.keys(this.cases).filter(k => (!name && !k.match(/\./)) || k.replace(/\.[^.]*$/, '.') === name + '.')
        .forEach(k => {
          if (this.tests[k]) result[k] = this.formatter.list_test(this.tests[k], prop)
          else result = { ...result, ...this.formatter.list_group(this.cases[<any>k], prop) }
        })
      result._status = this.num[name].ERROR ? 'ERROR' :
        this.num[name].WARN ? 'WARN' :
          this.num[name].OK ? 'OK' : 'NODATA'
      result._summary = this.num[name]
      return result
    },

    group_test: (test: Test, prop) => this.formatter.list_test(test, prop),
    group_fix: (fix: Fix) => this.formatter.list_fix(fix),
    group_group: (group: Case, prop) => {
      const name = group?.parent[group.parent.length - 1] || ''
      // report title
      const result: any = group ? { name, ...group } : {}
      if (prop.recursive) {
        // report entries
        result.tests = {}
        result.sub = {}
        Object.keys(this.cases).filter(k => (!result.name && !k.match(/\./)) || k.replace(/\.[^.]*$/, '.') === result.name + '.')
          .forEach(k => {
            if (this.tests[k]) result.tests[k] = this.formatter.group_test(this.tests[k], prop)
            else {
              result.sub[k] = this.formatter.group_group(this.cases[<any>k], prop)
            }
          })
      }
      // report summary
      result.status = this.num[name].ERROR ? 'ERROR' :
        this.num[name].WARN ? 'WARN' :
          this.num[name].OK ? 'OK' : 'NODATA'
      result.summary = this.num[name]
      return result
    },

    html_test: (test: Test, prop) => {
      const verbose = this.suite.verbose
      const name = test.case.parent[test.case.parent.length - 1]
      const description = test.case.description ? `<p>${test.case.description}</p>` : ''
      let data = `<tr class="${test.status.toLowerCase()} head"><th>${test.status}</th><td><b>${test.case.title}</b> (${name})</td></tr>`
      if (test.error) data += `<tr class="${test.status.toLowerCase()} fail"><td></td><td>${test.error.toString(this.lang).replace(' (preset:result)', '')}</td></tr>`
      if (verbose) {
        data += `<tr class="${test.status.toLowerCase()}"><td></td><td>${this.t('html.started')}: ${test.start}</td></tr>`
        data += `<tr class="${test.status.toLowerCase()}"><td></td><td><pre>${he.encode(test.log(test.error && this.suite.extend && verbose < 4 ? 4 : verbose))}</pre></td></tr>`
      }
      // TODO add fixes
      return `<html><head>
      <title>Checkup: ${test.case.title} (${name})</title>
      <style>${htmlStyle}</style>
      </head><body>
      <h1>Checkup: ${test.case.title} (${name})</h1>
      ${description}
      <table>${data}</table>
      </body></html>`
    },
    html_fix: (fix: Fix) => {
      const verbose = this.suite.verbose
      const { title, status, action, error } = fix
      const test = fix.test
      const name = test.case.parent[test.case.parent.length - 1]
      const description = fix.description ? `<p>${fix.description}</p>` : ''
      let data = `<tr class="${status.toLowerCase()} head"><th>${status}</th><td><b>${title}</b> (${name})</td></tr>`
      if (status === 'MANUAL' && fix.type === 'auto')`<tr><td></td><td><b>${this.t('fix.autoPossible')}</b></td></tr>`
      if (fix.action) {
        const lines = action.split(/\n/)
        data += `<tr class="${status.toLowerCase()} head"><td></td><td><b>${lines.shift()}</b></td></tr>`
        if (error) data += `<tr class="${status.toLowerCase()} fail"><td></td><td>${error.toString().replace(' (preset:result)', '')}</td></tr>`
        if (lines.join()) data += `<tr class="${status.toLowerCase()}"><td></td><td>${lines.join('\n')}</td></tr>`
      } else {
        if (error) data += `<tr class="${status.toLowerCase()}"><td></td><td>${error.toString().replace(' (preset:result)', '')}</td></tr>`
      }
      if (verbose) {
        data += `<tr class="${status.toLowerCase()}"><td></td><td><pre>${he.encode(fix.log(fix.error && this.suite.extend && verbose < 4 ? (fix.status === 'NODATA' ? 6 : 4) : verbose))}</pre></td></tr>`
      }
      return `<html><head>
      <title>Checkup: ${title} (${name})</title>
      <style>${htmlStyle}</style>
      </head><body>
      <h1>Checkup: ${title} (${name})</h1>
      ${description}
      <table>${data}</table>
      </body></html>`
    },
    html_group: (group: Case, prop) => {
      const verbose = this.suite.verbose
      const name = group?.parent[group.parent.length - 1] || ''
      const status = this.num[name].ERROR ? 'ERROR' :
        this.num[name].WARN ? 'WARN' :
          this.num[name].OK ? 'OK' : 'NODATA'
      const description = group?.description ? `<p>${group.description}</p>` : ''
      const summary = Object.keys(this.num[name]).reverse()
        .map(k => `<tr class="${k.toLowerCase()}"><td></td><td>${this.t('html.numTests', { count: this.num[name][k] })}</td><td>${k}</td></tr>`).join('')
        .replace('<td></td>', `<td>${status}</td>`)
      const list = Object.keys(this.cases).filter(k => !name || k.startsWith(name + '.'))
        .filter(k => this.cases[k].test)
        .map(k => {
          const test = this.tests[k]
          let data = `<tr class="${test.status.toLowerCase()} head" title="${test.case.description || ''}"><th>${test.status}</th><td><b>${test.case.title}</b> (${k})</td></tr>`
          if (test.error) data += `<tr class="${test.status.toLowerCase()} fail><td></td><td>${test.error.toString(this.lang).replace(' (preset:result)', '')}</td></tr>`
          if (verbose)
            data += `<tr class="${test.status.toLowerCase()}"><td></td><td><pre>${he.encode(test.log(test.error && this.suite.extend && verbose < 4 ? 4 : verbose))}</pre></td></tr>`
          return data
        })
        .join('\n')
      return `<html><head>
      <title>Checkup: ${group?.title || this.t('html.all')} ${name ? '(' + name + ')' : ''}</title>
      <style>${htmlStyle}</style>
      </head><body>
      <h1>Checkup: ${group?.title || this.t('html.all')} ${name ? '(' + name + ')' : ''}</h1>
      ${description}
      <h3>${this.t('html.summary')}:</h3><table class="summary">${summary}</table>
      <h3>${this.t('html.details')}:</h3><table class="list">${list}</table>
      </body></html>`
    },

    console_test: (test: Test, prop) => {
      // report test
      const verbose = this.suite.verbose
      const color = statusColor[test.status]
      const msg: string[] = []
      msg.push(color(`${statusSign[test.status]}${test.status} ${chalk.bold(test.case.title)} (${test.case.parent[test.case.parent.length - 1]})`))
      if (verbose) {
        if (test.case.description) msg.push(indent(test.case.description, inum))
        if (test.description) msg.push(indent(test.description.trim(), inum))
      }
      if (test.error) msg.push(indent(statusColor[test.status](test.error.toString(this.lang).replace(' (preset:result)', '')), inum))
      if (prop.result && verbose < 4) msg.push(`RESULT   ${inspect(test.result).replace(/\n/g, '\n         ')}`)
      const log = test.log(test.error && this.suite.extend && verbose < 4 ? 4 : verbose)
      if (log) {
        msg.push(indent(log, inum))
      }
      if (prop.recursive) {
        // report fixes
        test.fixes.forEach(fix => {
          const sub = this.formatter.console_fix(fix, prop)
          if (sub) msg.push(indent(sub, inum))
        })
      }
      return msg.join('\n')
    },
    console_fix: (fix: Fix, prop) => {
      if (!fix.title) return ''
      // report fix
      const verbose = this.suite.verbose
      const t = this.t
      const color = statusColor.fix[fix.status]
      const msg: string[] = []
      msg.push(color(`${statusSign[fix.status]}${fix.status} ${chalk.bold(fix.title)} (${fix.test.case.parent[fix.test.case.parent.length - 1]})`))
      if (fix.status === 'MANUAL' && fix.type === 'auto') msg.push(indent(t('fix.autoPossible'), inum))
      if (fix.description) msg.push(indent(fix.description.trim(), inum))
      if (fix.action) {
        const lines = fix.action.split(/\n/)
        msg.push(indent(color(chalk.bold(`${lines.shift()}`)), inum))
        if (fix.error) msg.push(indent(color(fix.error.toString(this.lang).replace(' (preset:result)', '')), inum + 4))
        if (lines.join()) msg.push(indent(lines.join('\n'), inum))
      } else {
        if (fix.error) msg.push(indent(statusColor[fix.status](fix.error.toString(this.lang).replace(' (preset:result)', '')), inum))
      }
      const log = fix.log(fix.error && this.suite.extend && verbose < 4 ? (fix.status === 'NODATA' ? 6 : 4) : verbose)
      if (log) msg.push(indent(log))
      return msg.join('\n')
    },
    console_group: (group: Case, prop, depth = 0) => {
      // report title
      const verbose = this.suite.verbose
      const t = this.t
      const name = group ? group.parent[group.parent.length - 1] : ''
      //      console.log(this.num)
      const status = this.num[name].ERROR ? 'ERROR' :
        this.num[name].WARN ? 'WARN' :
          this.num[name].OK ? 'OK' : 'NODATA'
      const color = statusColor[status]
      const msg: string[] = []
      if (group) msg.push(color(`${statusSign[status]}${status} ${chalk.bold(group.title)} (${name})`))
      if (group && group.description && verbose) msg.push(group.description)
      if (prop.recursive) {
        // report entries
        Object.keys(this.cases).filter(k => (!name && !k.match(/\./)) || k.replace(/\.[^.]*$/, '.') === name + '.')
          .forEach(k => {
            if (this.tests[k]) msg.push(indent(this.formatter.console_test(this.tests[k], prop), group ? inum : 0))
            else msg.push(indent(this.formatter.console_group(this.cases[<any>k], prop, depth + 1), group ? inum : 0))
          })
      }
      // report summary
      if (!depth) {
        msg.push('')
        msg.push(t('summary.overall', {
          count: this.num[name].total,
          criteria: (this.paths ? this.paths.join(', ') : t('summary.all'))
            + (this.tags ? t('summary.tags') + this.tags?.join(', ') : '')
        }))
        if (this.num[name].OK) msg.push(statusColor.OK(t('summary.ok', { count: this.num[name].OK })))
        if (this.num[name].WARN) msg.push(statusColor.WARN(t('summary.warn', { count: this.num[name].WARN })))
        if (this.num[name].ERROR) msg.push(statusColor.ERROR(t('summary.error', { count: this.num[name].ERROR })))
        if (this.num[name].NODATA) msg.push(statusColor.NODATA(t('summary.nodata', { count: this.num[name].NODATA })))
        //      if (this.num[name].fixes) msg.push(statusColor.FIX(t('summary.fixes', { count: this.num[name].fixes })))
        //      if (this.num[name].fixed) msg.push(statusColor.FIX(t('summary.fixed', { count: this.num[name].fixed })))
      }
      return msg.join('\n')
    }
  }

}

export default Runner
