import { createPool, DatabasePoolType } from 'slonik';

const slonikPool: { [index: string]: DatabasePoolType } = {}
const slonikCleanup: any = {}
const SLONIK_CLOSE_TIMEOUT = 300000

export function pgPool(setup: any, poolSize: number = 5) {
    const url = init(setup).connection.url
    let id = slonikPool[url + '_L']
        ? url + '_L'        // use biggest if already there
        : poolSize > 5      // big pool needed
            ? url + '_L'    // cretae big pool
            : url           // use small pool
    if (!slonikPool[id])
        slonikPool[id] = createPool(url, {
            maximumPoolSize: poolSize,
            idleTimeout: poolSize > 5 ? 300000 : 5000
        })
    if (slonikCleanup[id]) clearTimeout(slonikCleanup[id])
    slonikCleanup[id] = setTimeout(() => { slonikPool[id].end(); delete slonikPool[id] }, SLONIK_CLOSE_TIMEOUT)
    return slonikPool[id]
}

export function init(setup: any) {
    if (!setup.connection.url) {
        // get connection string
        let connString = `${setup.client === 'pg' ? 'postgres' : setup.client}://`
        if (setup.connection.user) {
            connString += `${setup.connection.user}`
            if (setup.connection.password) connString += `:${setup.connection.password.replace(/@/g, '%40')}`
            connString += '@'
        }
        connString += setup.connection.host || 'localhost'
        if (setup.connection.port) connString += `:${setup.connection.port}`
        if (setup.connection.database) connString += `/${setup.connection.database}`
        setup.connection.url = connString
    }
    return setup
}

export default { init, pgPool }
