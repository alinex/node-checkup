import { loadNamespace } from '../../i18n'

import Fix from '..'

loadNamespace('web')

export class FixWeb extends Fix {}

export default FixWeb
