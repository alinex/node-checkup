import FixWeb from '.'
import { T, exists } from '../../i18n';

class FixWebHTTP extends FixWeb {

  public async analyze () {
    const t = T(this.lang, 'web')
    this.title = t('http.fix.title')
    this.debug('analyze')

    // analyze the last dns call
    const code = this.test.result.code
    if (exists(`web:http.fix.${code}`))
      this.description += t(`http.fix.${code}`, { url: this.test.data.url }) + '\n'

    // ping nameservers
    return Promise.resolve()
      .then(() => {
        this.status = 'MANUAL'
        return
      })
      // general error
      .catch((e: Error) => {
        this.debug(e)
        this.error = e
      })
  }

}

export default FixWebHTTP
