import { loadNamespace } from '../../i18n'

import Fix from '..'

loadNamespace('database')

export class FixDatabase extends Fix {}

export default FixDatabase
