import Database from '.'
import { T } from '../../i18n';

class FixDatabaseMongoDB extends Database {

  public async analyze() {
    const t = T(this.lang, 'database')
    this.title = t('mongodb.fix.title')
    this.debug('analyze')

    // mongodb test
    const result = this.test.result
    let msg = ''
    if (result.uptime < (this.test.data.warn?.uptime || this.test.data.error?.uptime || 900)) // default 15 minutes
      msg += t('mongodb.fix.uptime', { count: result.uptime }) + '\n'
    if (result.warning)
      msg += t('mongodb.fix.warning', { count: result.warning }) + '\n'
    if (result.conn > 10000)
      msg += t('mongodb.fix.conn', { count: result.conn }) + '\n'
    if (result.cache_free < 10 * 1024 * 1024)
      msg += t('mongodb.fix.cache_free', { count: result.cache_free }) + '\n'
    if (result.queue_per_client > 0.2)
      msg += t('mongodb.fix.queue_per_client', { count: result.queue_per_client }) + '\n'
    if (result.read_latency > 1000)
      msg += t('mongodb.fix.read_latency', { count: result.read_latency }) + '\n'
    if (result.write_latency > 1000)
      msg += t('mongodb.fix.write_latency', { count: result.write_latency }) + '\n'
    if (!result.is_master)
      msg += t('mongodb.fix.is_master', { count: result.is_master }) + '\n'
    if (this.test.data.warn?.writable && result.readOnly)
      msg += t('mongodb.fix.writable', { count: result.read_only }) + '\n'
    if (msg) {
      this.description += msg
      this.status = 'MANUAL'
    } else this.status = 'IRRELEVANT'
  }

}

export default FixDatabaseMongoDB
