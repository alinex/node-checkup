import FixNet from '.'
import { T } from '../../i18n';

class FixNetTCP extends FixNet {

  public async analyze () {
    if (!this.context.host || !this.context.port) return

    const t = T(this.lang, 'net')
    this.title = t('tcp.fix.title')
    this.debug('analyze')

    // tcp test
    return this.subTest({
      test: 'net/tcp',
      data: { host: this.context.host, port: this.context.port }
    })
      .then((e) => {
        if (e.status !== 'OK') {
          this.description += t('tcp.fix.connect', { host: e.data.host, port: e.data.port, status: e.status, error: e.error })
          this.status = e.status
        } else this.status = 'OK'
      })

      // general error
      .catch((e: Error) => {
        this.debug(e)
        this.error = e
      })
  }

}

export default FixNetTCP
