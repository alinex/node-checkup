import { loadNamespace } from '../../i18n'

import Fix from '..'

loadNamespace('net')

export class FixNet extends Fix {}

export default FixNet
