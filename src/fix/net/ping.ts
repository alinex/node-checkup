import FixNet from '.'
import { T } from '../../i18n';

class FixNetPing extends FixNet {

  public async analyze () {
    if (!this.context.host) return

    const t = T(this.lang, 'net')
    this.title = t('ping.fix.title')
    this.debug('analyze')

    // tcp test
    return this.subTest({
      test: 'net/ping',
      data: { host: this.context.host }
    })
      .then((e) => {
        this.status = e.status
        if (e.status !== 'OK') {
          this.description += t('ping.fix.connect', { host: e.data.host, status: e.status, error: e.error })
        }
      })

      .then(() => {
//        this.status = 'MANUAL'
        return
      })
      // general error
      .catch((e: Error) => {
        this.debug(e)
        this.error = e
      })
  }

}

export default FixNetPing
