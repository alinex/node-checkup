import { getServers } from 'dns'

import FixNet from '.'
import { T, exists } from '../../i18n';

//import { indent } from '../../string';

//const altNameservers = [
//  '208.67.222.222', '208.67.220.220', // OpenDNS
//  '1.1.1.1', '1.0.0.1', // CloudFlare
//  '8.8.8.8', '8.8.4.4', // Google
//  '8.26.56.26', '8.20.247.20', // Comodo
//  '9.9.9.9', '149.112.112.112', // Quad9
//  '64.6.64.6', '64.6.65.6', // VersiSign
//]

class FixNetDNS extends FixNet {

  public async analyze () {
    const t = T(this.lang, 'net')
    this.title = t('dns.fix.title')
    this.debug('analyze')

    // analyze the last dns call
    const nameserver: string[] = this.test.data.nameserver || getServers()
    const domain = this.context.domain || this.test.data.domain
    this.description += t('dns.fix.servers', { count: nameserver.length, nameserver: nameserver.join(', ') }) + ' '
    if (this.test.error?.code && exists(`net:dns.fix.${this.test.error.code}`))
      this.description += t(`dns.fix.${this.test.error.code}`, { domain }) + '\n'

    // ping nameservers
    return this.subTests(nameserver.map(e => {
      return {
        test: 'net/udp',
        data: { host: e, port: 53 }
      }
    }))
      .then((sub) => {
        sub.forEach(e => {
          if (e.status !== 'OK')
            this.description += t('dns.fix.connect', { nameserver: e.data.host, status: e.status, error: e.error })
        })
      })

      .then(() => {
        this.status = 'MANUAL'
        return
      })
      // general error
      .catch((e: Error) => {
        this.debug(e)
        this.error = e
      })
  }

}

export default FixNetDNS
