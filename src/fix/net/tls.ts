import FixNet from '.'
import { T } from '../../i18n';

class FixNetTLS extends FixNet {

  public async analyze () {
    const t = T(this.lang, 'net')
    this.title = t('tls.fix.title')
    this.debug('analyze')

    // tcp test
    // mongodb test
    const result = this.test.result
    let msg = ''
    let version = t('tls.fix.tls_version') + '\n'
    result.problems.forEach((p: string) => {
      if (p.match(/Domain name mismatch/)) msg += t('tls.fix.nameMismatch', { for: result.for.join(', '), domain: this.test.data.domain })
      if (p.match(/TLS 1.[01] offered/)) {
        msg += version
        version = ''
      }
      if (p.match(/TLS 1.0 offered/)) msg += t('tls.fix.tls_1_0') + '\n'
      if (p.match(/TLS 1.1 offered/)) msg += t('tls.fix.tls_1_1') + '\n'
      if (p.match(/HSTS is not offered/)) msg += t('tls.fix.hsts') + '\n'
    })
    if (msg) {
      this.description += msg
      this.status = 'MANUAL'
    } else this.status = 'IRRELEVANT'
  }

}

export default FixNetTLS
