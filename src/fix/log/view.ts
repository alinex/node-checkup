import * as Builder from '@alinex/validator/lib/schema';

import FixLog from '.'
import { T } from '../../i18n';
import { indent } from '../../string';

class FixLogView extends FixLog {

  public static valNum = new Builder.NumberSchema({ integer: true, min: 0, default: 0 })

  public async analyze() {
    const t = T(this.lang, 'log')
    this.title = t('view.fix.title')
    this.debug('analyze')

    if (this.data.before || this.data.after) {
      const pos: { [key: number]: boolean } = {}
      this.test.result.pos.forEach((p: number) => {
        for (let i = 0; i <= this.data.before && i <= p; i++) pos[p - i] = true
        pos[p] = true
        for (let i = 0; i <= this.data.after && i + p < this.test.ds.data.length; i++) pos[p + i] = true
      })
      const events: string[] = []
      let n = 0
      Object.keys(pos).forEach((p: string) => {
        if (n && parseInt(p) > n + 1) events.push('---')
        events.push(JSON.stringify(this.test.ds.data[p]))
        n = parseInt(p)
      })
      this.description += t('view.fix.events') + '\n' + indent(events.join('\n')) + '\n'
    } else {
      const events = this.test.result.matches.map((e: any) => JSON.stringify(e)).join('\n')
      this.description += t('view.fix.events') + '\n' + indent(events) + '\n'
    }

    const raw = this.test.ds.meta.raw.toString()
    this.description += t('view.fix.raw') + '\n' + indent(raw) + '\n'
    this.status = 'MANUAL'
  }

}

export default FixLogView
