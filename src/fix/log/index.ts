import { loadNamespace } from '../../i18n'

import Fix from '..'

loadNamespace('log')

export class FixLog extends Fix { }

export default FixLog
