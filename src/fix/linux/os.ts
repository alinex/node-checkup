import { inspect } from 'util'

import FixLinux from '.'
import { T } from '../../i18n';
//import { indent } from '../../string';

class FixLinuxOS extends FixLinux {

  public async analyze() {
    const t = T(this.lang, 'linux')
    this.title = t('os.fix.title')
    this.debug('analyze')

    // mongodb test
    const result = this.test.result
    let msg = ''
    if (result.eol_days) msg += t('os.fix.eol', { count: result.eol_days, date: result.eol }) + '\n'
    if (result.security) msg += t('os.fix.security', { count: result.security }) + '\n'
    if (result.updates) msg += t('os.fix.updates', { count: result.updates }) + '\n'
    if (result.removes) msg += t('os.fix.removes', { count: result.removes }) + '\n'
    if (msg) {
      this.type = 'auto'
      if (result.dist_base === 'debian') {
        if (result.security || result.updates) msg += t('os.fix.upgradeDebian')
        if (result.eol_days) msg += t('os.fix.distUpgradeDebian')
      }
      if (result.dist_base === 'arch') {
        if (result.security || result.updates) msg += t('os.fix.upgradeArch')
      }
    }
    if (msg) {
      this.description += msg
      this.status = 'MANUAL'
    } else this.status = 'IRRELEVANT'
  }

  public async repair() {
    const t = T(this.lang, 'linux')
    this.action = t('os.fix.repair') + '\n'
    this.debug('repair')

    const result = this.test.result
    await this.shell(async () => {
      // commands
      let cmds = await this.commands(['apt', 'shutdown', 'pacman'], true)
      const nosudo = Object.keys(cmds).filter(e => !cmds[e]).map(e => inspect(e.substr(1)))
      if (nosudo.join()) {
        this.status = 'NODATA'
        this.error = new Error(t('core:fix.nosudoRepair', { count: nosudo.length, cmd: nosudo.join(', ') }))
        return
      }
      if (result.updates || result.removes) {
        const cmd: any = {
          update: {
            debian: `${cmds.$apt} upgrade -y`,
            arch: `${cmds.$pacman} -Su`
          },
          rmove: {
            debian: `${cmds.$apt} autoremove --purge -y`,
            arch: `${cmds.$pacman} -Rns $(pacman -Qdtq)`
          }
        }
        await this.execute(cmd.update[result.dist_base])
          .then(res => {
            if (res.exitCode) throw new Error(`Failed upgrade with status code ${res.exitCode}.`)
            else this.status = 'OK'
          })
        await this.execute(cmd.remove[result.dist_base])
          .then(res => {
            if (res.exitCode) throw new Error(`Failed autoremove with status code ${res.exitCode}.`)
            else this.status = 'OK'
          })
      }
      // reboot
      await this.execute(`test -e /var/run/reboot-required && ${cmds.$shutdown} -r now`)
        .then(res => {
          if (res.exitCode) throw new Error(`Failed to reboot system code: ${res.exitCode}.`)
          else this.status = 'OK'
        })
    })
      // general error
      .catch((e: Error) => {
        this.status = 'ERROR'
        this.error = e
      })
  }
}

export default FixLinuxOS
