import { loadNamespace } from '../../i18n'

import Fix from '..'

loadNamespace('linux')

export class FixLinux extends Fix {}

export default FixLinux
