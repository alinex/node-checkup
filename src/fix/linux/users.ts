import * as Builder from '@alinex/validator/lib/schema';

import FixLinux from '.'
import { T } from '../../i18n';
import { indent } from '../../string';

// scripts to be used
const lastUsers = (num = 5) => `last -F | head -n ${num}`

class FixLinuxUsers extends FixLinux {

  public static valUsers = new Builder.NumberSchema ({ integer: true, min: 1, default: 5 })

  public async analyze () {
    const t = T(this.lang, 'linux')
    this.title = t('users.fix.title')
    this.debug('analyze')

    await this.shell(async () => {
      // users
      await this.execute(lastUsers(this.data.users), false).then(res => {
        if (res.stdout) this.description += t('users.fix.users') + '\n' + indent(res.stdout) + '\n'
      })
    })
      .then(() => this.status = 'MANUAL')
      // general error
      .catch((e: Error) => {
        this.debug(e)
        this.error = e
      })
  }

}

export default FixLinuxUsers
