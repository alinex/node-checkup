import * as Builder from '@alinex/validator/lib/schema';
import { each, filter } from '@alinex/async'
import { inspect } from 'util'

import FixLinux from '.'
import { t, T } from '../../i18n';
import { indent } from '../../string';

// scripts to be used
const bigFiles = (du: string, dir: string, num: number) => `${du} -Sh '${dir}' 2>/dev/null | sort -rh | head -${num}`
const oldFiles = (find: string, dir: string, days: number) => `${find} '${dir}' -type f -mtime +${days} -printf '%TY-%Tm-%Td %TH:%TM\t%s\n' | sort`
const oldFilesDelete = (find: string, dir: string, days: number) => `${find} '${dir}' -type f -mtime +${days} -delete`
const packFiles = (find: string, dir: string, days: number, max: number) => {
  const add = max ? `! -mtime +${max}` : ''
  return `${find} '${dir}' -type f -mtime +${days} ${add} ! \\( -name '*.gz' -o -name '*.tgz' -o -name '*.zip' -o -name '*.bz2' -o -name '*.iso' -o -name '*.7z' \\) -printf '%TY-%Tm-%Td %TH:%TM\t%s\n' | sort`
}
const packFilesCompress = (find: string, gzip: string, dir: string, days: number, max: number) => {
  const add = max ? `! -mtime +${max}` : ''
  return `${find} '${dir}' -type f -mtime +${days} ${add} ! \\( -name '*.gz' -o -name '*.tgz' -o -name '*.zip' -o -name '*.bz2' -o -name '*.iso' -o -name '*.7z' \\) -exec ${gzip} {} \\;`
}
const mount = (dir: string) => `df -P '${dir}'  | tail -1 | awk '{ print $NF}'`

class FixLinuxDisk extends FixLinux {

  public static bigNum = new Builder.NumberSchema({ integer: true, min: 1, default: 5 })
  public static compress = new Builder.ObjectSchema({
    title: t('disk.fix.compress'),
    item: { '*': new Builder.NumberSchema({ title: t('disk.fix.days'), integer: true, min: 0 }) },
    default: { '/var/log': 7 }
  })
  public static delete = new Builder.ObjectSchema({
    title: t('disk.fix.delete'),
    item: { '*': new Builder.NumberSchema({ title: t('disk.fix.days'), integer: true, min: 0 }) },
    default: {
      '/tmp': 7,
      '/var/log': 30,
      '/home/*/.local/share/Trash': 30,
      '/home/*/Downloads': 90
    }
  })

  public async analyze () {
    const t = T(this.lang, 'linux')
    this.title = t('disk.fix.title')
    this.debug('analyze')

    await this.shell(async () => {
      // commands
      const cmds = await this.commands(['du', 'find'], true)
      const nosudo = Object.keys(cmds).filter(e => e.startsWith('$') && !cmds[e]).map(e => inspect(e.substr(1)))
      if (nosudo.join()) this.description += t('core:fix.nosudo', { count: nosudo.length, cmd: nosudo.join(', ') }) + '\n'
      // space
      const space = this.test.result.filter((e: any) => e.percent <= this.test.data.warn.percent)
      await each(space, async (disk: any) => {
        this.description += t('disk.fix.space', { disk }) + '\n'
        // big areas
        this.logFix('Find biggest directories and files.', false)
        await this.execute(bigFiles(cmds.$du || cmds.du, disk.mount, this.data.bigNum), false).then(res => {
          if (res.stdout) this.description += t('disk.fix.bigFiles', { disk }) + '\n' + indent(res.stdout) + '\n'
        })
        // delete
        this.logFix('Find deletable files.', false)
        let found = 0
        let temp = this.data.delete
        let injections = Object.keys(temp).filter((e: any) => e.match(/['";]/)).join(', ')
        if (injections) throw new Error(`Disallowed characters in path name: ${injections}`)
        await filter(Object.keys(temp), dir => this.execute(mount(dir), false).then(res => res.stdout == disk.mount))
          .then(list => each(list, dir => {
            return this.execute(oldFiles(cmds.$find || cmds.find, dir, temp[dir]), false).then(res => {
              if (!res.stdout) return
              const lines = res.stdout.split(/\n/)
              const size = lines.map((l: string) => parseInt(l.replace(/^.*?(\d+)$/, '$1'))).reduce((acc: number, c: number) => acc + c)
              found += size
              this.description += t('disk.fix.oldFiles', {
                dir,
                count: lines.length,
                age: temp[dir],
                first: new Date(lines[0].substr(0, 16)),
                last: new Date(lines[lines.length - 1].substr(0, 16)),
                size
              }) + '\n'
            })
          }))
        if (found) {
          this.description += t('disk.fix.oldFilesDelete', { found }) + '\n'
          this.type = 'auto'
        }
        // compress
        this.logFix('Find compressable files.', false)
        found = 0
        temp = this.data.compress
        injections = Object.keys(temp).filter((e: any) => e.match(/['";]/)).join(', ')
        if (injections) throw new Error(`Disallowed characters in path name: ${injections}`)
        await filter(Object.keys(temp), dir => this.execute(mount(dir), false).then(res => res.stdout == disk.mount))
          .then(list => each(list, dir => {
            return this.execute(packFiles(cmds.$find || cmds.find, dir, temp[dir], this.data.delete[dir]), false).then(res => {
              if (!res.stdout) return
              const lines = res.stdout.split(/\n/)
              const size = lines.map((l: string) => parseInt(l.replace(/^.*?(\d+)$/, '$1'))).reduce((acc: number, c: number) => acc + c)
              found += size
              this.description += t('disk.fix.packFiles', {
                dir,
                count: lines.length,
                age: temp[dir],
                first: new Date(lines[0].substr(0, 16)),
                last: new Date(lines[lines.length - 1].substr(0, 16)),
                size
              }) + ' '
            })
          }))
        if (found) {
          this.description += t('disk.fix.packFilesCompress', { found }) + '\n'
          this.type = 'auto'
        }
        // inodes
        const inodes = this.test.result.filter((e: any) => e.ipercent <= this.test.data.warn.percent)
        inodes.forEach((disk: any) => {
          this.description += t('disk.fix.inodes', { disk }) + '\n'
          // find most directories / files
          // find old temp / log files(number + size)
        })
      })
    })
      .then(() => this.status = 'MANUAL')
      // general error
      .catch((e: Error) => {
        this.debug(e)
        this.error = e
      })
  }

  public async repair () {
    const t = T(this.lang, 'linux')
    this.action = t('disk.fix.repair') + '\n'
    this.debug('repair')

    await this.shell(async () => {
      // commands
      const cmds = await this.commands(['du', 'find', 'gzip'], true)
      const nosudo = Object.keys(cmds).filter(e => e.startsWith('$') && !cmds[e]).map(e => inspect(e.substr(1)))
      if (nosudo.join()) {
        this.status = 'NODATA'
        this.error = new Error(t('core:fix.nosudo', { count: nosudo.length, cmd: nosudo.join(', ') }))
        return
      }
      this.status = 'OK'
      const space = this.test.result.filter((e: any) => e.percent <= this.test.data.warn.percent)
      await each(space, async (disk: any) => {
        // delete
        let temp = this.data.delete
        await filter(Object.keys(temp), dir => this.execute(mount(dir)).then(res => res.stdout == disk.mount))
          .then(list => each(list, dir => {
            this.logFix(`Delete old files in ${dir}`)
            return this.execute(oldFilesDelete(cmds.$find || cmds.find, dir, temp[dir])).then(res => {
              if (res.exitCode) throw new Error(`Problem deleting files in ${disk}`)
            })
          }))
        // compress
        temp = this.data.compress
        await filter(Object.keys(temp), dir => this.execute(mount(dir)).then(res => res.stdout == disk.mount))
          .then(list => each(list, dir => {
            this.logFix(`Compress old files in ${dir}`)
            return this.execute(packFilesCompress(cmds.$find || cmds.find, cmds.$gzip || cmds.gzip, dir, temp[dir], this.data.delete[dir])).then(res => {
              if (res.exitCode) throw new Error(`Problem compressing files in ${disk}`)
            })
          }))
      })
    })

      // general error
      .catch((e: Error) => {
        this.status = 'ERROR'
        this.error = e
      })
  }

}

export default FixLinuxDisk
