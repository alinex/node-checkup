//import * as Builder from '@alinex/validator/lib/schema';1
//import { inspect } from 'util'

import FixLinux from '.'
import { T } from '../../i18n';
import { indent } from '../../string';

// Maybe later use:
// ls - l / proc / 2168 / fd: #pipes, #sockets, #shm, #anon_inode, #files
// cat / proc / 2168 / limits: open files
// pmap - x 2168 # maybe too much

class FixLinuxProcess extends FixLinux {

  public async analyze () {
    const t = T(this.lang, 'linux')
    this.title = t('process.fix.title')
    this.debug('analyze')

    await this.shell(async () => {
      this.logFix('Get the process tree.', false)
      await this.execute(`ps -f --forest -C ${this.test.data.name}`, false)
      .then(res => {
        if (res.stderr || res.exitCode)
          throw new Error(`Could not get the process tree: rc ${res.exitCode} ${res.stderr ? ': ' + res.stderr : ''}`)
        this.description += '\n' + indent(t('process.fix.tree')) + '\n' + indent(res.stdout)
      })
      await this.execute(`ps -fL -C ${this.test.data.name}`, false)
        .then(res => {
          if (res.stderr || res.exitCode)
            throw new Error(`Could not get the process threads: rc ${res.exitCode} ${res.stderr ? ': ' + res.stderr : ''}`)
          this.description += '\n' + indent(t('process.fix.threads')) + '\n' + indent(res.stdout)
        })
    })
      .then(() => this.status = 'MANUAL')
      // general error
      .catch((e: Error) => {
        this.debug(e)
        this.error = e
      })

  }

}

export default FixLinuxProcess
