import * as Builder from '@alinex/validator/lib/schema';

import FixLinux from '.'
import { T } from '../../i18n';
import { indent } from '../../string';

class FixLinuxMemory extends FixLinux {

  public static valProcs = new Builder.NumberSchema({ integer: true, min: 1, default: 5 })

  public async analyze() {
    const t = T(this.lang, 'linux')
    this.title = t('memory.fix.title')
    this.debug('analyze')

    const virt = () => '/usr/bin/systemd-detect-virt'
    const memProcs = (num = 5) => `for pid in $(ps -eo pid,ppid | grep ' 1$' | sed 's/ *1$//');
    do pstree -p $pid | grep -o '([0-9]\\+)' | grep -o '[0-9]\\+' \\
    | xargs ps h -o %mem,%cpu,pid,cmd -p \\
    | awk '{memory+=$1;cpu+=$2;if(pid=="")pid=$3} END {printf "%2.1f\\t%2.1f\\t%-5s\\t%s\\n", memory,cpu,pid,$4}';
    done | sort -gr | grep -v awk | head -n ${num}`
    const swap = () => `for file in /proc/*/status ; do awk '/VmSwap|Name/{printf $2 " \t" $3}END{ print ""}' $file; done | sort -k 2 -n -r | head -n 5`

    // let pid = 'ps -eo pid,ppid'
    await this.shell(async () => {
      // vm
      let isVM = false
      this.logFix('Check if it is a virtual machine.', false)
      await this.execute(virt(), false).then(res => isVM = res.stdout && res.stdout !== 'none')
      this.description = t(isVM ? 'memory.fix.hardware' : 'memory.fix.vm')
      // cpu procs
      this.logFix('Get the processes with the most memory usage.', false)
      let procs: string = ''
      await this.execute(memProcs(this.data.procs), false).then(res => {
        if (res.stderr || res.exitCode)
          throw new Error(`Could not analyze high memory usage processes: rc ${res.exitCode} ${res.stderr ? ': ' + res.stderr : ''}`)
        procs = res.stdout
      })
      this.description += '\n' + indent(t('memory.fix.table')) + '\n' + indent(procs.replace(/ /g, ' \t'))
      // swap usage
      if (this.test.result.swap_free_percent < 0.9) {
        await this.execute(swap(), false).then(res => {
          this.description += '\n' + t('memory.fix.swapIntro')
          this.description += '\n' + indent(t('memory.fix.swap')) + '\n' + indent(res.stdout)
        })
      }
      this.description += '\n' + t('memory.fix.summary')
    })
      .then(() => this.status = 'MANUAL')
      // general error
      .catch((e: Error) => {
        this.debug(e)
        this.error = e
      })

  }

}

export default FixLinuxMemory
