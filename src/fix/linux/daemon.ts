import { inspect } from 'util'

import FixLinux from '.'
import { T } from '../../i18n';
import { indent } from '../../string';

class FixLinuxDaemon extends FixLinux {

  public async analyze () {
    const t = T(this.lang, 'linux')
    this.title = t('daemon.fix.title')
    this.debug('analyze')

    await this.shell(async () => {
      const result = this.test.result
      const service = this.test.data.service
      // switch
      if (result.status === 'exited') {
        this.description = t('daemon.fix.exited', { result })
        // let pid = 'ps -eo pid,ppid'
        this.logFix('Read service script.', false)
        return this.execute(`cat ${result.script}`, false)
          .then(res => this.description += '\n' + indent(res.stdout))
      } else if (result.age < this.test.data.warn.age) {
        this.description = t('daemon.fix.intro', { service, result })
        this.description += '\n' + t('daemon.fix.age', { result })
        this.description += '\n' + t('daemon.fix.manual', { service, result }).trim()
      } else {
        this.description = t('daemon.fix.intro', { service, result })
        this.description += '\n' + t('daemon.fix.manual', { service, result }).trim()
        this.type = 'auto'
      }
    })
      .then(() => this.status = 'MANUAL')
      // general error
      .catch((e: Error) => {
        this.debug(e)
        this.error = e
      })
  }

  public async repair () {
    const t = T(this.lang, 'linux')
    this.action = t('daemon.fix.repair') + '\n'
    this.debug('repair')

    const result = this.test.data.result
    const service = this.test.data.service
    await this.shell(async () => {
      // commands
      let cmds = await this.commands([`systemctl start ${service}`, `systemctl stop ${service}`, 'kill'], true)
      const nosudo = Object.keys(cmds).filter(e => !cmds[e]).map(e => inspect(e.substr(1)))
      if (nosudo.join()) {
        this.status = 'NODATA'
        this.error = new Error(t('core:fix.nosudoRepair', { count: nosudo.length, cmd: nosudo.join(', ') }))
        return
      }
      cmds = {
        $service: cmds[`$systemctl start ${service}`],
        $kill: cmds.$kill
      }
      // switch
      if (this.test.result.status === 'running') {
        this.logFix(t('daemon.fix.stop', { service: service }))
        return this.execute(`${cmds.$service} stop ${service}`)
          .then(() => this.execute(`${cmds.$kill} -0 ${result.pid}`))
          .then(res => {
            if (res.exitCode) {
              this.logFix(t('daemon.fix.kill'))
              return this.execute(`${cmds.$kill} -9 ${result.pid}`)
            }
          }).then(() => {
            return this.execute(`${cmds.$service} start ${service}`)
              .then(async res => {
                if (res.exitCode) {
                  this.action += t('daemon.fix.failed')
                  await this.execute(`systemctl status ${service}`)
                    .then(res => this.description += '\n' + indent(res.stdout))
                  throw new Error('Start was not possible!')
                }
                this.status = 'OK'
              })
          })
      } else {
        this.logFix(t('daemon.fix.start', { service: service }))
        return this.execute(`${cmds.$service} start ${service}`)
          .then(async res => {
            if (res.exitCode) {
              this.action += t('daemon.fix.failed')
              await this.execute(`systemctl status ${service}`)
                .then(res => this.description += '\n' + indent(res.stdout))
              throw new Error('Start was not possible!')
            }
            this.status = 'OK'
          })
      }
    })

      // general error
      .catch((e: Error) => {
        this.status = 'ERROR'
        this.error = e
      })
  }

}

export default FixLinuxDaemon
