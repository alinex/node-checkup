import * as Builder from '@alinex/validator/lib/schema';

import FixLinux from '.'
import { T } from '../../i18n';
import { indent } from '../../string';

class FixLinuxLoad extends FixLinux {

  public static valIowait = new Builder.NumberSchema({ sanitize: true, min: 0, max: 1, default: 0.2 })
  public static valSteal = new Builder.NumberSchema({ sanitize: true, min: 0, max: 1, default: 0.2 })
  public static valProcs = new Builder.NumberSchema({ integer: true, min: 1, default: 5 })

  public async analyze() {
    const t = T(this.lang, 'linux')
    this.title = t('load.fix.title')
    this.debug('analyze')

    const virt = () => '/usr/bin/systemd-detect-virt'
    const psWait = () => 'ps aux | awk \'(NR==1)||($8~/D/){print}\''
    const cpuProcs = (num = 5) => `for pid in $(ps -eo pid,ppid | grep ' 1$' | sed 's/ *1$//');
    do pstree -p $pid | grep -o '([0-9]\\+)' | grep -o '[0-9]\\+' \\
    | xargs ps h -o %mem,%cpu,pid,cmd -p \\
    | awk '{memory+=$1;cpu+=$2;if(pid=="")pid=$3} END {printf "%2.1f\\t%2.1f\\t%-5s\\t%s\\n", cpu,memory,pid,$4}';
    done | sort -gr | grep -v awk | head -n ${num}`

    await this.shell(async () => {
      // vm
      let isVM = false
      this.logFix('Check if it is a virtual machine.', false)
      await this.execute(virt(), false).then(res => isVM = res.stdout && res.stdout !== 'none')
      // steal
      if (this.test.result.steal > this.data.steal)
        this.description += t('load.fix.steal', { value: this.test.result.steal }) + '\n'
      // iowait
      if (this.test.result.iowait > this.data.iowait) {
        this.description += t('load.fix.iowait', { value: this.test.result.iowait })
        this.logFix('Processes waiting for IO using stat flag D.', false)
        await this.execute(psWait(), false).then(res => {
          if (res.stdout.split(/\n/).length > 1)
            this.description += t('load.fix.pswait', { text: indent(res.stdout) })
        })
        this.description += '\n'
      }
      // cpu procs
      this.logFix('Get the processes with the most current cpu usage.', false)
      let procs: string = ''
      await this.execute(cpuProcs(this.data.procs), false).then(res => {
        if (res.stderr || res.exitCode)
          throw new Error(`Could not analyze high cpu usage processes: rc ${res.exitCode} ${res.stderr ? ': ' + res.stderr : ''}`)
        procs = res.stdout
      })
      this.description = t(isVM ? 'load.fix.vm' : 'load.fix.hardware')
      this.description += '\n' + indent(t('load.fix.table')) + '\n' + indent(procs.replace(/ /g, ' \t'))
      this.description += '\n' + t('load.fix.summary')
    })
      .then(() => this.status = 'MANUAL')
      // general error
      .catch((e: Error) => {
        this.debug(e)
        this.error = e
      })

  }

}

export default FixLinuxLoad
