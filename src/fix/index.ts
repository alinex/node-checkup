import { each, map } from '@alinex/async';
import Debug from "debug";
import { inspect } from 'util'
import ValidationError from '@alinex/validator/lib/error'

import { shell, execute, Client, commands } from '../shell'
import Test from '../test'

const empty = {
  parent: [''],
  title: '',
  tags: [],
  data: {},
  cache: 0,
  fix: {},
  autofix: false
}

class Fix {
  protected debug: debug.Debugger
  protected data: any
  protected context: any
  public test: any
  protected lang: string
  public title: string | undefined
  public type: string = 'manual'
  public description: string = ''
  public action: string = ''
  public status: string = 'NODATA'
  public error: ValidationError | Error | undefined
  private client: Client | undefined
  private _log: string[] = []

  constructor(test: Test, context?: any) {
    this.debug = Debug(`checkup:${this.constructor.name.replace(/([a-z])([A-Z])/g, '$1:$2').toLowerCase()}`);
    this.test = test
    this.data = test.fixdata
    this.lang = test.lang
    this.context = context
  }

  public async analyze() { }
  public async repair() { }

  protected logFix(msg: string, log: boolean = true) {
    this.debug('FIX      %s', msg)
    if (log) this._log.push(`1:REPAIR   ${msg.replace(/\n/g, '\n         ')}`)
    else this._log.push(`5:ANALYZE  ${msg.replace(/\n/g, '\n         ')}`)
  }
  protected logProcess(msg: any, log: boolean = true) {
    this.debug('PROCESS  %o', msg)
    this._log.push(`${log ? 2 : 5}:PROCESS  ${inspect(msg)}`)
  }
  protected logStdout(msg: string, log: boolean = true) {
    this.debug('STDOUT   %s', msg)
    this._log.push(`${log ? 3 : 7}:STDOUT   ${msg.replace(/\n/g, '\n         ').trim()}`)
  }
  protected logStderr(msg: string, log: boolean = true) {
    this.debug('STDERR   %s', msg)
    this._log.push(`${log ? 3 : 6}:STDERR   ${msg.replace(/\n/g, '\n         ').trim()}`)
  }
  protected logCode(msg: number, log: boolean = true) {
    this.debug('EXITCODE %s', msg)
    this._log.push(`${log ? 2 : 5}:EXITCODE ${msg}`)
  }

  public log(verbose: number): string {
    return this._log.filter(line => verbose >= new Number(line.charAt(0)))
      .map(line => line.substr(2)).join('\n')
  }

  protected shell(fn: () => Promise<any>) {
    const setup = this.data.server || this.test.data.server
    return shell(setup, (conn) => { this.client = conn; return fn() })
  }
  protected execute(cmd: string, log: boolean = true): Promise<any> {
    this.logProcess(cmd, log)
    return execute(this.client, cmd).then((res => {
      if (res.stdout) this.logStdout(res.stdout, log)
      if (res.stderr) this.logStderr(res.stderr, log)
      this.logCode(res.exitCode, log)
      return res
    }))
  }
  protected commands(list: string[], sudo = false) {
    return commands(this.client, list, sudo)
  }

  protected subTest(setup: any): Promise<any> {
    const v = setup
    return import(`${__dirname}/../test/${v.test}`)
      .then(test => {
        const worker: Test = new test.default(this.test.path, { ...empty, data: v.data }, this.lang)
        return worker.run().then(() => {
          worker._log.forEach(e => this._log.push(`${parseInt(e.substr(0, 1)) + 4}:${e.substr(2)}`))
          return worker
        })
      })
  }
  protected subTests(setup: any[], analyze = false): Promise<any[]> {
    return map(setup, (v) => {
      return import(`${__dirname}/../test/${v.test}`)
        .then(test => {
          const worker: Test = new test.default(this.test.path, { ...empty, data: v.data }, this.lang)
          return worker.run().then(() => {
            worker._log.forEach(e => this._log.push(`${parseInt(e.substr(0, 1)) + 4}:${e.substr(2)}`))
            return worker
          })
        })
        .then((worker) => {
          if (analyze && worker.fixes.length) return each(worker.fixes, (f, i) => {
            return f.analyze().then(() => {
              if (f.description) this.description += f.description + '\n'
              f._log.forEach(e => this._log.push(e))
            })
          }).then(() => worker)
          return worker
        })
    })
  }
}

export default Fix
