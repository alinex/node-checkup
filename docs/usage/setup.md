title: Setup

# Setup file

All the additional tools like data stores, scheduler and more are defined with an extended setup file instead of parameters in CLI. They are building the base setting if called by CLI.
Everything is put together in a single configuration file which is specified in CLI using `-s <file>.yml`.

If a setup file is used, the CLI call only needs this and all other parameters can be predefined there: `checkup -s config/my-suite.yml`

!!! Example

    ```yml
    # base settings
    config: config/cases.yml
    verbose: 0
    extend: true

    # storages
    store:
      file:
        status: true
        error: true
        data: true
        action: true
      database:
        client: mongodb
        connection:
          database: alinex_server
        prefix: checkup_
  
    # scheduler
    cron: /5 * * * *
    ```

## Basic Settings

### config

This is a reference to suite configuration which defines all the cases. But it can also be given as configuration option `-c <file>.yml`.

```yml
config: config/cases.yml
```

See the [case definition](case.md) for the contents of this file.

### verbose, extend

Both are used to enhance the level of verbosity which is kept low if not set. The `verbose` setting is a [numeric level](api.md#verbosity) reaching from 0...9. The `extend` is a boolean flag used to upgrade the verbosity in case of a problem to level 4 in case of an error in test and up to 6 in case of problems in fix analyzation.

```yml
verbose: 4
extend: true
```

### progress

Show a progress bar while running instead of immediate results after each test is done. The report will be displayed hierarchical after all tests are done. This flag can also be set using cli parameter `--progress`.

```yml
progress: false
```

### analyze, autofix

By default the system will do an analyzation run which can give you some possible fixes. Sometimes it is also possible to automatically repair this.

```yml
analyze: true
autofix: false
```

You can also use the switches `-F` or `--no-fix` to skip repair and analyzation and `-f` or `--autofix` to run repair if analyzation was done.

### concurrency

Define how many processes may run in parallel in each state. Keep in mind that this setting is used in different parts allowing a higher complete number of parallelism through hierarchical calls.

```yml
concurrency: 10
```

To keep the checkup system working reliable the latency should be kept low. If it is to high, reduce the concurrency.

### cron

Default setting for all tests which are undefined. The format is the same as in the case setting. A setting of `none` will prevent all undefined cases from scheduler.

## Storage Engines

Data stores can be used to collect the results over a longer time to analyze them later. Each data store is different with different possibilities. See the configuration [details for each store](../store.md).

```yml
store:
  file:
    status: true
    error: true
    data: true
    action: true
  database:
    client: mongodb
    connection:
      database: alinex_server
    prefix: checkup_
```

{!docs/assets/abbreviations.txt!}
