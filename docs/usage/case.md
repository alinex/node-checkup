title: Case Definition

# Case Configuration

The configuration will define all possible test cases and therefore can be very long. Any [DataStore](https://alinex.gitlab.io/node-datastore/spec) is possible. Mostly a simple YAML file is used like here:

```yml
local:
  title: Local machine
  tags: local

local.uptime:
  title: Uptime > 5 Minutes
  description: Server should be up for more than 5 Minutes
  tags: linux
  test: ./linux/uptime
  data:
    warn:
      min: 5
  cache: 100
  fix:
    sleep: 600
  autofix: true
  cron: '*/5 * * * *'
```

The configuration structure is made as a object of named cases. Each key in the top most object contains a group or case and is named as a hierarchical path using dot as separator.

-   `title` will always be displayed in console log
-   `description` should explain the purpose of this case and is displayed in verbose mode or on error
-   `tags` (optional) is a array or comma delimited string of free tag names which are used to filter and select test cases
-   `group` definition of dynamic group use
-   `test` will reference the test script used to run this test
-   `data` (optional) is an object for specifying how to test
-   `cache` (optional) set a different time to live for analyzed data in seconds (default = 50)
-   `fix` (optional) data for the fix analyzation or repair
-   `autofix` (optional) if set to true and auto repair is possible it should be 
-   `cron` (optional) if used as server this defines when to start in scheduled run

!!! Info

    Have a look at the [specific test/fix](../test) for the concrete `data` and `fix` values.

## Inheritance

Some fields can be always set on an upper level and will be inherited by the lower ones. This contains:

-   `keys` which will be concatenated
-   `data` of which the first sub level will be merged further down it will be overwritten
-   `cache` the first found (from test to root) will be used
-   `fix` of which the first sub level will be merged further down it will be overwritten
-   `autofix` can be enabled/disabled on a specific test or anywhere above
-   `cron` can be changed in each element or set to `none` to disable

## Case

Each case is described by `title` and `description`. The `tags` are used to select cases in addition to the path.

The `test` references the concrete JavaScript function to run. Use a file starting with `./xxx` to use the test from within the checkup core test folder or any JavaScript package path.
This can be controlled by setting some `data` elements, se the test description for possible data entries. The `cache` setting defines the time in seconds to keep this test result as valid.

Some tests has fixes which may be assigned. Some of them only describes what to do, others can also do some repair methods.
The `fix` setting contains data for the fix itself. A test may put some of its own data or results also further to the fix.
To allow for repair the case has to set `autofix` to true. Also the `autofix` setting on the current suite run has to be set.

## Domain Names

Sometimes it will be a good idea to use Domain Names to specify a large group of tests. Maybe you name the group after the domain (network/cluster/host) and have services which also have external or internal names. Sometimes for convenience you may also use domain names in the same schema to identify processes, databases or data objects...

If you work in this way you have this one (backward) hierarchical name which is used to identify, access the element, access the documentation and run checks for it. To make checkup work best with it use the backword name as group:

```yaml
de: # group
  title: de
de.alinex: # group
  title: alinex.de
de.alinex.portal: # case
  title: portal.alinex.de
  # further configuration
```

If you structure it in this way the groups will work because the cases start with the same name. To make it easy for you the path may be given in the normal format and will be reversed automatically if nothing is found.

## Parent

A parent object needs a `title` and may also contain a `description`. The `tags`, `data` and `fix` elements are used for all the elements below (with the key extending this) as base. The objects extend or override each other top to bottom.

## Group

With a `group` entry this case will be a dynamic group which will result into a parent with multiple test cases below. Each group needs a `class` setting which will reference the group resolution. Use a file starting with `./xxx` to use the group from within the checkup core group folder or any JavaScript package path. All further fields are specific to the type of group (see the description of that group).

!!! example

    ```yml
    multi.http:
      title: HTTP
      group:
        class: './list'
        list:
          - data.url: http://www.brainjar.com/java/host/test.html
          - data.url: http://alinex.html
      test: web/http
    ```

Read more about [dynamic groups](../group.md) in it's own chapter.

## Cron settings

This is a quick reference to cron syntax like supported here:

Allowed fields:

    ┌──────────── minute 0-59
    │ ┌────────── hour 0-23
    │ │ ┌──────── day of month 1-31
    │ │ │ ┌────── month 1-12
    │ │ │ │ ┌──── day of week 0-7 (0 or 7 is sunday)
    │ │ │ │ │
    │ │ │ │ │
    * * * * *

Possibilities:

- `*` as any
- `a,b,c` to define multiple values
- `x-y` to define ranges
- `/n` only each n-th value

For month and week day you also may use names or short names. e.g:

-  January,February,March,April,June,July,August,September,October,November,December
-  Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec
-  Monday,Tuesday,Wednesday,Thursday,Friday,Saturday,Sunday
-  Mon,Tue,Web,Thu,Fri,Sat,Sun

| Expression          | Meaning                                                                                                                        |
| ------------------- | ------------------------------------------------------------------------------------------------------------------------------ |
| `0 12 * * *`        | 12 PM (noon) every day.                                                                                                        |
| `15 10 * * *`       | 10:15 AM every day.                                                                                                            |
| `* 14 * * *`        | Every minute starting at 2 PM and ending at 2:59 PM, every day.                                                                |
| `*/5 14 * * *`      | Every 5 minutes starting at 2 PM and ending at 2:55 PM, every day.                                                             |
| `*/5 14,18 * * *`   | Every 5 minutes starting at 2 PM and ending at 2:55 PM, and every 5 minutes starting at 6 PM and ending at 6:55 PM, every day. |
| `0-5 14 * * *`      | Every minute starting at 2 PM and ending at 2:05 PM, every day.                                                                |
| `10,44 14 * 3 WED`  | 2:10 PM and at 2:44 PM every Wednesday in the month of March.                                                                  |
| `15 10 * * MON-FRI` | 10:15 AM every Monday, Tuesday, Wednesday, Thursday and Friday.                                                                |
| `15 10 15 * *`      | 10:15 AM on the 15th day of every month.                                                                                       |
| `0 12 */5 * *`      | 12 PM (noon) every 5 days every month, starting on the first day of the month.                                                 |
| `11 11 11 11 *`     | Every November 11th at 11:11 AM.                                                                                               |

{!docs/assets/abbreviations.txt!}
