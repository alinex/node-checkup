title: CLI Usage

# Command Line Usage

The CLI allows to make a single test run and also can generate a bash completion script:

    checkup [options]       # run a specific set of tests
    checkup bashrc-script   # generate completion script

The general options are:

    --version       Show version number                                 [boolean]
    --quiet, -q     only output result                                  [boolean]
    --help          Show help                                           [boolean]

## Suite/Case Definition

To run a test you have to select the concrete suite to run:

    --case, -c     file or URI of case configuration to load             [string]

This makes it possible to use different case sets to load. See the [case definition](case.md) for the contents of this file.

### Run selection

Optionally some path definitions can be used to make a sub selection of the defined cases. Or it can be filtered by given a tags which have to be set:

    --path, -p      select tests from the given subpaths                  [array]
    --tag, -t       select tests which match all the given                [array]

Both attributes can be used multiple times. Multiple paths will select any case which is in one of them or the sub paths (they can also be specified partly ending at any character). But multiple tags are resolved as AND combination, so only cases are included, which include all defined tags. The group and name of the test case will automatically be added to tags.

### Output

Called using CLI a easy readable human information will be presented. Using the `verbose` flag the output detail level can be defined.

    --verbose, -v   extend verbosity                                      [count]
    --extend, -e    automatically extend verbosity on error             [boolean]
    --progress      show progress with final report                     [boolean]

The different [verbosity levels](api.md#verbosity) will look like:

!!! Example

    === "Level 0: no verbosity"

            Local machine
              ✔  Uptime > 5 Minutes

            1/1 test cases succeeding

    === "Level 1: -v"

            Local machine
              ✔  Uptime > 5 Minutes
                 REQUEST  uptime -s
                 RESPONSE exit code: 0

            1/1 test cases succeeding

    === "Level 2: -vv"

            Local machine
              ✔  Uptime > 5 Minutes
                 REQUEST  uptime -s
                 DATA     ????
                 RESPONSE exit code: 0

            1/1 test cases succeeding

    === "Level 3: -vvv"

            Local machine
              ✔  Uptime > 5 Minutes
                 REQUEST  uptime -s
                 DATA     ????
                 RESPONSE exit code: 0
                 RESULT   2020-08-24 15:58:29

            1/1 test cases succeeding

    === "Level 4: -vvvv"

            Local machine
              ✔ Uptime > 5 Minutes
                 REQUEST  uptime -s
                 DATA     ????
                 RESPONSE exit code: 0
                 RESULT   2020-08-24 15:58:29
                 VALIDATE Boot Time
                              A date nor time range is needed. The text will be converted into date.
                 VALIDATE The host should be running over 300 minutes.

            1/1 test cases succeeding

If a problem or error occur the detailed log will always be displayed:

!!! Example "Example with Warning"

    ```text
    Local machine
      ⚡ Uptime > 5 Minutes
         Error: The host was just started 271 minutes ago
         REQUEST  uptime -s
         RESPONSE exit code: 0
         RESULT   2020-08-24 15:58:29
         VALIDATE Boot Time
                      A date nor time range is needed. The text will be converted into date.
         VALIDATE The host should be running over 300 minutes.

    0/1 test cases succeeding
    1 test cases with warnings
    ```

More verbose settings are used to define the analyses output. See [Verbosity Levels](./api.md#verbosity) for a definition at what is displayed.

### Fixes

By default the system will do an analyzation run which can give you some possible fixes. Sometimes it is also possible to automatically repair this:

    -f, --autofix   automatically fix/repair if possible                [boolean]
    -F, --no-fix    switch off fixes (also the analyzation part)        [boolean]

The second switch may be needed for monitoring systems, which only need the status, not the solution.

## More possibilities

All the additional tools like data stores, scheduler and more are defined with an extended setup file instead of parameters. They are building the base setting and also the above settings may be given with such a configuration (if both the argument will win). See the [CLI Setup documentation](setup.md) for more details.

    --setup, -s     configuration of additional tools                    [string]

The following values may be given directly as parameter, too:

-   `case` - file or URI with configuration of cases
-   `verbose` - verbosity level as number 0..9
-   `extend` - flag if extended verbosity level is used in case of an error
-   `analyze` - run additional analyzation where possible
-   `autofix` - run repair methods if possible

Additionally you may specify:

-   `concurrency` - how many processes may run in parallel in each state (default 5)
-   `cron` - default setting for cron for all undefined tests

## Special Modes

### Scheduler

The scheduler will run jobs in a specific interval, defined by the `cron` setting. This can be done in an continuously running process in CLI, but it is also possible to run it in the server.

    --scheduler     flag to only run the scheduler                      [boolean]

The console output with a cron of `*/2 * * * *` meaning every two minutes for the load check will work like:

```text
$ bin/checkup --scheduler -q
2021-01-18T17:56:04.186+01:00 ✔   OK System Load (local.load)
2021-01-18T17:58:03.384+01:00 ✔   OK System Load (local.load)
2021-01-18T18:00:03.397+01:00 ✔   OK System Load (local.load)
2021-01-18T18:02:03.410+01:00 ✔   OK System Load (local.load)
2021-01-18T18:04:03.397+01:00 ✔   OK System Load (local.load)
2021-01-18T18:06:03.405+01:00 ✔   OK System Load (local.load)
2021-01-18T18:08:03.394+01:00 ✔   OK System Load (local.load)
```

!!! Note

    This cannot be combined with the `progress` setting, but all others are possible.

### Interactive Mode

The interactive mode let you define each test within the interactive CLI and run them.

    --interactive, -i   run in interactive mode                         [boolean]

All other options above can be used to create the initial setup, too.

The interactive mode is an endless loop in which the user select a command to execute. Some commands may ask more questions before running. The following key bindings exist:

- ++ctrl+c++ Cancel the prompt and exit the interactive mode.
- ++ctrl+g++ Reset the prompt to its initial state.
- ++space++ Select or deselct current entry.
- ++enter++ Commit settings and go on.
- ++up++ or ++down++ to navigate

The following commands are usable:

| Command | Description                                                                                                 |
| ------- | ----------------------------------------------------------------------------------------------------------- |
| help    | Show a short command overview.                                                                              |
| exit    | Close the command.                                                                                          |
| list    | List all loaded cases containing tests and groups.                                                          |
| run     | Run some or all of the test cases. This lets you change the selection based on paths and tags (from init or | last run). |
| add     | Add a new case to the list using a dialog with multiple questions.                                          |
| edit    | Edit an existing case. This is the same as the `add` dialog but with the current value as preset.           |
| remove  | Select one or multiple cases to delete.                                                                     |
| clear   | Clear all testcases.                                                                                        |
| config  | Update configuration settings in a dialog.                                                                  |

## Environment settings

The output will be colored if possible but

    FORCE_COLOR=0   will disable color output also if possible in terminal
    DEBUG=checkup   enable debug output (see below)
    DEBUG_DEPTH=10  enhance depth of object display in debugging

### Debugging

To see some more information you may enable the debug logging using the environment variable `DEBBUG=checkup*`. The following finer debug elements are alternatively usable:

-   `checkup*` - writing all from this module
-   `checkup:runner` - what is currently started/finished
-   `checkup:group*` - resolve of dynamic groups
-   `checkup:test*` - imediate output of what is tested
-   `checkup:fix*` - imediate output of what is repaired
-   `checkup:store*` - store containers for results
-   `datastore*` - DataStore is often used to retrieve data
-   `validator*` - Validator is often used for checking the retrieved values
-   `knex*` - RDBMS interface showing SQL with binding data

Also using `DEBUG_DEPTH=<number>` the depth of object output can be justified.

## Examples

Some example calls are:

!!! example "Run all tests"

    ```bash
    checkup
    ```

    ![default run](run.gif)

!!! example "Extend verbose mode on error"

    ```bash
    checkup -e
    ```

    ![default run](run-extend.gif)

!!! example "Use progress mode with complete report last"

    ```bash
    checkup --progress
    ```

    ![default run](run-process.gif)

{!docs/assets/abbreviations.txt!}
