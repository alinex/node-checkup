# Installation

Mostly this Module will be used within the [Alinex Server](https://alinex.gitlab.io/node-server), if this is the case follow the instructions there. 


## NodeJS Installation

To get the server up and running you have to install it on a linux system.

First if you don't have NodeJS v10 or newer running install it:

=== "Arch Linux"

    ```bash
    sudo pacman -S nodejs npm
    sudo pacman -S python-pygments # used in the helper to colorize cli output
    ```

=== "Debian"

    ```bash
    curl -sL https://deb.nodesource.com/setup_15.x | sudo -E bash -
    sudo apt-get install -y nodejs
    sudo apt-get install -y python-pygments # used in the helper to colorize cli output
    ```

## Package Install

Then you can simply install the NPM package:

```bash
npm install -g @alinex/checkup
```

Alternatively you may also download and install the code from gitlab:

```bash
clone https://gitlab.com/alinex/node-checkup
cd node-checkup
npm install
```

### Use native Postgres driver

This is not necessary, but can bring you a better performance for a production system.

You need PostgreSQL client libraries & tools installed. An easy way to check is to type `pg_config`. If `pg_config` is in your path, you should be good to go. If it's not in your path you'll need to install:

=== "Arch Linux"

    ```bash
    sudo pacman -S postgresql-libs
    ```

=== "Debian"

    ```bash
    sudo apt-get install libpq-dev g++ make
    ```

=== "RHEL/CentOS"

    ```bash
    sudo yum install postgresql-devel
    ```

=== "OS X"

    ```bash
    sudo brew install postgres
    ```

=== "Windows"

    - Install Visual Studio C++ (successfully built with Express 2010). Express is free.
    - Install PostgreSQL (http://www.postgresql.org/download/windows/)
    - Add your Postgre Installation's bin folder to the system path (i.e. C:\Program Files\PostgreSQL\9.3\bin).
    - Make sure that both libpq.dll and pg_config.exe are in that folder.

Now `pg_config` should be in your path and you can install the NodeJS module:

```bash
cd node-checkup
npm install pg-native
```

### Update Package

If it was installed through NPM you only have to call the update command:

```bash
npm update -g @alinex/server
```

If installed from git, do the following from within the application directory:

```bash
# remove local changes
$ git stash save --keep-index
$ git stash drop
# get new code and modules
$ git pull
$ npm install
```

{!docs/assets/abbreviations.txt!}
