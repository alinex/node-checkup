title: Server

# Alinex Server Application

To make checkup a central component it's best to use it as a server. The [Alinex Server](https://alinex.gitlab.io/node-server/) already includes the checkup module. But this is only a basic server, which you have to customize.

This makes the checkup available from everywhere using an easy Web API, but also allows to cache values store scheduled and other calls together and be integrated into other monitoring settings.

## Setup

To configure the server you have to:

-   setup `config/checkup/default.yml` with the test cases
-   add more suites with alternative names in the same folder
-   maybe configure `autologin` in user to be used for processes on specific servers to access in an easy way

The Scheduler is a cron like system which is mostly used within the [Server](server.md) and can be [configured](https://alinex.gitlab.io/node-server/usage/running/#checkup) there.

## REST Calls

A simple `GET {{SERVER}}/checkup` will run all tests, but you can specify the cases to use and the parameters to be used.

See the full [REST API](https://alinex.gitlab.io/node-server/service/checkup) for more information.

{!docs/assets/abbreviations.txt!}
