# Roadmap

**What's coming next?**

That's a big question on a freelancer project. The roadmap may change any time or progress may be stuck. But hopefully I will come further on in any way.

## Bugs

## Version 1.4 - June 2021

## Version 1.5 - July 2021

-   server
    - check complete cases config before start, with possible warning
    - auto reload config
      - check again after reload

## Features

-   graphite logger
-   email for problems (store)
    -   while not ok
    -   once per day on server/scheduler
    -   resolve
    -   escalate
    -   maintenance
-   load directory with config files (concat them)
-   cli run dynamic check --json '{"test": "xxx", "data": "xxx"}'
-   depend: if dependent test fails this can be set to same error
-   require: case (paths on accessing a website)
    -   Example: login form (key) -> login
    -   include
    -   run self after this
    -   use depend result as data
-   cli interactive
    -   show
    -   load cases
    -   save cases
-   doc
    -   usage/examples
-  server
    - extend with own packages (test/fix/group)

## Tests

-   linux
    -   ping+tcp+udp check connection from server to destination
-   database
    -   mysql (via knex)
    -   redis
    -   mongo/record
    -   mongo/list
    -   sql/list 
    -   postgres
        -   add tcp analyzis
        -   add connection analysis
        -   database/schema/space on warn size list unused indexes
- test/log
  - progress
    - check time
    - calculate diff
    - warn on min difference
  - data (extract value)
    - warn on different rules
  - ideas
    - transfer as less as needed, support rotation
      - find first date after <start-date> with pattern
        - grep zgrep '<start-date>' <path>/<log>*
        - if not found (exit code) loop day till today
        - end if no day found
      - data read
        - read by date                 ls -ltr <path>/<log>*
        - also compressed              | zcat -f
        - from found start date        | sed -n '/<start-day>/,$p'
  - examples
    - alinex server
      - error codes
      - response time
      - path
      - service
      - ip
    - apache combined
      - error codes
      - response time
      - path
      - ip
-   net
    -   SNMP
        -   host: ip, uptime, cpu, userd...
        -   printer
-   system
    -   requests
-   web:
    -   nginx stats
    -   tomcat stats
        - tomcat log java.lang.OutOfMemoryError: Java heap space
          -> restart tomcat 
    -   REST service checks
-   application
    - mail server 
      - connection
        - connect smtp
        - connect imap
        - connect pop3
      - login
        - https://wiki.ubuntuusers.de/Mailserver_testen/
        - https://nodemailer.com/smtp/testing/
      - mail
        - send mail to self
        - check mail
      - blacklist
    -   ftp
    -   activemq
    -   rabbitmq
    -   elasticsearch
-   monitor
    -   mocha
    -   JUnit
    -   TestCafe
    -   selenium
-   code
    -   javascript security https://retirejs.github.io/retire.js/
    -   nodejs/python/ruby security https://owasp.org/www-project-dependency-check/
    -   java... security https://opensource.com/article/20/8/static-code-security-analysis https://github.com/wireghoul/graudit
    -   java class metrics https://www.spinellis.gr/sw/ckjm/

## Fix

-   net/tls
    -   detect os/software
    -   disable old protocols in config and restart

## Groups

-   linux
    - discovery services
        - tls for all configured ssl domains in apache/nginx
        - multiple tomcats
-   sql (knex)
-   subdomains using search engines

{!docs/assets/abbreviations.txt!}
