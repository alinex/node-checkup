title: Overview

# Alinex Checkup

![checkup icon](https://assets.gitlab-static.net/uploads/-/system/project/avatar/12586261/checkup-icon.png){: .right .icon}

Framework running powerful in-deep tests for standalone use or to enhance monitoring.

Features:

-   easy **configuration** with dynamic groups
-   big range of possible **tests**, extensible
-   detailed, multilingual **reporter** (console, object, html)
-   extended **analyzation** with suggested fixes
-   automatic **repair** on suggested fixes
-   different result **storages** (file, database)
-   **scheduled** runner

Integrated into [Alinex Server](https://alinex.gitlab.io/node-server):

-   callable per **REST** call
-   easy **monitoring integration**
-   flexibel test **caching** to keep load low

But the real power come from all the different tests which are predefined and can be used with some easy configuration of cases (at the moment 19 tests are ready to be used):

-   [Network checks](test/net.md)
-   [Console/SSH linux checks](test/linux.md)
-   [Web checks with HTTP/S + REST](test/web.md)
-   Application checks - planned
-   [Logfile checks](test/log.md)
-   [Database checks](test/database.md) with database content
-   Code Analysis - planned
-   Monitoring - planned
-   [System self checks](test/system.md)

This should not replace the monitoring system but assist and extend your monitoring setup by using these tests within it. Another use case is to use it for manual check and repair.

## Usage

It can be used from [command line](usage/cli.md) but also as [module](usage/api.md) in other NodeJS scripts.

![default run](usage/run-process.gif)

The main use cases are:

-   **Testing**: Call it from CLI, or as REST service to check if your system is working correctly.
    -   This can assist you in case you think something is not working.
    -   Within an automated deployment to check that deploy worked.
    -   In the daemon start script it can be used to really tell then the node is really working to be enabled in load balancer.
-   **Repair Mode**: Let it repair your system, if possible using CLI. Mostly you will call the fix in a second call after you got an error with the analyzation report.
-   **Monitoring Probe**: As sensor for any other monitoring system.
    -   This helps to easily add complex checks which your monitoring system is not capable for.
    -   Limit the workload on your system if multiple monitoring systems will call the same test.
-   **Forecast**: If you call the tests regularly you can enable the database store to collect all the results and analyze them.
    -   Finding the cause for some problems by detecting the time, then the performance changed.
    -   Determine till then system has to be enlarged to compensate the rising usage.

!!! info

    All is possible, but for using it as monitoring probe you should better use it as server within [Alinex Server](https://alinex.gitlab.io/node-server) to leverage resource usage.

## Architecture

The following parts build the base:

- **Setup** - Definition of a specific checkup (in config file)
- **Case** - Each element within the config is a case which can be a test or group case
- **Suite** - Selection of cases for one test run (by path and tags)
- **Group** - Class for dynamic group resolution (references from case)
- **Test** - Class doing the checkup (script referenced from case)
- **Fix** - Class containing analysis and possible repair method (script referenced from test)
- **Analyze** - Part of a fix which will further analyze a problem and only report it
- **Repair** - Automatic repair method
- **Store** - The data stores can be enabled to collect information over time

It is called using API returning JSON data structures or using CLI with terminal output. If used in a server setup an integrated cache will help reducing the load to the system under test.

## Documentation

Read all about the [Checkup](https://alinex.gitlab.io/node-checkup) in the chapters:

-   [Usage](usage/structure.md) - how to use and work with this module
-   [Test](test) - the possible test methods with fixes
-   [Development](development) - some information about the internal structure and how to extend it

If you want to have an offline access to the documentation, feel free to download: [alinex-checkup.pdf](alinex-checkup.pdf).

## Support

I don't give any paid support but you may create [GitLab Issues](https://gitlab.com/alinex/node-checkup/-/issues):

-   Bug Reports or Feature: Please make sure to give as much information as possible. And explain how the system should behave in your opinion.
-   Code Fixing or Extending: If you help to develop this package I am thankful. Develop in a fork and make a merge request after done. I will have a look at it.
-   Should anybody be willing to join the core team feel free to ask, too.
-   Translations: If you are willing to translate into other languages we will find the easiest way for you to do the translation and include it. Also see [i18n documentation](development/i18n.md).
-   Any other comment or discussion as far as it is on this package is also kindly accepted.

Please use [GitLab Issues](https://gitlab.com/alinex/node-checkup/-/issues) for all of them, it only requires you to register with a free account.

{!docs/assets/stats-pdf-license.txt!}

{!docs/assets/abbreviations.txt!}
