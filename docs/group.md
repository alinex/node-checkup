# Dynamic Groups

With a `group` entry a case will be a dynamic group which will be resolved into a parent with multiple test cases below. Each group needs a `class` setting which will reference the group resolution. All further fields are specific to the type of group (see the description of that group).

The resolve process will take place within each runner start, but it will be cached for 15 minutes (configurable). So if the data below is changed either restart the checkup, clear the group cache or wait.

## List

The list is a universal shortcut to write tests which are mostly identical. The source is based on the [DataStore](https://alinex.gitlab.io/node-datastore/api/loader/) which can be a direct data set or a local or remote resource.

The list contains objects, which will be merged with the setting in the group and added below it.

!!! example

    Use preset inline data:

    ```yml
    multi.http:
      title: HTTP
      group:
        class: ./list
        store: # DataStore source to be used
          data:
            - data.url: http://www.brainjar.com/java/host/test.html
            - data.url: http://alinex.html
        cache: 900 # how long to wait till the resolve will take place again
      test: web/http
    ```

    This will add the following two subentries while resolving:

    ```js
    multi.http.rqf3sv5QAijx {
      title: 'http://www.brainjar.com/java/host/test.html',
      test: './web/http',
      parent: [ '', 'multi', 'multi.http', 'multi.http.rqf3sv5QAijx' ],
      tags: [ 'multi', 'web', 'http' ],
      data: { url: 'http://www.brainjar.com/java/host/test.html' },
      cache: 50,
      fix: {},
      autofix: false
    }
    { 'data.url': 'http://alinex.de' }
    multi.http.YmSzMFnJpBxR {
      title: 'http://alinex.de',
      test: './web/http',
      parent: [ '', 'multi', 'multi.http', 'multi.http.YmSzMFnJpBxR' ],
      tags: [ 'multi', 'web', 'http' ],
      data: { url: 'http://alinex.de' },
      cache: 50,
      fix: {},
      autofix: false
    }
    ```

The store will contain objects containing the path and value to be merged into the group to make the element.

Possible settings are:

-   `id` - name of the sub element (default a generated hash)
-   `title` - will be used as title, if not given the first data value will be used
-   `tags` - will be added to the list
-   `data.*` - will be added to the data object

A generated hash is used as id to make it unique and also get the same id with the same setting every time.

## Linux

Add all or multiple [linux tests](test/linux.md) and for a given list of hosts.

This can add tests for:

- `./linux/uptime`
- `./linux/load `
- `./linux/memory `
- `./linux/os`
- `./linux/disk` for each mounted file system
- `./linux/service` for known services like `apache2`, `nginx`, `postgresql`, `mongodb`, `mysql`

!!! example

    ```yaml
    multi.linux:
      title: Linux Tests
      group:
        class: './linux'
    #   default:
    #     username: admin
    #   server:
    #     - host: operations.host.test.dvb
    #       username: admin
    #       id: name for path
    #   host: /etc/hosts
    #   filter: RegExp

    #   include: # folder or test, only detect the listed ones
    #     - ./linux # group
    #     - ./linux/uptime # test
    #     - ./linux/service#mongodb # instance
    #   exclude: # folder or test, exclude also if found
    
    # data: # give default settings per test or instance
    #   ./linux/uptime:
    #     warn:
    #       min: 5
    # cache: 100

    # fix: # give default settings for fixes per test or instance
    # autofix: true

    # cron: # give cron time per test or instance
    #   ./database/postgres/cluster: '*/5 * * * **
    ```

    This will add the following subentries while resolving:

    ```js
    {
      'multi.linux.localhost.uptime': {
        title: 'Uptime at localhost',
        parent: [ '', 'multi', 'multi.linux', 'multi.linux.localhost.uptime' ],
        tags: [ 'multi', 'localhost' ],
        data: {},
        cache: 50,
        fix: {},
        autofix: false,
        test: './linux/uptime'
      },
      'multi.linux.localhost.load': {
        title: 'Load at localhost',
        parent: [ '', 'multi', 'multi.linux', 'multi.linux.localhost.load' ],
        tags: [ 'multi', 'localhost' ],
        data: {},
        cache: 50,
        fix: {},
        autofix: false,
        test: './linux/load'
      },
      'multi.linux.localhost.memory': {
        title: 'Memory at localhost',
        parent: [ '', 'multi', 'multi.linux', 'multi.linux.localhost.memory' ],
        tags: [ 'multi', 'localhost' ],
        data: {},
        cache: 50,
        fix: {},
        autofix: false,
        test: './linux/memory'
      },
      'multi.linux.localhost.os': {
        title: 'Os at localhost',
        parent: [ '', 'multi', 'multi.linux', 'multi.linux.localhost.os' ],
        tags: [ 'multi', 'localhost' ],
        data: {},
        cache: 50,
        fix: {},
        autofix: false,
        test: './linux/os'
      },
      'multi.linux.localhost.disk./': {
        title: 'Disk at localhost',
        parent: [ '', 'multi', 'multi.linux', 'multi.linux.localhost.disk./' ],
        tags: [ 'multi', 'localhost' ],
        data: {},
        cache: 50,
        fix: {},
        autofix: false,
        test: './linux/disk'
      }
    }
    ```

**Server/Hosts**

Within the group the list of hosts has to be defined in one of three methods:

1. If nothing given `localhost` is used.
2. Within the `server` list you give the server settings like described in the [linux tests](test/linux.md).
3. Use `host` to define a [DataStore](https://alinex.gitlab.io/node-datastore) using file or any other protocol to load a list of hosts (first value from each list entry is used as host). Here you may also set a RegExp `filter` to select which entries to use. This can easily be used with `/etc/hosts`.

If a list of hosts is defined, the settings below `default` are used to set server settings like username and authentication which are common to all hosts.

**Include/Exclude**

The `ìnclude` and `exclude` settings will be used to select what will be automatically detected:

- nothing means to detect all that is possible
- if `include` is defined only the ones listed are detected
- if `exclude` is defined the ones listed here are excluded, also if within `include`

Both `include` and `exclude` contains arrays of:

- group
- group/test
- group/test#instance

**Defaults**

The last section defines the default `data` and `fix` settings. They are within the name of

- group/test
- group/test#instance

And they will overwrite each other in the above order if one matches multiple entries.

## Postgres

Add all or multiple [postgres tests](test/database.md) for a given database cluster.

This can add tests for:

- `./database/postgres/cluster`
- `./database/postgres/database`
- `./database/postgres/schema`
- `./database/postgres/table`
- `./database/postgres/space`
- `./database/postgres/user`

!!! example

    ```yaml
    multi.postgres:
      title: Postgres Tests
      group:
        class: './postgres'
        connection:
          - # host: localhost         # host of database
            # port: 5432              # port to connect to
            # user: alex              # if needed
            # password: IdontKnow     # if needed
              database: my_db         # database name

    #   include: # folder or test, only detect the listed ones
    #     - ./database/posgres # group
    #     - ./database/posgres/dataabase # test
    #     - ./database/posgres/database#jobs # instance
    #   exclude: # folder or test, exclude also if found
    
    # data: # give default settings per test or instance
    #   ./database/postgres/cluster:
    #     warn:
    #       uptime: 300
    # cache: 100
    
    # fix: # give default settings for fixes per test or instance
    # autofix: true

    # cron: # give cron time per test or instance
    #   ./database/postgres/cluster: '*/5 * * * **
    ```

    This will add the following subentries while resolving:

    ```js
    {
      title: 'Cluster at localhost',
      parent: [ '', 'multi', 'multi.postgres', 'multi.postgres.localhost.cluster' ],
      tags: [ 'multi', 'localhost' ],
      data: { connection: { database: 'alex' } },
      cache: 50,
      fix: {},
      autofix: false,
      cron: 'none',
      test: './database/postgres/cluster'
    }
    ```

**Database Connection**

Within the group the list of connections has to be defined with each element like in the tests.

The group will create cases for each given connection.

**Include/Exclude**

The `ìnclude` and `exclude` settings will be used to select what will be automatically detected:

-   nothing means to detect all that is possible
-   if `include` is defined only the ones listed are detected
-   if `exclude` is defined the ones listed here are excluded, also if within `include`

Both `include` and `exclude` contains arrays of:

-   group
-   group/test
-   group/test#instance

**Defaults**

The last section defines the default `data` and `fix` settings. They are within the name of

-   group/test
-   group/test#instance

And they will overwrite each other in the above order if one matches multiple entries.

{!docs/assets/abbreviations.txt!}
