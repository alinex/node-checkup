title: Store

# Storage Engines

Each store can use the following hook functions to initialize:

Initialize Phase:

-   `init()` - initialize store while new suite is being created
-   `setUp()` - prepare storage streams, called before running tests

Data Events:

-   `status(path: string, type: string, start: Date, status: string, error?: Error)`
-   `data(path: string, type: string, start: Date, status: string, result: any)`
-   `action(path: string, type: string, status: string, error: Error)`

Shutdown Phase:

-   `cleanup()` - do maintenance stuff like removing of old entries, called after run
-   `tearDown()` - close storage streams, called after running tests

## Environment

The following elements are accessable:

-   `setup` - configuration of store
-   `debug` - logger for debug messages

{!docs/assets/abbreviations.txt!}
