title: Test

# Additional Test

Additionally to the tests which are already there, some user specific tests can be used. To make them add them with your own package, which you install into the checkup system. Use it by giving it's include path starting with the package name in the case configuration.

## Environment

To do all this the `Test` class contains:

-   `this.path` - The path from the case like 'local.cpu'.
-   `this.case` - The complete case definition from configuration (but resolved).
-   `this.data` - Data settings from configuration after sanitize.
-   `this.fixdata` - Fix data settings from configuration after sanitize.
-   `this.lang` - Language to be used (if multilingual support).
-   `this.cache` - Cache to be used if needed.
-   `this.debug` - `Debug` instance to log.

## Configuration

The test may contain some additional configuration for the storages. At the moment only the `exclude` setting is supported for the [database store](../store.md#database-store). 

```ts
// general excludes for store
public store = { exclude: ['pos'] }
```

This defines the default setting and will exclude `result.pos` value to be stored. The default can be overridden by giving a custom setting like `./log/event:-` which will store everything.

## Methods to Implement

-   `async this.run ()`

## Result Elements

-   `this.description` - A further description of what this test will do.
-   `this.start` - Start date of the test to know the time from then the values are (if given by cache).
-   `this.status` - Status for this test which has to be set.
-   `this.error` - If something is problematic the concrete error message.
-   `this.result` - Resulting data from test, maybe stored for long term analysis.
-   `this.fixes` - List of additional fixes (class instances).
-   `this._log` - Output log list. Each line should start with the verbosity level followed by colon.

## Log Helper

Each of the following log helper will do the same, add the element to the `this._log` element with the correct verbose level:

-   `this.logRequest (msg: string)`
-   `this.logData (msg: any)`
-   `this.logStatus (msg: string)`
-   `this.logResponse (msg: string)`
-   `this.logReturn (msg: any)`
-   `this.logProcess (msg: string)`
-   `this.logExitcode (msg: number)`
-   `this.logStderr (msg: string)`
-   `this.logStdout (msg: string)`
-   `this.logValidation (msg: string)`
-   `this.logResult (msg: any)`

## Basic Structure

```ts
import { Test } from '@@alinex/checkup/lib/test'

import { T } from '../i18n';

class TestXxx extends Test {
  public async run () {
    const t = T(this.lang, 'linux')
    // INCLUDE THE CODE
    // USE t TO MAKE TRANSLATIONS
  }
}

export default TestXxx
```

This is a simple setup in which the `run` method will be implemented. This should:

1.  Check the case configuration for this test and maybe sanitize the values.
2.  Get the data for the test and transform it into a result object.
3.  Run the error and warn checks like defined by the case.
4.  Maybe add some fixes which help in further analyzation and repair.

## Full Example

A more complete example with the use of @alinex/validator will lookmlike:

```ts
import Validator from '@alinex/validator';
import * as Builder from '@alinex/validator/lib/schema';

import { Test } from '@@alinex/checkup/lib/test'
import { T } from './i18n';

// define data check
const item = {
  time: new Builder.NumberSchema({
    title: 'time to load page',
    unit: { from: 'ms', to: 'ms' },
    greater: 0
  })
}
const schemaBase = new Builder.ObjectSchema({
  item: {
    domain: new Builder.DomainSchema(),
    port: new Builder.PortSchema(),
    warn: new Builder.ObjectSchema({
      title: 'Warn Check',
      item,
      denyUndefined: true,
      default: { time: 5000 }
    }),
    error: new Builder.ObjectSchema({
      title: 'Error Check',
      item,
      denyUndefined: true,
      default: { time: 30000, code: [200] }
    })
  },
  mandatory: ['domain', 'port'],
  denyUndefined: true
})

class TestXxx extends Test {
  public async run () {
    const t = T(this.lang, 'web')

    // check data settings
    await Validator.load(clone(schemaBase), this.lang, { data: this.data, source: `preset:${this.path}.data` })
      .then(val => this.data = val.get())
    this.description = t('http.test.description', this.data)

    // setup validation
    const valWarn = new Validator(new Builder.ObjectSchema({
      title: t('core:test.validateWarn'),
      item: {
        ...(typeof this.data.warn?.code !== 'undefined' && {
          time: new Builder.NumberSchema({
            title: t('http.test.time', { value: this.data.warn.time }),
            max: this.data.warn.time
          })
        }),
        ...(typeof this.data.warn?.code !== 'undefined' && {
          code: new Builder.NumberSchema({
            title: t('http.test.code', { value: this.data.warn.code.join(', ') }),
            allow: this.data.warn.code
          })
        }),
        ...((typeof this.data.warn?.allow !== 'undefined' || typeof this.data.warn?.disallow !== 'undefined') && {
          body: new Builder.StringSchema({
            title: t('http.test.body', { allow: this.data.warn.allow?.join(', ') || '*', disallow: this.data.warn.allow?.join(', ') || '[]' }),
            ...(typeof this.data.warn?.allow !== 'undefined' && { allow: this.data.warn.allow }),
            ...(typeof this.data.warn?.disallow !== 'undefined' && { disallow: this.data.warn.disallow }),
          })
        })
      }
    }))
    const valError = new Validator(new Builder.ObjectSchema({
      title: t('core:test.validateError'),
      item: {
        ...(typeof this.data.error?.code !== 'undefined' && {
          time: new Builder.NumberSchema({
            title: t('http.test.time', { value: this.data.error.time }),
            max: this.data.error.time
          })
        }),
        ...(typeof this.data.error?.code !== 'undefined' && {
          code: new Builder.NumberSchema({
            title: t('http.test.code', { value: this.data.error.code.join(', ') }),
            allow: this.data.error.code
          })
        }),
        ...((typeof this.data.error?.allow !== 'undefined' || typeof this.data.error?.disallow !== 'undefined') && {
          body: new Builder.StringSchema({
            title: t('http.test.body', { allow: this.data.error.allow?.join(', ') || '*', disallow: this.data.error.allow?.join(', ') || '[]' }),
            ...(typeof this.data.error?.allow !== 'undefined' && { allow: this.data.error.allow }),
            ...(typeof this.data.error?.disallow !== 'undefined' && { disallow: this.data.error.disallow }),
          })
        })
      }
    }))

    // get data
    return load(this.data.domain, this.data.port)
      // validate result
      .then(ds => {
        this.result = {
          time: ds.meta.time,
          code: ds.meta.status,
          codeText: ds.meta.statusText,
          length: ds.meta.headers['content-length'],
          body: ds.get()
        }
        this.logResult(this.result)
        this.status = 'OK'
        return ds
      })

      // error check
      .then(() => {
        this.logValidation(valError.describe())
        const ds = { data: this.result, source: 'preset:result' }
        return valError.load(this.lang, ds)
          .then(_val => {
            this.logValidation(valWarn.describe())
            return valWarn.load(this.lang, ds)
              .catch(e => {
                this.status = 'WARN'
                this.error = e
                this.fixes.push(new FixHttp(this))
              })
          })
          .catch(e => {
            this.status = 'ERROR'
            this.error = e
            this.fixes.push(new FixHttp(this))
          })
      })

      // general error
      .catch((e: any) => {
        if (e) {
          this.status = 'NODATA'
          this.error = e
          // maybe add some fixes here
        }
      })
  }
}

export TestXxx
```

## Cache

Sometimes it may be helpful to use the cache, if different tests of the same or similar classes use the same source data. This may reduce the pressure on the system under test. You may store the retrieved raw data for a few seconds. To do so use a key name starting with `!<type>:...` this will not collide with the keys used in the worker itself. Also it may be needed on some tests which have to calculate the difference between multiple calls in scheduler.

As an example the `test/log` class uses it to not call the server multiple times, while `database/postgres/*` classes use it to calculate the change in database between calls.

```ts
const CACHE_TTL = 30

export class TestLog extends Test {
  ...
  protected async load(path: string, cmd: string, options: any = {}): Promise<DataStore> {
    ...
    this.ds = <DataStore>this.cache.get(`!log:${uri}`)
    ....
    this.cache.set(`!log:${uri}`, this.ds!, CACHE_TTL)
  }
}
```

{!docs/assets/abbreviations.txt!}
