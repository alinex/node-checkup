title: Group

# Additional Group

The dynamic group can work in two modes:

1.  adding the same test multiple times
2.  adding different tests for the same element (host)

The group can be dump and simply create new tests out of a list or by actively discover what is needed.

## Environment

-   `this.path` - group path in case definition
-   `this.case` - group definition
-   `this.lang` - language to be used for reporting
-   `this.debug` - debug logger instance

## Methods to Implement

-   `async this.resolve()`

## Result Elements

- `this.children` - collection of generated elements


{!docs/assets/abbreviations.txt!}
