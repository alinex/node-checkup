title: Overview

# Development

This pages help to develop and extend this module further. There are some internal information which helps understanding this package more. But they are not complete!

!!! info

    See also the subpages in the menu.

## Extend

To extend the functionality of checkup you may add new classes through sub packages:

-   [Test](test.md)
-   [Fix](fix.md)
-   [Group](group.md)

To integrate them only install the package and use their names in the configuration for `test` and `group` (fixes are linked from the tests).

## Module Usage

Because of the big list of supported alternatives this module depends on a lot of other modules but only the ones really used are loaded on demand.

Only the following are loaded always:

-   @alinex/validator
-   @alinex/async
-   node-cache
-   i18next
-   momentjs
-   numeral
-   chalk
-   debug

All others are only loaded on demand. But it will install a bigger collection of modules on your system to be prepared if specific tests/fixes are used.

{!docs/assets/abbreviations.txt!}
