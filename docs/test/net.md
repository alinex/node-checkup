title: Network

# Network Checks

This are low level network tests which can be used to check for the network connections to servers.

## ping

This will run an ICMP type 8 echo command multiple times to check the availability of a foreign host. This will not only check the availability but also the roundtrip time for the packages.

!!! warning

    Not every server is able to respond because of firewall settings. The server and network should allow ICMP type 8 iuncomming for the request and icmp type 0 outgoing for the resonse.

!!! abstract "Data"

    ```yaml
    data:
        host: 8.8.8.8                   # IP or domain to check
        num: 5                          # number of requests
        warn:
    #       loss: 0                     # The threshold for loss of packets in percent
            avg: 1000 ms                # Average round trip time in milliseconds
    #       min: 1000 ms                # Minimum round trip time in milliseconds
    #       max: 1000 ms                # Maximum round trip time in milliseconds
        error:
            loss: 0                     # The threshold for loss of packets in percent
    #       avg: 1000 ms                # Average round trip time in milliseconds
    #       min: 1000 ms                # Minimum round trip time in milliseconds
    #       max: 1000 ms                # Maximum round trip time in milliseconds
    ```

The Result data will contain:

-   `total` - number of packages sent
-   `received` - number of packages received
-   `loss` - loss of packages in percent
-   `time` - time needed for all of them in milliseconds
-   `min` - minimum round trip time in milliseconds
-   `max` - maximum round trip time in milliseconds
-   `avg` - average round trip time in milliseconds
-   `mdev` - standard deviation time in milliseconds

**Fixes**

No further analyzation or fixing is possible here.

**Result**

??? example "Console output"

    ```ts
    ✔  OK Ping (net.ping)
        Ping another server using ICMP
        REQUEST  LANG=C ping -c 5 8.8.8.8
        STDOUT   PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
                64 bytes from 8.8.8.8: icmp_seq=1 ttl=116 time=27.0 ms
                64 bytes from 8.8.8.8: icmp_seq=2 ttl=116 time=26.5 ms
                64 bytes from 8.8.8.8: icmp_seq=3 ttl=116 time=22.3 ms
                64 bytes from 8.8.8.8: icmp_seq=4 ttl=116 time=23.1 ms
                64 bytes from 8.8.8.8: icmp_seq=5 ttl=116 time=21.9 ms

                --- 8.8.8.8 ping statistics ---
                5 packets transmitted, 5 received, 0% packet loss, time 4005ms
                rtt min/avg/max/mdev = 21.926/24.201/27.080/2.172 ms
        EXITCODE 0
        VALIDATE Prüfen der abgerufenen Daten
                    Ein Datenobjekt mit Wertepaaren. Die Werte haben das folgende Format:
                    -   min: NumberSchema
                        Ein nummerischer Wert.
                    -   avg: NumberSchema
                        Ein nummerischer Wert.
                    -   max: NumberSchema
                        Ein nummerischer Wert.
                    -   mdev: NumberSchema
                        Ein nummerischer Wert.
                    -   total: NumberSchema
                        Ein nummerischer Wert.
                    -   received: NumberSchema
                        Ein nummerischer Wert.
                    -   loss: NumberSchema
                        Ein nummerischer Wert. Es wird der erste numerische Wert aus dem Text extrahiert und Prozente werden umgerechnet.
                    -   time: NumberSchema
                        Ein nummerischer Wert.
                    Alle definierten Schlüssel sind notwendig.
        RESULT   {
                min: 21.926,
                avg: 24.201,
                max: 27.08,
                mdev: 2.172,
                total: 5,
                received: 5,
                loss: 0,
                time: 4005
                }
        VALIDATE Prüfung der Werte auf Fehler
                    Ein Datenobjekt mit Wertepaaren. Die Werte haben das folgende Format:
                    -   loss: Es dürfen nicht mehr als 0% packete verloren gehen.
                        Ein nummerischer Wert. Der Wert muss kleiner oder gleich 0 sein.
        VALIDATE Prüfung der Werte auf Warnungen
                    Ein Datenobjekt mit Wertepaaren. Die Werte haben das folgende Format:
                    -   avg: Die durchschnittliche Paketlaufzeit darf nicht mehr als 1 s betragen.
                        Ein nummerischer Wert. Der Wert muss kleiner oder gleich 1000 sein.
    ```

## tcp

The TCP connection test can be used to check if a service is available through the network.

!!! abstract "Data"

    ```yaml
    data:
        host: google.com                # IP or domain to check
        port: 80                        # server port
        timeout: 30 s                   # timeout to wait for connection
        warn:
            time: 500 ms                # Maximum time for connection in milliseconds
        error:
            time: 5 s                   # Maximum time for connection in milliseconds
    ```

The Result data will contain:

-   `time` - time to establish connection in ms
-   `ip` - remote IP address
-   `port` - remote port
-   `status` - set to `connected`

**Fixes**

If called with context `host` and `port` it will try to connect this.

No further repair is possible here.

**Result**

??? example "Console output"

    ```ts
    ✔  OK TCP Google HTTP (net.tcp)
        Connect to Port 80 of google search engine
        PROCESS  telnet 172.217.21.206 80 (timeout 10000)
        RESULT   { time: 121, ip: '172.217.21.206', port: 80, status: 'connected' }
        VALIDATE Prüfung der Werte auf Fehler
                    Ein Datenobjekt mit Wertepaaren. Die Werte haben das folgende Format:
                    -   time: tcp.test.time
                        Ein nummerischer Wert. Der Wert muss kleiner oder gleich 1000 sein.
        VALIDATE Prüfung der Werte auf Warnungen
                    Ein Datenobjekt mit Wertepaaren. Die Werte haben das folgende Format:
                    -   time: tcp.test.time
                        Ein nummerischer Wert. Der Wert muss kleiner oder gleich 200 sein.
    ```

## udp

The UDP connection test can be used to check if a service is available through the network.

!!! abstract "Data"

    ```yaml
    data:
        host: google.com                # IP or domain to check
        port: 80                        # server port
        timeout: 30 s                   # timeout to wait for connection
        warn:
            time: 500 ms                # Maximum time for connection in milliseconds
        error:
            time: 5 s                   # Maximum time for connection in milliseconds
    ```

The Result data will contain:

-   `time` - time to establish connection in ms
-   `ip` - remote IP address
-   `status` - set to `connected`

**Fixes**

No further analyzation or fixing is possible here.

**Result**

??? example "Console output"

    ```ts
    ✔  OK UDP Google HTTP (net.tcp)
        Connect to Port 80 of google search engine
        PROCESS  nc -u 172.217.21.206 80
        RESULT   { time: 121, ip: '172.217.21.206', status: 'connected' }
        VALIDATE Prüfung der Werte auf Fehler
                    Ein Datenobjekt mit Wertepaaren. Die Werte haben das folgende Format:
                    -   time: tcp.test.time
                        Ein nummerischer Wert. Der Wert muss kleiner oder gleich 1000 sein.
        VALIDATE Prüfung der Werte auf Warnungen
                    Ein Datenobjekt mit Wertepaaren. Die Werte haben das folgende Format:
                    -   time: tcp.test.time
                        Ein nummerischer Wert. Der Wert muss kleiner oder gleich 200 sein.
    ```

## dns

The DNS check uses two different modes:

1.  Local mode - if no nameserver and no type is defined a lookup for domain names will be made using the same operating system facilities as most other programs. This will use the local configured nameservers and will also respect the `/etc/hosts` settings.
2.  Remote mode - if nameserver or a record type is defined. This will not respect `/etc/hosts` and will directly work through the given or systems nameserver. This also allows to look for specific entries.

!!! abstract "Data"

    ```yaml
    data:
        domain: google.com              # Domain to check
    #   nameserver: 8.8.8.8             # List of nameservers to use
    #   type: MX                        # Type of record to retrieve
        warn:
            time: 500 ms                # Maximum time for connection in milliseconds
    #       min: 1                      # number of entries
    #       max: 2                      # number of entries
        error:
            time: 5 s                   # Maximum time for connection in milliseconds
            min: 1                      # number of entries
    #       max: 2                      # number of entries
    ```

The type can be:

-   `A` - IPv4 addresses<
-   `AAAA` - IPv6 addresses
-   `ANY` - any records
-   `CNAME` - canonical name records
-   `MX` - mail exchange records
-   `NAPTR` - name authority pointer records
-   `NS` - name server records
-   `PTR` - pointer records
-   `SOA` - start of authority records
-   `SRV` - service records
-   `TXT` - text records

The Result data will contain:

-   `time` - time to retrieve addresses
-   `list` - result list (depends on type)

List entries by type:

-   `A` or `AAAA`
    -   `type` - record type
    -   `address` - address
    -   `ttl` - time to live in seconds (validity of result)
-   `MX`
    -   `type` - record type
    -   `exchange` - mailserver domain name
    -   `priority` - priority (higher to be used first)
-   `CNAME` or `ǸS`
    -   `type` - record type
    -   `value` - domain name
-   `TXT`
    -   `type` - record type
    -   `entries` - list of text records
-   `SOA`
    -   `type` - record type
    -   `nsname` - domain name
    -   `hostmaster` - domain name
    -   `serial` - number
    -   `refresh` - time in seconds
    -   `retry` - time in seconds
    -   `expire` - time in seconds
    -   `minttl` - time in seconds

**Fixes**

Some errors will be explained and if the used nameservers are checked, too. There may be a problem with the nameserver or the domain is misspelled or not defined. The used nameservers will be connected on UDP port 53 to check if they are available.

No further fixing is possible here.

**Result**

??? example "Console output"

    ```ts
    ✔  OK DNS lookup (net.dns)
        Google
        Dieser Test prüft über local lookup und fragt ANY Einträge für die Domain google.com ab.
        PROCESS  nslookup google.com
        RETURN   [
                { address: '172.217.23.142', family: 4 },
                { address: '2a00:1450:400a:803::200e', family: 6 }
                ]
        RESULT   {
                time: 75,
                list: [
                    { address: '172.217.23.142', type: 'A' },
                    { address: '2a00:1450:400a:803::200e', type: 'AAAA' }
                ]
                }
        VALIDATE Prüfung der Werte auf Fehler
                    Ein Datenobjekt mit Wertepaaren. Die Werte haben das folgende Format:
                    -   list: Mindestens 1 Domain Eintrag muss vorhanden sein.
                        Eine Liste mit Werten. Mindestend 1 Element wird benötigt.
                    -   time: Die Zeit zum Auflösen des Domainnamen darf nicht mehr als 5 s sein.
                        Ein nummerischer Wert. Der Wert muss kleiner oder gleich 5000 sein.
        VALIDATE Prüfung der Werte auf Warnungen
                    Ein Datenobjekt mit Wertepaaren. Die Werte haben das folgende Format:
                    -   time: Die Zeit zum Auflösen des Domainnamen darf nicht mehr als 500 ms sein.
                        Ein nummerischer Wert. Der Wert muss kleiner oder gleich 500 sein.
    ```

## ssh

A secure shell connection will be established.

!!! abstract "Data"

    ```yaml
    data:
        host: google.com                # IP or domain to check
    #   port: 22                        # port to connect
        warn:
            time: 500 ms                # Maximum time for connection in milliseconds
        error:
            time: 5 s                   # Maximum time for connection in milliseconds
    ```

The Result data will contain:

-   `ident` - version string
-   `connected` - flag if connection was established
-   `time` - time to connect in ms
-   `key_algorithm` - list of server key exchange algrithms
-   `key_formats` - server host key formats
-   `ciphers` - server cipher suites
-   `hmac` - server HMAC algorithms
-   `compression` - server compression algorithms

**Fixes**

Additionally the TCP connection can be checked, too.

No further fixing is possible here.

**Result**

??? example "Console output"

    ```ts
    ✔  OK SSH (net.ssh)
        This test will check the ssh conntection to 127.0.0.1.
        PROCESS  ssh ???@127.0.0.1
        RETURN   Local ident: 'SSH-2.0-ssh2js0.4.10'
                Client: Trying 127.0.0.1 on port 22 ...
                Client: Connected
                Parser: IN_INIT
                Parser: IN_GREETING
                Parser: IN_HEADER
                Remote ident: 'SSH-2.0-OpenSSH_7.6p1 Ubuntu-4ubuntu0.3'
                Outgoing: Writing KEXINIT
                Parser: IN_PACKETBEFORE (expecting 8)
                Parser: IN_PACKET
                Parser: pktLen:1076,padLen:6,remainLen:1072
                Parser: IN_PACKETDATA
                Parser: IN_PACKETDATAAFTER, packet: KEXINIT
                Comparing KEXINITs ...
                (local) KEX algorithms:
                curve25519-sha256@libssh.org,curve25519-sha256,ecdh-sha2-nistp256,ecdh-sha2-nistp384,ecdh-sha2-nistp521,diffie-hellman-group-exchange-sha256,diffie-hellman-group14-sha256,diffie-hellman-group16-sha512,diffie-hellman-group18-sha512,diffie-hellman-group14-sha1
                (remote) KEX algorithms:
                curve25519-sha256,curve25519-sha256@libssh.org,ecdh-sha2-nistp256,ecdh-sha2-nistp384,ecdh-sha2-nistp521,diffie-hellman-group-exchange-sha256,diffie-hellman-group16-sha512,diffie-hellman-group18-sha512,diffie-hellman-group14-sha256,diffie-hellman-group14-sha1
                KEX algorithm: curve25519-sha256@libssh.org
                (local) Host key formats: ssh-ed25519,ecdsa-sha2-nistp256,ecdsa-sha2-nistp384,ecdsa-sha2-nistp521,ssh-rsa
                (remote) Host key formats: ssh-rsa,rsa-sha2-512,rsa-sha2-256,ecdsa-sha2-nistp256,ssh-ed25519
                Host key format: ssh-ed25519
                (local) Client->Server ciphers:
                aes128-ctr,aes192-ctr,aes256-ctr,aes128-gcm,aes128-gcm@openssh.com,aes256-gcm,aes256-gcm@openssh.com
                (remote) Client->Server ciphers:
                chacha20-poly1305@openssh.com,aes128-ctr,aes192-ctr,aes256-ctr,aes128-gcm@openssh.com,aes256-gcm@openssh.com
                Client->Server Cipher: aes128-ctr
                (local) Server->Client ciphers:
                aes128-ctr,aes192-ctr,aes256-ctr,aes128-gcm,aes128-gcm@openssh.com,aes256-gcm,aes256-gcm@openssh.com
                (remote) Server->Client ciphers:
                chacha20-poly1305@openssh.com,aes128-ctr,aes192-ctr,aes256-ctr,aes128-gcm@openssh.com,aes256-gcm@openssh.com
                Server->Client Cipher: aes128-ctr
                (local) Client->Server HMAC algorithms: hmac-sha2-256,hmac-sha2-512,hmac-sha1
                (remote) Client->Server HMAC algorithms:
                umac-64-etm@openssh.com,umac-128-etm@openssh.com,hmac-sha2-256-etm@openssh.com,hmac-sha2-512-etm@openssh.com,hmac-sha1-etm@openssh.com,umac-64@openssh.com,umac-128@openssh.com,hmac-sha2-256,hmac-sha2-512,hmac-sha1
                Client->Server HMAC algorithm: hmac-sha2-256
                (local) Server->Client HMAC algorithms: hmac-sha2-256,hmac-sha2-512,hmac-sha1
                (remote) Server->Client HMAC algorithms:
                umac-64-etm@openssh.com,umac-128-etm@openssh.com,hmac-sha2-256-etm@openssh.com,hmac-sha2-512-etm@openssh.com,hmac-sha1-etm@openssh.com,umac-64@openssh.com,umac-128@openssh.com,hmac-sha2-256,hmac-sha2-512,hmac-sha1
                Server->Client HMAC algorithm: hmac-sha2-256
                (local) Client->Server compression algorithms: none,zlib@openssh.com,zlib
                (remote) Client->Server compression algorithms: none,zlib@openssh.com
                Client->Server compression algorithm: none
                (local) Server->Client compression algorithms: none,zlib@openssh.com,zlib
                (remote) Server->Client compression algorithms: none,zlib@openssh.com
                Server->Client compression algorithm: none
                Outgoing: Writing KEXECDH_INIT
                Parser: IN_PACKETBEFORE (expecting 8)
                Parser: IN_PACKET
                Parser: pktLen:188,padLen:8,remainLen:184
                Parser: IN_PACKETDATA
                Parser: IN_PACKETDATAAFTER, packet: KEXECDH_REPLY
                Checking host key format
                Checking signature format
                Verifying host fingerprint
                Host accepted by default (no verification)
                Verifying signature
                Outgoing: Writing NEWKEYS
                Parser: IN_PACKETBEFORE (expecting 8)
                Parser: IN_PACKET
                Parser: pktLen:12,padLen:10,remainLen:8
                Parser: IN_PACKETDATA
                Parser: IN_PACKETDATAAFTER, packet: NEWKEYS
                Outgoing: Writing SERVICE_REQUEST (ssh-userauth)
                Parser: IN_PACKETBEFORE (expecting 16)
                Parser: IN_PACKET
                Parser: Decrypting
                Parser: pktLen:28,padLen:10,remainLen:16
                Parser: IN_PACKETDATA
                Parser: Decrypting
                Parser: HMAC size:32
                Parser: IN_PACKETDATAVERIFY
                Parser: Verifying MAC
                Parser: IN_PACKETDATAVERIFY (Valid HMAC)
                Parser: IN_PACKETDATAAFTER, packet: SERVICE_ACCEPT
                Outgoing: Writing USERAUTH_REQUEST (none)
                Parser: IN_PACKETBEFORE (expecting 16)
                Parser: IN_PACKET
                Parser: Decrypting
                Parser: pktLen:44,padLen:19,remainLen:32
                Parser: IN_PACKETDATA
                Parser: Decrypting
                Parser: HMAC size:32
                Parser: IN_PACKETDATAVERIFY
                Parser: Verifying MAC
                Parser: IN_PACKETDATAVERIFY (Valid HMAC)
                Parser: IN_PACKETDATAAFTER, packet: USERAUTH_FAILURE
                Client: none auth failed
                Outgoing: Writing USERAUTH_REQUEST (password)
                Parser: IN_PACKETBEFORE (expecting 16)
                Parser: IN_PACKET
                Parser: Decrypting
                Parser: pktLen:44,padLen:19,remainLen:32
                Parser: IN_PACKETDATA
                Parser: Decrypting
                Parser: HMAC size:32
                Parser: IN_PACKETDATAVERIFY
                Parser: Verifying MAC
                Parser: IN_PACKETDATAVERIFY (Valid HMAC)
                Parser: IN_PACKETDATAAFTER, packet: USERAUTH_FAILURE
                Client: password auth failed
                Outgoing: Writing DISCONNECT (BY_APPLICATION)
        RESULT   {
                ident: 'SSH-2.0-OpenSSH_7.6p1 Ubuntu-4ubuntu0.3',
                connected: true,
                time: 21,
                key_algorithm: [
                    'urve25519-sha256',
                    'curve25519-sha256@libssh.org',
                    'ecdh-sha2-nistp256',
                    'ecdh-sha2-nistp384',
                    'ecdh-sha2-nistp521',
                    'diffie-hellman-group-exchange-sha256',
                    'diffie-hellman-group16-sha512',
                    'diffie-hellman-group18-sha512',
                    'diffie-hellman-group14-sha256',
                    'diffie-hellman-group14-sha1'
                ],
                key_formats: [
                    'sh-rsa',
                    'rsa-sha2-512',
                    'rsa-sha2-256',
                    'ecdsa-sha2-nistp256',
                    'ssh-ed25519'
                ],
                ciphers: [
                    'hacha20-poly1305@openssh.com',
                    'aes128-ctr',
                    'aes192-ctr',
                    'aes256-ctr',
                    'aes128-gcm@openssh.com',
                    'aes256-gcm@openssh.com'
                ],
                hmac: [
                    'mac-64-etm@openssh.com',
                    'umac-128-etm@openssh.com',
                    'hmac-sha2-256-etm@openssh.com',
                    'hmac-sha2-512-etm@openssh.com',
                    'hmac-sha1-etm@openssh.com',
                    'umac-64@openssh.com',
                    'umac-128@openssh.com',
                    'hmac-sha2-256',
                    'hmac-sha2-512',
                    'hmac-sha1'
                ],
                compression: [ 'one', 'zlib@openssh.com' ]
                }
        VALIDATE Check values for errors
                    A map like data object. The items have the following format:
                    -   connected: BooleanSchema
                        A boolean value. It is true for [ true ] and false for [ false ]. The value has to be 'true'.
                    -   time: The time to connect should be less than 5 s.
                        A numeric value. The value has to be less or equal 5000.
        VALIDATE Check values for warnings
                    A map like data object. The items have the following format:
                    -   time: The time to connect should be less than 500 ms.
                        A numeric value. The value has to be less or equal 500.
    ```

## tls

An extensive SSL/TLS test much like https://www.ssllabs.com/ssltest/analyze.html with nearly the same scoring.
To run such a test it will take some time (arround 5 minutes). This will do a lot more than [web/tls](web.md#tls):

-   protocols
-   cipher
-   forward secrecy
-   certificates
-   http header
-   vulnerabilities
-   client simulations
-   rating

A good report will be presented in the verbose logging and only the major values are in the result.

!!! abstract "Data"

    ```yaml
    data:
        domain: google.com              # domain to check
    #   port: 443                       # port to connect
        warn:
            score: 80 %                 # Minimum score
        error:
            score: 50 %                 # Minimum score
    ```

The quality is often described with an letter (A-F) which is the same as the score shown here:

-   score >= 80 -> A
-   score >= 65 -> B
-   score >= 50 -> C
-   score >= 35 -> D
-   score >= 20 -> E
-   score < 20 -> F

The Result data will contain:

-   `time` - time till test finished in s
-   `score` - perceptual score (1 is best)
-   `grade` - overall grade (A...F)
-   `problems` - list of problems
-   `SSL2` - flag if this is allowed
-   `SSL3` - flag if this is allowed
-   `TLS1` - flag if this is allowed
-   `TLS1_1` - flag if this is allowed
-   `TLS1_2` - flag if this is allowed
-   `TLS1_3` - flag if this is allowed
-   `valid` - flag, set to `true` if valid
-   `from` - creation date of certificate
-   `to` - expiration date of certificate
-   `for` - list of valid domains
-   `subject` - owner of the certificate
    -   `C` - country
    -   `ST` - state/province
    -   `L` - locality
    -   `O` - organization
    -   `OU` - organizational unit
    -   `CN` - common name
-   `remaining` - remaining number of seconds for certificate

**Fixes**

Additionally the TCP connection can be checked, too.

No further fixing is possible here.

**Result**

??? example "Console output"

    ```ts
    ✔   OK SSL/TSL (net.tls)
        Dieser Test macht eine umfangreiche SSL/TLS Prüfung für alinex.de.
        PROCESS  /home/alex/code/node-checkup/src/test/net/../../../bin/testssl alinex.de:443
        STDOUT    Start 2021-04-01 17:12:44        -->> 185.26.156.226:443 (alinex.de) <<--
                
                rDNS (185.26.156.226):  --
                Service detected:       HTTP
                
                
                Testing protocols via sockets except NPN+ALPN 
                
                SSLv2      not offered (OK)
                SSLv3      not offered (OK)
                TLS 1      not offered
                TLS 1.1    not offered
                TLS 1.2    offered (OK)
                TLS 1.3    offered (OK): final
                NPN/SPDY   not offered
                ALPN/HTTP2 h2, http/1.1 (offered)
                
                Testing cipher categories 
                
                NULL ciphers (no encryption)                      not offered (OK)
                Anonymous NULL Ciphers (no authentication)        not offered (OK)
                Export ciphers (w/o ADH+NULL)                     not offered (OK)
                LOW: 64 Bit + DES, RC[2,4], MD5 (w/o export)      not offered (OK)
                Triple DES Ciphers / IDEA                         not offered
                Obsoleted CBC ciphers (AES, ARIA etc.)            not offered
                Strong encryption (AEAD ciphers) with no FS       not offered
                Forward Secrecy strong encryption (AEAD ciphers)  offered (OK)
                
                
                Testing server's cipher preferences 
                
                Has server cipher order?     no (NOT ok)
                Negotiated protocol          TLSv1.3
                Negotiated cipher            TLS_AES_256_GCM_SHA384, 256 bit ECDH (P-256) (limited sense as client will pick)
                Cipher per protocol
                
                Hexcode  Cipher Suite Name (OpenSSL)       KeyExch.   Encryption  Bits     Cipher Suite Name (IANA/RFC)
                -----------------------------------------------------------------------------------------------------------------------------
                SSLv2
                - 
                SSLv3
                - 
                TLSv1
                - 
                TLSv1.1
                - 
                TLSv1.2 (no server order, thus listed by strength)
                xc030   ECDHE-RSA-AES256-GCM-SHA384       ECDH 256   AESGCM      256      TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384              
                x9f     DHE-RSA-AES256-GCM-SHA384         DH 4096    AESGCM      256      TLS_DHE_RSA_WITH_AES_256_GCM_SHA384                
                xcca8   ECDHE-RSA-CHACHA20-POLY1305       ECDH 256   ChaCha20    256      TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305_SHA256        
                xc02f   ECDHE-RSA-AES128-GCM-SHA256       ECDH 256   AESGCM      128      TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256              
                x9e     DHE-RSA-AES128-GCM-SHA256         DH 4096    AESGCM      128      TLS_DHE_RSA_WITH_AES_128_GCM_SHA256                
                TLSv1.3 (no server order, thus listed by strength)
                Oops: openssl s_client connect problem
                x1302   TLS_AES_256_GCM_SHA384            ECDH 256   AESGCM      256      TLS_AES_256_GCM_SHA384                             
                x1303   TLS_CHACHA20_POLY1305_SHA256      ECDH 256   ChaCha20    256      TLS_CHACHA20_POLY1305_SHA256                       
                x1301   TLS_AES_128_GCM_SHA256            ECDH 256   AESGCM      128      TLS_AES_128_GCM_SHA256                             
                
                
                Testing robust forward secrecy (FS) -- omitting Null Authentication/Encryption, 3DES, RC4 
                
                FS is offered (OK)           TLS_AES_256_GCM_SHA384
                                            TLS_CHACHA20_POLY1305_SHA256
                                            ECDHE-RSA-AES256-GCM-SHA384
                                            DHE-RSA-AES256-GCM-SHA384
                                            ECDHE-RSA-CHACHA20-POLY1305
                                            TLS_AES_128_GCM_SHA256
                                            ECDHE-RSA-AES128-GCM-SHA256
                                            DHE-RSA-AES128-GCM-SHA256 
                Elliptic curves offered:     prime256v1 secp384r1 
                DH group offered:            Unknown DH group (4096 bits)
                
                Testing server defaults (Server Hello) 
                
                TLS extensions (standard)    "renegotiation info/#65281" "server name/#0"
                                            "EC point formats/#11" "status request/#5"
                                            "next protocol/#13172" "supported versions/#43"
                                            "key share/#51" "max fragment length/#1"
                                            "application layer protocol negotiation/#16"
                                            "extended master secret/#23"
                Session Ticket RFC 5077 hint no -- no lifetime advertised
                SSL Session ID support       yes
                Session Resumption           Tickets no, ID: yes
                TLS clock skew               Random values, no fingerprinting possible 
                Signature Algorithm          SHA256 with RSA
                Server key size              RSA 4096 bits (exponent is 65537)
                Server key usage             Digital Signature, Key Encipherment
                Server extended key usage    TLS Web Server Authentication, TLS Web Client Authentication
                Serial / Fingerprints        0336EC05CE9245080CC8938779095DBE076D / SHA1 6EA3569BA45C697DDFE3642A2133BEC39E9C17EE
                                            SHA256 B02CDCAC86FC97E0334D0731A82AAB99D564D193B3CFA7845DC065A1C9A59187
                Common Name (CN)             alinex.de  (CN in response to request w/o SNI: *.uberspace.de )
                subjectAltName (SAN)         alinex.de 
                Trust (hostname)             Ok via SAN and CN (SNI mandatory)
                Chain of trust               Ok   
                EV cert (experimental)       no 
                Certificate Validity (UTC)   85 >= 60 days (2021-03-27 18:50 --> 2021-06-25 19:50)
                ETS/"eTLS", visibility info  not present
                Certificate Revocation List  --
                OCSP URI                     http://r3.o.lencr.org
                OCSP stapling                offered, not revoked
                OCSP must staple extension   --
                DNS CAA RR (experimental)    not offered
                Certificate Transparency     yes (certificate extension)
                Certificates provided        2
                Issuer                       R3 (Let's Encrypt from US)
                Intermediate cert validity   #1: ok > 40 days (2021-09-29 21:21).  R3 <--  DST Root CA X3
                Intermediate Bad OCSP (exp.) Ok
                
                
                Testing HTTP header response @ "/" 
                
                HTTP Status Code             301 Moved Permanently, redirecting to "https://www.alinex.de/"
                HTTP clock skew              0 sec from localtime
                Strict Transport Security    365 days=31536000 s, just this domain
                Public Key Pinning           --
                Server banner                nginx
                Application banner           --
                Cookie(s)                    (none issued at "/") -- maybe better try target URL of 30x
                Security headers             X-Frame-Options SAMEORIGIN
                                            X-XSS-Protection 1; mode=block
                                            X-Content-Type-Options nosniff
                                            Referrer-Policy strict-origin-when-cross-origin
                Reverse Proxy banner         --
                
                
                Testing vulnerabilities 
                
                Heartbleed (CVE-2014-0160)                not vulnerable (OK), no heartbeat extension
                CCS (CVE-2014-0224)                       not vulnerable (OK)
                Ticketbleed (CVE-2016-9244), experiment.  not vulnerable (OK), no session ticket extension
                ROBOT                                     Server does not support any cipher suites that use RSA key transport
                Secure Renegotiation (RFC 5746)           supported (OK)
                Secure Client-Initiated Renegotiation     not vulnerable (OK)
                CRIME, TLS (CVE-2012-4929)                not vulnerable (OK)
                BREACH (CVE-2013-3587)                    no gzip/deflate/compress/br HTTP compression (OK)  - only supplied "/" tested
                POODLE, SSL (CVE-2014-3566)               not vulnerable (OK), no SSLv3 support
                TLS_FALLBACK_SCSV (RFC 7507)              No fallback possible (OK), no protocol below TLS 1.2 offered
                SWEET32 (CVE-2016-2183, CVE-2016-6329)    not vulnerable (OK)
                FREAK (CVE-2015-0204)                     not vulnerable (OK)
                DROWN (CVE-2016-0800, CVE-2016-0703)      not vulnerable on this host and port (OK)
                                                            make sure you don't use this certificate elsewhere with SSLv2 enabled services
                                                            https://censys.io/ipv4?q=B02CDCAC86FC97E0334D0731A82AAB99D564D193B3CFA7845DC065A1C9A59187 could help you to find out
                LOGJAM (CVE-2015-4000), experimental      not vulnerable (OK): no DH EXPORT ciphers, no common prime detected
                BEAST (CVE-2011-3389)                     not vulnerable (OK), no SSL3 or TLS1
                LUCKY13 (CVE-2013-0169), experimental     not vulnerable (OK)
                Winshock (CVE-2014-6321), experimental    not vulnerable (OK)
                RC4 (CVE-2013-2566, CVE-2015-2808)        no RC4 ciphers detected (OK)
                
                
                Running client simulations (HTTP) via sockets 
                
                Android 4.4.2                TLSv1.2 ECDHE-RSA-AES256-GCM-SHA384, 384 bit ECDH (P-384)
                Android 5.0.0                TLSv1.2 ECDHE-RSA-AES128-GCM-SHA256, 384 bit ECDH (P-384)
                Android 6.0                  TLSv1.2 ECDHE-RSA-AES128-GCM-SHA256, 256 bit ECDH (P-256)
                Android 7.0 (native)         TLSv1.2 ECDHE-RSA-AES128-GCM-SHA256, 256 bit ECDH (P-256)
                Android 8.1 (native)         TLSv1.2 ECDHE-RSA-AES128-GCM-SHA256, 256 bit ECDH (P-256)
                Android 9.0 (native)         TLSv1.3 TLS_AES_128_GCM_SHA256, 384 bit ECDH (P-384)
                Android 10.0 (native)        TLSv1.3 TLS_AES_128_GCM_SHA256, 384 bit ECDH (P-384)
                Chrome 74 (Win 10)           TLSv1.3 TLS_AES_128_GCM_SHA256, 384 bit ECDH (P-384)
                Chrome 79 (Win 10)           TLSv1.3 TLS_AES_128_GCM_SHA256, 384 bit ECDH (P-384)
                Firefox 66 (Win 8.1/10)      TLSv1.3 TLS_AES_128_GCM_SHA256, 256 bit ECDH (P-256)
                Firefox 71 (Win 10)          TLSv1.3 TLS_AES_128_GCM_SHA256, 256 bit ECDH (P-256)
                IE 6 XP                      No connection
                IE 8 Win 7                   No connection
                IE 8 XP                      No connection
                IE 11 Win 7                  TLSv1.2 DHE-RSA-AES256-GCM-SHA384, 4096 bit DH  
                IE 11 Win 8.1                TLSv1.2 DHE-RSA-AES256-GCM-SHA384, 4096 bit DH  
                IE 11 Win Phone 8.1          No connection
                IE 11 Win 10                 TLSv1.2 ECDHE-RSA-AES256-GCM-SHA384, 256 bit ECDH (P-256)
                Edge 15 Win 10               TLSv1.2 ECDHE-RSA-AES256-GCM-SHA384, 256 bit ECDH (P-256)
                Edge 17 (Win 10)             TLSv1.2 ECDHE-RSA-AES256-GCM-SHA384, 256 bit ECDH (P-256)
                Opera 66 (Win 10)            TLSv1.3 TLS_AES_128_GCM_SHA256, 384 bit ECDH (P-384)
                Safari 9 iOS 9               TLSv1.2 ECDHE-RSA-AES256-GCM-SHA384, 256 bit ECDH (P-256)
                Safari 9 OS X 10.11          TLSv1.2 ECDHE-RSA-AES256-GCM-SHA384, 256 bit ECDH (P-256)
                Safari 10 OS X 10.12         TLSv1.2 ECDHE-RSA-AES256-GCM-SHA384, 256 bit ECDH (P-256)
                Safari 12.1 (iOS 12.2)       TLSv1.3 TLS_CHACHA20_POLY1305_SHA256, 384 bit ECDH (P-384)
                Safari 13.0 (macOS 10.14.6)  TLSv1.3 TLS_CHACHA20_POLY1305_SHA256, 384 bit ECDH (P-384)
                Apple ATS 9 iOS 9            TLSv1.2 ECDHE-RSA-AES256-GCM-SHA384, 256 bit ECDH (P-256)
                Java 6u45                    No connection
                Java 7u25                    No connection
                Java 8u161                   TLSv1.2 ECDHE-RSA-AES256-GCM-SHA384, 256 bit ECDH (P-256)
                Java 11.0.2 (OpenJDK)        TLSv1.3 TLS_AES_128_GCM_SHA256, 256 bit ECDH (P-256)
                Java 12.0.1 (OpenJDK)        TLSv1.3 TLS_AES_128_GCM_SHA256, 256 bit ECDH (P-256)
                OpenSSL 1.0.2e               TLSv1.2 ECDHE-RSA-AES256-GCM-SHA384, 256 bit ECDH (P-256)
                OpenSSL 1.1.0l (Debian)      TLSv1.2 ECDHE-RSA-AES256-GCM-SHA384, 256 bit ECDH (P-256)
                OpenSSL 1.1.1d (Debian)      TLSv1.3 TLS_AES_256_GCM_SHA384, 384 bit ECDH (P-384)
                Thunderbird (68.3)           TLSv1.3 TLS_AES_128_GCM_SHA256, 256 bit ECDH (P-256)
                
                
                Rating (experimental) 
                
                Rating specs (not complete)  SSL Labs's 'SSL Server Rating Guide' (version 2009q from 2020-01-30)
                Specification documentation  https://github.com/ssllabs/research/wiki/SSL-Server-Rating-Guide
                Protocol Support (weighted)  100 (30)
                Key Exchange     (weighted)  100 (30)
                Cipher Strength  (weighted)  90 (36)
                Final Score                  96
                Overall Grade                A+
                
                Done 2021-04-01 17:13:44 [  65s] -->> 185.26.156.226:443 (alinex.de) <<--
                
                
                /tmp/testssl-218357.json
        EXITCODE 0
        RESULT   {
                time: 65,
                SSL2: false,
                SSL3: false,
                TLS1: false,
                TLS1_1: false,
                TLS1_2: true,
                TLS1_3: true,
                score: 0.96,
                grade: 'A+',
                problems: [],
                valid: true,
                subject: { CN: 'alinex.de' },
                from: 2021-03-27T17:50:00.000Z,
                to: 2021-06-25T17:50:00.000Z,
                for: [ 'alinex.de' ],
                remaining: 7776000
                }
    ```

{!docs/assets/abbreviations.txt!}
