title: Web

# Web Pages and Services

This are a few checks which are very customizable to be able to test a wide range of web technologies.

## http

Simple HTTP call.

!!! abstract "Data"

    ```yaml
    data:
        url:                            # page to request
        warn:
            time: 5 s                   # time to load page in milliseconds
    #       code: 200                   # list of allowed http codes
    #       allow: /OK/                 # regexp pattern to be allowed
    #       disallow: /FAILURE/         # regexp pattern to be disallowed
        error:
            time: 30 s                  # time to load page in milliseconds
            code: 200                   # list of allowed http codes
    #       allow: /OK/                 # regexp pattern to be allowed
    #       disallow: /FAILURE/         # regexp pattern to be disallowed
    ```

The Result data will contain:

-   `time` - time to load page in milliseconds
-   `code` - HTTP status code returned
-   `code_text` - title for HTTP status code
-   `length` - length of content in bytes
-   `body` - text content

**Fixes**

The analyzation will explain the HTTP error codes, DNS resolution and connection to Port will be checked.

No further repair is possible here.

**Result**

??? example "Console output"

    ```ts
    ✔  OK HTTP (web.http)
        Simple HTTP Website call
        REQUEST  http://www.brainjar.com/java/host/test.html
        STATUS   200 OK
        RESPONSE content-type: text/html
                last-modified: Tue, 27 May 2003 15:17:04 GMT
                accept-ranges: bytes
                etag: "0d85676324c31:0"
                server: Microsoft-IIS/8.5
                x-powered-by: ASP.NET
                date: Thu, 01 Oct 2020 20:40:48 GMT
                connection: close
                content-length: 308
        RETURN   <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
                <html>
                <head>
                <title>Test HTML File</title>
                <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
                </head>
                <body>

                <p>This is a very simple HTML file.</p>

                </body>
                </html>
        RESULT   {
                time: 759,
                time2: 843,
                code: 200,
                code_text: 'OK',
                length: '308',
                body: '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">\r\n' +
                    '<html>\r\n' +
                    '<head>\r\n' +
                    '<title>Test HTML File</title>\r\n' +
                    '<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />\r\n' +
                    '</head>\r\n' +
                    '<body>\r\n' +
                    '\r\n' +
                    '<p>This is a very simple HTML file.</p>\r\n' +
                    '\r\n' +
                    '</body>\r\n' +
                    '</html>\r\n'
                }
        VALIDATE Prüfung der Werte auf Warnungen
                    Ein Datenobjekt mit Wertepaaren. Die Werte haben das folgende Format:
                    -   code: http.test.code
                        Ein nummerischer Wert. Der Wert sollte sein:
                        - gleich 200
    ```

## tls

Simple SSL/TLS check for valid certificate. A deeper SSL/TLS test can be made using [net/tls](net.md#tls) which will also calculate a quality score and make client simulations but takes more time to complete.

!!! abstract "Data"

    ```yaml
    data:
        domain:                         # domain to check
        port:                           # port (defaults to 443)
        warn:
            time: 1 week                # minimum remaining time
        error:
            time: 1 d                   # minimum remaining time
    ```

The Result data will contain:

-   `valid` - flag, set to `true` if valid
-   `subject` - owner of the certificate
    -   `C` - country
    -   `ST` - state/province
    -   `L` - locality
    -   `O` - organization
    -   `OU` - organizational unit
    -   `CN` - common name
-   `from` - creation date of certificate
-   `to` - expiration date of certificate
-   `for` - list of valid domains
-   `remaining` - remaining number of seconds for certificate
-   `chain` - list of chained certificates

**Fixes**

The analyzation will explain the HTTP error codes, DNS resolution and connection to Port will be checked.

No further repair is possible here.

**Result**

??? example "Console output"

    ```ts
    ✔  OK TLS (web.tls)
        Certificate Check
        tls.test.description
        REQUEST  HEAD https://google.com:443
        DATA     { method: 'HEAD' }
        RESPONSE {"subject":{"C":"US","ST":"California","L":"Mountain View","O":"Google
                LLC","CN":"*.google.com"},"issuer":{"C":"US","O":"Google Trust Services","CN":"GTS CA
                1O1"},"subjectaltname":"DNS:*.google.com, DNS:*.android.com, DNS:*.appengine.google.com,
                DNS:*.bdn.dev, DNS:*.cloud.google.com, DNS:*.crowdsource.google.com, DNS:*.datacompute.google.com,
                DNS:*.g.co, DNS:*.gcp.gvt2.com, DNS:*.gcpcdn.gvt1.com, DNS:*.ggpht.cn, DNS:*.gkecnapps.cn,
                DNS:*.google-analytics.com, DNS:*.google.ca, DNS:*.google.cl, DNS:*.google.co.in,
                DNS:*.google.co.jp, DNS:*.google.co.uk, DNS:*.google.com.ar, DNS:*.google.com.au,
                DNS:*.google.com.br, DNS:*.google.com.co, DNS:*.google.com.mx, DNS:*.google.com.tr,
                DNS:*.google.com.vn, DNS:*.google.de, DNS:*.google.es, DNS:*.google.fr, DNS:*.google.hu,
                DNS:*.google.it, DNS:*.google.nl, DNS:*.google.pl, DNS:*.google.pt, DNS:*.googleadapis.com,
                DNS:*.googleapis.cn, DNS:*.googlecnapps.cn, DNS:*.googlecommerce.com, DNS:*.googlevideo.com,
                DNS:*.gstatic.cn, DNS:*.gstatic.com, DNS:*.gstaticcnapps.cn, DNS:*.gvt1.com, DNS:*.gvt2.com,
                DNS:*.metric.gstatic.com, DNS:*.urchin.com, DNS:*.url.google.com, DNS:*.wear.gkecnapps.cn,
                DNS:*.youtube-nocookie.com, DNS:*.youtube.com, DNS:*.youtubeeducation.com, DNS:*.youtubekids.com,
                DNS:*.yt.be, DNS:*.ytimg.com, DNS:android.clients.google.com, DNS:android.com,
                DNS:developer.android.google.cn, DNS:developers.android.google.cn, DNS:g.co, DNS:ggpht.cn,
                DNS:gkecnapps.cn, DNS:goo.gl, DNS:google-analytics.com, DNS:google.com, DNS:googlecnapps.cn,
                DNS:googlecommerce.com, DNS:source.android.google.cn, DNS:urchin.com, DNS:www.goo.gl,
                DNS:youtu.be, DNS:youtube.com, DNS:youtubeeducation.com, DNS:youtubekids.com,
                DNS:yt.be","infoAccess":{"OCSP - URI":["http://ocsp.pki.goog/gts1o1core"],"CA Issuers -
                URI":["http://pki.goog/gsr2/GTS1O1.crt"]},"bits":256,"pubkey":{"type":"Buffer","data":[4,104,153,217,150,171,160,101,59,76,135,207,213,219,211,195,211,89,184,92,51,14,24,23,94,28,65,24,131,42,120,160,45,138,119,108,108,84,8,59,110,229,208,29,113,195,52,154,164,29,204,173,28,50,118,12,94,161,2,217,61,199,195,174,59]},"asn1Curve":"prime256v1","nistCurve":"P-256","valid_from":"Sep
                22 15:22:19 2020 GMT","valid_to":"Dec 15 15:22:19 2020
                GMT","fingerprint":"60:B6:04:A7:17:35:40:18:7E:DE:BA:14:4D:F7:4D:B7:18:F5:03:D9","fingerprint256":"07:44:5F:6A:CA:D6:B6:CA:D5:2D:1E:A8:C1:1F:AB:5D:E1:D8:F4:6D:62:54:E5:FE:73:0A:FA:BA:C7:79:AB:26","ext_key_usage":["1.3.6.1.5.5.7.3.1"],"serialNumber":"ECEA6F188F56B17202000000007C350B","raw":{"type":"Buffer","data":[48,130,9,113,48,130,8,89,160,3,2,1,2,2,17,0,236,234,111,24,143,86,177,114,2,0,0,0,0,124,53,11,48,13,6,9,42,134,72,134,247,13,1,1,11,5,0,48,66,49,11,48,9,6,3,85,4,6,19,2,85,83,49,30,48,28,6,3,85,4,10,19,21,71,111,111,103,108,101,32,84,114,117,115,116,32,83,101,114,118,105,99,101,115,49,19,48,17,6,3,85,4,3,19,10,71,84,83,32,67,65,32,49,79,49,48,30,23,13,50,48,48,57,50,50,49,53,50,50,49,57,90,23,13,50,48,49,50,49,53,49,53,50,50,49,57,90,48,102,49,11,48,9,6,3,85,4,6,19,2,85,83,49,19,48,17,6,3,85,4,8,19,10,67,97,108,105,102,111,114,110,105,97,49,22,48,20,6,3,85,4,7,19,13,77,111,117,110,116,97,105,110,32,86,105,101,119,49,19,48,17,6,3,85,4,10,19,10,71,111,111,103,108,101,32,76,76,67,49,21,48,19,6,3,85,4,3,12,12,42,46,103,111,111,103,108,101,46,99,111,109,48,89,48,19,6,7,42,134,72,206,61,2,1,6,8,42,134,72,206,61,3,1,7,3,66,0,4,104,153,217,150,171,160,101,59,76,135,207,213,219,211,195,211,89,184,92,51,14,24,23,94,28,65,24,131,42,120,160,45,138,119,108,108,84,8,59,110,229,208,29,113,195,52,154,164,29,204,173,28,50,118,12,94,161,2,217,61,199,195,174,59,163,130,7,7,48,130,7,3,48,14,6,3,85,29,15,1,1,255,4,4,3,2,7,128,48,19,6,3,85,29,37,4,12,48,10,6,8,43,6,1,5,5,7,3,1,48,12,6,3,85,29,19,1,1,255,4,2,48,0,48,29,6,3,85,29,14,4,22,4,20,67,126,166,226,150,55,81,88,218,215,94,114,51,137,0,219,112,253,38,164,48,31,6,3,85,29,35,4,24,48,22,128,20,152,209,248,110,16,235,207,155,236,96,159,24,144,27,160,235,125,9,253,43,48,104,6,8,43,6,1,5,5,7,1,1,4,92,48,90,48,43,6,8,43,6,1,5,5,7,48,1,134,31,104,116,116,112,58,47,47,111,99,115,112,46,112,107,105,46,103,111,111,103,47,103,116,115,49,111,49,99,111,114,101,48,43,6,8,43,6,1,5,5,7,48,2,134,31,104,116,116,112,58,47,47,112,107,105,46,103,111,111,103,47,103,115,114,50,47,71,84,83,49,79,49,46,99,114,116,48,130,4,194,6,3,85,29,17,4,130,4,185,48,130,4,181,130,12,42,46,103,111,111,103,108,101,46,99,111,109,130,13,42,46,97,110,100,114,111,105,100,46,99,111,109,130,22,42,46,97,112,112,101,110,103,105,110,101,46,103,111,111,103,108,101,46,99,111,109,130,9,42,46,98,100,110,46,100,101,118,130,18,42,46,99,108,111,117,100,46,103,111,111,103,108,101,46,99,111,109,130,24,42,46,99,114,111,119,100,115,111,117,114,99,101,46,103,111,111,103,108,101,46,99,111,109,130,24,42,46,100,97,116,97,99,111,109,112,117,116,101,46,103,111,111,103,108,101,46,99,111,109,130,6,42,46,103,46,99,111,130,14,42,46,103,99,112,46,103,118,116,50,46,99,111,109,130,17,42,46,103,99,112,99,100,110,46,103,118,116,49,46,99,111,109,130,10,42,46,103,103,112,104,116,46,99,110,130,14,42,46,103,107,101,99,110,97,112,112,115,46,99,110,130,22,42,46,103,111,111,103,108,101,45,97,110,97,108,121,116,105,99,115,46,99,111,109,130,11,42,46,103,111,111,103,108,101,46,99,97,130,11,42,46,103,111,111,103,108,101,46,99,108,130,14,42,46,103,111,111,103,108,101,46,99,111,46,105,110,130,14,42,46,103,111,111,103,108,101,46,99,111,46,106,112,130,14,42,46,103,111,111,103,108,101,46,99,111,46,117,107,130,15,42,46,103,111,111,103,108,101,46,99,111,109,46,97,114,130,15,42,46,103,111,111,103,108,101,46,99,111,109,46,97,117,130,15,42,46,103,111,111,103,108,101,46,99,111,109,46,98,114,130,15,42,46,103,111,111,103,108,101,46,99,111,109,46,99,111,130,15,42,46,103,111,111,103,108,101,46,99,111,109,46,109,120,130,15,42,46,103,111,111,103,108,101,46,99,111,109,46,116,114,130,15,42,46,103,111,111,103,108,101,46,99,111,109,46,118,110,130,11,42,46,103,111,111,103,108,101,46,100,101,130,11,42,46,103,111,111,103,108,101,46,101,115,130,11,42,46,103,111,111,103,108,101,46,102,114,130,11,42,46,103,111,111,103,108,101,46,104,117,130,11,42,46,103,111,111,103,108,101,46,105,116,130,11,42,46,103,111,111,103,108,101,46,110,108,130,11,42,46,103,111,111,103,108,101,46,112,108,130,11,42,46,103,111,111,103,108,101,46,112,116,130,18,42,46,103,111,111,103,108,101,97,100,97,112,105,115,46,99,111,109,130,15,42,46,103,111,111,103,108,101,97,112,105,115,46,99,110,130,17,42,46,103,111,111,103,108,101,99,110,97,112,112,115,46,99,110,130,20,42,46,103,111,111,103,108,101,99,111,109,109,101,114,99,101,46,99,111,109,130,17,42,46,103,111,111,103,108,101,118,105,100,101,111,46,99,111,109,130,12,42,46,103,115,116,97,116,105,99,46,99,110,130,13,42,46,103,115,116,97,116,105,99,46,99,111,109,130,18,42,46,103,115,116,97,116,105,99,99,110,97,112,112,115,46,99,110,130,10,42,46,103,118,116,49,46,99,111,109,130,10,42,46,103,118,116,50,46,99,111,109,130,20,42,46,109,101,116,114,105,99,46,103,115,116,97,116,105,99,46,99,111,109,130,12,42,46,117,114,99,104,105,110,46,99,111,109,130,16,42,46,117,114,108,46,103,111,111,103,108,101,46,99,111,109,130,19,42,46,119,101,97,114,46,103,107,101,99,110,97,112,112,115,46,99,110,130,22,42,46,121,111,117,116,117,98,101,45,110,111,99,111,111,107,105,101,46,99,111,109,130,13,42,46,121,111,117,116,117,98,101,46,99,111,109,130,22,42,46,121,111,117,116,117,98,101,101,100,117,99,97,116,105,111,110,46,99,111,109,130,17,42,46,121,111,117,116,117,98,101,107,105,100,115,46,99,111,109,130,7,42,46,121,116,46,98,101,130,11,42,46,121,116,105,109,103,46,99,111,109,130,26,97,110,100,114,111,105,100,46,99,108,105,101,110,116,115,46,103,111,111,103,108,101,46,99,111,109,130,11,97,110,100,114,111,105,100,46,99,111,109,130,27,100,101,118,101,108,111,112,101,114,46,97,110,100,114,111,105,100,46,103,111,111,103,108,101,46,99,110,130,28,100,101,118,101,108,111,112,101,114,115,46,97,110,100,114,111,105,100,46,103,111,111,103,108,101,46,99,110,130,4,103,46,99,111,130,8,103,103,112,104,116,46,99,110,130,12,103,107,101,99,110,97,112,112,115,46,99,110,130,6,103,111,111,46,103,108,130,20,103,111,111,103,108,101,45,97,110,97,108,121,116,105,99,115,46,99,111,109,130,10,103,111,111,103,108,101,46,99,111,109,130,15,103,111,111,103,108,101,99,110,97,112,112,115,46,99,110,130,18,103,111,111,103,108,101,99,111,109,109,101,114,99,101,46,99,111,109,130,24,115,111,117,114,99,101,46,97,110,100,114,111,105,100,46,103,111,111,103,108,101,46,99,110,130,10,117,114,99,104,105,110,46,99,111,109,130,10,119,119,119,46,103,111,111,46,103,108,130,8,121,111,117,116,117,46,98,101,130,11,121,111,117,116,117,98,101,46,99,111,109,130,20,121,111,117,116,117,98,101,101,100,117,99,97,116,105,111,110,46,99,111,109,130,15,121,111,117,116,117,98,101,107,105,100,115,46,99,111,109,130,5,121,116,46,98,101,48,33,6,3,85,29,32,4,26,48,24,48,8,6,6,103,129,12,1,2,2,48,12,6,10,43,6,1,4,1,214,121,2,5,3,48,51,6,3,85,29,31,4,44,48,42,48,40,160,38,160,36,134,34,104,116,116,112,58,47,47,99,114,108,46,112,107,105,46,103,111,111,103,47,71,84,83,49,79,49,99,111,114,101,46,99,114,108,48,130,1,4,6,10,43,6,1,4,1,214,121,2,4,2,4,129,245,4,129,242,0,240,0,117,0,7,183,92,27,229,125,104,255,241,176,198,29,35,21,199,186,230,87,124,87,148,183,106,238,188,97,58,26,105,211,162,28,0,0,1,116,182,159,134,192,0,0,4,3,0,70,48,68,2,32,98,179,175,54,211,213,42,68,205,212,165,210,188,98,128,159,47,192,107,101,142,190,139,163,145,124,105,168,238,119,151,77,2,32,74,183,15,189,126,217,168,253,238,189,62,114,160,150,5,141,186,89,7,6,61,115,0,21,91,137,253,243,158,82,158,87,0,119,0,231,18,242,176,55,126,26,98,251,142,201,12,97,132,241,234,123,55,203,86,29,17,38,91,243,224,243,75,242,65,84,110,0,0,1,116,182,159,132,213,0,0,4,3,0,72,48,70,2,33,0,145,200,110,65,229,108,48,214,186,59,11,21,141,45,66,208,247,160,77,198,211,83,10,74,52,108,208,131,228,199,163,61,2,33,0,195,94,229,67,181,113,177,187,187,170,26,59,191,242,69,75,118,59,83,146,123,2,168,49,215,109,60,29,78,80,24,126,48,13,6,9,42,134,72,134,247,13,1,1,11,5,0,3,130,1,1,0,13,235,82,115,164,38,73,85,22,107,165,82,72,71,46,135,159,174,91,209,144,193,93,56,119,199,163,169,15,117,246,99,68,58,21,26,154,66,227,212,3,188,96,13,75,213,157,69,41,48,107,12,219,41,115,82,187,111,38,20,142,9,189,246,21,77,224,244,92,21,205,56,159,252,244,64,230,169,226,97,24,114,177,29,154,107,227,113,30,14,92,174,230,186,210,1,49,79,250,18,29,225,138,164,149,23,76,36,207,202,145,120,192,157,88,188,163,157,238,148,95,54,169,70,71,129,159,111,160,85,203,246,2,3,112,150,253,185,52,201,13,36,247,216,139,253,169,220,159,106,134,6,126,62,249,157,151,88,102,71,130,142,69,78,240,173,22,94,153,172,173,53,79,229,30,170,18,233,242,68,14,191,35,121,241,104,140,75,231,63,237,174,12,216,108,179,37,239,167,137,106,29,120,161,88,247,83,116,74,144,85,137,127,28,151,70,38,34,110,130,54,98,93,70,222,64,3,224,191,22,98,224,198,30,197,192,19,196,182,234,44,185,111,137,31,11,33,76,124,48,202,82,59,181,61,113]}}
        RESULT   {
                valid: true,
                from: '2020-09-22T15:22:19.000Z',
                to: '2020-12-15T15:22:19.000Z',
                for: [
                    '*.google.com',
                    '*.android.com',
                    '*.appengine.google.com',
                    '*.bdn.dev',
                    '*.cloud.google.com',
                    '*.crowdsource.google.com',
                    '*.datacompute.google.com',
                    '*.g.co',
                    '*.gcp.gvt2.com',
                    '*.gcpcdn.gvt1.com',
                    '*.ggpht.cn',
                    '*.gkecnapps.cn',
                    '*.google-analytics.com',
                    '*.google.ca',
                    '*.google.cl',
                    '*.google.co.in',
                    '*.google.co.jp',
                    '*.google.co.uk',
                    '*.google.com.ar',
                    '*.google.com.au',
                    '*.google.com.br',
                    '*.google.com.co',
                    '*.google.com.mx',
                    '*.google.com.tr',
                    '*.google.com.vn',
                    '*.google.de',
                    '*.google.es',
                    '*.google.fr',
                    '*.google.hu',
                    '*.google.it',
                    '*.google.nl',
                    '*.google.pl',
                    '*.google.pt',
                    '*.googleadapis.com',
                    '*.googleapis.cn',
                    '*.googlecnapps.cn',
                    '*.googlecommerce.com',
                    '*.googlevideo.com',
                    '*.gstatic.cn',
                    '*.gstatic.com',
                    '*.gstaticcnapps.cn',
                    '*.gvt1.com',
                    '*.gvt2.com',
                    '*.metric.gstatic.com',
                    '*.urchin.com',
                    '*.url.google.com',
                    '*.wear.gkecnapps.cn',
                    '*.youtube-nocookie.com',
                    '*.youtube.com',
                    '*.youtubeeducation.com',
                    '*.youtubekids.com',
                    '*.yt.be',
                    '*.ytimg.com',
                    'android.clients.google.com',
                    'android.com',
                    'developer.android.google.cn',
                    'developers.android.google.cn',
                    'g.co',
                    'ggpht.cn',
                    'gkecnapps.cn',
                    'goo.gl',
                    'google-analytics.com',
                    'google.com',
                    'googlecnapps.cn',
                    'googlecommerce.com',
                    'source.android.google.cn',
                    'urchin.com',
                    'www.goo.gl',
                    'youtu.be',
                    'youtube.com',
                    'youtubeeducation.com',
                    'youtubekids.com',
                    'yt.be'
                ],
                remaining: 7257600
                }
        VALIDATE Check values for errors
                    A map like data object. The items have the following format:
                    -   valid: tls.test.valid
                        A boolean value. It is true for [ true ] and false for [ false ]. The value has to be
                        'true'.
        VALIDATE Check values for warnings
                    A map like data object. The items have the following format:
    ```

## apache

Analyze Apache by using the apache server status page. Therefore the mod_status module have to be installed and configured to be ac cessible by this test.

??? info "Setup Apache"

    The `server-status` component is enabled by default using the file `/etc/apache2/mods-enabled/status.conf`:

    ```text
    <IfModule mod_status.c>
            # Allow server status reports generated by mod_status,
            # with the URL of http://servername/server-status
            # change the "192.0.0.0/16" to allow access from other hosts.

            <Location /server-status>
                    SetHandler server-status
                    Require local
                    Require ip 192.168.0.0/16
            </Location>

            # Keep track of extended status information for each request
            ExtendedStatus On

            # Determine if mod_status displays the first 63 characters of a request or
            # the last 63, assuming the request itself is greater than 63 chars.
            # Default: Off
            #SeeRequestTail On

            <IfModule mod_proxy.c>
                    # Show Proxy LoadBalancer status in mod_status
                    ProxyStatus On
            </IfModule>

    </IfModule>

    # vim: syntax=apache ts=4 sw=4 sts=4 sr noet
    ```

    You may call the status from cli using `apachectl status`.

!!! abstract "Data"

    ```yaml
    data:
        url: http://172.17.100.10/server-status  # page to request
        warn:
            time: 1 s                   # time to load page in milliseconds
    #       code: 200                   # list of allowed http codes
        error:
            time: 30 s                  # time to load page in milliseconds
            code: 200                   # list of allowed http codes
    ```

The Result data will contain:

-   `time` - time to load page in milliseconds
-   `code` - HTTP status code returned
-   `code_text` - title for HTTP status code
-   `version` - apache version text
-   `MPM` - engine used: prefork, worker opr event
-   `uptime` - time in seconds since last apache server start
-   `load_1mTotal` - total 1 minute load (like shown in top)
-   `load_5mTotal` - total 5 minute load (like shown in top)
-   `load_15mTotal` - total 15 minute load (like shown in top)
-   `requests` - number of requests
-   `requests_diff` - number of requests per second
-   `transfer` - number of bytes
-   `transfer_diff` - number of bytes per second
-   `workers_busy` - number of busy workers
-   `workers_idle` - number of idle workers
-   `conn` - current connections
-   `conn_writing` - connections writing
-   `conn_keepalive` - connections waiting
-   `conn_losing` - connections closing
-   `cache_usage` - percentage usage of cache
-   `cache_index_usage` - percentage usage of index cache

**Fixes**

The analyzation will explain the HTTP error codes, DNS resolution and connection to Port will be checked.

No further repair is possible here.

**Result**

??? example "Console output"

    ```ts
    ✔   OK Apache Status (web.apache)
        Dieser Test prüft den Apache Web Server Status unter http://172.17.100.10/server-status.
        REQUEST  http://172.17.100.10/server-status?auto
                (2021-05-03T14:36:35.457+02:00)
        DATA     { format: 'properties', ignoreError: true }
        STATUS   200 OK
        RESPONSE date: Mon, 03 May 2021 12:36:35 GMT
                server: Apache/2.4.25 (Debian)
                vary: Accept-Encoding
                content-length: 3688
                connection: close
                content-type: text/plain; charset=ISO-8859-1
        RETURN   {
                '172': [
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    [
                    undefined, undefined, undefined, undefined, undefined, undefined,
                    undefined, undefined, undefined, undefined, undefined, undefined,
                    undefined, undefined, undefined, undefined, undefined, undefined,
                    undefined, undefined, undefined, undefined, undefined, undefined,
                    undefined, undefined, undefined, undefined, undefined, undefined,
                    undefined, undefined, undefined, undefined, undefined, undefined,
                    undefined, undefined, undefined, undefined, undefined, undefined,
                    undefined, undefined, undefined, undefined, undefined, undefined,
                    undefined, undefined, undefined, undefined, undefined, undefined,
                    undefined, undefined, undefined, undefined, undefined, undefined,
                    undefined, undefined, undefined, undefined, undefined, undefined,
                    undefined, undefined, undefined, undefined, undefined, undefined,
                    undefined, undefined, undefined, undefined, undefined, undefined,
                    undefined, undefined, undefined, undefined, undefined, undefined,
                    undefined, undefined, undefined, undefined, undefined, undefined,
                    undefined, undefined, undefined, undefined, undefined, undefined,
                    undefined, undefined, undefined, undefined,
                    ... 1 more item
                    ]
                ],
                ServerVersion: 'Apache/2.4.25 (Debian) OpenSSL/1.0.2l',
                ServerMPM: 'event',
                Server: 'Built: 2018-03-31T08:47:16',
                CurrentTime: 'Monday, 03-May-2021 14:36:35 CEST',
                RestartTime: 'Thursday, 11-Feb-2021 11:39:29 CET',
                ParentServerConfigGeneration: 89,
                ParentServerMPMGeneration: 88,
                ServerUptimeSeconds: 7005425,
                ServerUptime: '81 days 1 hour 57 minutes 5 seconds',
                Load1: 0.04,
                Load5: 0.13,
                Load15: 0.09,
                Total: 'kBytes: 2825916635',
                CPUUser: 2490.74,
                CPUSystem: 550.25,
                CPUChildrenUser: 0,
                CPUChildrenSystem: 0,
                CPULoad: 0.0434091,
                Uptime: 7005425,
                ReqPerSec: 16.0564,
                BytesPerSec: 413071,
                BytesPerReq: 25726.2,
                BusyWorkers: 17,
                IdleWorkers: 58,
                ConnsTotal: 201,
                ConnsAsyncWriting: 0,
                ConnsAsyncKeepAlive: 75,
                ConnsAsyncClosing: 97,
                Scoreboard:
                '......G..G.G..G.....................G.....................G.........G......G..G..G..........G..................................................G...............G..............._____R___RR__R__RR___RR__.........................RR_____R_R____R__________...G....................._R_______W_______R___R___........................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................',
                TLSSessionCacheStatus: null,
                CacheType: 'SHMCB',
                CacheSharedMemory: 512000,
                CacheCurrentEntries: 530,
                CacheSubcaches: 32,
                CacheIndexesPerSubcaches: 88,
                CacheTimeLeftOldestAvg: 27,
                CacheTimeLeftOldestMin: 1,
                CacheTimeLeftOldestMax: 98,
                CacheIndexUsage: '18%',
                CacheUsage: '23%',
                CacheStoreCount: 10431,
                CacheReplaceCount: 0,
                CacheExpireCount: 9901,
                CacheDiscardCount: 0,
                CacheRetrieveHitCount: 5954,
                CacheRetrieveMissCount: 21200,
                CacheRemoveHitCount: 0,
                CacheRemoveMissCount: 0
                }
                (2021-05-03T14:36:35.588+02:00)
        RESULT   {
                time: 59,
                code: 200,
                code_text: 'OK',
                length: '3688',
                version: 'Apache/2.4.25 (Debian) OpenSSL/1.0.2l',
                MPM: 'event',
                uptime: 7005425,
                load_1mTotal: 0.04,
                load_5mTotal: 0.13,
                load_15mTotal: 0.09,
                requests_diff: 16.0564,
                bytes_diff: 413071,
                bytes_request_diff: 25726.2,
                workers_busy: 17,
                workers_idle: 58,
                conn: 201,
                conn_writing: 0,
                conn_keepalive: 75,
                conn_losing: 97,
                cache_usage: 0.23,
                cache_index_usage: 0.18
                }
        VALIDATE Prüfung der Werte auf Fehler
                    Ein Datenobjekt mit Wertepaaren. Die Werte haben das folgende Format:
                    -   time: Die Abfragezeit der Webseite darf nicht mehr als 30 s betragen.
                        Ein nummerischer Wert. Der Wert muss kleiner oder gleich 30000 sein.
                    -   code: Der Status Code muss 200 sein.
                        Ein nummerischer Wert. Der Wert sollte sein:
                        - gleich 200
        VALIDATE Prüfung der Werte auf Warnungen
                    Ein Datenobjekt mit Wertepaaren. Die Werte haben das folgende Format:
                    -   time: Die Abfragezeit der Webseite darf nicht mehr als 5 s betragen.
                        Ein nummerischer Wert. Der Wert muss kleiner oder gleich 5000 sein.    ```

{!docs/assets/abbreviations.txt!}
