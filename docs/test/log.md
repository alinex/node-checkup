title: Log

# Log Analyzation

Logfile analyzation helps to:

-   find errors in any software which is logged away
-   help to check if a process did run
-   check last process result
-   check the progress in processes

But as the access to the logs, the file format and the contents vary in any system a lot of different and sometimes individual tests are needed.
You may also use multiple log checks on the same file, which will automatically use the cache to not transfer data from server twice.

## Event

This will check if a specific event exists or did not exist in the last time range. Therefore the log has to contain a readable date/time field,

!!! abstract "Data"

    ```yaml
    data:
    #   server:
    #       username: user                  # current user is used if not specified
    #       password: pass                  # password or use key below
    #       host: my.server.de              # server to connect (mandatory)
    #       port: 22                        # port if not default
    #       privateKey: /home/alex/.ssh/id_rsa       # key file, default is within the current user home
    #       passphrase: my-secure-phrase    # optional passphrase for key
    #   path: b
        command: journalctl -all -e -o short-iso --since "5 minutes ago"
        pattern: /(?<date>\d+-\d+-\d+T\d+:\d+:\d+\+\d+)\s+(?<host>[^\s]+)\s+(?<app>[^\s]+?)\[(?<pid>\d+)\]:\s(?<message>.*)/g
        range: 5 min
        # extract data elements: regexp will replace with $1…$3
        extract: 
            id: /(\[)(.*?)(\])/g
        # filter
        include: 
            message: /ERROR/
        exclude:
            host: localhost
        # check
        warn:
    #       min: 1  # used for success message
            max: 0  # error found

    fix:
        before: 3   # events to show before selected ones
        after: 3    # events to show after selected ones
    ```

This will do three things:
 
1.  load the log entries
2.  parse them into events
3.  filter events by include/exclude

Loading log entries should be made as slim as possible, meaning not to transfer whole logs but only the lines possibly needed. Therefore on the `server` within the `path` the `command` will be called. To transfer as less data as possible do a pre-filtering which is as specific as possible on the remote machine. If this is not needed you may also use a simple `cat` command. 
The `range` will filter the events by date after parsing them. This is only needed if this is not already done precisely on the remote machine.
Using `extract` variable data elements can be removed from the message to make it easier to match.
The filter can be used with both `include` and `exclude` patterns or strings for different fields. Only matching events are kept.
Afterwards the number of resulting log events is calculated and stored.

The following sections will show how to configure some known log files.

**Pattern**

The pattern can have the following parsed names:

-  `date` - needed with the event time
-  `message` - needed with the real message
-  `pid` - optional numeric process id
-  others will be kept as strings

The following elements should not be used:

- `original` - will be set to whole message (on extract)
- `variables` - will collect extracted variables (on extract)

**Extract**

Extraction is used to move variable parts out of the message. It will generate the `original` message which further contains them and the `variables` map with arrays of the extracted values by name.

Here a map for each field with a regexp pattern containing two to three groups are possible:

-   before
-   variable part
-   after (optional)

If the pattern has the `g` flag the variable will be stored as array with all matches, else a simple string is stored.

!!! Warning 

    Everything which is not within a group will get lost (kept only in the `original`).

**Include / Exclude**

Both containing a map of regular expressions for each event key will be used to filter them.

**Fixes**

Only analyzation is available here. All the matched events will be displayed  if configured with the ones around and in addition the raw log will also be shown.

**Result**

??? example "Console output"

    ```ts
    ⚡  WARN Syslog Event (log.event)
        Dieser Test prüft die Logeinträge auf localhost.
        Error at matched: Der Wert muss kleiner oder gleich 0 sein, aber 2 wurde angegeben.
        REQUEST  cd /; journalctl -all -e -o short-iso --since "5 minutes ago"
        RESPONSE code: 0
        RETURN   [
                  {
                    date: '2021-03-02T12:26:03+0100',
                    host: 'pc-alex',
                    app: 'dbus-daemon',
                    pid: '363',
                    message: `[system] Activating via systemd: service name='org.freedesktop.home1' unit='dbus-org.freedesktop.home1.service' requested by ':1.252' (uid=0 pid=63937
                    comm="sudo systemctl start sshd ")`
                  },
                  {
                    date: '2021-03-02T12:26:03+0100',
                    host: 'pc-alex',
                    app: 'audit',
                    pid: '63937',
                    message: `USER_ACCT pid=63937 uid=1000 auid=1000 ses=2 subj==unconfined msg='op=PAM:accounting grantors=pam_unix,pam_permit,pam_time acct="alex" exe="/usr/bin/sudo"
                    hostname=? addr=? terminal=/dev/pts/5 res=success'`
                  },
                  {
                    date: '2021-03-02T12:26:03+0100',
                    host: 'pc-alex',
                    app: 'audit',
                    pid: '63937',
                    message: `CRED_REFR pid=63937 uid=1000 auid=1000 ses=2 subj==unconfined msg='op=PAM:setcred grantors=pam_faillock,pam_permit,pam_env,pam_faillock acct="root"
                    exe="/usr/bin/sudo" hostname=? addr=? terminal=/dev/pts/5 res=success'`
                  },
                  ... some more
                ]             
        RESULT   {
                  num: 29,
                  pos: [ 11, 26 ],
                  matched: 2,
                  matches: [
                    {
                      date: 2021-03-02T11:26:03.000Z,
                      host: 'pc-alex',
                      app: 'sudo',
                      pid: 63937,
                      message: 'pam_unix(sudo:session): session closed for user root'
                    },
                    {
                      date: 2021-03-02T11:26:03.000Z,
                      host: 'pc-alex',
                      app: 'sudo',
                      pid: 63942,
                      message: 'pam_unix(sudo:session): session closed for user root'
                    }
                  ]
                }
        VALIDATE Prüfung der Werte auf Fehler
                    Ein Datenobjekt mit Wertepaaren.
        VALIDATE Prüfung der Werte auf Warnungen
                    Ein Datenobjekt mit Wertepaaren. Die Werte haben das folgende Format:
                    -   matched: Es darf maximal ein treffendes Ereignis im Log stehen.
                        Ein nummerischer Wert. Ein ganzzahliger Wert wird benötigt. Der Wert muss gleich oder zwischen 0 und 0 sein.
    🛠  MANUAL Anzeige der Logeinträge (log.event)
        Folgende Events sind hierbei aufgefallen:
            {"date":"2021-03-02T11:26:03.000Z","host":"pc-alex","app":"audit","pid":1,"message":"SERVICE_START pid=1 uid=0 auid=4294967295 ses=4294967295 subj==unconfined msg='unit=sshd
            comm=\"systemd\" exe=\"/usr/lib/systemd/systemd\" hostname=? addr=? terminal=? res=success'","original":"SERVICE_START pid=1 uid=0 auid=4294967295 ses=4294967295
            subj==unconfined msg='unit=sshd comm=\"systemd\" exe=\"/usr/lib/systemd/systemd\" hostname=? addr=? terminal=? res=success'"}
            {"date":"2021-03-02T11:26:03.000Z","host":"pc-alex","app":"sudo","pid":63937,"message":"pam_unix(sudo:session): session closed for user
            root","original":"pam_unix(sudo:session): session closed for user root"}
            {"date":"2021-03-02T11:26:03.000Z","host":"pc-alex","app":"audit","pid":63937,"message":"USER_END pid=63937 uid=1000 auid=1000 ses=2 subj==unconfined
            msg='op=PAM:session_close grantors=pam_limits,pam_unix,pam_permit acct=\"root\" exe=\"/usr/bin/sudo\" hostname=? addr=? terminal=/dev/pts/5 res=success'","original":"USER_END
            pid=63937 uid=1000 auid=1000 ses=2 subj==unconfined msg='op=PAM:session_close grantors=pam_limits,pam_unix,pam_permit acct=\"root\" exe=\"/usr/bin/sudo\" hostname=? addr=?
            terminal=/dev/pts/5 res=success'"}
            ---
            {"date":"2021-03-02T11:26:03.000Z","host":"pc-alex","app":"audit","pid":1,"message":"SERVICE_START pid=1 uid=0 auid=4294967295 ses=4294967295 subj==unconfined
            msg='unit=mongodb comm=\"systemd\" exe=\"/usr/lib/systemd/systemd\" hostname=? addr=? terminal=? res=success'","original":"SERVICE_START pid=1 uid=0 auid=4294967295
            ses=4294967295 subj==unconfined msg='unit=mongodb comm=\"systemd\" exe=\"/usr/lib/systemd/systemd\" hostname=? addr=? terminal=? res=success'"}
            {"date":"2021-03-02T11:26:03.000Z","host":"pc-alex","app":"sudo","pid":63942,"message":"pam_unix(sudo:session): session closed for user
            root","original":"pam_unix(sudo:session): session closed for user root"}
            {"date":"2021-03-02T11:26:03.000Z","host":"pc-alex","app":"audit","pid":63942,"message":"USER_END pid=63942 uid=1000 auid=1000 ses=2 subj==unconfined
            msg='op=PAM:session_close grantors=pam_limits,pam_unix,pam_permit acct=\"root\" exe=\"/usr/bin/sudo\" hostname=? addr=? terminal=/dev/pts/5 res=success'","original":"USER_END
            pid=63942 uid=1000 auid=1000 ses=2 subj==unconfined msg='op=PAM:session_close grantors=pam_limits,pam_unix,pam_permit acct=\"root\" exe=\"/usr/bin/sudo\" hostname=? addr=?
            terminal=/dev/pts/5 res=success'"}
        Das Log auf dem Server war hierbei:
            -- Journal begins at Mon 2020-12-07 19:52:23 CET, ends at Tue 2021-03-02 12:26:03 CET. --
            2021-03-02T12:26:03+0100 pc-alex dbus-daemon[363]: [system] Activating via systemd: service name='org.freedesktop.home1' unit='dbus-org.freedesktop.home1.service' requested
            by ':1.252' (uid=0 pid=63937 comm="sudo systemctl start sshd ")
            2021-03-02T12:26:03+0100 pc-alex kernel: audit: type=1101 audit(1614684363.731:138): pid=63937 uid=1000 auid=1000 ses=2 subj==unconfined msg='op=PAM:accounting
            grantors=pam_unix,pam_permit,pam_time acct="alex" exe="/usr/bin/sudo" hostname=? addr=? terminal=/dev/pts/5 res=success'
            2021-03-02T12:26:03+0100 pc-alex kernel: audit: type=1110 audit(1614684363.731:139): pid=63937 uid=1000 auid=1000 ses=2 subj==unconfined msg='op=PAM:setcred
            grantors=pam_faillock,pam_permit,pam_env,pam_faillock acct="root" exe="/usr/bin/sudo" hostname=? addr=? terminal=/dev/pts/5 res=success'
            2021-03-02T12:26:03+0100 pc-alex kernel: audit: type=1105 audit(1614684363.731:140): pid=63937 uid=1000 auid=1000 ses=2 subj==unconfined msg='op=PAM:session_open
            grantors=pam_limits,pam_unix,pam_permit acct="root" exe="/usr/bin/sudo" hostname=? addr=? terminal=/dev/pts/5 res=success'
            ... some more
    ```

## Log Examples

### Syslog

This analyzes the system messages which can be found on different locations:

- `journalctl` on SystemD managed systems
- `/var/log/syslog` on Debian
- `/var/log/messages` on RedHat

::: journalctl

    ```yaml
    command: journalctl -all -e -o short-iso
    pattern: /(?<date>\d+-\d+-\d+T\d+:\d+:\d+\+\d+)\s+(?<host>[^\s]+)\s+(?<app>[^\s]+?)\[(?<pid>\d+)\]:\s(?<message>.*)/g
    ```
    
!!! info

    The format is the same, but only with journalctl it can easily be read from a given date to read since the last invocation.

Details

- `journalctl --since <SQL Date>`
- `journalctl --lines 100`
- `journalctl --all -e -o json`
- `journalctl --all -e -o short-iso` - date host cmd[pid]: message 

{!docs/assets/abbreviations.txt!}
