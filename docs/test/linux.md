title: Linux

# Linux Shell Tests

All of the following tests will use the local shell or a remote ssh shell to gather data. Therefore you can select the connection as data element:

!!! abstract "Data"

    ```yaml
    server:
        username: user                  # current user is used if not specified
        password: pass                  # password or use key below
        host: my.server.de              # server to connect (mandatory)
        port: 22                        # port if not default
        privateKey: /home/alex/.ssh/id_rsa       # key file, default like shown
        passphrase: my-secure-phrase    # optional passphrase for key
    ```

This can be defined in any test or fix of the Linux module.
If no server is set, the local system will be checked.

!!! attention

    This tests are tested with debian and arch linux, so it may need some adjustments for other linux distributions. If you need it on other linux or unix distributions make an [issue](https://gitlab.com/alinex/node-checkup/-/issues) for me to adjust it.

## uptime

The host uptime is checked. If it is lower than the `age` level it shows that it was rebooted a short time ago. This is never an error but a information to be warned about.

!!! abstract "Data"

    ```yaml
    data:
    #   server: ...                     # to work on remote server (see above)
        warn:
            age: 1 h                    # minimum time since last boot
    fix:
        users: 5                        # number of last user entries to show in analysis
    ```

Result values are:

-   `boot` - date/time of last boot
-   `age` - time since last boot in seconds

**Fixes**

In case of a warning:

1. The last active user sessions are added, which will give you a hint on who was active and who may know more about the cause for the reboot.

**Repair**

No automatic fixing is possible here.

**Result**

??? example "Console output"

    ```ts
    ⚡  WARN Just Booted Up (local.uptime)
        Error at age (preset:result): The number has to be greater or equal 18000 but 11904 was given.
        Server should be up for more than 5 Minutes. If not this is a warning to get you informed about the previous boot.
        REQUEST  uptime -s
        RESPONSE code: 0
        RETURN   '2020-09-17 07:05:22'
        VALIDATE Check retrieved data elements
                    A date or time value. A text containing a formatted date will be parsed.
        RESULT   { boot: 2020-09-17T05:05:22.000Z, age: 11904 }
        VALIDATE Check values for warnings
                    A map like data object. The items have the following format:
                    -   age: The host should be running over 18000 minutes.
                        A numeric value. The value has to be greater or equal 18000.
        Analyze the last active users (manual)
        The last active users are:
            alex     tty1         :0               Thu Sep 17 07:05:43 2020   still logged in
            reboot   system boot  4.15.0-117-gener Thu Sep 17 07:05:29 2020   still running
            alex     tty1         :0               Wed Sep 16 17:12:33 2020 - down                      (05:56)
            reboot   system boot  4.15.0-117-gener Wed Sep 16 17:11:41 2020 - Wed Sep 16 23:08:43 2020  (05:57)
            alex     tty1         :0               Wed Sep 16 07:04:06 2020 - down                      (09:05)
    ```

## load

The host's system load is calculated and checked. Together with the number of CPUs the system load per CPU should not be too high. Thresholds are possible for each load value, iowait and steal time. Mostly the default setting may be enought.

!!! abstract "Data"

    ```yaml
    data:
    #   server: ...                     # to work on remote server (see above)
        warn:
            # load_1m: 300%
            load_5m: 200%
            # load_15m: 150%
            iowait: 30%
            steal: 20%
        error:
            # load_1m: 800%
            load_5m: 500%
            # load_15m: 400%
            iowait: 50%
            steal: 50%
    fix:
        iowait: 20%                     # threshold to add iowait analysis
        steal: 20%                      # threshold to add steal analysis
        procs: 5                        # number of processes to show
    ```

Result values:

-   `load_1mTotal` - total 1 minute load (like shown in top)
-   `load_5mTotal` - total 5 minute load (like shown in top)
-   `load_15mTotal` - total 15 minute load (like shown in top)
-   `cpus` - number of CPU cores
-   `user` - percentage of user time (0..1)
-   `system` - percentage of system time (0..1)
-   `nice` - percentage of nice process time (0..1)
-   `idle` - percentage of idle cpu time (0..1)
-   `iowait` - percentage of time spend in waiting on IO (0..1)
-   `hw_irq` - percentage of time spend in waiting on hard interrupts (0..1)
-   `sw_irq` - percentage of time spend in waiting on soft interrupts (0..1)
-   `steal` - percentage of time spend in waiting to for hypervisor (0..1)
-   `load_1m` - percentage of 1 minute load
-   `load_5m` - percentage of 5 minute load
-   `load_15m` - percentage of 15 minute load

**Fixes**

In case of error or warning the following analyses will be done:

1. Suggestion to check hypervisor for high steal time.
2. On high iowait a list of io waiting processes.
3. Suggestion to enlarge CPUs or memory on virtual machines.
4. List of highest CPU usage programs, which may be stopped or further analyzed.

**Repair**

No automatic fixing is possible here.

**Result**

??? example "Console output"

    ```ts
    ✖   ERROR System Load (local.load)
        Error at load_5m (preset:result): The number has to be lower than 0.1 but 0.565 was given.
        The load should not be too high to have a performant working system.
        REQUEST  cat /proc/loadavg && grep -c processor /proc/cpuinfo && LANG=C top -bn2 | grep Cpu | tail -n 1 | sed 's/[^0-9. ]//g'
        RESPONSE code: 0
        RETURN   1.89 2.26 2.41 3/1839 19599
                4
                5.6   2.3   0.5  91.3   0.2   0.0   0.1   0.0
        VALIDATE Check retrieved data elements
                    A list of elements. A single string will be split up by /[\s\n]+/. The items have the following format:
                    -   0: load 1m
                        A numeric value.
                    -   1: load 5m
                        A numeric value.
                    -   2: load 15m
                        A numeric value.
                    -   5: cpus
                        A numeric value.
                    -   6: user
                        A numeric value.
                    -   7: system
                        A numeric value.
                    -   8: nice
                        A numeric value.
                    -   9: idle
                        A numeric value.
                    -   10: iowait
                        A numeric value.
                    -   11: hwirq
                        A numeric value.
                    -   12: swirq
                        A numeric value.
                    -   13: steal
                        A numeric value.
                    At least 14 items are needed.
        RESULT   {
                load_1m_total: 1.89,
                load_5m_total: 2.26,
                load_15m_total: 2.41,
                cpus: 4,
                user: 0.055999999999999994,
                system: 0.023,
                nice: 0.005,
                idle: 0.9129999999999999,
                iowait: 0.002,
                hw_irq: 0,
                sw_irq: 0.001,
                steal: 0,
                load_1m: 0.4725,
                load_5m: 0.565,
                load_15m: 0.6025
                }
        VALIDATE Check values for warnings
                    A map like data object. The items have the following format:
                    -   load_5m: The 5 minute load per CPU core should be lower than 200%.
                        A numeric value. The value has to be less than 2.
                    -   iowait: The CPU time spent in wait (on disk) should be lower than 30%.
                        A numeric value. The value has to be less than 0.3.
                    -   steal: The time in involuntary wait by virtual cpu while hypervisor is servicing another machine (stolen) should be lower than 20%.
                        A numeric value. The value has to be less than 0.2.
        VALIDATE Check values for warnings
                    A map like data object. The items have the following format:
                    -   load_5m: The 5 minute load per CPU core should be lower than 10%.
                        A numeric value. The value has to be less than 0.1.
        Fix high CPU (manual)
        This is a virtual machine, so you are able to enhance (cpu/memory) resources to allow a higher overall load. Alternatively look at the top processes. Maybe you can stop one of them to reduce the overall
        load:
            CPU     RAM     PPID        Command
            37.8    19.7    2577         /usr/share/code/code
            19.8    27.8    2600         /opt/google/chrome/chrome
            6.3    1.1    1661         /usr/bin/ssh-agent
            4.1    0.6    2188         /usr/bin/kwin_x11
            1.8    4.4    2258         /usr/bin/cli
        Find all processes for each application using: pstree -ap <PPID>. Also have a look at iotop for processes with high disk io. After stopping some processes the load should get down slowly.
    ```

## memory

The memory usage on the host will be analyzed and a warning or error will be if the available memory or free swap space is under the defined percent value.

!!! abstract "Data"

    ```yaml
    data:
    #   server: ...                     # to work on remote server (see above)
        warn:
            mem_available: 0.1
            swap_free: 0.5
        error:
            mem_available: 0.01
            swap_free: 0.1
    fix:
        procs: 5                        # number of processes to show
    ```

The following results will be returned:

-   `mem_total` - total memory in KiB
-   `mem_free` - free memory in KiB
-   `mem_available` - available memory (free + cache) in KiB
-   `mem_available_percent` - percentage of available memory (0..1)
-   `swap_total` - total swap memory in KiB
-   `swap_free` - free swap memory in KiB
-   `swap_free_percent` - percentage of free swap memory (0..1)

**Fixes**

In case of error or warning the following analyses will be done:

1. Suggestion to check hypervisor for high steal time.
2. Suggestion to enlarge CPUs or memory on virtual machines.
3. List of highest memory usage programs, which may be stopped or further analyzed.
4. List of processes with the most swap usage.

**Repair**

No automatic fixing is possible here.

**Result**

??? example "Console output"

    ```ts
    ⚡  WARN System Memory (local.memory)
        Error at PercentAvailable (preset:result): The number has to be greater or equal 1 but 0.68 was given.
        Low available memory and free swap memory indicate to an overloaded and inperformant system.
        REQUEST  sed 's/ kB//;s/: \+/,/' /proc/meminfo
        RESPONSE code: 0
        RETURN   {
                MemTotal: 16302128,
                MemFree: 6210088,
                MemAvailable: 11128572,
                Buffers: 465264,
                Cached: 4666816,
                SwapCached: 0,
                Active: 5449808,
                Inactive: 3866784,
                'Active(anon)': 3999156,
                'Inactive(anon)': 484584,
                'Active(file)': 1450652,
                'Inactive(file)': 3382200,
                Unevictable: 112,
                Mlocked: 112,
                SwapTotal: 16653308,
                SwapFree: 16653308,
                Dirty: 1072,
                Writeback: 0,
                AnonPages: 4184704,
                Mapped: 1284716,
                Shmem: 499828,
                Slab: 502000,
                SReclaimable: 419652,
                SUnreclaim: 82348,
                KernelStack: 24384,
                PageTables: 114384,
                NFS_Unstable: 0,
                Bounce: 0,
                WritebackTmp: 0,
                CommitLimit: 24804372,
                Committed_AS: 17343356,
                VmallocTotal: 34359738367,
                VmallocUsed: 0,
                VmallocChunk: 0,
                HardwareCorrupted: 0,
                AnonHugePages: 0,
                ShmemHugePages: 0,
                ShmemPmdMapped: 0,
                CmaTotal: 0,
                CmaFree: 0,
                HugePages_Total: 0,
                HugePages_Free: 0,
                HugePages_Rsvd: 0,
                HugePages_Surp: 0,
                Hugepagesize: 2048,
                DirectMap4k: 415336,
                DirectMap2M: 12044288,
                DirectMap1G: 4194304
                }
        VALIDATE Check retrieved data elements
                    A map like data object. The items have the following format:
                    -   MemTotal: NumberSchema
                        A numeric value. The value has to be greater or equal 0.
                    -   MemFree: NumberSchema
                        A numeric value. The value has to be greater or equal 0.
                    -   MemAvailable: NumberSchema
                        A numeric value. The value has to be greater or equal 0.
                    -   SwapTotal: NumberSchema
                        A numeric value. The value has to be greater or equal 0.
                    -   SwapFree: NumberSchema
                        A numeric value. The value has to be greater or equal 0.
        RESULT   {
                mem_total: 16302128,
                mem_free: 6210088,
                mem_available: 11128572,
                mem_available_percent: 0.68,
                swap_total: 16653308,
                swap_free: 16653308,
                swap_free_percent: 1
                }
        VALIDATE Check values for warnings
                    A map like data object. The items have the following format:
                    -   swap_free_percent: The free swap space should be above 10%.
                        A numeric value. The value has to be greater or equal 0.1.
                    -   mem_available_percent: The available memory should be above 100%.
                        A numeric value. The value has to be greater or equal 1.
        VALIDATE Check values for errors
                    A map like data object. The items have the following format:
                    -   swap_free_percent: The free swap space should be above 10%.
                        A numeric value. The value has to be greater or equal 0.1.
                    -   mem_available_percent: The available memory should be above 1%.
                        A numeric value. The value has to be greater or equal 0.01.
        Fix high memory usage (manual)
        This is a virtual machine, so you are able to enhance (cpu/memory) resources to allow a higher overall load. Alternatively look at the top processes. Maybe you can stop one of them to reduce the overall
        load:
            RAM     CPU    PPID        Command
            19.0    9.5    1926         /opt/google/chrome/chrome
            11.5    27.9    1919         /usr/share/code/code
            4.1    0.1    1884         /usr/bin/akonadi_unifiedmailbox_agent
            3.0    1.5    1921         /usr/share/skypeforlinux/skypeforlinux
            1.6    1.3    1727         /usr/bin/plasmashell
        Find all processes for each application using: pstree -ap <PPID>. Also have a look at iotop for processes with high disk io. After stopping some processes the load should get down slowly.
    ```

## os

The host operating system is checked. The current version will be analyzed, the number of waiting updates and if a reboot is required.

!!! abstract "Data"

    ```yaml
    data:
    #   server: ...                     # to work on remote server (see above)
        warn:
            eol: 180 d                  # number of days to warn before end of life
            reboot: true                # if set a warn will occur if reboot is required
            updates: 10                 # warn for number of updates
            security: 5
    #       removes: 20
    ```

Result values are:

-   `os` - ooperating syst type: linux, windowsnt, darwin, sunos
-   `kernel` - kernel version number
-   `architecture` - architecture, mostly 'x86_64'
-   `dist_base` - base distribution type: debian, arch, gentoo, redhat, SuSE, mandrake
-   `dist_base_rev` - revision name or code name of base distribution
-   `dist` - distribution used
-   `code` - code name of installed version
-   `revision` - revision number of installed version
-   `virtual` - is this a virtual machine
-   `reboot` - should the system be rebooted
-   `updates` - total number of updates waiting to be installed
-   `security` - number of security updates needed
-   `removes` - number of packages which can be removed
-   `eol` - end of life if known
-   `eol_days` - number of days remaining (negative if behind end of life)

**Fixes**

In case of a warning or error:

1. The updates will be explained.
2. Description on how to reboot if necessary.

**Repair**

Both upgrade (but only a safe upgrade) will be done and the host will be rebooted afterwards if needed.

**Result**

??? example "Console output"

    ```ts
    ✔   OK Update (local.os)
        Check for updates.
        Das Betriebssystem wird analysiert, die Anzahl der updates wird ermittelt und ob ein reboot notwendig ist.
        PROCESS  uname | tr '[:upper:]' '[:lower:]' && uname -r && uname -m && ls /etc/arch-release /etc/gentoo-release /etc/redhat-release
                /etc/SuSE-release /etc/mandrake-release /etc/debian_version 2>/dev/null
        STDOUT   linux
                4.15.0-124-generic
                x86_64
                /etc/debian_version
        EXITCODE 2
        PROCESS  sudo -n /usr/sbin/apt update
        STDOUT   OK:1 http://ppa.launchpad.net/micahflee/ppa/ubuntu bionic InRelease
                OK:2 http://de.archive.ubuntu.com/ubuntu bionic InRelease
                Holen:3 http://archive.neon.kde.org/user bionic InRelease [131 kB]
                OK:4 http://apt.postgresql.org/pub/repos/apt buster-pgdg InRelease
                OK:5 http://packages.microsoft.com/repos/vscode stable InRelease
                OK:6 http://de.archive.ubuntu.com/ubuntu bionic-updates InRelease
                OK:7 http://ppa.launchpad.net/peek-developers/stable/ubuntu bionic InRelease
                OK:8 https://deb.nodesource.com/node_12.x bionic InRelease
                OK:9 http://apt.postgresql.org/pub/repos/apt jessie-pgdg InRelease
                OK:10 http://de.archive.ubuntu.com/ubuntu bionic-backports InRelease
                OK:11 https://linux.teamviewer.com/deb stable InRelease
                OK:12 http://security.ubuntu.com/ubuntu bionic-security InRelease
                Es wurden 131 kB in 2 s geholt (66,6 kB/s).
                Paketlisten werden gelesen...
                Abhängigkeitsbaum wird aufgebaut....
                Statusinformationen werden eingelesen....
                Aktualisierung für 7 Pakete verfügbar. Führen Sie »apt list --upgradable« aus, um sie anzuzeigen.
        STDERR   WARNING: apt does not have a stable CLI interface. Use with caution in scripts.
        EXITCODE 0
        PROCESS  cat /etc/debian_version && egrep '^NAME=|^DISTRIB_ID=' /etc/os-release /etc/lsb-release 2>/dev/null | head -n 1 && grep
                '_CODENAME=' /etc/lsb-release /etc/os-release 2>/dev/null | head -n 1 && egrep '^DISTRIB_RELEASE=|^VERSION_ID='
                /etc/lsb-release /etc/os-release 2>/dev/null | head -n 1
        PROCESS  grep -q '^flags.* hypervisor' /proc/cpuinfo
        PROCESS  ls /var/run/reboot-required 2>/dev/null
        PROCESS  apt-get upgrade -s 2>/dev/null | egrep ^Inst | wc -l && apt-get upgrade -s 2>/dev/null | egrep ^Inst | grep -i $(lsb_release
                -c | cut -f 2)-security | wc -l && apt-get --dry-run autoremove 2>/dev/null | egrep ^Remv | wc -l
        STDOUT   buster/sid
                /etc/os-release:NAME="KDE neon"
                /etc/lsb-release:DISTRIB_CODENAME=bionic
                /etc/lsb-release:DISTRIB_RELEASE=18.04
        EXITCODE 0
        EXITCODE 1
        EXITCODE 2
        STDOUT   0
                0
                0
        EXITCODE 0
        RESULT   {
                os: 'linux',
                kernel: '4.15.0-124-generic',
                architecture: 'x86_64',
                dist_base: 'debian',
                dist_base_rev: 'buster',
                dist: 'KDE neon',
                code: 'bionic',
                revision: '18.04',
                virtual: false,
                reboot: false,
                updates: 0,
                security: 0,
                removes: 0
                }
        VALIDATE Prüfung der Werte auf Fehler
                    Ein Datenobjekt mit Wertepaaren.
        VALIDATE Prüfung der Werte auf Warnungen
                    Ein Datenobjekt mit Wertepaaren. Die Werte haben das folgende Format:
                    -   reboot: Das Systeme sollte nichr neu gebootet werden müssen.
                        Ein logischer Wert. Er ist wahr für [ true ] und falsch für [ false ].
                    -   updates: Es solten weniger als 20 Pakete zum update bereit stehen.
                        Ein nummerischer Wert. Der Wert muss kleiner als 20 sein.
                    -   security: Es sollten weniger als 10 sicherheitsrelevante Pakete zum update bereit stehen.
                        Ein nummerischer Wert. Der Wert muss kleiner als 10 sein.
    ```

## process

A check which will analyze a process group. A lot of measurements are taken.

!!! abstract "Data"

    ```yaml
    data:
    #   server: ...                     # to work on remote server (see above)
        name: chrome                    # process name
        warn:
    #       cpu_user: 0.5
    #       cpu_system: 0.5
            cpu_guest: 0.3
            cpu_wait: 0.3
    #       cpu_total: 0.8
    #       disk_read: 10 KiB
    #       disk_write: 10 KiB
    #       mem_physical: 500 MiB
    #       mem_virtual: 16 GiB
    #       mem_stack: 100 MiB
    #       mem_swap: 20 MiB
    #       threads: 200
    #       files_num: 1000
    #       files_free: 100
        error:                          # all the same options like warn
            cpu_guest: 0.3
            cpu_wait: 0.3
    ```

This test will always report about all processes for the given name. This means that some values are summarized, while others collect a variant list.

!!! Warning

    You need to have the `sysstat` package with `pidstat` command installed to get all information.

The result values will be:

-   `command` - command from first process id
-   `pid` - system process id list
-   `uid` - user id number list (multiple if different are used)
-   `cpu_user` - Percentage of CPU used by the tasks while executing at the user level (application).
-   `cpu_system` - (pidstat needed) Percentage of CPU used by the tasks while executing at the system level (kernel).
-   `cpu_guest` - (pidstat needed) Percentage of CPU spent by the tasks in virtual machine (running a virtual processor).
-   `cpu_wait` - (pidstat needed) Percentage of CPU spent by the tasks while waiting to run.
-   `cpu_total` - Total percentage of CPU time used by the tasks.
-   `cpus` - (pidstat needed) Processor number list, which the tasks are attached to.
-   `disk_read` - (pidstat needed) Number of kilobytes the tasks has caused to be read from disk per second.
-   `disk_write` - (pidstat needed) Number of kilobytes the tasks has caused, or shall cause to be written to disk per second.
-   `mem_physical` - Resident Set Size: The non-swapped physical memory used by the tasks in kilobytes.
-   `mem_virtual` - Virtual Size: The virtual memory usage of entire tasks in kilobytes.
-   `mem_stack` - The amount of memory in kilobytes reserved for the tasks as stack, but not necessarily used.
-   `mem_swap` - The swap memory used by this tasks.
-   `threads` - Number of threads associated with current tasks.
-   `files_num` - Number of file descriptors associated with current tasks.
-   `filesLimit` - Maximum number of open files.
-   `files_free` - Number of free file handles for process.

**Fixes**

In case of error or warning the following additional information are reported:

1. Process tree with RAM, CPU and command
2. Thread list

**Repair**

No repair is implemented, yet.

**Result**

```js
✔  OK Process (local.process)
    Check Chrome process.
    This test will check the chrome process on localhost.
    PROCESS  LANG=C pidstat -C chrome -l | tail -n +4
    STDOUT   23:24:43     1000      2148    0.26    0.12    0.00    0.11    0.38     1  /opt/google/chrome/chrome
             23:24:43     1000      2197    0.00    0.00    0.00    0.00    0.00     1  /opt/google/chrome/chrome --type=zygote
             --no-zygote-sandbox
             23:24:43     1000      2198    0.00    0.00    0.00    0.00    0.00     3  /opt/google/chrome/chrome --type=zygote
             23:24:43     1000      2282    0.00    0.00    0.00    0.00    0.00     3  /opt/google/chrome/chrome --type=zygote
             23:24:43     1000      2379    0.03    0.01    0.00    0.02    0.04     1  /opt/google/chrome/chrome --type=gpu-process
             --field-trial-handle=8729633145620423228,16188253820756015615,131072
             --gpu-preferences=MAAAAAAAAAAgAAAQAAAAAAAAAAAAAAAAAABgAAAAAAAQAAAAAAAAAAAAAAAAAAAACAAAAAAAAAA= --shared-files
             23:24:43     1000      2386    0.14    0.04    0.00    0.00    0.18     0  /opt/google/chrome/chrome --type=utility
             --utility-sub-type=network.mojom.NetworkService --field-trial-handle=8729633145620423228,16188253820756015615,131072
             --lang=de --service-sandbox-type=network --shared-files
             23:24:43     1000      2412    0.00    0.00    0.00    0.00    0.00     2  /opt/google/chrome/chrome --type=utility
             --utility-sub-type=storage.mojom.StorageService --field-trial-handle=8729633145620423228,16188253820756015615,131072
             --lang=de --service-sandbox-type=utility --shared-files
             23:24:43     1000      2568    0.00    0.00    0.00    0.00    0.00     3  /opt/google/chrome/chrome --type=renderer
             --field-trial-handle=8729633145620423228,16188253820756015615,131072 --lang=de --extension-process --num-raster-threads=2
             --enable-main-frame-before-activation --renderer-client-id=5 --no-v8-untrusted-code-mitigations --shared-files
             23:24:43     1000      2648    0.00    0.00    0.00    0.00    0.00     2  /opt/google/chrome/chrome --type=renderer
             --field-trial-handle=8729633145620423228,16188253820756015615,131072 --lang=de --extension-process --num-raster-threads=2
             --enable-main-frame-before-activation --renderer-client-id=6 --no-v8-untrusted-code-mitigations --shared-files
             23:24:43     1000      2662    0.00    0.00    0.00    0.00    0.00     1  /opt/google/chrome/chrome --type=renderer
             --field-trial-handle=8729633145620423228,16188253820756015615,131072 --lang=de --extension-process --num-raster-threads=2
             --enable-main-frame-before-activation --renderer-client-id=7 --no-v8-untrusted-code-mitigations --shared-files
             23:24:43     1000      2670    0.00    0.00    0.00    0.01    0.01     0  /opt/google/chrome/chrome --type=renderer
             --field-trial-handle=8729633145620423228,16188253820756015615,131072 --lang=de --extension-process --num-raster-threads=2
             --enable-main-frame-before-activation --renderer-client-id=8 --no-v8-untrusted-code-mitigations --shared-files
             23:24:43     1000      2686    0.02    0.01    0.00    0.00    0.03     2  /opt/google/chrome/chrome --type=renderer
             --field-trial-handle=8729633145620423228,16188253820756015615,131072 --lang=de --extension-process --num-raster-threads=2
             --enable-main-frame-before-activation --renderer-client-id=9 --no-v8-untrusted-code-mitigations --shared-files
             23:24:43     1000      3601    0.00    0.00    0.00    0.00    0.00     0  /opt/google/chrome/chrome --type=renderer
             --field-trial-handle=8729633145620423228,16188253820756015615,131072 --lang=de --extension-process --num-raster-threads=2
             --enable-main-frame-before-activation --renderer-client-id=16 --no-v8-untrusted-code-mitigations --shared-files
             23:24:43     1000      3623    0.02    0.01    0.00    0.00    0.03     1  /opt/google/chrome/chrome --type=renderer
             --field-trial-handle=8729633145620423228,16188253820756015615,131072 --lang=de --extension-process --num-raster-threads=2
             --enable-main-frame-before-activation --renderer-client-id=18 --no-v8-untrusted-code-mitigations --shared-files
             23:24:43     1000      3667    0.00    0.00    0.00    0.00    0.00     2  /opt/google/chrome/chrome --type=renderer
             --field-trial-handle=8729633145620423228,16188253820756015615,131072 --lang=de --extension-process --num-raster-threads=2
             --enable-main-frame-before-activation --renderer-client-id=23 --no-v8-untrusted-code-mitigations --shared-files
             23:24:43     1000      3678    0.01    0.01    0.00    0.01    0.02     2  /usr/bin/plasma-browser-integration-host
             chrome-extension://cimiefiiaegbelhefglklhhakcgmhkai/
             23:24:43     1000      3776    0.01    0.00    0.00    0.00    0.01     0  /opt/google/chrome/chrome --type=renderer
             --field-trial-handle=8729633145620423228,16188253820756015615,131072 --lang=de --extension-process --num-raster-threads=2
             --enable-main-frame-before-activation --renderer-client-id=38 --no-v8-untrusted-code-mitigations --shared-files
             23:24:43     1000     19177    0.00    0.00    0.00    0.00    0.00     3  /opt/google/chrome/chrome --type=renderer
             --field-trial-handle=8729633145620423228,16188253820756015615,131072 --lang=de --num-raster-threads=2
             --enable-main-frame-before-activation --renderer-client-id=236 --no-v8-untrusted-code-mitigations --shared-files
             23:24:43     1000     19215    0.20    0.02    0.00    0.01    0.22     3  /opt/google/chrome/chrome --type=renderer
             --field-trial-handle=8729633145620423228,16188253820756015615,131072 --lang=de --extension-process --num-raster-threads=2
             --enable-main-frame-before-activation --renderer-client-id=237 --no-v8-untrusted-code-mitigations --shared-files
             23:24:43     1000     19274    0.00    0.00    0.00    0.00    0.00     2  /opt/google/chrome/chrome --type=utility
             --utility-sub-type=audio.mojom.AudioService --field-trial-handle=8729633145620423228,16188253820756015615,131072 --lang=de
             --service-sandbox-type=none --shared-files
             23:24:43     1000     24690    0.00    0.00    0.00    0.00    0.00     0  /opt/google/chrome/chrome --type=renderer
             --field-trial-handle=8729633145620423228,16188253820756015615,131072 --lang=de --num-raster-threads=2
             --enable-main-frame-before-activation --renderer-client-id=318 --no-v8-untrusted-code-mitigations --shared-files
    EXITCODE 0
    PROCESS  LANG=C pidstat -C chrome -d | tail -n +4
    STDOUT   23:24:43     1000      2148      8.78     20.77      4.43      37  chrome
             23:24:43     1000      2197      0.68      0.00      0.00       1  chrome
             23:24:43     1000      2198      0.33      0.00      0.00       0  chrome
             23:24:43     1000      2282      1.21      0.61      0.00       0  chrome
             23:24:43     1000      2379      0.44      0.00      0.00       2  chrome
             23:24:43     1000      2386      1.29      5.59      0.11       0  chrome
             23:24:43     1000      2412      0.26      0.18      0.00       0  chrome
             23:24:43     1000      2568      0.10      0.00      0.00       1  chrome
             23:24:43     1000      2648      0.10      0.01      0.00       0  chrome
             23:24:43     1000      2662      0.03      0.01      0.00       0  chrome
             23:24:43     1000      2670      0.11      0.00      0.00       0  chrome
             23:24:43     1000      2686      0.04      0.00      0.00       0  chrome
             23:24:43     1000      3601      0.00      0.01      0.00       0  chrome
             23:24:43     1000      3623      0.00      0.00      0.00       0  chrome
             23:24:43     1000      3776      0.00      0.00      0.00       0  chrome
             23:24:43     1000     19177      0.00      0.00      0.00       0  chrome
             23:24:43     1000     19215      0.06      0.13      0.00       0  chrome
             23:24:43     1000     19274      0.00      0.00      0.00       0  chrome
    EXITCODE 0
    PROCESS  LANG=C pidstat -C chrome -r | tail -n +4
    STDOUT   23:24:43     1000      2148     70.94      0.03  942468  258140   1.58  chrome
             23:24:43     1000      2197      0.16      0.00  462124   61748   0.38  chrome
             23:24:43     1000      2198      0.17      0.00  462156   62292   0.38  chrome
             23:24:43     1000      2282      0.34      0.00  462156   16428   0.10  chrome
             23:24:43     1000      2379      0.94      0.00  659308   96656   0.59  chrome
             23:24:43     1000      2386      3.40      0.00  561800  101252   0.62  chrome
             23:24:43     1000      2412      0.42      0.00  529372   39636   0.24  chrome
             23:24:43     1000      2568      0.11      0.00 4768696   81556   0.50  chrome
             23:24:43     1000      2648      0.24      0.00 4795320  100076   0.61  chrome
             23:24:43     1000      2662      0.16      0.00 4774840   86776   0.53  chrome
             23:24:43     1000      2670      0.50      0.00 4792344  100368   0.62  chrome
             23:24:43     1000      2686      0.27      0.00 4787132   92468   0.57  chrome
             23:24:43     1000      3601      0.19      0.00 4785132   92820   0.57  chrome
             23:24:43     1000      3623      0.12      0.00 4768736   87084   0.53  chrome
             23:24:43     1000      3667      0.12      0.00 4770784   83532   0.51  chrome
             23:24:43     1000      3776      0.62      0.00 4792256   99824   0.61  chrome
             23:24:43     1000     19177      0.15      0.00 4773072   90700   0.56  chrome
             23:24:43     1000     19215     14.92      0.00 4948544  230524   1.41  chrome
             23:24:43     1000     19274      0.16      0.00  760728   55088   0.34  chrome
             23:24:43     1000     24690      0.07      0.00 4751068   54624   0.34  chrome
    EXITCODE 0
    PROCESS  LANG=C pidstat -C chrome -s | tail -n +4
    STDOUT   23:24:43     1000      2148     132     100  chrome
             23:24:43     1000      2197     132      36  chrome
             23:24:43     1000      2198     132      44  chrome
             23:24:43     1000      2282     132      52  chrome
             23:24:43     1000      2379     132      52  chrome
             23:24:43     1000      2386     132      20  chrome
             23:24:43     1000      2412     132      24  chrome
             23:24:43     1000      2568     132      44  chrome
             23:24:43     1000      2648     132     108  chrome
             23:24:43     1000      2662     132      44  chrome
             23:24:43     1000      2670     132      44  chrome
             23:24:43     1000      2686     132      44  chrome
             23:24:43     1000      3601     132      44  chrome
             23:24:43     1000      3623     132      44  chrome
             23:24:43     1000      3667     132      44  chrome
             23:24:43     1000      3776     132      48  chrome
             23:24:43     1000     19177     132      44  chrome
             23:24:43     1000     19215     216     212  chrome
             23:24:43     1000     19274     132      32  chrome
             23:24:43     1000     24690     132      32  chrome
    EXITCODE 0
    PROCESS  LANG=C pidstat -C chrome -v | tail -n +4
    STDOUT   23:24:44     1000      2148      24     368  chrome
             23:24:44     1000      2197       1      13  chrome
             23:24:44     1000      2198       1      14  chrome
             23:24:44     1000      2282       1      15  chrome
             23:24:44     1000      2379      11      61  chrome
             23:24:44     1000      2386       7      50  chrome
             23:24:44     1000      2412       5      59  chrome
             23:24:44     1000      2568      11      31  chrome
             23:24:44     1000      2648      12      41  chrome
             23:24:44     1000      2662      11      31  chrome
             23:24:44     1000      2670      11      31  chrome
             23:24:44     1000      2686      11      31  chrome
             23:24:44     1000      3601      11      36  chrome
             23:24:44     1000      3623      11      31  chrome
             23:24:44     1000      3667      11      30  chrome
             23:24:44     1000      3776      11      30  chrome
             23:24:44     1000     19177      11      35  chrome
             23:24:44     1000     19215      13      57  chrome
             23:24:44     1000     19274       7      28  chrome
             23:24:44     1000     24690      10      30  chrome
    EXITCODE 0
    PROCESS  grep VmSwap /proc/2148/status /proc/2197/status /proc/2198/status /proc/2282/status /proc/2379/status /proc/2386/status
             /proc/2412/status /proc/2568/status /proc/2648/status /proc/2662/status /proc/2670/status /proc/2686/status
             /proc/3601/status /proc/3623/status /proc/3667/status /proc/3678/status /proc/3776/status /proc/19177/status
             /proc/19215/status /proc/19274/status /proc/24690/status
    STDOUT   /proc/2148/status:VmSwap:           0 kB
             /proc/2197/status:VmSwap:           0 kB
             /proc/2198/status:VmSwap:           0 kB
             /proc/2282/status:VmSwap:           0 kB
             /proc/2379/status:VmSwap:           0 kB
             /proc/2386/status:VmSwap:           0 kB
             /proc/2412/status:VmSwap:           0 kB
             /proc/2568/status:VmSwap:           0 kB
             /proc/2648/status:VmSwap:           0 kB
             /proc/2662/status:VmSwap:           0 kB
             /proc/2670/status:VmSwap:           0 kB
             /proc/2686/status:VmSwap:           0 kB
             /proc/3601/status:VmSwap:           0 kB
             /proc/3623/status:VmSwap:           0 kB
             /proc/3667/status:VmSwap:           0 kB
             /proc/3678/status:VmSwap:           0 kB
             /proc/3776/status:VmSwap:           0 kB
             /proc/19177/status:VmSwap:           0 kB
             /proc/19215/status:VmSwap:           0 kB
             /proc/19274/status:VmSwap:           0 kB
             /proc/24690/status:VmSwap:           0 kB
    EXITCODE 0
    RESULT   {
               uid: [ 1000 ],
               pid: [
                  2148,  2197,  2198,  2282,
                  2379,  2386,  2412,  2568,
                  2648,  2662,  2670,  2686,
                  3601,  3623,  3667,  3678,
                  3776, 19177, 19215, 19274,
                 24690
               ],
               cpu_user: 0.6900000000000002,
               cpu_system: 0.22000000000000003,
               cpu_guest: 0,
               cpu_wait: 0.16000000000000003,
               cpu_total: 0.92,
               cpus: [ 1, 3, 0, 2 ],
               command: '/opt/google/chrome/chrome',
               disk_read: 13.429999999999998,
               disk_write: 27.310000000000002,
               mem_virtual: 62348036,
               mem_physical: 1891592,
               mem_stack: 2724,
               threads: 191,
               files: 1022,
               mem_swap: 0
             }
    VALIDATE Prüfung der Werte auf Fehler
                 Ein Datenobjekt mit Wertepaaren. Die Werte haben das folgende Format:
                 -   cpu_guest: CPU should not spenT more than 60% by the tasks in virtual machine (running a virtual processor).
                     Ein nummerischer Wert. Der Wert muss kleiner oder gleich 0.6 sein.
                 -   cpu_wait: CPU should not spent more than 60% by the tasks while waiting to run.
                     Ein nummerischer Wert. Der Wert muss kleiner oder gleich 0.6 sein.
    VALIDATE Prüfung der Werte auf Warnungen
                 Ein Datenobjekt mit Wertepaaren. Die Werte haben das folgende Format:
                 -   cpu_guest: CPU should not spenT more than 30% by the tasks in virtual machine (running a virtual processor).
                     Ein nummerischer Wert. Der Wert muss kleiner oder gleich 0.3 sein.
                 -   cpu_wait: CPU should not spent more than 30% by the tasks while waiting to run.
                     Ein nummerischer Wert. Der Wert muss kleiner oder gleich 0.3 sein.
```

## daemon

A service running as system daemon will be checked. It has to be running and if specified listening on some ip/port combinations. A warning will be given if it was just started to inform you about this.

!!! abstract "Data"

    ```yaml
    data:
    #   server: ...                     # to work on remote server (see above)
        service: mongodb
        warn:
            age: 5 min                  # number of seconds the service has to be running
        error:
            listen: true                # listen on at least one ip/port
    #       listen:                     # specific definition with type/ip/port
    #         - type: TCP
    #           ip: '*'
    #           port: 27017
    fix:
        users: 5                        # number of last user entries to show in analysis
    # autofix: true                     # enable repair mode
    ```

The `listen` parameter can be true or has a list of specific combinations while only some of the parts (type/ip/port) has to be specified.

!!! info

    To get the listening port and ip `sudo lsof` is used. This may cause to ask for a password depending on your server's configuration. If this is not possible don't use the `error.listen` check.

The result values will be:

-   `title` - title of service
-   `script` - start script path
-   `active` - current state of daemon (true/false)
-   `status` - named state (more detailed)
-   `date` - concrete time at which this state was reached
-   `age` - time since last state change in seconds
-   `pid` - main pid of process which started (may no longer be active)

**Fixes**

In case of error or warning the following analyses will be done:

1. Information on how to restart the service.
2. Display the service script if possible to check if something is wrong.

**Repair**

If possible (using sudo) the service will be restarted.

**Result Exception**

```js
✖   ERROR MongoDB Daemon (local.daemon)
    Error at active (preset:result): The value has to be 'true'.
    The mongo database should be running and listening on port.
    REQUEST  service mongodb status
    RESPONSE code: 3
    RETURN   * mongodb.service - An object/document-oriented database
                Loaded: loaded (/lib/systemd/system/mongodb.service; enabled; vendor preset: enabled)
                Active: inactive (dead) since Thu 2020-09-17 19:37:56 CEST; 10min ago
                  Docs: man:mongod(1)
               Process: 1064 ExecStart=/usr/bin/mongod --unixSocketPrefix=${SOCKETPATH} --config ${CONF} $DAEMON_OPTS (code=exited, status=0/SUCCESS)
              Main PID: 1064 (code=exited, status=0/SUCCESS)

             Sep 17 18:39:19 alex-notebook systemd[1]: Started An object/document-oriented database.
             Sep 17 19:37:56 alex-notebook systemd[1]: Stopping An object/document-oriented database...
             Sep 17 19:37:56 alex-notebook systemd[1]: Stopped An object/document-oriented database.
    VALIDATE Check retrieved data elements
                 A text string. The value should be:
                 -   match regular expression /Active:/
    RESULT   {
               title: 'An object/document-oriented database',
               script: '/lib/systemd/system/mongodb.service',
               active: false,
               status: 'dead',
               date: 1600364276000,
               age: 600,
               pid: '1064'
             }
    VALIDATE Check values for errors
                 A map like data object. The items have the following format:
                 -   active: The service has to be active.
                     A boolean value. It is true for [ true ] and false for [ false ]. The value has to be 'true'.
                 -   status: The status is a sub element of the active flag.
                     A text string. The value should be:
                     -   equal 'running'
                 All defined keys are mandatory.
    VALIDATE Check values for warnings
                 A map like data object. The items have the following format:
                 -   age: The service should be running over 5 min.
                     A numeric value. If no unit is given it is assumed as s. The value will be converted to s. The value has to be greater or equal 300.
    Analyze the last active users (manual)
    The last active users are:
        alex     tty1         :0               Thu Sep 17 18:41:01 2020   still logged in
        reboot   system boot  4.15.0-117-gener Thu Sep 17 18:39:12 2020   still running
        alex     tty1         :0               Thu Sep 17 07:05:43 2020 - down                      (08:29)
        reboot   system boot  4.15.0-117-gener Thu Sep 17 07:05:29 2020 - Thu Sep 17 15:34:52 2020  (08:29)
        alex     tty1         :0               Wed Sep 16 17:12:33 2020 - down                      (05:56)
    Fix daemon problems (auto)
    To fix problems on not correctly running 'An object/document-oriented database' daemon restart it.
    To manually restart a daemon you should generally:
    1.  stop daemon if running: `systemctl stop mongodb`
    2.  stop main process (if it is further running): `sudo kill -9 1064`
    3.  start daemon if running: `systemctl start mongodb`
    4.  check service again: `systemctl status mongodb` or this test
    Start/restart daemon => OK
    FIX      Start daemon mongodb because not running
    PROCESS  'sudo systemctl start mongodb'
    EXITCODE 0
```

## disk

One or all disk devices on the host will be checked. It will report if the available space is too less.
Because a disk will get unusable if no space is left or if no more INodes can be added both will be checked.

!!! abstract "Data"

    ```yaml
    data:
    #   server: ...                     # to work on remote server (see above)
        mount: /                        # root will also be the default if nothing given
        warn:
            percent: 20%                # alert if less than defined disk space is free
            ipercent: 20%               # alert if less than defined number of inodes are free
    #       free: 100 GB                # alert if less than defined disk space is free
    #       ifree: 1000                 # alert if less than defined number of inodes are free
        error:
            percent: 10%                # alert if less than defined disk space is free
            ipercent: 10%               # alert if less than defined number of inodes are free
    #       free: 100 GB                # alert if less than defined disk space is free
    #       ifree: 1000                 # alert if less than defined number of inodes are free
    fix:
        bigNum: 5                       # number of big file/dir entries
        compress:                       # directories with number of days to compress files
            '/var/log': 7               # (0 = yesterday)
        delete:                         # directories with number of days to delete files
            '/tmp': 7                   # (0 = yesterday)
            '/var/log': 30
            '/home/*/.local/share/Trash': 30
            '/home/*/Downloads': 90
    # autofix: true                     # enable repair mode
    ```

The result will be:

-   `device` - path of device
-   `mount` - mount path
-   `total` - total size of device in KiB
-   `free` - free space in KiB
-   `percent` - percentage of free space (0..1)
-   `itotal` - total number of inodes available
-   `ifree` - free inodes on device
-   `ipercent` - percentage of free inodes

**Fixes**

In case of error or warning the following analyses will be done:

1. Search the biggest folders/files within the share.
2. Search for deletable files older x days.
3. search for compressible days older y days.

In which folders to search may be specified if you would not like the default (above). As this is interpreted on the shell you also may use wildcards like `*` or `?`.
If something to delete or compress is found it may also be cleaned up automatically.

**Repair**

This will run the same search for deletable or compressible files as the analysis. But instead of collecting information it will delete/compress them.

**Result**

??? example "Console output"

    ```js
    ⚡  WARN Disks (local.disk)
        Error at 0.percent (preset:result): The number has to be greater than 0.9 but 0.6079422472981344 was given.
        Check space on all disks.
        REQUEST  LANG=C df -x tmpfs -x devtmpfs -x squashfs --output=source,itotal,iavail,size,avail,target -k | sed 's/Mounted on/Mounted/;s/ \+/,/g'
        RETURN   [
                {
                    Filesystem: '/dev/nvme0n1p2',
                    Inodes: 30187520,
                    IFree: 27793817,
                    '1K-blocks': 475219048,
                    Avail: 288905736,
                    Mounted: '/'
                },
                {
                    Filesystem: '/dev/nvme0n1p1',
                    Inodes: 0,
                    IFree: 0,
                    '1K-blocks': 523248,
                    Avail: 517084,
                    Mounted: '/boot/efi'
                }
                ]
        VALIDATE Check retrieved data elements
                    A map like data object. The items have the following format:
                    -   Mounted: The device has to be mounted at /.
                        A text string. The value should be:
                        -   equal '/'
                    The items have the following format:
                    -   *: Current usage values.
                        A map like data object. The items have the following format:
                        -   Filesystem: StringSchema
                            A text string.
                        -   Inodes: NumberSchema
                            A numeric value.
                        -   IFree: NumberSchema
                            A numeric value.
                        -   1K-blocks: NumberSchema
                            A numeric value.
                        -   Avail: NumberSchema
                            A numeric value.
                        -   Mounted: StringSchema
                            A text string.
                        All defined keys are mandatory.
        RESULT   {
                device: '/dev/nvme0n1p2',
                itotal: 30187520,
                ifree: 27793817,
                ipercent: 0.9207055432178596,
                total: 475219048,
                free: 288905736,
                percent: 0.6079422472981344,
                mount: '/'
                }
        VALIDATE Check values for errors
                    A map like data object. The items have the following format:
                    -   percent: The disk has to have more than 10% of free disk space.
                        A numeric value. The value has to be greater than 0.1.
                    -   ipercent: The disk has to have more than 10% of free inode entries.
                        A numeric value. The value has to be greater than 0.1.
        VALIDATE Check values for warnings
                    A map like data object. The items have the following format:
                    -   percent: The disk has to have more than 90% of free disk space.
                        A numeric value. The value has to be greater than 0.9.
                    -   ipercent: The disk has to have more than 20% of free inode entries.
                        A numeric value. The value has to be greater than 0.2.
        Fix full disk (manual)
        The following analyses may not be complete because this user has no sudo rights for the 'find', 'du' command to find all files! Better use a user with sudo rights.
        The free space on /dev/nvme0n1p2 which is mounted under / is only 60.79%.
        The biggest files and directories on / are:
            15G     /home/alex/.local/share/Trash/files
            15G     /home/alex/Downloads
            6,9G    /home/alex/Videos/test
            6,1G    /home/alex/.thunderbird/so8mthku.default/ImapMail/mail.divibib-2.com/INBOX.sbd
            5,9G    /home/alex/.local/share/torbrowser/tbb/x86_64/tor-browser_de/Browser/Downloads
        In the directory /var/log there are 156 files older than 30 days (last modification). They reach from 04/05/2018 to 08/16/2020 and have a total size of 3.66 GB. As there are some old files found, they may
        be deleted automatically.
        In the directory /var/log there are 11 uncompressed files older than 7 days (last modification). They reach from 08/20/2020 to 09/07/2020 and have a total size of 335.9 MB. As there are some uncompressed
        files found, they may be packed automatically.
        ✔   Old files will be deleted or compressed.
        PROCESS  "df -P /tmp  | tail -1 | awk '{ print $NF}'"
        PROCESS  "df -P /var/log  | tail -1 | awk '{ print $NF}'"
        PROCESS  "df -P /home/*/.local/share/Trash  | tail -1 | awk '{ print $NF}'"
        STDOUT   /
        EXITCODE 0
        STDOUT   /
        EXITCODE 0
        STDOUT   /
        EXITCODE 0
        FIX      Delete old files in /tmp
        PROCESS  'sudo -n /usr/bin/find /tmp -type f -mtime +7 -delete'
        FIX      Delete old files in /var/log
        PROCESS  'sudo -n /usr/bin/find /var/log -type f -mtime +30 -delete'
        FIX      Delete old files in /home/*/.local/share/Trash
        PROCESS  'sudo -n /usr/bin/find /home/*/.local/share/Trash -type f -mtime +30 -delete'
        EXITCODE 0
        EXITCODE 0
        EXITCODE 0
        PROCESS  "df -P /var/log  | tail -1 | awk '{ print $NF}'"
        STDOUT   /
        EXITCODE 0
        FIX      Compress old files in /var/log
        PROCESS  "sudo -n /usr/bin/find /var/log -type f -mtime +7 ! -mtime +30 ! \\( -name '*.gz' -o -name '*.tgz' -o -name '*.zip' -o -name '*.bz2' -o -name '*.iso' -o -name '*.7z' \\) -exec sudo -n /bin/gzip {}
                \\;"
        EXITCODE 0
    ```

## file

A specified file path will be analyzed with all it's metadata.

!!! abstract "Data"

    ```yaml
    data:
    #   server: ...                     # to work on remote server (see above)
        path: /var/log/myapp            # which path to analyze
        #lines: 100                     # read the first number of lines (0 -> nothing to read)
        # warn:
        #    minSize: 100 iB
        #    maxSize:
        #    minFree: 1 GiB
        #    type: regular file
        #    minAccess: 2018-10-05 00:00:00
        #    maxAccess:
        #    minChange:
        #    maxChange:
        #    allow: /Status: OK/
        #    disallow: 
        # error:
        #    minSize: 100 iB
        #    maxSize:
        #    minFree: 100 MiB
        #    type: regular file
        #    minAccess: 2018-10-05 00:00:00
        #    maxAccess:
        #    minChange:
        #    maxChange:
        #    allow: /Status: OK/
        #    disallow: 
    ```

Result values:

- `mount` - mount point of used device
- `size` - size in bytes
- `free` - free space on device in bytes
- `mode` - raw mode in octal notation like '100644'
- `read` - array with user, group and other rights
- `write` - array with user, group and other rights
- `execute` - array with user, group and other rights
- `sticky` - sticky bit is set
- `set_gid` - set group id flag
- `set_uid` - set user id flag
- `type` - filetype: 'socket', 'symbolic link', 'regular file', 'block device', 'directory', 'character device', 'FIFO'
- `uid` - user id of owner
- `owner` - owner name
- `gid` - group id
- `group` - group name
- `access` - last access time
- `change` - last change time
- `metadata` - last metadata change time
- `create` - creation time (if possible)
- `mimetype` - mime-type
- `charset` - character set
- `content` - file content

**Fixes**

No further analyzation or fixing is possible here.

**Result**

??? example "Console output"

    ```ts
    ✔   OK File (linux.file)
        Check file meta.
        Dieser Test prüft die aktuelle Systemlast auf localhost.
        PROCESS  du -bsD /etc/fstab | cut -f 1 && LANG=C stat -c '%m %f %u %U %g %G %X %Y %Z %W' /etc/fstab && df --output=avail /etc/fstab | tail -n 1 && file -biz /etc/fstab | sed 's/;
                charset=/ /'
        STDOUT   916
                / 81a4 0 root 0 root 1617349886 1615555577 1615555577 1615455943
                164982336
                text/plain us-ascii
        EXITCODE 0
        PROCESS  head -n 5 /etc/fstab
        STDOUT   # /etc/fstab: static file system information.
                #
                # Use 'blkid' to print the universally unique identifier for a device; this may
                # be used with UUID= as a more robust way to name devices that works even if
                # disks are added and removed. See fstab(5).
        EXITCODE 0
        RESULT   {
                mount: '/',
                size: 916,
                free: 168941912064,
                mode: '100644',
                read: [ 'user', 'group', 'other' ],
                write: [ 'user', false, false ],
                execute: [ false, false, false ],
                sticky: false,
                set_gid: false,
                set_uid: false,
                type: 'regular file',
                uid: 0,
                owner: 'root',
                gid: 0,
                group: 'root',
                access: 2021-04-02T07:51:26.000Z,
                change: 2021-03-12T13:26:17.000Z,
                metadata: 2021-03-12T13:26:17.000Z,
                create: 2021-03-11T09:45:43.000Z,
                mimetype: 'text/plain',
                charset: 'us-ascii',
                content: '# /etc/fstab: static file system information.\n' +
                    '#\n' +
                    "# Use 'blkid' to print the universally unique identifier for a device; this may\n" +
                    '# be used with UUID= as a more robust way to name devices that works even if\n' +
                    '# disks are added and removed. See fstab(5).'
                }
    ```

{!docs/assets/abbreviations.txt!}
