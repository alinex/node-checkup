title: System

# System Self Analysis

This tests will check the current running system itself. It may be the checkup system alone if called by CLI or the NodeJS Server (maybe [Alinex Server](https://alinex.gitlab.io/node-server)) which currently is running the checkup.

## latency

Event loop latency is an essential, but often overlooked performance metric for Node.js applications.

The event loop cannot process multiple tasks simultaneously. Instead tasks are placed in a queue and processed sequentially. This means one task can be held up because the one before it is taking a long time to complete. A bottleneck can be identified by observing an abnormally large amount of lag between processing asynchronous tasks. Because all task execution originates from the event loop it is the best location for initial bottleneck measurements.

!!! abstract "Data"

    ```yaml
    data:
        warn:
            time: 10 ms                 # maximum time event waiting to be executed
        error:
            time: 1 s                   # maximum time event waiting to be executed
    ```

The only result is:

-   `time` - time an event takes to get through the event loop in milliseconds

**Fixes**

No further analyzation or fixing is possible here.

**Result**

??? example "Console output"

    ```ts
    ✔   OK Latency (system.latency)
        Check the JavaScript event loop lag.
        RESULT   { time: 0.256 }
        VALIDATE Prüfung der Werte auf Fehler
                    Ein Datenobjekt mit Wertepaaren. Die Werte haben das folgende Format:
                    -   time: Die Verzögerung in der JavaScript Event Schleife darf nicht über 1 s sein.
                        Ein nummerischer Wert. Der Wert muss kleiner oder gleich 1000 sein.
        VALIDATE Prüfung der Werte auf Warnungen
                    Ein Datenobjekt mit Wertepaaren. Die Werte haben das folgende Format:
                    -   time: Die Verzögerung in der JavaScript Event Schleife darf nicht über 10 ms sein.
                        Ein nummerischer Wert. Der Wert muss kleiner oder gleich 10 sein.
    ```

## process

Analyze the JavaScript process in which the checkup is running. The analyzation will be done two times in a range, to calculate the average difference per second.

This will show how much resources are needed by this process. The maximum allowed limit can be specified:

!!! abstract "Data"

    ```yaml
    data:
        warn:
            cpu: 50%
            rss: 512 MiB
        error:
            cpu: 80%
            rss: 1 GiB
    ```

The result will contain:

-   `cpuPercent` - percentage of cpu usage (100% means all cpu cores full time)
-   `maxRSS` - size of RAM usage in Bytes
-   `userCPUTime` - user execution time in ms
-   `systemCPUTime` - system execution time in ms
-   `fsRead` - number of file read operations
-   `fsWrite` - number of file write operations

**Fixes**

No further analyzation or fixing is possible here.

**Result**

??? example "Console output"

    ```ts
    ✔   OK Process (system.process)
        Check the JavaScript Process.
        REQUEST  process.resourceUsage()
        RETURN   {
                userCPUTime: 6711693,
                systemCPUTime: 156272,
                maxRSS: 205548,
                sharedMemorySize: 0,
                unsharedDataSize: 0,
                unsharedStackSize: 0,
                minorPageFault: 48251,
                majorPageFault: 0,
                swappedOut: 0,
                fsRead: 0,
                fsWrite: 0,
                ipcSent: 0,
                ipcReceived: 0,
                signalsCount: 0,
                voluntaryContextSwitches: 2466,
                involuntaryContextSwitches: 2687
                }
        REQUEST  process.resourceUsage()
        RETURN   {
                userCPUTime: 6716505,
                systemCPUTime: 156384,
                maxRSS: 205548,
                sharedMemorySize: 0,
                unsharedDataSize: 0,
                unsharedStackSize: 0,
                minorPageFault: 48260,
                majorPageFault: 0,
                swappedOut: 0,
                fsRead: 0,
                fsWrite: 0,
                ipcSent: 0,
                ipcReceived: 0,
                signalsCount: 0,
                voluntaryContextSwitches: 2469,
                involuntaryContextSwitches: 2687
                }
        RESULT   {
                userCPUTime: 481.2,
                systemCPUTime: 11.2,
                cpuPercent: 0.1231,
                maxRSS: 210481152,
                fsRead: 0,
                fsWrite: 0
                }
        VALIDATE Prüfung der Werte auf Fehler
                    Ein Datenobjekt mit Wertepaaren. Die Werte haben das folgende Format:
                    -   cpu: Die CPU Nutzung muss unter 80% sein.
                        Ein nummerischer Wert. Der Wert muss kleiner oder gleich 0.8 sein.
                    -   rss: Der Speicherverbrauch darf maximal 1 GiB betragen.
                        Ein nummerischer Wert. Der Wert muss kleiner oder gleich 1073741824 sein.
        VALIDATE Prüfung der Werte auf Warnungen
                    Ein Datenobjekt mit Wertepaaren. Die Werte haben das folgende Format:
                    -   cpu: Die CPU Nutzung muss unter 50% sein.
                        Ein nummerischer Wert. Der Wert muss kleiner oder gleich 0.5 sein.
                    -   rss: Der Speicherverbrauch darf maximal 512 MiB betragen.
                        Ein nummerischer Wert. Der Wert muss kleiner oder gleich 536870912 sein.
    ```

{!docs/assets/abbreviations.txt!}
