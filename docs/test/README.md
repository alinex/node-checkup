title: Overview

# Possible Tests

The following test areas are supported:

| Type     | Test                                              | Repair | Description                                         |
| -------- | ------------------------------------------------- | :----: | --------------------------------------------------- |
| Linux    | [Uptime](linux.md#uptime)                         |   -    | info about system uptime and restart cause          |
|          | [Load](linux.md#load)                             |   -    | CPU load average analyzation                        |
|          | [Memory](linux.md#memory)                         |   -    | memory analyzation                                  |
|          | [OS](linux.md#os)                                 |   X    | OS version info, updates and needed restart         |
|          | [Process](linux.md#process)                       |   -    | Process statistics and analyzation                  |
|          | [Daemon](linux.md#daemon)                         |   X    | daemon status with restart                          |
|          | [Disk](linux.md#disk)                             |   X    | disk usage with cleanup files                       |
|          | [File](linux.md#file)                             |   -    | file information                                    |
| Network  | [Ping](net.md#ping)                               |   -    | ping to check network                               |
|          | [TCP](net.md#tcp)                                 |   -    | TCP connection test                                 |
|          | [UDP](net.md#udp)                                 |   -    | UDP connection test                                 |
|          | [DNS](net.md#dns)                                 |   -    | nameserver query with checking entries              |
|          | [SSH](net.md#ssh)                                 |   -    | connection test with authentication                 |
|          | [TLS](net.md#tls)                                 |   -    | deep analysis of SSH/TLS quality with vulnerability |
| Log      | [Event](log.md#event)                             |   -    | Logfile analyzation                                 |
| Database | SQL [Record](database.md#sqlrecord)               |   -    | SQL content check                                   |
|          | Postgres [Cluster](database.md#postgrescluster)   |   -    | PostgreSQL cluster analyzation                      |
|          | Postgres [Database](database.md#postgresdatabase) |   -    | PostgreSQL database analyzation                     |
|          | Postgres [Schema](database.md#postgresschema)     |   -    | PostgreSQL database schema analyzation              |
|          | Postgres [Table](database.md#postgrestable)       |   -    | PostgreSQL database table analyzation               |
|          | Postgres [Tablespace](database.md#postgresspace)  |   -    | PostgreSQL tablespace analyzation                   |
|          | Postgres [User](database.md#postgresuser)         |   -    | PostgreSQL user analyzation                         |
|          | [MongoDB](database.md#mongodb)                    |   -    | MongoDB health parameter analyzation                |
| Web      | [HTTP](web.md#http)                               |   -    | calling web URL                                     |
|          | [TLS](web.md#tls)                                 |   -    | simple SSH/TLS analysis                             |
|          | [Apache](web.md#apache)                           |   -    | Apache status analysis                              |
| System   | [Latency](system.md#latency)                      |   -    | current latency                                     |
|          | [Process](system.md#process)                      |   -    | process runing checkup system                       |

Each test can have fixes which may run automatically (if enabled in case configuration and given as option for the current suite).

!!! info

    Each test and fix may have some specification which can be set. If nothing is given for the `data.warn` or `data.error` object a default specification is used. In this documentation the default is always given as example, other settings are marked with comments.

{!docs/assets/abbreviations.txt!}
